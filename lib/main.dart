import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/design/ResponsiveInfo.dart';
import 'package:saveappforios/projectconstants/DataConstants.dart';
import 'package:saveappforios/views/Bankvoucher.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'database/DBTables.dart';
import 'database/DatabaseHelper.dart';
import 'home.dart';
import 'login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MySaving',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,

      ),
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    startTime();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(


      body: Container(

          height: double.infinity,
          width: double.infinity,
          // child: FittedBox(child: Image.asset('images/splashbg.png'),
          //     fit: BoxFit.cover),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/splashbg.png"),
              fit: BoxFit.cover,
            ),
          ),

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[

              Padding(padding: const EdgeInsets.only(top: 0.0),
                child: Align(alignment: Alignment.center,
                    child: Image.asset('images/logo.png', fit: BoxFit.contain,
                      width: ResponsiveInfo.isSmallMobile(context)
                          ? ResponsiveInfo.isMobile(context) ? 150 : 180
                          : 250,)),),
              // Align(   child: FittedBox(child: Image.asset('images/splashbg.png'),
              //     fit: BoxFit.cover),),


              Align(alignment: Alignment(0.0, 0.25),
                child: Text("Save", style: TextStyle(color: Colors.white,
                    fontSize: ResponsiveInfo.isSmallMobile(context)
                        ? ResponsiveInfo.isMobile(context) ? 30.0 : 40
                        : 50,
                    fontWeight: FontWeight.w900),),),

              Align(alignment: Alignment(0.0, 0.35),
                child: Text("My Personal App", style: TextStyle(
                    color: Colors.white,
                    fontSize: ResponsiveInfo.isSmallMobile(context)
                        ? ResponsiveInfo.isMobile(context) ? 14.0 : 16
                        : 19,
                    fontWeight: FontWeight.w100),),),


            ],
          )


      ),


      //Navigator.of(context).pushReplacementNamed("/home");

    );
  }
  showPinDialog()
  {

    showDialog(
        context: context,
        builder: (_) {
          return MyDialog();
        }).then((value) => {




      value['data'],

      checkPin(value['data'])







    });

  }

  checkPin(String pin) async
  {
    DatabaseHelper dbhelper=new DatabaseHelper();
    String data ="";



    List<Map<String,dynamic>>allaccountsettings=await dbhelper.queryAllRows(DatabaseTables.TABLE_APP_PIN);


    for (Map ab in allaccountsettings) {
      print(ab);

      int id = ab["keyid"];
      data = ab["data"];

    }

    if(data.compareTo(pin)==0)
    {

      final datacount = await SharedPreferences.getInstance();

      String? token=datacount.getString(DataConstants.userkey);

      if (token != null) {
        if (token.isNotEmpty) {
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => MyDashboardPage(title: "dashboard",)
          )
          );
        }
        else {
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => Loginpage(title: "login",)
          )
          );
        }
      }
      else {
        Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => Loginpage(title: "dashboard",)
        )
        );
      }
    }
    else{


    }




  }

  startTime() async
  {
    await Future.delayed(Duration(seconds: 5));

    showUserDialog();


  }


  showUserDialog() async{

    final datacount = await SharedPreferences.getInstance();

    bool? _switchValue = datacount.getBool(
        DataConstants.userDataPermissionkey);

    if(_switchValue!=null)
      {

        if (_switchValue) {

          route();

        }
        else{

          Dialog errorDialog = Dialog(
            // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
            child: Container(
              height: double.infinity,
              width: double.infinity,

              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Expanded(child: SingleChildScrollView(

                    child: Stack(

                      children: [
                        Padding(padding: EdgeInsets.all(5),

                          child:Text("\n\nUsage of User Information\n\n"

                              "User Information such as email,phonenumber,emailid are used by Century Gate Software Solutions Private Limited and its partners and service providers towards various business purposes which, amongst others, includes: "
                              "\n\n1. to facilitate the User’s requests and the Services;"
                              "\n2. to undertake research and analytics for offering or improving Services, Century Gate Software Solutions Private Limited Products, their security and service quality;"
                              "\n3. to share with a User, updates on changes to Century Gate Software Solutions Private Limited services and their terms and conditions and provide details of similar products and services;"
                              "\n4. to aid strategic development, data collection and business analytics for Century Gate Software Solutions Private Limited or its partners;"
                              "\n5. to take up or investigate any complaints/claims/disputes;  "
                              "\n6. to respond to your queries or feedback submitted by the User; "
                              "\n7. for conducting audits and for record keeping purposes;"
                              "\n8. for complying with Applicable Laws including the directions of any Governmental Authorities;"
                              "\n9. for selective offers and promotions; and "
                              "\n10. for providing a personalized experience and to enhance a User’s experience in relation to the application or the website. "
                              "\n\nStorage of User Information\n\n"
                              "User Information in the possession and control of Century Gate Software Solutions Private Limited is stored in accordance with Applicable Laws. Storage and transfer of User Information is carried out in compliance with all Applicable Laws, including without limitation, the Information Technology Act, 2000 and the Information Technology (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011. By using any of the Century Gate Software Solutions Private Limited Products or Services, the User agrees and consents to the collection, storage and use of the User Information collected in the manner described in and in accordance with this Privacy Policy. "
                              "\n\nDissemination of User Information\n\n"
                              "The User acknowledges the User Information and other data may be exported outside of India, processed and stored in a foreign country or countries where it may be accessed by other entities. User Information may also be transferred in accordance with Applicable Laws to Governmental Authorities, to affiliates of Century Gate Software Solutions Private Limited and any potential buyers of the shares or assets of Century Gate Software Solutions Private Limited. Century Gate Software Solutions Private Limited may also distribute User Information (not being in the nature of sensitive personal information) with other third parties, analytics providers and other third parties for provision of services to Century Gate Software Solutions Private Limited and/or the Users by such third parties.",style: TextStyle(
                              fontSize: 15

                          ),) ,
                        )






                      ],

                    ),






                  ),flex: 6,),

                  Expanded(child: Stack(

                    children: [

                      Align(

                          alignment: FractionalOffset.center,

                          child:  Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,

                            children: [

                              Expanded(child: TextButton(onPressed: () {


                                Navigator.of(context).pop();
                              },
                                  child: Text('Deny', style: TextStyle(color: Colors.black, fontSize: 18.0),)),flex: 1,),

                              Expanded(child: TextButton(onPressed: () {
                                datacount.setBool(
                                    DataConstants.userDataPermissionkey,true);

                                Navigator.of(context).pop();
                                route();
                              },
                                  child: Text('Allow', style: TextStyle(color: Colors.blue, fontSize: 18.0),)),flex: 1,)



                            ],

                          )






                      )

                    ],



                  ),flex: 1,)






                ],
              ),
            ),
          );
          showDialog(context: context, builder: (BuildContext context) => errorDialog);

        }


      }
    else{

      Dialog errorDialog = Dialog(
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
        child: Container(
          height: double.infinity,
          width: double.infinity,

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Expanded(child: SingleChildScrollView(

                child: Stack(

                  children: [
                    Padding(padding: EdgeInsets.all(5),

                    child:Text("\n\nUsage of User Information\n\n"

                      "User Information such as email,phonenumber,emailid are used by Century Gate Software Solutions Private Limited and its partners and service providers towards various business purposes which, amongst others, includes: "
                        "\n\n1. to facilitate the User’s requests and the Services;"
                        "\n2. to undertake research and analytics for offering or improving Services, Century Gate Software Solutions Private Limited Products, their security and service quality;"
                        "\n3. to share with a User, updates on changes to Century Gate Software Solutions Private Limited services and their terms and conditions and provide details of similar products and services;"
                        "\n4. to aid strategic development, data collection and business analytics for Century Gate Software Solutions Private Limited or its partners;"
                        "\n5. to take up or investigate any complaints/claims/disputes;  "
                        "\n6. to respond to your queries or feedback submitted by the User; "
                        "\n7. for conducting audits and for record keeping purposes;"
                        "\n8. for complying with Applicable Laws including the directions of any Governmental Authorities;"
                        "\n9. for selective offers and promotions; and "
                        "\n10. for providing a personalized experience and to enhance a User’s experience in relation to the application or the website. "
                        "\n\nStorage of User Information\n\n"
                        "User Information in the possession and control of Century Gate Software Solutions Private Limited is stored in accordance with Applicable Laws. Storage and transfer of User Information is carried out in compliance with all Applicable Laws, including without limitation, the Information Technology Act, 2000 and the Information Technology (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011. By using any of the Century Gate Software Solutions Private Limited Products or Services, the User agrees and consents to the collection, storage and use of the User Information collected in the manner described in and in accordance with this Privacy Policy. "
                        "\n\nDissemination of User Information\n\n"
                        "The User acknowledges the User Information and other data may be exported outside of India, processed and stored in a foreign country or countries where it may be accessed by other entities. User Information may also be transferred in accordance with Applicable Laws to Governmental Authorities, to affiliates of Century Gate Software Solutions Private Limited and any potential buyers of the shares or assets of Century Gate Software Solutions Private Limited. Century Gate Software Solutions Private Limited may also distribute User Information (not being in the nature of sensitive personal information) with other third parties, analytics providers and other third parties for provision of services to Century Gate Software Solutions Private Limited and/or the Users by such third parties.",style: TextStyle(
                        fontSize: 15

                    ),) ,
                    )






                  ],

                ),






              ),flex: 6,),

              Expanded(child: Stack(

                children: [

                  Align(

                    alignment: FractionalOffset.center,

                    child:  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,

                      children: [

                        Expanded(child: TextButton(onPressed: () {


                          Navigator.of(context).pop();
                        },
                            child: Text('Deny', style: TextStyle(color: Colors.black, fontSize: 18.0),)),flex: 1,),

                        Expanded(child: TextButton(onPressed: () {
                          datacount.setBool(
                              DataConstants.userDataPermissionkey,true);

                          Navigator.of(context).pop();
                          route();
                        },
                            child: Text('Allow', style: TextStyle(color: Colors.blue, fontSize: 18.0),)),flex: 1,)



                      ],

                    )






                  )

                ],



              ),flex: 1,)






            ],
          ),
        ),
      );
      showDialog(context: context, builder: (BuildContext context) => errorDialog);

    }



  }




  route() async {
    final datacount = await SharedPreferences.getInstance();

    String? token = datacount.getString(DataConstants.userkey);

    bool? _switchValue = datacount.getBool(
        DataConstants.isappPin);


    if (_switchValue != null) {
      if (!_switchValue) {
        if (token != null) {
          if (token.isNotEmpty) {
            Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => MyDashboardPage(title: "dashboard",)
            )
            );
          }
          else {
            Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => Loginpage(title: "login",)
            )
            );
          }
        }
        else {
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => Loginpage(title: "dashboard",)
          )
          );
        }
      }
      else {
        showPinDialog();
      }
    }
    else {
      if (token != null) {
        if (token.isNotEmpty) {
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => MyDashboardPage(title: "dashboard",)
          )
          );
        }
        else {
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => Loginpage(title: "login",)
          )
          );
        }
      }
      else {
        Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => Loginpage(title: "dashboard",)
        )
        );
      }
    }
  }

}

  class MyDialog extends StatefulWidget {



  @override
  _MyDialogState createState() => new _MyDialogState();
  }

  class _MyDialogState extends State<MyDialog> {
  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  TextEditingController namecontroller=new TextEditingController();

  List<int> arr=[1,2,3,4,5,6,7,8,9,-1,0,-2];

  @override
  void initState() {
  // TODO: implement initState

  var now = DateTime.now();

  // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

  month=now.month.toString();
  year=now.year.toString();



  setState(() {
  int monthnumber=int.parse(month);
  currentmonthindex=monthnumber-1;

  currentmonth=months[monthnumber-1];
  currentyear=year;

  currentyearnumber=int.parse(currentyear);
  });
  super.initState();
  }


  @override
  Widget build(BuildContext context) {

  double width = MediaQuery.of(context).size.width;

  double height = MediaQuery.of(context).size.height;
  return AlertDialog(
  title:  Text(
  "App lock"),
  content: Container(
  width: width,
  height: height,
  child: Column(

  children: [


  Padding(
  padding: const EdgeInsets.only(left:5.0,right: 5.0,top:5,bottom: 0),
  // padding: EdgeInsets.all(15),
  child: new Theme(data: new ThemeData(
  hintColor: Colors.black45,

  ), child: TextField(

  controller: namecontroller,
  keyboardType: TextInputType.none,
  maxLength: 6,
  obscureText: true,
  maxLines: 1,



  decoration: InputDecoration(
  focusedBorder: OutlineInputBorder(
  borderSide: BorderSide(color: Colors.black, width: 0.5),
  ),
  enabledBorder: OutlineInputBorder(
  borderSide: BorderSide(color: Colors.black, width: 0.5),
  ),
  hintText: 'Enter 6 digit PIN',




  ),




  )),
  ),

  Padding(padding: EdgeInsets.all(5),
  child: GridView.builder(
  physics: BouncingScrollPhysics(),

  shrinkWrap: true,
  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
  crossAxisCount: 3,
  crossAxisSpacing: 0.0,
  mainAxisSpacing: 0.0,
  childAspectRatio:1

  ),
  itemCount: arr.length,
  itemBuilder: (context, index) {
  return(arr[index]==-2) ?
  Padding(padding: EdgeInsets.all(5),

  child: Container(

  decoration: BoxDecoration(
  border: Border.all(color: Colors.black45),borderRadius:    BorderRadius.all(
  Radius.circular(5.0) //                 <--- border radius here
  ),
  ),width:60,height:60,

  child:InkWell(
  child:new Icon(Icons.keyboard_backspace,size: 40,),

  onTap: (){

  String a=namecontroller.text.toString();

  String b=  a.substring(0,a.length-1);
  namecontroller.text=b;


  },



  )



  ),

  )


      : Padding(padding: EdgeInsets.all(5),child:Container(
  child:InkWell(
  child: Center(child:Text(arr[index].toString())),

  onTap: (){

  setState(() {
  if(namecontroller.text.length<6) {
  String a = namecontroller.text.toString();

  a = a + arr[index].toString();

  namecontroller.text = a;
  }
  });





  },
  ),

  decoration: BoxDecoration(
  border: Border.all(color: Colors.black45),borderRadius:    BorderRadius.all(
  Radius.circular(5.0) //                 <--- border radius here
  ),
  ),width:60,height:60,

  ));
  },
  ),),


  Padding(padding: EdgeInsets.all(10),

  child: TextButton(onPressed: () {


  if(namecontroller.text.toString().isNotEmpty)
  {



  Navigator.of(context).pop({'data':namecontroller.text.toString()});
  }



  }, child: Center(
  child:Text("Submit",style: TextStyle(fontSize: 14),) ,
  ) ,),

  )


  ],


  ),
  ),
  );
  }





  }


