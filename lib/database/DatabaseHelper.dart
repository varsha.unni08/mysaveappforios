import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

import 'DBTables.dart';

class DatabaseHelper {
  static final _databaseName = "SaveDatabase.db";
  static final _databaseVersion = 1;



  // only have a single app-wide reference to the database
  late Database _database;






  Future<Database> getdatabase() async {

    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_WALLET +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');
    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_VISITCARD +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_TASK +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_PASSWORD +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_LIABILITY +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');
    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_INSURANCE +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_DOCUMENT +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_BUDGET +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_ASSET +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');
    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_APP_PIN +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_ACCOUNTSETTINGS +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.INVESTMENT_table +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.DIARYSUBJECT_table +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.DIARY_table +
        ' (' +
        'keyid' +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        'data ' +
        ' TEXT NOT NULL' +
        ')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_ACCOUNTS +
        ' (' +
        DatabaseTables.ACCOUNTS_id +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        DatabaseTables.ACCOUNTS_VoucherType +
        ' INTEGER , ' +
        DatabaseTables.ACCOUNTS_entryid +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_date +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_setupid +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_amount +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_type +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_remarks +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_year +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_month +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_billId+
        ' TEXT , '+
        DatabaseTables.ACCOUNTS_billVoucherNumber+
        ' TEXT'+')');

    await db.execute('CREATE TABLE ' +
        DatabaseTables.TABLE_ACCOUNTS_RECEIPT +
        ' (' +
        DatabaseTables.ACCOUNTS_id +
        ' INTEGER PRIMARY KEY  AUTOINCREMENT , ' +
        DatabaseTables.ACCOUNTS_entryid +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_date +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_setupid +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_amount +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_type +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_remarks +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_year +
        ' TEXT , ' +
        DatabaseTables.ACCOUNTS_month +
        ' TEXT ' ')');


    String CREATE_TARGET = "CREATE TABLE " + DatabaseTables.TABLE_TARGETCATEGORY + "("
        + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT, "+ "iconimage"+" BLOB"
        + ")";

    db.execute(CREATE_TARGET);


    String CREATE_TABLE_MILESTONE = "CREATE TABLE " + DatabaseTables.TABLE_MILESTONE + "("
        + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
        + ")";

    db.execute(CREATE_TABLE_MILESTONE);


    String CREATE_TARGET_TABLE = "CREATE TABLE " + DatabaseTables.TABLE_TARGET + "("
        + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT ," + "reached" + " TEXT "+")";

    db.execute(CREATE_TARGET_TABLE);


    String CREATE_TABLE_ADDEDAMOUNT_MILESTONE = "CREATE TABLE " + DatabaseTables.TABLE_ADDEDAMOUNT_MILESTONE + "("
        + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

    db.execute(CREATE_TABLE_ADDEDAMOUNT_MILESTONE);


  }



  Future<int> insert(Map<String, dynamic> row,String tablename) async {
    Database db = await getdatabase();
    return await db.insert(tablename, row);
  }

  Future<int> deleteAll(String tablename) async {
    Database db = await getdatabase();
    return await db.delete(tablename);
  }


  Future<int> updateAccountData(Map<String, dynamic> row,String tablename,String id) async {
    Database db = await getdatabase();
    return await db.update(tablename, row,where: "${DatabaseTables.ACCOUNTS_id} = ?",whereArgs: [id]);
  }

  // All of the rows are returned as a list of maps, where each map is
  // a key-value list of columns.
  Future<List<Map<String, dynamic>>> queryAllRows(String tablename) async {
    Database db = await getdatabase();
    return await db.query(tablename);
  }

  Future<dynamic> getAccountDataByBillid(String id) async {
    Database db = await getdatabase();
    return await db.query(
        DatabaseTables.TABLE_ACCOUNTS,
        where: "${DatabaseTables.ACCOUNTS_billId} = ? ",
        whereArgs: [id],
        limit: 1
    );
  }



  Future<dynamic> getDataByid(String tablename,String id) async {
    Database db = await getdatabase();
    return await db.query(
        tablename,
        where: "keyid = ? ",
        whereArgs: [id],
        limit: 1
    );
  }

  Future<dynamic> getAccounDataByEntryid(String entryid) async {
    Database db = await getdatabase();
    return await db.query(
        DatabaseTables.TABLE_ACCOUNTS,
        where: "${DatabaseTables.ACCOUNTS_entryid} = ? ",
        whereArgs: [entryid]

    );
  }

  Future<dynamic> getAccounDataBySetupid(String setupid) async {
    Database db = await getdatabase();
    return await db.query(
        DatabaseTables.TABLE_ACCOUNTS,

        where: "ACCOUNTS_setupid = ? ",
        whereArgs: [setupid]

    );
  }

  Future<dynamic> deleteDataByid(String id,String table) async {
    Database db = await getdatabase();
    return await db.delete(table,where: "keyid=?",whereArgs: [id]);
  }

  Future<int> update(Map<String, dynamic> row,String tablename,String keyid) async {
    Database db = await getdatabase();


    return await db.update(tablename, row,where: "keyid=?",whereArgs: [keyid]);
  }

  Future<List<Map<String, dynamic>>> getAccountDataByMonthYear(String month,String year) async {
    Database db = await getdatabase();
    return await db.query(DatabaseTables.TABLE_ACCOUNTS,
    where: "${DatabaseTables.ACCOUNTS_month} = ? and ${DatabaseTables.ACCOUNTS_year} = ? ",
    whereArgs: [month,year]);
  }


  Future<List<Map<String, dynamic>>> getAccountDataByID(String id) async {
    Database db = await getdatabase();
    return await db.query(DatabaseTables.TABLE_ACCOUNTS,
        where: "${DatabaseTables.ACCOUNTS_id} = ?",
        whereArgs: [id],
    limit: 1);
  }



}
