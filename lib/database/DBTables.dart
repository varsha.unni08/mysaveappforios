class DatabaseTables {
  static String DIARYSUBJECT_table = "DIARYSUBJECT_table";
  static String DIARY_table = "DIARY_table";
  static String TABLE_BUDGET = "TABLE_BUDGET";
  static String INVESTMENT_table = "INVESTMENT_table";
  static String TABLE_TASK = "TABLE_TASK";
  static String TABLE_ACCOUNTSETTINGS = "TABLE_ACCOUNTSETTINGS";
  static String TABLE_INSURANCE = "TABLE_INSURANCE";
  static String TABLE_LIABILITY = "TABLE_LIABILITY";
  static String TABLE_ASSET = "TABLE_ASSET";
  static String TABLE_ACCOUNTS = "TABLE_ACCOUNTS";
  static String TABLE_ACCOUNTS_RECEIPT = "TABLE_ACCOUNTS_RECEIPT";
  static String TABLE_APP_PIN = "TABLE_APP_PIN";
  static String TABLE_WALLET = "TABLE_WALLET";
  static String TABLE_DOCUMENT = "TABLE_DOCUMENT";
  static String TABLE_PASSWORD = "TABLE_PASSWORD";
  static String TABLE_VISITCARD = "TABLE_VISITCARD";
  static String TABLE_TARGETCATEGORY="TABLE_TARGETCATEGORY";
  static String TABLE_MILESTONE="TABLE_MILESTONE";
  static String TABLE_TARGET="TABLE_TARGET";
  static String TABLE_ADDEDAMOUNT_MILESTONE="TABLE_ADDEDAMOUNT_MILESTONE";






  static String ACCOUNTS_id = "ACCOUNTS_id";
  static String ACCOUNTS_entryid = "ACCOUNTS_entryid";
  static String ACCOUNTS_date = "ACCOUNTS_date";
  static String ACCOUNTS_setupid = "ACCOUNTS_setupid";
  static String ACCOUNTS_amount = "ACCOUNTS_amount";
  static String ACCOUNTS_type = "ACCOUNTS_type";
  static String ACCOUNTS_remarks = "ACCOUNTS_remarks";
  static String ACCOUNTS_month = "ACCOUNTS_month";
  static String ACCOUNTS_year = "ACCOUNTS_year";
  static String ACCOUNTS_cashbanktype = "ACCOUNTS_cashbanktype";
  static String ACCOUNTS_VoucherType = "ACCOUNTS_VoucherType";
  static String ACCOUNTS_billId = "ACCOUNTS_billId";
  static String ACCOUNTS_billVoucherNumber = "ACCOUNTS_billVoucherNumber";




  static List<String>accountColumns=["ACCOUNTS_id","ACCOUNTS_entryid","ACCOUNTS_date","ACCOUNTS_setupid"
  ,"ACCOUNTS_amount","ACCOUNTS_type","ACCOUNTS_remarks","ACCOUNTS_month","ACCOUNTS_year","ACCOUNTS_cashbanktype",
  "ACCOUNTS_VoucherType","ACCOUNTS_billId","ACCOUNTS_billVoucherNumber"];



}
