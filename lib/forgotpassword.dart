

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io';

import 'connection/DataConnection.dart';
import 'home.dart';
import 'login.dart';
import 'package:http/http.dart' as http;

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'projectconstants/DataConstants.dart';

import 'dart:async';

void main() {
  // runApp(MyApp());


  runApp(MyApp());




}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'forgotpassword',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,

        ),
        debugShowCheckedModeBanner: false,
        home: ForgotpasswordPage(title: 'forgotpassword',mobile: '',)


    );



  }
}

class ForgotpasswordPage extends StatefulWidget {
  ForgotpasswordPage({Key? key, required this.title,required this.mobile}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  String mobile;

  @override
  _ForgotpasswordPageState createState() => _ForgotpasswordPageState(mobile);
}

class _ForgotpasswordPageState extends State<ForgotpasswordPage> {
  String mobilenumber ;


  _ForgotpasswordPageState(this.mobilenumber);

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      // _counter++;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    // sleep(const Duration(seconds:6));
    //startTime();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }



  route() {
    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => Loginpage(title: "dashboard",)
    )
    );
  }


  // startTime() async {
  //   var duration = new Duration(seconds: 6);
  //   return new Timer(duration, route);
  // }
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    TextEditingController passwordcontroller = new TextEditingController();
    TextEditingController confirmpasswordcontroller = new TextEditingController();
    return Scaffold(




      body: Container(

          height: double.infinity,
          width: double.infinity,
          // child: FittedBox(child: Image.asset('images/splashbg.png'),
          //     fit: BoxFit.cover),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/splashbg.png"),
              fit: BoxFit.cover,
            ),
          ),

          child:Stack(
            children: <Widget>[

              SingleChildScrollView(

                child: Column(


                 children: [

                   Padding(
                     padding: const EdgeInsets.only(left:15.0,right: 15.0,top:140,bottom: 0),
                     // padding: EdgeInsets.all(15),
                     child: new Theme(data: new ThemeData(
                       hintColor: Colors.white,

                     ), child: Text('Your mobile number is '+mobilenumber,style: TextStyle(color: Colors.white, fontSize: 15)))
                   ),


                   Padding(
                     padding: const EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 0),
                     // padding: EdgeInsets.all(15),
                     child: new Theme(data: new ThemeData(
                       hintColor: Colors.white,

                     ), child: TextField(
                       obscureText: true,
                       controller: passwordcontroller,

                       decoration: InputDecoration(
                         focusedBorder: OutlineInputBorder(
                           borderSide: BorderSide(color: Colors.white, width: 0.5),
                         ),
                         enabledBorder: OutlineInputBorder(
                           borderSide: BorderSide(color: Colors.white, width: 0.5),
                         ),
                         hintText: 'New password',

                       ),
                     )),
                   ),


                   Padding(
                     padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                     // padding: EdgeInsets.all(15),
                     child: new Theme(data: new ThemeData(
                       hintColor: Colors.white,

                     ), child: TextField(
                       obscureText: true,
                       controller: confirmpasswordcontroller,

                       decoration: InputDecoration(
                         focusedBorder: OutlineInputBorder(
                           borderSide: BorderSide(color: Colors.white, width: 0.5),
                         ),
                         enabledBorder: OutlineInputBorder(
                           borderSide: BorderSide(color: Colors.white, width: 0.5),
                         ),
                         hintText: 'Confirm password',

                       ),
                     )),
                   ),


                   Padding(
                     padding: const EdgeInsets.all(15),
                     child :   Container(
                       height: 50,
                       width: 150,
                       decoration: BoxDecoration(
                           color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                       child: TextButton(
                         onPressed: () {


                           if(passwordcontroller.text.toString().compareTo(confirmpasswordcontroller.text.toString())==0)
                             {


                               DataConnectionStatus.check().then((value)
                               {

                                 if (value != null && value) {
                                   // Internet Present Case

                                   updatePassword(passwordcontroller.text.toString(), mobilenumber);


                                 }
                                 else{

                                   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                     content: Text("Check your internet connection"),
                                   ));

                                 }
                               }
                               );




                             }
                           else{

                             ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                               content: Text("Please enter the password properly"),
                             ));
                           }




                                                 },
                         child: Text(
                           'Submit',
                           style: TextStyle(color: Colors.white, fontSize: 15),
                         ),
                       ),
                     ),),



                 ],




                ),


              )


            ],
          )


      ),





      //Navigator.of(context).pushReplacementNamed("/home");

    );



  }


  updatePassword(String password,String mobilenumber) async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(DataConstants.baseurl+DataConstants.changePassword),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        }, body: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      'mobile': mobilenumber,
      'password': password,
      'uuid': date,
      'timestamp': date
    }

    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    int status = jsondata['status'];

    if(status==1)
      {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Password changed successfully"),
        ));


        Navigator.pop(context);
      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed to change password"),
      ));
    }

   // print(response);
  }



}
