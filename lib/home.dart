import 'dart:collection';
import 'dart:convert';
import 'dart:io' show Platform;
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/Profilepage.dart';
import 'package:saveappforios/Settings.dart';
import 'package:saveappforios/database/DatabaseHelper.dart';
import 'package:saveappforios/design/ResponsiveInfo.dart';
import 'package:saveappforios/projectconstants/DataConstants.dart';
import 'package:saveappforios/projectconstants/LanguageSections.dart';
import 'package:saveappforios/views/AccountSetup.dart';
import 'package:saveappforios/views/Bankvoucher.dart';

import 'package:saveappforios/views/DocumentManager.dart';
import 'package:saveappforios/views/PasswordManager.dart';
import 'package:saveappforios/views/Visitingcard.dart';
import 'package:saveappforios/views/journalvoucher.dart';
import 'package:saveappforios/views/my_dream.dart';

import 'Notifications.dart';
import 'database/DBTables.dart';
import 'fragments/Homepage.dart';
import 'fragments/More.dart';
import 'fragments/Network.dart';
import 'fragments/Report.dart';
import 'package:shared_preferences/shared_preferences.dart';


void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: MyDashboardPage(title: 'registration'),
      debugShowCheckedModeBanner: false,
    );
  }
}

  class MyDashboardPage extends StatefulWidget {
  MyDashboardPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyDashboardPage createState() => _MyDashboardPage();
  }

  class _MyDashboardPage extends State<MyDashboardPage> {
  int _counter = 0;
  int _selectedIndex=0;

  bool _fromTop=true;

  // final dbHelper = new DatabaseHelper();

  static const List<Widget> _pages = <Widget>[
  HomePage(title: 'home'),
  // NetworkPage(title: "Network"),
  ReportPage(title: "report"),

  MorePage(title: "more"),
  ];

  var  dbHelper = new DatabaseHelper();

  List<String>targetdata=["Vehicle","New home","Education","Emergency","Healthcare","Party","Charity"];
  List<String>targetimagedata=["images/car.png","images/home.png","images/education.png","images/emergency.png","images/healthcare.png","images/party.png","images/charity.png"];


  @override
  void initState() {
  // TODO: implement initState

showDeviceInfo();
  loadAccountSettingsToDB();


  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);
  super.initState();
  }

  loadLanguageData() async
  {

    final datacount = await SharedPreferences.getInstance();

    String? langdata= datacount.getString(LanguageSections.lan);

    if(langdata!=null)
    {


    }
    else{

      langdata="en";
    }

    datacount.setString(LanguageSections.lan,langdata);
  }


  @override
  void dispose() {
    // TODO: implement dispose
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }



  void _incrementCounter() {
  setState(() {
  // This call to setState tells the Flutter framework that something has
  // changed in this State, which causes it to rerun the build method below
  // so that the display can reflect the updated values. If we changed
  // _counter without calling setState(), then the build method would not be
  // called again, and so nothing would appear to happen.
  // _counter++;
  });
  }

  @override
  Widget build(BuildContext context) {
  // This method is rerun every time setState is called, for instance as done
  // by the _incrementCounter method above.
  //
  // The Flutter framework has been optimized to make rerunning build methods
  // fast, so that you can just rebuild anything that needs updating rather
  // than having to individually change instances of widgets.

  //252c45
  return Scaffold(

    appBar: AppBar(
      toolbarHeight: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?60:70:100,


      flexibleSpace:Container(

        width: double.infinity,

        padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(25),


        child: new Row(

            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              Expanded(
                  child: Container(

                        margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 25, 0, 0):EdgeInsets.fromLTRB(0, 35, 0, 0):EdgeInsets.fromLTRB(0, 45, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,

                      children: [

                        // InkWell(child: Icon(Icons.menu,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:40,color: Colors.white,),
                        //   onTap: (){
                        //     showGeneralDialog(
                        //       barrierLabel: "Label",
                        //       barrierDismissible: true,
                        //       barrierColor: Colors.black.withOpacity(0.5),
                        //       transitionDuration: Duration(milliseconds: 700),
                        //       context: context,
                        //       pageBuilder: (context, anim1, anim2) {
                        //         return Align(
                        //           alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
                        //           child: Container(
                        //             height: (MediaQuery.of(context).size.height)/1.3,
                        //             width: (MediaQuery.of(context).size.width),
                        //             child: SingleChildScrollView(
                        //
                        //
                        //                 child: Column(
                        //
                        //                   children: [
                        //
                        //
                        //                     Container(child: Padding(
                        //                       padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),
                        //
                        //                       //padding: EdgeInsets.symmetric(horizontal: 15),
                        //                       child: Row(
                        //                         textDirection: TextDirection.ltr,
                        //                         children: <Widget>[
                        //
                        //                           GestureDetector(
                        //                             onTap: () {
                        //
                        //                               Navigator.push(
                        //                                   context, MaterialPageRoute(builder: (_) => JournalVoucherPage(title: "journal voucher")));

                        //
                        //                               // Navigator.push(
                        //                               //     context, MaterialPageRoute(builder: (_) => JournalVoucherPage(title: "journal voucher")));
                        //
                        //                             },
                        //
                        //                             child: Image.asset(
                        //                               'images/journal.png',
                        //                               width: 40,
                        //                               height: 40,
                        //                               fit: BoxFit.cover,
                        //                             ),
                        //
                        //                           ),
                        //
                        //
                        //                           Expanded(child: new Theme(data: new ThemeData(
                        //                               hintColor: Colors.white
                        //                           ), child: Align(
                        //                               alignment: Alignment.centerLeft,
                        //
                        //
                        //
                        //                               child:Padding(
                        //
                        //
                        //                                   padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        //                                   child: TextButton(
                        //
                        //
                        //                                     onPressed: () {
                        //
                        //                                       Navigator.push(
                        //                                           context, MaterialPageRoute(builder: (_) => JournalVoucherPage(title: "journal voucher")));
                        //
                        //
                        //                                     }, child: Text('Journal voucher',style: TextStyle(color: Colors.black,fontSize: 16),),
                        //                                   )))))
                        //                         ],
                        //                       ),
                        //                     )),
                        //
                        //                     Divider(color: Colors.black38,
                        //                       thickness: 1,),
                        //
                        //                     Container(child: Padding(
                        //                       padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),
                        //
                        //                       //padding: EdgeInsets.symmetric(horizontal: 15),
                        //                       child: Row(
                        //                         textDirection: TextDirection.ltr,
                        //                         children: <Widget>[
                        //
                        //                           GestureDetector(
                        //
                        //                             onTap: () {
                        //                               Navigator.push(
                        //                                   context, MaterialPageRoute(builder: (_) => BankVoucherPage(title: "bank voucher")));
                        //
                        //                             }
                        //
                        //                             , child: Image.asset(
                        //                             'images/bank.png',
                        //                             width: 40,
                        //                             height: 40,
                        //                             fit: BoxFit.cover,
                        //                           ),
                        //
                        //                           ),
                        //
                        //
                        //
                        //                           Expanded(child: new Theme(data: new ThemeData(
                        //                               hintColor: Colors.white
                        //                           ), child: Align(
                        //                               alignment: Alignment.centerLeft,
                        //
                        //
                        //
                        //                               child:Padding(
                        //
                        //
                        //                                   padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        //                                   child: TextButton(
                        //
                        //
                        //                                     onPressed: () {
                        //
                        //
                        //                                       Navigator.push(
                        //                                           context, MaterialPageRoute(builder: (_) => BankVoucherPage(title: "bank voucher")));
                        //
                        //
                        //
                        //
                        //                                     }, child: Text('Bank voucher',style: TextStyle(color: Colors.black,fontSize: 16),),
                        //                                   )))))
                        //                         ],
                        //                       ),
                        //                     )),
                        //                     Divider(color: Colors.black38,
                        //                       thickness: 1,),
                        //                     Container(child: Padding(
                        //                       padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),
                        //
                        //                       //padding: EdgeInsets.symmetric(horizontal: 15),
                        //                       child: Row(
                        //                         textDirection: TextDirection.ltr,
                        //                         children: <Widget>[
                        //
                        //                           GestureDetector(
                        //                             child:  Image.asset(
                        //                               'images/accounting.png',
                        //                               width: 40,
                        //                               height: 40,
                        //                               fit: BoxFit.cover,
                        //                             ),
                        //                             onTap: (){
                        //
                        //                               Navigator.push(
                        //                                 context, MaterialPageRoute(builder: (_) => AccountSettinglistpage(title: "Payment",accountType: "",)));

                        //
                        //                             },
                        //
                        //                           ),
                        //
                        //
                        //                           Expanded(child: new Theme(data: new ThemeData(
                        //                               hintColor: Colors.white
                        //                           ), child: Align(
                        //                               alignment: Alignment.centerLeft,
                        //
                        //
                        //
                        //                               child:Padding(
                        //
                        //
                        //                                   padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        //                                   child: TextButton(
                        //
                        //
                        //                                     onPressed: () {
                        //
                        //                                       Navigator.push(
                        //                                           context, MaterialPageRoute(builder: (_) => AccountSettinglistpage(title: "Payment",accountType: "",)));
                        //
                        //
                        //
                        //
                        //
                        //                                     }, child: Text('Account settings',style: TextStyle(color: Colors.black,fontSize: 16),),
                        //                                   )))))
                        //                         ],
                        //                       ),
                        //                     )),
                        //                     Divider(color: Colors.black38,
                        //                       thickness: 1,),
                        //                     Container(child: Padding(
                        //                       padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),
                        //
                        //                       //padding: EdgeInsets.symmetric(horizontal: 15),
                        //                       child: Row(
                        //                         textDirection: TextDirection.ltr,
                        //                         children: <Widget>[
                        //
                        //                           GestureDetector(
                        //                             child: Image.asset(
                        //                               'images/card.png',
                        //                               width: 40,
                        //                               height: 40,
                        //                               fit: BoxFit.cover,
                        //                             ),
                        //
                        //                             onTap: (){
                        //
                        //                               Navigator.push(
                        //                                   context, MaterialPageRoute(builder: (_) => Visitcardpage(title: "Visiting card history")));
                        //
                        //                             },
                        //
                        //                           ),
                        //
                        //
                        //                           Expanded(child: new Theme(data: new ThemeData(
                        //                               hintColor: Colors.white
                        //                           ), child: Align(
                        //                               alignment: Alignment.centerLeft,
                        //
                        //
                        //
                        //                               child:Padding(
                        //
                        //
                        //                                   padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        //                                   child: TextButton(
                        //
                        //
                        //                                     onPressed: () {
                        //
                        //                                       Navigator.push(
                        //                                           context, MaterialPageRoute(builder: (_) => Visitcardpage(title: "Visiting card history")));
                        //
                        //
                        //
                        //                                     }, child: Text('Visiting card',style: TextStyle(color: Colors.black,fontSize: 16),),
                        //                                   )))))
                        //                         ],
                        //                       ),
                        //                     )),
                        //                     Divider(color: Colors.black38,
                        //                       thickness: 1,),
                        //                     Container(child: Padding(
                        //                       padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),
                        //
                        //                       //padding: EdgeInsets.symmetric(horizontal: 15),
                        //                       child: Row(
                        //                         textDirection: TextDirection.ltr,
                        //                         children: <Widget>[
                        //
                        //                           GestureDetector(
                        //                             child: Image.asset(
                        //                               'images/passcode.png',
                        //                               width: 40,
                        //                               height: 40,
                        //                               fit: BoxFit.cover,
                        //                             ),
                        //
                        //                             onTap: (){
                        //
                        //                               Navigator.push(
                        //                                   context, MaterialPageRoute(builder: (_) => PasswordManagerpage(title: "passwordmanager")));
                        //
                        //                             },
                        //
                        //                           ),
                        //
                        //                           Expanded(child: new Theme(data: new ThemeData(
                        //                               hintColor: Colors.white
                        //                           ), child: Align(
                        //                               alignment: Alignment.centerLeft,
                        //
                        //
                        //
                        //                               child:Padding(
                        //
                        //
                        //                                   padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        //                                   child: TextButton(
                        //
                        //
                        //                                     onPressed: () {
                        //
                        //                                       Navigator.push(
                        //                                           context, MaterialPageRoute(builder: (_) => PasswordManagerpage(title: "passwordmanager")));
                        //
                        //
                        //
                        //                                     }, child: Text('Password manager',style: TextStyle(color: Colors.black,fontSize: 16),),
                        //                                   )))))
                        //                         ],
                        //                       ),
                        //                     )),
                        //                     Divider(color: Colors.black38,
                        //                       thickness: 1,),
                        //                     Container(child: Padding(
                        //                       padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),
                        //
                        //                       //padding: EdgeInsets.symmetric(horizontal: 15),
                        //                       child: Row(
                        //                         textDirection: TextDirection.ltr,
                        //                         children: <Widget>[
                        //                           GestureDetector(
                        //                             child: Image.asset(
                        //                               'images/foldermanage.png',
                        //                               width: 40,
                        //                               height: 40,
                        //                               fit: BoxFit.cover,
                        //                             ),
                        //                             onTap: (){
                        //
                        //                               Navigator.push(
                        //                                   context, MaterialPageRoute(builder: (_) => DocumentManagerPage(title: "passwordmanager")));
                        //

                        //
                        //
                        //                             },
                        //
                        //                           ),
                        //                           Expanded(child: new Theme(data: new ThemeData(
                        //                               hintColor: Colors.white
                        //                           ), child: Align(
                        //                               alignment: Alignment.centerLeft,
                        //
                        //
                        //
                        //                               child:Padding(
                        //
                        //
                        //                                   padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        //                                   child: TextButton(
                        //
                        //
                        //                                     onPressed: () {
                        //
                        //                                       Navigator.push(
                        //                                           context, MaterialPageRoute(builder: (_) => DocumentManagerPage(title: "passwordmanager")));
                        //
                        //
                        //
                        //
                        //                                     }, child: Text('Document manager',style: TextStyle(color: Colors.black,fontSize: 16),),
                        //                                   )))))
                        //                         ],
                        //                       ),
                        //                     ))
                        //                     ,
                        //
                        //                     Divider(color: Colors.black38,
                        //                       thickness: 1,),
                        //
                        //                     Container(child: Padding(
                        //                       padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),
                        //
                        //                       //padding: EdgeInsets.symmetric(horizontal: 15),
                        //                       child: Row(
                        //                         textDirection: TextDirection.ltr,
                        //                         children: <Widget>[
                        //                           GestureDetector(
                        //                             child: Image.asset(
                        //                               'images/aim.png',
                        //                               width: 40,
                        //                               height: 40,
                        //                               fit: BoxFit.cover,
                        //                             ),
                        //                             onTap: (){
                        //
                        //                                Navigator.push(
                        //                                   context, MaterialPageRoute(builder: (_) => MyDream( )));
                        //
                        //
                        //
                        //
                        //                             },
                        //
                        //                           ),
                        //                           Expanded(child: new Theme(data: new ThemeData(
                        //                               hintColor: Colors.white
                        //                           ), child: Align(
                        //                               alignment: Alignment.centerLeft,
                        //
                        //
                        //
                        //                               child:Padding(
                        //
                        //
                        //                                   padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        //                                   child: TextButton(
                        //
                        //
                        //                                     onPressed: () {
                        //
                        //                                       Navigator.push(
                        //                                           context, MaterialPageRoute(builder: (_) => MyDream()));
                        //
                        //
                        //
                        //
                        //                                     }, child: Text('My Dream',style: TextStyle(color: Colors.black,fontSize: 16),),
                        //                                   )))))
                        //                         ],
                        //                       ),
                        //                     ))
                        //                     ,
                        //                   ],
                        //
                        //
                        //                 )),
                        //             margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
                        //             decoration: BoxDecoration(
                        //               color: Colors.white,
                        //               borderRadius: BorderRadius.circular(10),
                        //             ),
                        //           ),
                        //         );
                        //       },
                        //       transitionBuilder: (context, anim1, anim2, child) {
                        //         return SlideTransition(
                        //           position: Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0)).animate(anim1),
                        //           child: child,
                        //         );
                        //       },
                        //     );
                        //
                        //   },
                        // ),

                        Image.asset("images/sl.png",width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:80,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:80,),

                         Text("My Personal app",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 11:15:19,color: Color(0xff007a74),),maxLines: 2,),

                        InkWell(child: Icon(Icons.person,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:40,color: Colors.black,),
                          onTap: (){

                            Navigator.push(
                                context, MaterialPageRoute(builder: (_) => ProfilePage(title: "profilepage",)));

                          },
                        ),

                        InkWell(child: Icon(Icons.notifications_none,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:40,color: Colors.black,),
                          onTap: (){
                            Navigator.push(
                                context, MaterialPageRoute(builder: (_) => NotificationPage(title: "notificationpage",)));


                          },
                        ),

                        InkWell(child: Icon(Icons.settings,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:40,color: Colors.black,),
                          onTap: (){

                            Navigator.push(context, MaterialPageRoute(
                                builder: (context) => SettingsPage(title: "dashboard",)
                            )
                            );


                          },
                        ),





                      ],

                    ),
                  ),

                flex: 2,

              )

              ,





            ]),














        color: Color(0x6D83D6DF),









      ),
      backgroundColor: Color(0x6D83D6DF),


      centerTitle: false,

    ),

      body: Container(

          height: double.infinity,
          width: double.infinity,


          child:Stack(  alignment: Alignment.topLeft,  children: <Widget>[


            Align(
                child:Column(

              children: [
                Expanded(child: Container(
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),

        // padding: const EdgeInsets.all(20),

        child:_pages.elementAt(_selectedIndex),),flex: 6,
  ),


                Expanded(child:
                    Container(










    )


                ,flex: 1,)
                
                
            ],
              
              
          )
            ),

            Align(

              alignment: Alignment.bottomCenter,child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                currentIndex: _selectedIndex,
                selectedItemColor: Colors.white,
                unselectedItemColor: Colors.grey,
                backgroundColor: Color(0xff096c6c),
                showUnselectedLabels: true,


                onTap:_onItemTap ,

                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(


                    icon: ImageIcon(AssetImage('images/home.png')),
                    label: 'Home',

                  ),

                  // BottomNavigationBarItem(
                  //   icon: ImageIcon(AssetImage('images/network.png')),
                  //   label: 'Network',
                  //   backgroundColor: Color(0xff096c6c),
                  //
                  //
                  // ),

                  BottomNavigationBarItem(
                    icon: ImageIcon(AssetImage('images/file.png')),
                    label: 'Report',
                    backgroundColor: Color(0xff096c6c),

                  ),

                  BottomNavigationBarItem(
                    icon: ImageIcon(AssetImage('images/more.png')),
                    label: 'More',
                    backgroundColor: Color(0xff096c6c),


                  ),




                ]

            ),)



          ],







          )


      )

  );

  }

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void loadAccountSettingsToDB() async
  {
    final datacount = await SharedPreferences.getInstance();

    bool? token=datacount.getBool(DataConstants.AccSettingsFirst);

    if(token==null || !token)
    {

      for(String s in DataConstants.expenses )
      {

        var mjobject=new Map();
        mjobject['Accountname']=s;
        mjobject['Accounttype']="Expense account";
        mjobject['Amount']="0";
        mjobject['Type']="Debit";

        var js=json.encode(mjobject);

        Map<String, dynamic> data_To_Table=new Map();
        data_To_Table['data']=js.toString();
        int id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_ACCOUNTSETTINGS);

        print("expense account : $id");

      }

      for(String s in DataConstants.reciptAccount )
      {



        var mjobject=new Map();
        mjobject['Accountname']=s;
        mjobject['Accounttype']="Income account";
        mjobject['Amount']="0";
        mjobject['Type']="Credit";

        var js=json.encode(mjobject);

        Map<String, dynamic> data_To_Table=new Map();
        data_To_Table['data']=js.toString();
        final id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_ACCOUNTSETTINGS);

        print("Income account : $id");

      }

      var mjobject=new Map();
      mjobject['Accountname']="Cash";
      mjobject['Accounttype']="Cash";
      mjobject['Amount']="0";
      mjobject['Type']="Debit";

      var js=json.encode(mjobject);

      Map<String, dynamic> data_To_Table=new Map();
      data_To_Table['data']=js.toString();
      final id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_ACCOUNTSETTINGS);


      for(int i=0;i<targetdata.length;i++)
      {

        final ByteData bytes = await rootBundle.load(targetimagedata[i]);
        final Uint8List list = bytes.buffer.asUint8List();

        Map<String,dynamic>mp=new HashMap();

        mp["data"]=targetdata[i];
        mp["iconimage"]=list;

        new DatabaseHelper().insert(mp, DatabaseTables.TABLE_TARGETCATEGORY);


      }



      datacount.setBool(DataConstants.AccSettingsFirst,true);
    }


  }

  void apptrackPermission() async
  {
   // final status = await AppTrackingTransparency.requestTrackingAuthorization();

    // If the system can show an authorization request dialog
    final datacount = await SharedPreferences.getInstance();

    bool? switchValue = datacount.getBool(
        DataConstants.ApptrackPermissionkey);


    if(switchValue!=null) {
      if (switchValue) {
        // Show a custom explainer dialog before the system dialog

        // Request system's tracking authorization dialog
        // await AppTrackingTransparency.requestTrackingAuthorization();
      }
      else {
        await showCustomTrackingDialog(context);
        // Wait for dialog popping animation

      }
    }
    else{
      await showCustomTrackingDialog(context);

    }

  }

  showCustomTrackingDialog(BuildContext context) {






  }

  showDeviceInfo() async
  {
    if (Platform.isIOS) {
      print(Platform.operatingSystem); // "ios"
   String d=Platform.operatingSystemVersion;



      print(d);

   List<String>data=   d.split(" ");
   if(data.length>0)
     {
       String  aStr = data[1];
       print(aStr);

       double vd=double.parse(aStr);

       if(vd>=14)
         {

           apptrackPermission();
         }



     }



      // iOS 13.1, iPhone 11 Pro Max iPhone
    }

  }


}



