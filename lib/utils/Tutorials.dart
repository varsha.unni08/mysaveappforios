import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml_parser/xml_parser.dart';
class Tutorial{


    static  showTutorial(String msg,BuildContext context,String key) async
   {

      final datacount = await SharedPreferences.getInstance();
      if(datacount.getString(key).toString().compareTo("1")==0)
      {

      }
      else {
         Widget yesButton = TextButton(
            child: Text("Ok"),
            onPressed: () {

               Navigator.pop(context);
               datacount.setString(key,"1");


            },
         );


         // set up the AlertDialog
         AlertDialog alert = AlertDialog(
            title: Text("Save"),
            content: SingleChildScrollView(

               child: Column(

                  children: [

                     Text(msg)
                  ],


               ),


            )








          ,
            actions: [yesButton],
         );

         // show the dialog
         showDialog(
            context: context,
            builder: (BuildContext context) {
               return alert;
            },
         );
      }
   }




   static final  String paymentvouchertutorial="paymentvouchertutorial";
   static final  String reciptvouchertutorial="reciptvouchertutorial";
   static final  String journalvouchertutorial="journalvouchertutorial";
   static final  String accsetuptutorial="accsetuptutorial";
   static final  String bankvouchertutorial="bankvouchertutorial";
   static final  String billingtutorial="billingtutorial";
   static final  String wallettutorial="wallettutorial";
   static final  String cashbankbalancetutorial="cashbankbalancetutorial";
   static final  String assettutorial="assettutorial";
   static final  String liabilitytutorial="liabilitytutorial";
   static final  String insurancetutorial="insurancetutorial";
   static final  String investmenttutorial="investmenttutorial";
   static final  String mydiarytutorial="mydiarytutorial";
   static final  String budgettutorial="budgettutorial";
   static final  String transactiontutorial="transactiontutorial";
   static final  String ledgertutorial="ledgertutorial";
   static final  String cashbalanceledgertutorial="cashbalanceledgertutorial";
   static final  String incomexptutorial="incomexptutorial";
   static final  String listofmyassetstutorial="listofmyassetstutorial";
   static final  String listofliabilitiestutorial="listofliabilitiestutorial";
   static final  String listofmyinsurancetutorial="listofmyinsurancetutorial";
   static final  String listofmyinvestmenttutorial="listofmyinvestmenttutorial";
   static final  String addassettutorial="addassettutorial";
    static final  String remindsss="addassettutorial";
    static final  String viewchart="viewchart";
    static final  String visitcard="visitcard";

    static final  String password="password";
    static final  String document="document";

    static final String document_tutorial="You can save your highly important documents and its informations to google drive  by clicking on '+' button.\n\n By clicking '+' button , the app shows the file type which is document(pdf,doc,html, etc.) and photo from gallery.\n\n then choose your GmailId for uploading file to google drive,it will automatically upload to google drive.\n\n These information are highly secure safe.\n\n We do not use these informations for any purposes ";



    static final String passwordtutorial="You can save your highly important password informations by clicking on '+' button.\n\n We will not share these informations to third parties.\n\n We will keep this informations safely ";


    static final String viewcharttutorial="This chart shows total amount calculation in income and expense  acount types according to year wise ";

    static final String visitingcard_tutorial="You can create your own visting card with your name,addresses,etc. by clicking on '+' button in bottom side";

    static final String paymenttutorial="Expenses/ payment voucher \n\nPayment is used to record expenses/payments\n\n"+
       "1.The screen displays current month transactions.\n\n"+
   "2. Use “Edit/Delete” for modification/deletion.\n\n"+
   "3. Touch + button to enter new expenses/payment.\n\n"+
   "4. Select date of payment.\n\n"+
   "5. Select the payment account from the list or press + to create a new account.\n\n"+
   "6. Enter the amount paid.\n\n"+
   "7. Select the mode of payment - Cash or Bank.\n\n"+
   "8.If payment mode is bank, select the bank account or press + to create a new bank account.\n\n"+
   "9.Enter remarks if any and save. ";

   static final String recipttutorial="Receipt Voucher\n\n"+

   "Receipts is used to record receipts/incomes\n\n"+

   "1. The screen displays current month transactions.\n\n"+
   "2. Use “Edit/Delete” for modification/deletion.\n\n"+
   "3. Touch + button to enter new receipt/income.\n\n"+
   "4. Select date of receipt.\n\n"+
   "5. Select the receipt account from the list or press + to create a new account.\n\n"+
   "6. Enter the amount received.\n\n"+
   "7. Select the mode of receipt - Cash or Bank.\n\n"+
   "8. If receipt mode is bank, select the bank account or press + to create a new bank account.\n\n"+
   "9. Enter remarks if any and save\n\n";

   static final String journaltutorial="Journal Voucher\n\n"+

   "Journal is used for adjustment entries between two accounts.\n\n"+

   "1. The screen displays current month transactions.\n\n"+
   "2. Use “Edit/Delete” for modification/deletion.\n\n"+
   "3. Touch + button to enter new transaction\n\n"+
   "4. Select date of Journal.\n\n"+
   "5.Select the debit account from the list or press + to create a new account.\n\n"+
   "6.Enter the amount.\n\n"+
   "7. Select the credit account from the list or press + to create a new account.\n\n"+
   "8. Enter remarks if any and save.\n\n";


    static final String accountsetuptutorial="Account Setup\n\n"+

    "To create an account head (ledger).\n\n"+

    "1. Display all existing/default account heads in alphabetical order\n\n"+
    "2. Use “Edit/Delete” for modification/deletion.\n\n"+
    "3. Press + button to create a new ledger.\n\n"+
    "4. Enter the account name.\n\n"+
   " 5. Select the category.\n\n"+
    "6. Enter the opening balance if any.\n\n"+
    "7. Select the balance type - debit or credit and save.\n\n";


    static final String bankvoucherttorial="Bank Voucher\n\n"+
    
    "To bank transactions like cash deposits and withdrawals.\n\n"+
    
   " 1. The screen displays current month transactions.\n\n"+
    "2. Use “Edit/Delete” for modification/deletion.\n\n"+
    "3. Touch + button to enter new transaction\n\n"+
    "4. Select date of deposit/withdrawal.\n\n"+
    "5. Select the bank account or press + to create a new account.\n\n"+
    "6. Enter the amount.\n\n"+
    "7. Select type of transaction – deposit/withdrawal\n\n"+
    "8. Enter remarks if any and save";

    static final String billtutorial="Billing\n\n"+
    
    "To issue a sales/service bill\n\n"+
    
    "1. The screen displays current month transactions.\n\n"+
    "2. Use “Edit/Delete” for modification/deletion.\n\n"+
   " 3. Use “Get Receipt” to receive bill amount from the customer.\n\n"+
    "4. Touch + button to enter new transaction\n\n"+
    "5. Select date of bill.\n\n"+
    "6. Select the customer or press + to create a new customer.\n\n"+
    "7. Enter the amount.\n\n"+
    "8. Select type of income account or press + to create a new income account.\n\n"+
    "9. Enter remarks if any and save.\n\n";
    
    static final String wallettitorial=
    "This is a virtual wallet. \n\n"
    
    "1. Screen displays expenses of current month and wallet balance.\n\n"
    "2. Touch + button to add money to the wallet. \n\n"
    "3. Select the date.\n\n"
    "4. Enter the amount and save.\n\n";
    
    
    static final String cashbankstmt="Cash/Bank Statement\n\n"
    
   " 1. Screen shows the current closing balance of cash and bank accounts.\n\n"
   " 2. Select period to show the transactions\n\n"
    "3. Click “View” to display transactions for the selected period.\n\n";

    static final String assetTutorial="Asset\n\n"
    
    "To list movable and immovable assets. \n\n"
    
  "  1. The Screen displays already saved assets. \n\n"
    "2. Use “Edit or Delete” for modification.\n\n"
    "3. Press + button to create a new asset. “Example – Car”\n\n"
    "4. Category by default will be Asset account\n\n"
    "5. Enter the current value if any\n\n"
    "6. All assets will be in Debit as default\n\n"
    "7. Enter save button to create an asset\n\n"
   " 8. If required, enter the date of purchase\n\n"
    "9. Set Reminder dates such as insurance renewal date, Tax payable date, etc.\n\n"
    "10. Select date and type description\n\n"
    "11. Press the reminder button again for another date if needed. \n\n"
    "12. The dates will set automatically in reminder and will display in Daily Task.\n\n";
    
    
    static final liability_tutorial="Liability\n\n"
    
    "To list loans and liabilities\n\n"
    
    "1. The screen displays already saved loans and liabilities.\n\n"
    "2. Use “Edit or Delete” for modification.\n\n"
    "3. Press + button to create a new liability. “Example – Housing loan”\n\n"
    "4. Category by default will be Liability account\n\n"
    "5. Enter the current balance\n\n"
    "6. All liabilities will be in Credit as default.\n\n"
    "7. Enter the save button to create a liability.\n\n"
    "8. Select repayment type - EMI/Non-EMI.\n\n"
    "9. Enter EMI amount\n\n"
    "10. Enter number of EMI remains\n\n"
    "11. Select payment date of EMI.\n\n"
    "12. System will display the closing date.\n\n"
    "13. The payment dates will set automatically in reminder and will display in Daily Task.\n\n";
    
    static final String insuranceentrytutorial="Insurance\n\n"
    
    "Used to record information about the insurance policies.\n\n"
    
  "  1. The screen displays already saved insurance.\n\n"
    "2. Use “Edit or Delete” for modification.\n\n"
    "3. Press + button to create a new insurance. “Example – Life insurance”\n\n"
    "4. Category by default will be insurance\n\n"
    "5. Enter the paid-up value.\n\n"
    "6. All insurance will be in Debit as default.\n\n"
    "7. Enter the save button to create an insurance.\n\n"
    "8. Enter the premium amount\n\n"
    "9. Select the premium payment frequency.\n\n"
    "10. Closing date and remarks if any.\n\n"
    "11. The premium dates will set automatically in reminder and will display in Daily Task.\n\n ";
    
    static final String investment_tutorial="Investments\n\n"
    
    "Used to record information about the investments.\n\n"
    
    "1. The screen displays already saved investments.\n\n"
    "2. Use “Edit or Delete” for modification.\n\n"
    "3. Press + button to create a new investment. “Example – Recurring deposit scheme”\n\n"
    "4. Category by default will be investment.\n\n"
    "5. Enter the current deposit value.\n\n"
    "6. All insurance will be in Debit as default.\n\n"
    "7. Select payment frequency\n\n"
    "8. Enter instalment amount\n\n"
    "9. Enter number of instalments remains\n\n"
    "10. Enter date of payment and remarks if any.\n\n"
    "11. System will display the closing date.\n\n"
    "12. The payment dates will set automatically in reminder and will display in Daily Task.\n\n";
    
    
    static final String mydiarytuorial="My Diary\n\n"
    
    "Used to record thoughts, experience, passion and hobbies.\n\n"
    
    "1. The screen displays already saved notes\n\n"
    "2. Press the arrow button to download PDF formats of selected subjects for the selected period.\n\n"
    "3. Press + button to create a new note.\n\n"
    "4. Select language\n\n"
    "5. Select date\n\n"
    "6. Press + button to create new subject\n\n"
    "7. Start typing or record your thoughts and experience.\n\n"
    "8. Press save button to save data\n\n";


    static final budgetutorial="Budget\n\n"
    
    "Budget is used to set budget and budgetary provision.\n\n"

    "1. Select the year of budget\n\n"
    "2. Select the expense heads from the list.\n\n"
    "3. Enter the monthly amount.\n\n"
  "  4. System will automatically allocate the entered amount to all months when submitting.\n\n"
   " 5. Edit monthly figure as per requirement.\n\n"
  "  6. When entering expenses through payment voucher, the user gets a warning message if the budgetary provision exceeds for the selected head. \n\n";

    static final transaction_tutorial="Transactions\n\n"
    "This report shows all the transactions in the given period in a double entry manner.";

    static final ledger_tutorial="Ledger\n\n"
   " All existing ledgers are displayed on the first page along with its closing balance. Touching the view button, it shows date wise entries for the given period. You can also download this report in pdf format by touching the down arrow.";


    static final cashbalanceledger_tutorial="Cash and Bank Balances\n\n"
    "This is a report similar to the Ledger report. Only accounts in the Cash or Bank Account category are shown here. It can also be downloaded in pdf.";

    static final incomexp_tutorial="Income and Expenditure Statement.\n\n"
    "This report shows the excess or deficit of income over expenses for a particular period."
    "Touch Search after entering the date period, it will display the summary of Total Income and Total Expenses."
    "The details can be seen by touching the down arrow to the right of each of them.";


    static final reminders_tutorial="Reminders\n\n"
    "Display all reminders that are generated from Task, Asset, Liability, Investment and insurance."
    "There is an option to search for reminders for a particular date.";


    static final listofmyassets_tutorial="List of My Assets\n\n"
    "It lists all the assets recorded in the Asset module. Touch the view to see the closing balance and transaction details recorded in each.";


    static final listofliabilities_tutorial="It lists all the liabilities recorded in the Liability module. Touch the view to see the closing balance and transaction details recorded in each.";


    static final listofmyinsurance_tutorial="It lists all the insurances recorded in the Insurance module. Touch the view to see the closing balance and transaction details recorded in each.";


    static final listofmyinvestment_tutorial="It lists all the Investments recorded in the Investment module. Touch the view to see the closing balance and transaction details recorded in each.";








}