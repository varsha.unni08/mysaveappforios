import 'dart:convert';

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/domain/TemregData.dart';
// import 'package:save_flutter/domain/country.dart';
import 'dart:io';
import 'connection/DataConnection.dart';
import 'domain/TemregData.dart';
import 'domain/country.dart';
import 'home.dart';
import 'login.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'projectconstants/DataConstants.dart';
import 'projectconstants/ServerMessageType.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;

import 'package:shared_preferences/shared_preferences.dart';

void main() {
  // runApp(MyApp());


  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: Otppage(title: 'registration'),
      debugShowCheckedModeBanner: false,
    );
  }
}


class Otppage extends StatefulWidget{


  Otppage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyOtpState createState() => _MyOtpState();
}


class _MyOtpState extends State<Otppage> {
  int _counter = 0;

  int code=0;

  List<Countrydata> users = <Countrydata>[
    const Countrydata('Android'),
    const Countrydata('Flutter'),
    const Countrydata('ReactNative'),
    const Countrydata('iOS')
  ];
  String dropdownValue = 'Select country';

  TextEditingController otpcodeController=new TextEditingController();

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      // _counter++;
    });
  }

  @override
  void initState() {
    // TODO: implement initState

    DataConnectionStatus.check().then((value)
    {

      if (value != null && value) {
        // Internet Present Case

        sendOtpCode(TemRegData.mobilenumber);

      }
      else{

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Check your internet connection"),
        ));

      }
    }
    );


    super.initState();


    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    // sleep(const Duration(seconds:6));
    // startTime();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {


    return new Scaffold(
        backgroundColor: Colors.black12,
        resizeToAvoidBottomInset: false,

        body:Card(

            elevation: 5,
            color: Color(0xfff3f3f3),
       child: Stack(
            children: <Widget>[

    // SingleChildScrollView(
    // child: Column(
    //
    // children: <Widget>[


      Align(
        alignment: Alignment.topCenter,
        child:  Padding(
          padding: const EdgeInsets.only(left:15.0,right: 15.0,top:110,bottom: 0),
          // padding: EdgeInsets.all(15),
          child: Text(
            'Please enter the OTP code that sent to your mobile number '+TemRegData.mobilenumber,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
                fontSize: 14),


          ),
        ),

      ),

      Align(
        alignment: Alignment.topCenter,
        child:  Padding(
          padding: const EdgeInsets.only(left:15.0,right: 15.0,top:150,bottom: 0),
          // padding: EdgeInsets.all(15),
          child: Image.asset('images/otpphone.png',width: 80,height: 80,),
        ),

      ),


      Align(
        alignment: Alignment.center,
        child:  Padding(
          padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
          // padding: EdgeInsets.all(15),
          child: new Theme(data: new ThemeData(
              hintColor: Colors.black26
          ), child: TextField(
            controller: otpcodeController,
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black26, width: 0.5),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black26, width: 0.5),
              ),
              hintText: 'Enter the otp code',
            ),
          )),
        ),

      ),
      Align(
        alignment: Alignment.bottomCenter,
        child:  Padding(
          padding: const EdgeInsets.only(left:15.0,right: 15.0,top:90,bottom: 190),
          // padding: EdgeInsets.all(15),
          child: TextButton(
            onPressed: () {  sendOtpCode( TemRegData.mobilenumber); },
            child : Text('Did not get OTP ? Resend',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 14),),


          ),
        ),

      ),


      Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 100),
          child :   Container(
            height: 50,
            width: 150,
            decoration: BoxDecoration(
                color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
            child: TextButton(
              onPressed: () {

                if(otpcodeController.value.text.isNotEmpty)
                {

                  if(otpcodeController.value.text.compareTo(code.toString())==0)
                  {


                    DataConnectionStatus.check().then((value)
                    {

                      if (value != null && value) {
                        // Internet Present Case

                       registerUser();
                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Check your internet connection"),
                        ));

                      }
                    }
                    );
                  }

                  else{
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Incorrect otp code"),
                    ));

                  }



                }
                else{
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text("Please enter your otp code"),
                  ));

                }





              },
              child: Text(
                'Submit',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
            ),
          ),),),

    // ]
    // ),
    // ),
    ],
    ),
    ));





  }

  sendOtpCode(String mobilenumber) async
  {

    var rng = new Random();
     code = rng.nextInt(9000) + 1000;

    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //   content: Text("Your code is : "+code.toString()),
    // ));




    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();


    String message=DataConstants.buildServerMessage(ServerMessageType.registration,code.toString(),"","");

    String u=message.replaceAll(" ", "%20");


    print('the message is '+u);

    String url=DataConstants.smsbaseurl+DataConstants.smsMethode+"?token="+DataConstants.apikey+
        "&sender="+DataConstants.sender+
        "&number="+mobilenumber+
        "&route="+DataConstants.route+
        "&type="+DataConstants.type+
        "&sms="+u+"&templateid="+DataConstants.registrationtemplateid;



    var dataasync = await http.get(
      Uri.parse(url),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

      //     body: <String, String>{
      //   'Content-Type': 'application/x-www-form-urlencoded',
      //
      //   'uuid': date,
      //   'timestamp': date
      // }

    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);




  }


  registerUser() async {

    final datacount = await SharedPreferences.getInstance();

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl+DataConstants.userAuthenticate),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

          body: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

        'uuid': date,
        'timestamp': date,
            'country_id':TemRegData.countryid,

            'sp_reg_code': TemRegData.sponserregCode,
            'sp_reg_id': TemRegData.spregid,
            'stateid':TemRegData.stateid,
            'language': TemRegData.language,
            'name': TemRegData.name,
            'mobile':TemRegData.mobilenumber,
            'password': TemRegData.password,
            'email': TemRegData.email,
            'timestamp':date,
          }

    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    print(jsondata);


    int status = jsondata['status'];
    String token=jsondata['token'];

    datacount.setString(DataConstants.userkey, token);

    if(status==1)
      {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Registration completed successfully"),
        ));

        DataConnectionStatus.check().then((value)
        {

          if (value != null && value) {
            // Internet Present Case

            sendRegistrationMessage();
          }
          else{

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Check your internet connection"),
            ));

          }
        }
        );




      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("There is a problem while registration"),
      ));
    }


  }


  sendRegistrationMessage() async
  {
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();


    String message=DataConstants.buildServerMessage(ServerMessageType.registration_Confirm_password,code.toString(),"","");

    String u=message.replaceAll(" ", "%20");


    print('the message is '+u);

    String url=DataConstants.smsbaseurl+DataConstants.smsMethode+"?token="+DataConstants.apikey+
        "&sender="+DataConstants.sender+
        "&number="+TemRegData.mobilenumber+
        "&route="+DataConstants.route+
        "&type="+DataConstants.type+
        "&sms="+u+"&templateid="+DataConstants.registration_Confirm_templateid;



    var dataasync = await http.get(
      Uri.parse(url),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

      //     body: <String, String>{
      //   'Content-Type': 'application/x-www-form-urlencoded',
      //
      //   'uuid': date,
      //   'timestamp': date
      // }

    );

    // _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => Loginpage(title: "login",)
    )
    );
  }
}