import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/design/ResponsiveInfo.dart';
import 'package:saveappforios/projectconstants/DataConstants.dart';
import 'package:saveappforios/projectconstants/LanguageSections.dart';
import 'package:saveappforios/views/InvoicePage.dart';

// import 'package:save_flutter/projectconstants/DataConstants.dart';
 import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
 import 'connection/DataConnection.dart';
import 'domain/CountryData.dart';
import 'domain/CountryList.dart';
import 'domain/InvoiceData.dart';
import 'domain/Profiledata.dart';
import 'domain/StateData.dart';
import 'domain/Statelist.dart';
import 'package:xml_parser/xml_parser.dart';

//import 'package:custom_progress_dialog/custom_progress_dialog.dart';

import 'package:http/http.dart' as http;

import 'domain/TemregData.dart';

import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

import 'domain/bill_entity.dart';


void main() {
  // runApp(MyApp());

  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'login',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: ProfilePage(title: 'login'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ProfilePage extends StatefulWidget {
  final String title;

  const ProfilePage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> {


  String dropdownValue = 'Select country';
  String dropdownstate="Select state";
  String profileimage="";


  String languagedropdown='English',namehint="Name",emailhint="Email";

  bool  isstateVisible=false,isSponserVisible=false;

  List<CountryData> countrydatalist=[];

  List<Statelist> statedatalist=[];

  List<String> statedata_data=[];

  List<String> countr_data=[];

  late CountryData cddata;

  bool istermsconditionchecked=false;

  String countrid="0",stateid="0",submit="Submit";

  String name="";
  String email="",mobile="";
  String amounttopay="";

  String id="";

  double cgst=0,igst=0,sgst=0,othertaxamount=0,totalamount=0;

  TextEditingController emailcontroller=new TextEditingController();

  TextEditingController namecontroller=new TextEditingController();

  String mobilenumber="",profile="";


  @override
  void initState() {
    // TODO: implement initState



    checkLanguage();

    DataConnectionStatus.check().then((value)
    {

      if (value != null && value) {
        // Internet Present Case

        getProfile();

       // getCountryData();


      }
      else{

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Check your internet connection"),
        ));

      }
    }
    );
    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          toolbarHeight: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?60:70:80,

          flexibleSpace: Container(

              color: Color(0xFF096c6c),
              width: double.infinity,
              padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
              child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Container(
                          margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(2, 10, 0, 0) : EdgeInsets.fromLTRB(4, 20, 0, 0):EdgeInsets.fromLTRB(6, 30, 0, 0),
                          alignment: Alignment.center,
                          child: Center(
                              child: new InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Icon(Icons.arrow_back,color: Colors.white,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,)))),
                      flex: 1,
                    ),
                    Expanded(
                      child: Container(
                          margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 10, 0, 0) : EdgeInsets.fromLTRB(15, 20, 0, 0):EdgeInsets.fromLTRB(20, 30, 0, 0),
                          alignment: Alignment.centerLeft,
                          child: Row(children: [

                            Expanded(child: Text(
                             profile,
                              style: TextStyle(
                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13 : 16:19, color: Colors.white),
                            ),flex: 2,),
                            //
                            // Expanded(child:Center(
                            //     child: new InkWell(
                            //         onTap: () async {
                            //
                            //
                            //           getBill();
                            //
                            //
                            //
                            //
                            //
                            //         },
                            //         child: Icon(Icons.list_alt_sharp,color: Colors.white,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,)))  ,flex: 1,)



                          ],)),
                      flex: 5,
                    ),
                  ])) ,
          backgroundColor: Color(0xFF096c6c),
          centerTitle: false,

        ),
        body: Stack(children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage("images/splashbg.png"),
                    fit: BoxFit.fill)),
          ),
          SingleChildScrollView(
              child: Column(children: <Widget>[
            Align(
                alignment: FractionalOffset.topCenter,

                  child: Padding(
        padding: EdgeInsets.all(2),
        child: Container(

          height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?120:150:180,
          width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 120:150:180,

    child:  Stack(children: [

      Align(
          alignment: FractionalOffset.bottomCenter,

          child:

          Container(
              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?100:130:160,
              width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 100:130:160,

              child:CircleAvatar(
                backgroundImage: NetworkImage(
                    DataConstants.profileimgbaseurl+profileimage,
                    scale: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?100:130:160


                ),



                // child: Image.network(
                //   DataConstants.profileimgbaseurl+profileimage,
                //   fit: BoxFit.fill,
                //   errorBuilder: (context, url, error) => new Icon(Icons.account_circle_rounded,size: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?90:120:160,),
                // ),


              )

            // child:Image.asset("images/user.png")
          ) ),



      Align(
          alignment: FractionalOffset.bottomCenter,
          child: Padding(

    padding:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(45, 0, 0, 0) : EdgeInsets.fromLTRB(60, 0, 0, 0):EdgeInsets.fromLTRB(80, 0, 0, 0),

    child: FloatingActionButton(
      onPressed: () async {

        Widget yesButton = TextButton(
            child: Text("Yes",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
            onPressed: () async {

              Navigator.pop(context);

              final ImagePicker _picker = ImagePicker();
              // Pick an image
              final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

              // print(image!.path) ;

              if(image!=null) {


                CroppedFile? croppedFile = await ImageCropper()
                    .cropImage(
                    sourcePath: image.path,
                    aspectRatioPresets: [
                      CropAspectRatioPreset.square,
                      CropAspectRatioPreset.ratio3x2,
                      CropAspectRatioPreset.original,
                      CropAspectRatioPreset.ratio4x3,
                      CropAspectRatioPreset.ratio16x9
                    ],
                    // androidUiSettings: AndroidUiSettings(
                    //     toolbarTitle: 'Cropper',
                    //     toolbarColor: Colors.deepOrange,
                    //     toolbarWidgetColor: Colors.white,
                    //     initAspectRatio: CropAspectRatioPreset
                    //         .original,
                    //     lockAspectRatio: false),
                    // iosUiSettings: IOSUiSettings(
                    //   minimumAspectRatio: 1.0,
                    // )
                );


                uploadImage(croppedFile);
              }


            });



        Widget noButton = TextButton(
          child: Text("No",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
          onPressed: () {
            Navigator.pop(context);
          },
        );

        // set up the AlertDialog
        AlertDialog alert = AlertDialog(
          title: Text("Save",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
          content: Text("We will keep your profile photo secure, We do not going to share outside.Do you want to continue ?",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
          actions: [yesButton, noButton],
        );

        // show the dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );







      },
      child: Icon(
        Icons.edit,
        color: Colors.white,
        size: 29,
      ),
      backgroundColor: Color(0xffbfefcc),
      tooltip: 'Capture Picture',
      elevation: 5,
      splashColor: Colors.grey,
    ),
    )



          )
    ]),
    )




       ),





                    ),

                Padding(
                  padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18) ,

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.ltr,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment:
                    MainAxisAlignment.center,
                    //Center Row contents horizontally,
                    crossAxisAlignment:
                    CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(child:
                      Text(mobilenumber+" :  " ,style: TextStyle(color: Colors.white,fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),

                      ),flex:1),
                      Expanded(child: Text("  "+TemRegData.mobilenumber,style: TextStyle(color: Colors.white,fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19))
                      ,flex: 1,)
                    ],
                  ),
                ),

                Padding(
                  padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18),
                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: new Theme(data: new ThemeData(
                      hintColor: Colors.white
                  ), child: TextField(

                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      hintText: namehint,

                    ),
                    onChanged: (text) {
                      TemRegData.name=text;

                    },
                    controller: namecontroller,
                  )),
                ),

                // Padding(
                //     padding: const EdgeInsets.all(15),
                //     child:Container(
                //       width: double.infinity,
                //       height: 60.0,
                //       decoration: BoxDecoration(
                //         border: Border.all(
                //           color: Colors.white,
                //           // red as border color
                //         ),
                //       ),
                //       child: DropdownButtonHideUnderline(
                //         child: ButtonTheme(
                //           alignedDropdown: true,
                //           child: InputDecorator(
                //             decoration: const InputDecoration(border: OutlineInputBorder()),
                //             child: DropdownButtonHideUnderline(
                //               child: DropdownButton(
                //
                //                 value: dropdownValue,
                //                 items: countr_data
                //                     .map<DropdownMenuItem<String>>((String value) {
                //                   return DropdownMenuItem<String>(
                //                     value: value,
                //                     child: Text(value),
                //                   );
                //                 }).toList(),
                //                 onChanged: (String? newValue) {
                //                   setState(() {
                //                     dropdownValue = newValue!;
                //
                //                     DataConnectionStatus.check().then((value)
                //                     {
                //
                //                       if (value != null && value) {
                //                         // Internet Present Case
                //
                //                         getState(dropdownValue);
                //
                //
                //                       }
                //                       else{
                //
                //                         ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                //                           content: Text("Check your internet connection"),
                //                         ));
                //
                //                       }
                //                     }
                //                    );
                //
                //
                //
                //                   });
                //
                //                 },
                //                 //  value: dropdownValue,
                //                 style: Theme.of(context).textTheme.bodyText1,
                //
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     )),
                //
                //
                //
                // isstateVisible?  Padding(
                //     padding: const EdgeInsets.all(15),
                //     child:Container(
                //       width: double.infinity,
                //       height: 60.0,
                //       decoration: BoxDecoration(
                //         border: Border.all(
                //           color: Colors.white,
                //           // red as border color
                //         ),
                //       ),
                //       child: DropdownButtonHideUnderline(
                //
                //         child: ButtonTheme(
                //           alignedDropdown: true,
                //           child: InputDecorator(
                //             decoration: const InputDecoration(border: OutlineInputBorder()),
                //             child: DropdownButtonHideUnderline(
                //               child: DropdownButton(
                //
                //                 isExpanded: true,
                //                 value: dropdownstate,
                //                 items: statedata_data
                //                     .map<DropdownMenuItem<String>>((String value) {
                //                   return DropdownMenuItem<String>(
                //                     value: value,
                //                     child: Text(value),
                //                   );
                //                 }).toList(),
                //                 onChanged: (String? newValue) {
                //                   setState(() {
                //                     dropdownstate = newValue!;
                //
                //                     checkState(dropdownstate);
                //
                //
                //                   });
                //                 },
                //                 style: Theme.of(context).textTheme.bodyText1,
                //
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     )):new Container(),























                Padding(
                  padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:10.0,right: 10.0,top:10,bottom: 0):EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 0):EdgeInsets.only(left:18.0,right: 18.0,top:18,bottom: 0),
                  // padding: EdgeInsets.all(15),
                  child: new Theme(data: new ThemeData(
                      hintColor: Colors.white
                  ), child: TextField(
                    controller: emailcontroller,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      hintText: emailhint,
                    ),

                    onChanged: (text) {
                      TemRegData.email=text;

                    },



                  )),
                ),

                // Padding(
                //     padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18),
                //     child:Container(
                //       width: double.infinity,
                //       height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?   60.0:70:90,
                //       decoration: BoxDecoration(
                //         border: Border.all(
                //           color: Colors.white,
                //           // red as border color
                //         ),
                //       ),
                //       child: DropdownButtonHideUnderline(
                //         child: ButtonTheme(
                //           alignedDropdown: true,
                //           child: InputDecorator(
                //             decoration: const InputDecoration(border: OutlineInputBorder()),
                //             child: DropdownButtonHideUnderline(
                //               child: DropdownButton(
                //
                //                 value: languagedropdown,
                //                 items: DataConstants.arrofLanguages
                //                     .map<DropdownMenuItem<String>>((String value) {
                //                   return DropdownMenuItem<String>(
                //                     value: value,
                //                     child: Text(value),
                //                   );
                //                 }).toList(),
                //                 onChanged: (String? item) async{
                //
                //                   String languagedata="";
                //
                //                   if(item.toString().compareTo("తెలుగు")==0)
                //                   {
                //                     languagedata="te";
                //                   }
                //                   else if(item.toString().compareTo("English")==0)
                //                   {
                //                     languagedata="en";
                //                   }
                //
                //                   else if(item.toString().compareTo("मराठी")==0)
                //                   {
                //                     languagedata="mr";
                //                   }
                //                   else if(item.toString().compareTo("தமிழ்")==0)
                //                   {
                //                     languagedata="ta";
                //                   }
                //
                //                   else   if(item.toString().compareTo("ಕನ್ನಡ")==0)
                //                   {
                //                     languagedata="kn";
                //                   }
                //
                //                   else if(item.toString().compareTo("हिंदी")==0)
                //                   {
                //                     languagedata="hi";
                //                   }
                //
                //
                //                   else if(item.toString().compareTo("മലയാളം")==0)
                //                   {
                //                     languagedata="ml";
                //                   }
                //
                //                   else
                //                   {
                //                     languagedata="ar";
                //                   }
                //
                //                   final datacount = await SharedPreferences.getInstance();
                //                   datacount.setString(LanguageSections.lan, languagedata);
                //
                //                   setState(() {
                //                     languagedropdown = item!.toString();
                //
                //                     checkLanguage();
                //                     TemRegData.language=languagedropdown;
                //                   });
                //                 },
                //                 style: Theme.of(context).textTheme.bodyText1,
                //
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     )),


                Padding(
                  padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(18),
                  child :   Container(
                    height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?50:70:90,
                    width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 150:180:250,
                    decoration: BoxDecoration(
                        color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                      onPressed: () {
                        // Navigator.push(
                        //     context, MaterialPageRoute(builder: (_) => Otppage(title: "Otp",)));

                        // TemRegData trmg=new TemRegData(namecontroller.value.text,emailcontroller.value.text
                        // ,dropdownValue,dropdownstate,mobilecontroller.value.text,sponsermobilecontroller.value.text,passwordcontroller.value.text,languagedropdown);


                        updateProfile(emailcontroller.value.text,countrid,stateid,namecontroller.value.text);

                                             },
                      child: Text(
                        submit,
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                    ),
                  ),),




          ]))
        ]));
  }

  getBill() async
  {
    final datacount = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl+DataConstants.getbill+"?timestamp="+date+"&regid="+id),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!

      },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;
    print(response);

    BillEntity invd=BillEntity.fromJson(jsonDecode(response));

    DateTime dt=DateTime.now();

    String dat=dt.day.toString()+"-"+dt.month.toString()+"-"+dt.year.toString();

    if(invd.status==1)
      {

        String bill=invd.data.billnoPrefix+""+invd.data.billNo;

        double d=double.parse(invd.data.amt);

        String sgs=invd.data.sgst;
        String cgs=invd.data.cgst;
        String igs=invd.data.igst;

        String other="0";

        if (countrid.compareTo("1")==0) {
          //layout_actualprice.setVisibility(View.GONE);

          if (stateid.compareTo("12")==0) {


            cgst=double.parse(cgs);
            igst=double.parse(igs);
            sgst=double.parse(sgs);
            double othertax=double.parse(other);



            totalamount=d+cgst+sgst;



          }
          else{

            String bill=invd.data.billnoPrefix+""+invd.data.billNo;

            double d=double.parse(invd.data.amt);



            String other="0";

            cgst=double.parse(cgs);
            igst=double.parse(igs);
            sgst=double.parse(sgs);
            double othertax=double.parse(other);

            totalamount=d+igst;



          }
        }
        else{

          String bill=invd.data.billnoPrefix+""+invd.data.billNo;

          double d=double.parse(invd.data.amt);





          cgst=double.parse(cgs);
          igst=double.parse(igs);
          sgst=double.parse(sgs);
          double othertax=double.parse(other);





          totalamount=d+othertax;

        }



        Navigator.push(
            context, MaterialPageRoute(builder: (_) => InvoicePage(title: "Payment",date:dat,billno:bill,buyer:name+"\n"+email,transactions: invd.data.cashTransactionId,amount: d.roundToDouble().toString(),sgst: sgst.roundToDouble().toString(),cgst: cgst.roundToDouble().toString()
            .toString(),igst: igst.roundToDouble().toString(),amounttopay: totalamount.roundToDouble().toString(),countryid: countrid,stateid: stateid,)));

      }




  }






  checkState(String state)
  {

    for (Statelist sd in statedatalist)
    {

      if(sd.state_name.compareTo(state)==0)
      {

        stateid=sd.id;
        TemRegData.state=state;
        TemRegData.stateid=sd.id;
      }

    }
  }


  getProfile() async {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getUserDetails +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);
    var json = jsonDecode(response);

     Profiledata profile=Profiledata.fromJson(json);

     print(profile.data.full_name);

    TemRegData.name=profile.data.full_name;
    TemRegData.email=profile.data.email_id;

    name=profile.data.full_name;
    email=profile.data.email_id;
    id=profile.data.id;

     namecontroller.text=profile.data.full_name;
     emailcontroller.text=profile.data.email_id;
    mobile=profile.data.mobile;

     setState(() {
       TemRegData.mobilenumber=profile.data.mobile;
       profileimage=profile.data.profile_image;
     });

     countrid=profile.data.country_id;
     stateid=profile.data.state_id;

    // getCountryData(profile.data.country_id,profile.data.state_id);

  }


  updateProfile(String email,String countryid,String stateid,String name) async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.updateProfile +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
        body: <String, String>{
    "name":name,
   "user_email":email,
    "stateid":stateid,
   "language":"English",
    "country_id":countryid,
   "timestamp":date
        }
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    var json = jsonDecode(response);

    if (json['status'] == 1) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Profile updated successfully"),
      ));

      Navigator.of(context).pop();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("failed"),
      ));
    }
  }


  uploadImage(CroppedFile? file) async {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");


    final datacount = await SharedPreferences.getInstance();
    var request = http.MultipartRequest('POST', Uri.parse(DataConstants.baseurl+DataConstants.uploadUserProfile));
    request.headers.addAll( { "Authorization": datacount.getString(DataConstants.userkey)!,
      'Content-Type': 'application/x-www-form-urlencoded'});


    request.files.add(
        http.MultipartFile.fromBytes(
            'file',
            File(file!.path).readAsBytesSync(),
            filename: file.path.split("/").last
        )
    );

    _progressDialog.dismissProgressDialog(context);
    var res = await request.send();
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    getProfile();

    print(responseString);

  }

  checkLanguage() async
  {


    final datacount = await SharedPreferences.getInstance();

    String? language =await LanguageSections.setLanguage();

    String? langdata= datacount.getString(LanguageSections.lan);

    if(langdata!=null)
      {


      }
    else{

      langdata="en";
    }

   if(langdata.compareTo("ar")==0)
    {

      setState(() {
        languagedropdown="اَلْعَرَبِيَّة";
      });


    }

  else  if(langdata.compareTo("te")==0)
    {


      setState(() {
        languagedropdown="తెలుగు";
      });

    }
    else if(langdata.compareTo("en")==0)
    {


      setState(() {
        languagedropdown="English";
      });
    }

    else if(langdata.compareTo("mr")==0)
    {


      setState(() {
        languagedropdown="मराठी";
      });

    }
    else if(langdata.compareTo("ta")==0)
    {


      setState(() {
        languagedropdown="தமிழ்";
      });

    }

    else   if(langdata.compareTo("kn")==0)
    {


      setState(() {
        languagedropdown="ಕನ್ನಡ";
      });
    }

    else if(langdata.compareTo("hi")==0)
    {


      setState(() {
        languagedropdown="हिंदी";
      });
    }


    else if(langdata.compareTo("ml")==0)
    {


      setState(() {
        languagedropdown="മലയാളം";
      });
    }







    updateLanguageValues(langdata);


  }


  updateLanguageValues(String l)async
  {
    String response = await    LanguageSections.getLanguageResponse(l);
    List<XmlElement> ?elements = XmlElement.parseString(
      response,
      returnElementsNamed: ['string'],

    );

    for(XmlElement xe in elements!)
    {


      if(xe.attributes![0].value.toString().compareTo("phonenumber")==0)
        {

          setState(() {
            mobilenumber=xe.text.toString();

          });

        }
      else if(xe.attributes![0].value.toString().compareTo("profile")==0){

        setState(() {
          profile=xe.text.toString();

        });

      }


      else if(xe.attributes![0].value.toString().compareTo("submit")==0){

        setState(() {
          submit=xe.text.toString();

        });

      }

      else if(xe.attributes![0].value.toString().compareTo("name")==0){

        setState(() {
          namehint=xe.text.toString();

        });

      }

      else if(xe.attributes![0].value.toString().compareTo("email")==0){

        setState(() {
          emailhint=xe.text.toString();

        });

      }






    }



  }


}

class MyClipper extends CustomClipper<Rect> {


  @override
  Rect getClip(Size size) {
    return Rect.fromLTWH(0, 0, 160, 160);
  }

  @override
  bool shouldReclip(covariant CustomClipper<Rect> oldClipper) {
    return false;
  }
}
