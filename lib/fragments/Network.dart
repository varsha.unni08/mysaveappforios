import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../design/ResponsiveInfo.dart';
import '../domain/NetworkDashboard.dart';
import '../domain/NetworkSlider.dart';
import '../domain/NetworkSliderData.dart';
import '../domain/SalesInfo.dart';
import '../domain/SalesinfoData.dart';
import '../domain/ShareSlider.dart';
import '../domain/Tokendata.dart';
import '../projectconstants/DataConstants.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import '../views/InvoicePage.dart';

class NetworkPage extends StatefulWidget {
  final String title;

  const NetworkPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NetworkPage();

}

class _NetworkPage extends State<NetworkPage> {


  List<NetworkSlider> nsd = [];

  List<Widget> nsdwidget = [];
  bool isviewVisible = false, isSharelinkvisible = false;

  bool isDashboardvisible = false;

  late NetworkDashboard dashboard;
  String token = "";
  String mobile = "";

  String networkimgurl="";

  List<String> listdata = ["Dashboard", "Share link", "Change position"];

  String countryid="0",stateid="0";

  String name="";
  String email="",phonenumber="";
  double amounttopay=0;

  double d=0;
  double cgst=0,igst=0,sgst=0,othertaxamount=0,totalamount=0;

  @override
  void initState() {
    // TODO: implement initState

    getSettingsValue();
    getProfile(0);
    getSliders();
    super.initState();
  }





  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < nsd.length; i++) {
      nsdwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.89349112,
        width: double.infinity,
        child: Card(
            child:
            Image.network(DataConstants.sliderimageurl + nsd[i].imagepath)),
      ));
    }

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body:     isviewVisible
            ? SingleChildScrollView(
            child: Column(
              children: [
                Container(
                    height: (MediaQuery.of(context).size.width) / 1.89349112,
                    width: double.infinity,
                    child: PageIndicatorContainer(
                        length: nsd.length,
                        align: IndicatorAlign.bottom,
                        indicatorSpace: 14.0,
                        padding: const EdgeInsets.all(10),
                        indicatorColor: Colors.black12,
                        indicatorSelectorColor: Colors.blueGrey,
                        shape: IndicatorShape.circle(size: 10),
                        child: PageView.builder(
                          scrollDirection: Axis.horizontal,
                          // controller: controller,
                          itemBuilder: (BuildContext context, int index) {
                            return getNetWorkWidget(index);
                          },
                          itemCount: nsd.length,
                          // children: nsdwidget,
                        ))),

                SingleChildScrollView(
                  child: Column(
                    children: [
                      Card(
                        elevation: 5,
                        child: new InkWell(
                            onTap: () {
                              setState(() {
                                if (!isDashboardvisible) {
                                  isDashboardvisible = true;
                                } else {
                                  isDashboardvisible = false;
                                }
                              });
                            },
                            child: Container(
                                child: Column(children: [
                                  Container(
                                      width: double.infinity,
                                      child: Padding(
                                          padding:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(8) : EdgeInsets.all(11):EdgeInsets.all(14),
                                          child: Row(
                                              textDirection: TextDirection.rtl,
                                              children: [

                                                Expanded(child: Container(
                                                  child: TextButton(
                                                    onPressed: () {

                                                      setState(() {
                                                        if (!isDashboardvisible) {
                                                          isDashboardvisible = true;
                                                        } else {
                                                          isDashboardvisible = false;
                                                        }
                                                      });
                                                    },
                                                    child: Image.asset(
                                                      'images/dropdownarrow.png',
                                                      width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  15 :20:25,
                                                      height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  15:20:25,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),

                                                ),flex: 1,)

                                                ,

                                                Expanded(child: Container(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.centerLeft,
                                                    child: Center(


                                                          child:TextButton(
                                                            child: Text(listdata[0],
                                                                style: TextStyle(
                                                                    fontSize: 15,
                                                                    color: Colors
                                                                        .black)),
                                                            onPressed: () {


                                                              setState(() {
                                                                if (!isDashboardvisible) {
                                                                  isDashboardvisible = true;
                                                                } else {
                                                                  isDashboardvisible = false;
                                                                }
                                                              });


                                                            },
                                                          ) ,



                                                    ),
                                                  ),

                                                ),flex: 2,)

                                                ,

                                              ]))),
                                  isDashboardvisible
                                      ? Column(children: [
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      //Center Row contents horizontally,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      "Total payment received : 0",
                                                      maxLines: 2,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        ),
                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      "Total matched \n " +
                                                          dashboard.data
                                                              .binary_matched,
                                                      maxLines: 2,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      //Center Row contents horizontally,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      "Carry \nLeft : "+dashboard.data.carry_left+"\nRight : "+dashboard.data.carry_right,
                                                      maxLines: 2,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        ),
                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      "Binary \nLeft : "+dashboard.data.binary_left+"\nRight : "+dashboard.data.binary_right,
                                                      maxLines: 2,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      //Center Row contents horizontally,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      dashboard.data.binary_left+"\nLeft",
                                                      maxLines: 2,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        ),

                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      dashboard.data.binary_right+"\nRight",
                                                      maxLines: 2,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      //Center Row contents horizontally,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      "Binary : "+dashboard.data.binary_amt+"\nReferal : "+dashboard.data.referral_commission_amt+"",
                                                      maxLines: 2,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        ),

                                        Card(
                                          child: Container(
                                              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                              child: Center(
                                                  child: Align(
                                                    alignment:
                                                    Alignment.center,
                                                    child: Text(
                                                      "Level : "+dashboard.data.level_amt+"\nAcheivement bonus : "+dashboard.data.achievement_amt,
                                                      maxLines: 3,
                                                      textAlign:
                                                      TextAlign.center,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13:15:17),
                                                    ),
                                                  ))),
                                          color: Color(0xffd1efe9),
                                        )
                                      ],
                                    )
                                  ])
                                      : Container()
                                ]))),
                      ),
                      Card(
                          elevation: 5,
                          child: new InkWell(
                              onTap: () {
                                setState(() {
                                  if (isSharelinkvisible) {
                                    isSharelinkvisible = false;
                                  } else {
                                    isSharelinkvisible = true;
                                  }
                                });
                              },
                              child: Container(
                                  width: double.infinity,
                                  child: Container(


                                    // child :Container(
                                    //     width: double.infinity,
                                      child: Padding(
                                          padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?EdgeInsets.all(8):EdgeInsets.all(11):EdgeInsets.all(14),
                                          child: Container(
                                              child: Column(
                                                children: [
                                                  Row(
                                                      textDirection:
                                                      TextDirection.rtl,
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [

                                                        Expanded(child: Container(
                                                          child: TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                if (isSharelinkvisible) {
                                                                  isSharelinkvisible =
                                                                  false;
                                                                } else {
                                                                  isSharelinkvisible =
                                                                  true;
                                                                }
                                                              });
                                                            },
                                                            child: Image.asset(
                                                              'images/dropdownarrow.png',
                                                              width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?15:20:25,
                                                              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 15:20:25,
                                                              fit: BoxFit.cover,
                                                            ),
                                                          ),

                                                        ),flex: 1,),

                                                        Expanded(child: Container(
                                                          child: Align(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: Center(
                                                                child: Container(
                                                                  child:TextButton(
                                                                    onPressed:
                                                                        () {
                                                                      setState(
                                                                              () {
                                                                            if (isSharelinkvisible) {
                                                                              isSharelinkvisible =
                                                                              false;
                                                                            } else {
                                                                              isSharelinkvisible =
                                                                              true;
                                                                            }
                                                                          });
                                                                    },



                                                                    child:  Text(
                                                                        listdata[
                                                                        1],
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                            ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 15:17:18,
                                                                            color:
                                                                            Colors.black))

                                                                    ,
                                                                  )



                                                                )),
                                                          ),

                                                        ),flex: 2,)



                                                      ]),
                                                  isSharelinkvisible
                                                      ? Padding(
                                                      padding:
                                                      EdgeInsets.all(
                                                          10),
                                                      child: Container(
                                                          width: double
                                                              .infinity,
                                                          // height:100,

                                                          child: Column(
                                                            children: [
                                                              InkWell(
                                                                child: Text(
                                                                    DataConstants.domain +
                                                                        "index.php/web/signup?sponserid=" +
                                                                        token,
                                                                    maxLines:
                                                                    1,
                                                                    style: new TextStyle(
                                                                        color:
                                                                        Colors.blue,
                                                                        decoration: TextDecoration.underline)),
                                                                onTap: () =>
                                                                {
                                                                  // launch(
                                                                  //     DataConstants
                                                                  //             .baseurl +
                                                                  //         "index.php/web/signup?sponserid=" +
                                                                  //         token,
                                                                  //     forceWebView:
                                                                  //         true)
                                                                },
                                                              ),
                                                              Container(
                                                                  width:
    ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 120 :140:160,
                                                                  height:
                                                                  ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  50 :60:70,
                                                                  color: Colors
                                                                      .blueGrey,
                                                                  margin: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets
                                                                      .all(
                                                                      10) :EdgeInsets
                                                                      .all(
                                                                      15):EdgeInsets
                                                                      .all(
                                                                      20),
                                                                  child:
                                                                  TextButton(
                                                                    child:
                                                                    Text(
                                                                      'Share',
                                                                      style:
                                                                      TextStyle(
                                                                        fontSize:
                                                                        ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?  13.0:15:17,
                                                                        color:
                                                                        Colors.white,
                                                                      ),
                                                                    ),
                                                                    onPressed:
                                                                        () {
                                                                      getNetWorkData(
                                                                          mobile,
                                                                          1);
                                                                    },
                                                                  ))
                                                            ],
                                                          )))
                                                      : Container()
                                                ],
                                              )))

                                    // )

                                  )))),
                      Card(
                        elevation: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 5:8:11,
                        child: new InkWell(
                            onTap: () {
                              showSingleOptiondialog(dashboard);
                            },
                            child: Container(
                                width: double.infinity,
                                child: Padding(
                                    padding:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(8) : EdgeInsets.all(11):EdgeInsets.all(14),
                                    child: Row(
                                        textDirection: TextDirection.rtl,
                                        children: [

                                          Expanded(child: Container(
                                            child: TextButton(
                                              onPressed: () {
                                                showSingleOptiondialog(
                                                    dashboard);
                                              },
                                              child: Image.asset(
                                                'images/dropdownarrow.png',
                                                width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 15:20:25,
                                                height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 15:20:25,
                                                fit: BoxFit.cover,
                                              ),
                                            ),

                                          ),flex: 1,),

                                          Expanded(child: Container(
                                            child: Align(
                                              alignment:
                                              Alignment.centerLeft,
                                              child: Center(
                                                  child: Container(
                                                      child: TextButton(
                                                        onPressed: () {
                                                          showSingleOptiondialog(
                                                              dashboard);
                                                        },
                                                        child: TextButton(onPressed: () {  showSingleOptiondialog(
                                                            dashboard); },
                                                          child:     Text(listdata[2],
                                                              style: TextStyle(
                                                                  fontSize:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 13:15:17,
                                                                  color:
                                                                  Colors.black)),

                                                        )



                                                        ,
                                                      ))),
                                            ),

                                          ),flex: 2,)


                                        ])))),
                      ),
                    ],
                  ),
                )


              ],
            )):
        Stack(children: [


          Container(
            width: (MediaQuery.of(context).size.width),
            height: (MediaQuery.of(context).size.height),
            child: Image.network(networkimgurl,errorBuilder:  (context, error, stackTrace) {
              return Container(

                alignment: Alignment.center,
                child: Text("Loading.....",style: TextStyle(fontSize: 17),),
              );
            },
              width: (MediaQuery.of(context).size.width),
              height: (MediaQuery.of(context).size.height),
              fit: BoxFit.fill,),


          ),

          Align(
            alignment: Alignment.bottomCenter,
            child:   Container(
              height: 50,
              width: 150,
              decoration: BoxDecoration(
                  color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
              child: TextButton(
                onPressed: () {


                  getProfile(1);


                },
                child: Text(
                  'Purchase',
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              ),
            ),
          )






        ],));
  }

  getSliders() async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getSliderImages +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;
    NetworkSliderData data = NetworkSliderData.fromJson(jsonDecode(response));
    setState(() {
      nsd.clear();

      nsd.addAll(data.data);
    });
  }

  getProfile(int code) async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getUserDetails +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;

    token = datacount.getString(DataConstants.userkey)!;

    print(response);
    var json = jsonDecode(response);

    // Profiledata profile=Profiledata.fromJson(json);

    if (json['status'] == 1) {
      var data = json['data'];
      //var jsondata=jsonDecode(data);

      mobile = data['mobile'];
      countryid=data['country_id'];
      stateid=data['state_id'];
      name=data['full_name'];
      email=data['email_id'];

      if(code==0)
      {
        getNetWorkData(mobile, 0);
      }
      else{


        startPayment();


      }



    } else {
      print("Something wrong");
    }
  }



  startPayment() async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl + DataConstants.getSettingsValue+"?timestamp="+date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!,

      },
    );
    String response = dataasync.body;

    print(response);


    var json = jsonDecode(response);

    if (json['status'] == 1) {

      var js=json['data'];

      String networkimage=js['network_image'];
      String basicamount=js['one_bv_required_amount'];
      d=double.parse(basicamount);


      // cgst=double.parse(js['cgst']);
      //   igst=double.parse(js['igst']);
      //   sgst=double.parse(js['sgst']);
      //   othertaxamount=double.parse(js['other_tax']);


      if (countryid.compareTo("1")==0) {
        //layout_actualprice.setVisibility(View.GONE);

        if (stateid.compareTo("12")==0) {


          double cg=double.parse(js['cgst']);
          double  ig=double.parse(js['igst']);
          double  sg=double.parse(js['sgst']);
          double othertax=double.parse(js['other_tax']);

          cgst=d*(cg/100);
          igst=d*(ig/100);
          sgst=d*(sg/100);

          totalamount=d+cgst+sgst;



        }
        else{

          double cg=double.parse(js['cgst']);
          double  ig=double.parse(js['igst']);
          double  sg=double.parse(js['sgst']);
          double othertax=double.parse(js['other_tax']);

          cgst=d*(cg/100);
          igst=d*(ig/100);
          sgst=d*(sg/100);

          totalamount=d+igst;



        }
      }
      else{

        double cg=double.parse(js['cgst']);
        double  ig=double.parse(js['igst']);
        double  sg=double.parse(js['sgst']);
        double othertax=double.parse(js['other_tax']);

        cgst=d*(cg/100);
        igst=d*(ig/100);
        sgst=d*(sg/100);
        double ot=d*(othertax/100);

        totalamount=d+ot;

      }






      Dialog sharedialog = Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          //this right here
          child: Container(
              height: (MediaQuery.of(context).size.width),
              width:(MediaQuery.of(context).size.width)/1.9,
              child:SingleChildScrollView(
                child: Column(
                  children: [

                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Center(child:Text("Payment Confirmation"))
                    ),

                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Actual price  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              d.toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")==0)?     Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Sgst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              sgst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")==0)? Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Cgst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              cgst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")!=0)  ? Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Igst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              igst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Total price  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              totalamount.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),

                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Center(child:TextButton(onPressed: () {


                          amounttopay=totalamount;


                          Navigator.pop(context);

                          DateTime t=DateTime.now();
                          String orderstring=t.millisecond.toString();

                          getDataFromPayment(orderstring);







                        }, child: Text("Confirm"),))
                    ),


                  ],
                ),)
          ));

      showDialog(
          context: context, builder: (BuildContext context) => sharedialog);
















    } else {

    }
  }



  getDataFromPayment(String orderstring) async
  {

    showGeneralDialog(
      barrierLabel: "Label",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return Align(
          alignment: true ? Alignment.topCenter : Alignment.bottomCenter,
          child: Container(
            height: (MediaQuery.of(context).size.height)/1.5,
            width: (MediaQuery.of(context).size.width),
            child: SingleChildScrollView(


                child: Column(

                  children: [


                    Container(child: Padding(
                      padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                      //padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Text("For activation of SAVE App transfer the bill amount of Rs. 2950/- to the bank given below and WhatsApp transaction details and ID/registered mobile number to 9946109501",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),),
                    )),

                    Divider(color: Colors.black38,
                      thickness: 1,),

                    Container(child: Padding(
                      padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                      //padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Text("Century Gate Software Solutions Pvt Ltd\n Canara Bank \nAccount number : 45501400000018 \nMICR: 680015901 \nIFSC: CNRB0014550 \n",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),),

                    )),

                    Container(child: Padding(
                      padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                      //padding: EdgeInsets.symmetric(horizontal: 15),
                      child:TextButton(onPressed: () {

                        Navigator.pop(context);

                      }, child: Text("Ok",style: TextStyle( fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.blue),),



                      ) ,
                    )),



                  ],


                )),
            margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        );
      },
      transitionBuilder: (context, anim1, anim2, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, true ? -1 : 1), end: Offset(0, 0)).animate(anim1),
          child: child,
        );
      },
    );








//     ProgressDialog _progressDialog = ProgressDialog();
//     _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
//
//     var date = new DateTime.now().millisecond;
//
//     String orderid=date.toString();
//
//     var dataasync = await http.post(
//         Uri.parse("https://mysaving.in/IntegraAccount/"+"paytmapi/initTransaction.php?timestamp="+orderid),
//
//         headers: <String, String>{
//           'Content-Type': 'application/x-www-form-urlencoded',
//
//         }, body: <String, String>{
//       'Content-Type': 'application/x-www-form-urlencoded',
//       'requestType':'Payment',
//       'mid':DataConstants.TestMerchantID,
//       'websiteName':DataConstants.Website,
//       'orderId':orderid,
//       'callbackUrl':DataConstants.callbackUrl,
//       'value':amounttopay.roundToDouble().toString(),
//       'custId':'cust_001',
//       'inittrans':DataConstants.initiatetransactionurl,
//       'merchantkey':DataConstants.TestMerchantKey,
//       'timestamp':orderid
//     }
//
//     );
//
//     _progressDialog.dismissProgressDialog(context);
//
//
//     String response=dataasync.body;
//     var js=jsonDecode(response);
//     Tokendata td=Tokendata.fromJson(js);
//
//     if(td.body!=null)
//     {
//
//       String host = "https://securegw.paytm.in/";
//
// //        String orderDetails = "MID: " + Utils.PaytmCredentials.TestMerchantID + ", OrderId: " + "ORDERID_98765" + ", TxnToken: " + txnTokenString
// //                + ", Amount: " + "1.00";
//
//       //Log.e(TAG, "order details "+ orderDetails);
//
//       String callBackUrl = host + "theia/paytmCallback?ORDER_ID="+orderid;
//
//       print("Transaction id : "+td.body.txnToken);
//
//       //       var response = AllInOneSdk.startTransaction(
//       //           DataConstants.TestMerchantID, orderid, amounttopay.roundToDouble().toString(), td.body.txnToken, callBackUrl, true, true);
//       //       response.then((value) {
//       //         print(value);
//       //
//       //         String  result = value.toString();
//       //
//       // dynamic m=value;
//       //
//       // Map<dynamic, dynamic> ab1 =m ;
//       //
//       //         print("Result Data is : "+result);
//       //
//       //        // {CURRENCY: INR, GATEWAYNAME: SBI, RESPMSG: Txn Success, BANKNAME: State Bank of India, PAYMENTMODE: NB, MID: eAoUUb21380278750445, RESPCODE: 01, TXNAMOUNT: 2950.00, TXNID: 20220113111212800110168076403338727, ORDERID: 784, STATUS: TXN_SUCCESS, BANKTXNID: 13090179464, TXNDATE: 2022-01-13 15:21:18.0, CHECKSUMHASH: 69NGywshzGCJ02+Pmx1em2UGab4eEyQQGveu9oeBOOXqVk6LR4lq+DyjU9ctwZBC+uFictNSEcexbtCybxtlgBpQB4Ku8QQ2Pn9cqQM4ZJM=}
//       //
//       //        // var js=jsonDecode(result);
//       //
//       //         //String transactionid=js['TXNID'];
//       //
//       //
//       //         purchaseApp(ab1['TXNID']);
//       //
//       //
//       //
//       //
//       //
//       //
//       //
//       //
//       //       }).catchError((onError) {
//       //         if (onError is PlatformException) {
//       //           setState(() {
//       //             String  result = onError.message.toString() + " \n  " + onError.details.toString();
//       //
//       //             print("Result Data is Error : "+result);
//       //           });
//       //         } else {
//       //           setState(() {
//       //             String  result = onError.toString();
//       //
//       //             print("Result Data is Error : "+result);
//       //           });
//       //         }
//       //       });
//
//
//     }
//
//
//
//     _progressDialog.dismissProgressDialog(context);



  }


  purchaseApp(String transactionid) async
  {

    print("transaction id data : "+transactionid);

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

    var date = new DateTime.now().millisecond;
    final datacount = await SharedPreferences.getInstance();
    String orderid=date.toString();

    var dataasync = await http.post(
        Uri.parse(DataConstants.baseurl+DataConstants.addSalesInfo),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!

        }, body: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      'cash_transaction_id':transactionid,
      'timestamp':orderid,
    }

    );

    _progressDialog.dismissProgressDialog(context);


    String response=dataasync.body;

    print("Add sales info : "+response);
    dynamic json = jsonDecode(response);


    Salesinfo salesinfo=Salesinfo.fromJson(json);

    if(salesinfo.status==1)
    {

      // var jsonObject_data=json["data"];
      // var json1 = jsonDecode(jsonObject_data.toString());


      // print("Bill no : "+billno+" , "+"billprefix : "+billprefix);

      SalesinfoData data=salesinfo.data;
      String billno=data.bill_no;
      String billprefix=data.billno_prefix;

      String bill=billprefix+""+billno;

      DateTime dt=DateTime.now();

      String date=dt.day.toString()+"-"+dt.month.toString()+"-"+dt.year.toString();


      // const InvoicePage(
      //     {Key? key, required this.title, required this.date,required this.billno,required  this.buyer,required this.transactions, required this.amount,required this.sgst,required this.cgst, required this.igst,required this.amounttopay})
      // : super(key: key);

      Navigator.push(
          context, MaterialPageRoute(builder: (_) => InvoicePage(title: "Payment",date:date,billno:bill,buyer:name+"\n"+email,transactions: transactionid,amount: d.toString(),sgst: sgst.toString(),cgst: cgst
          .toString(),igst: igst.toString(),amounttopay: amounttopay.toString(),countryid: countryid,stateid: stateid,)));




      getProfile(0);


    }
    else{

      print("Error");

    }




  }

  getNetWorkData(String mobilenumber, int code) async {
    print("mobile : " + mobilenumber);
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.showMemberDetails +
          "?timestamp=" +
          date.toString() +
          "&mobile=" +
          mobilenumber),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;

    var json = jsonDecode(response);

    NetworkDashboard ndashboard = NetworkDashboard.fromJson(json);

    if (ndashboard.status == 1) {
      if (code == 0) {
        //print(response);

        setState(() {
          isviewVisible = true;
          dashboard = ndashboard;
        });
      } else {
        shareLink();
      }
    }
  }

  shareLink() async {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,
        textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getSettingsSlider +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    ShareSlider shareSlider = ShareSlider.fromJson(jsonDecode(response));

    if (shareSlider.status == 1) {
      showShareDialog(shareSlider);
    }

    print(response);
  }

  showShareDialog(ShareSlider shareSlider) {
    List<Widget> sliderwidget = [];

    PageController pageController = new PageController();

    for (var i = 0; i < shareSlider.data.length; i++) {
      sliderwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.89349112,
        width: double.infinity,
        child: Card(
            child: Image.network(
                DataConstants.sliderimageurl + shareSlider.data[i].image)),
      ));
    }

    Dialog sharedialog = Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        //this right here
        child: Container(
            height: double.infinity,
            width: double.infinity,
            child:SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                      width: 220,
                      height: 220 / 0.609375,
                      child: PageIndicatorContainer(
                          length: shareSlider.data.length,
                          align: IndicatorAlign.bottom,
                          indicatorSpace: 14.0,
                          padding: const EdgeInsets.all(10),
                          indicatorColor: Colors.black12,
                          indicatorSelectorColor: Colors.blueGrey,
                          shape: IndicatorShape.circle(size: 10),
                          child: PageView.builder(
                            scrollDirection: Axis.horizontal,
                            controller: pageController,
                            itemBuilder: (BuildContext context, int index) {
                              return sliderwidget[index];
                            },
                            itemCount: shareSlider.data.length,
                            // children: nsdwidget,
                          ))),
                  Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(5),
                          child: Container(
                              color: Colors.blueGrey,
                              child: TextButton(
                                onPressed: () {
                                  Clipboard.setData(ClipboardData(
                                      text: DataConstants.baseurl +
                                          "index.php/web/signup?sponserid=" +
                                          token));

                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text("Copied to clipboard"),
                                  ));
                                },
                                child: Text(
                                  "Copy link",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ))),
                      SizedBox(width: 170,child:Container(
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: new Theme(
                                  data: new ThemeData(hintColor: Colors.white),
                                  child: Text(
                                    DataConstants.baseurl +
                                        "index.php/web/signup?sponserid=" +
                                        token,
                                    maxLines: 2,
                                    style: TextStyle(color: Colors.blue),
                                  )))))
                    ],
                  ),
                  Container(
                      color: Colors.blueGrey,
                      child: TextButton(
                        onPressed: () async {
                          var status =
                          await Permission.manageExternalStorage.status;

                            print("permission granted");

                            var date = new DateTime.now().toIso8601String();

                            String url = DataConstants.sliderimageurl +
                                shareSlider.data[pageController.page!.toInt()]
                                    .image; // <-- 1
                            var response = await http.get(Uri.parse(url)); // <--2
                            var documentDirectory =
                            await getApplicationDocumentsDirectory();
                            var firstPath = documentDirectory.path + "/images";
                            var filePathAndName = documentDirectory.path +
                                '/images/' +
                                date.toString() +
                                ".jpg";

                            await Directory(firstPath)
                                .create(recursive: true); // <-- 1
                            File file2 = new File(filePathAndName); // <-- 2
                            file2.writeAsBytesSync(response.bodyBytes);

                            bool fileExists = await file2.exists();

                            if (fileExists) {
                              print("file exists : " + file2.path);

                              //  Share.share('check out my website https://example.com');

                              Share.shareFiles([file2.path],
                                  text: shareSlider
                                      .data[pageController.page!.toInt()]
                                      .description);
                            }

                        },
                        child: Text(
                          "Share",
                          style: TextStyle(color: Colors.white),
                        ),
                      ))
                ],
              ),)
        ));

    showDialog(
        context: context, builder: (BuildContext context) => sharedialog);
  }

  Widget getNetWorkWidget(int p) {
    return nsdwidget[p];
  }

  showSingleOptiondialog(NetworkDashboard ds) {
    List<String> positions = ["auto", "zigzag", "left", "right"];

//String autoposition=

    int checkedposition = 0;

    for (int i = 0; i < positions.length; i++) {
      if (ds.data.position_downline_setup_default.compareTo(positions[i]) ==
          0) {
        checkedposition = i;
        break;
      }
    }

    AlertDialog dialog = AlertDialog(
        title: Text("Change Position"),
        content: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: positions
                  .map((e) => RadioListTile(
                title: Text(e),
                value: e,
                groupValue: positions[checkedposition],
                selected: positions[checkedposition] == e,
                onChanged: (value) {
                  // if (value != _singleNotifier.currentCountry) {
                  //   // _singleNotifier.updateCountry(value);

                  print(value);

                  if (value.toString().compareTo("left") == 0 ||
                      value.toString().compareTo("right") == 0) {
                    // networkFragment.updatePosition(item,Integer.parseInt(network.getId()));
                    // networkFragment.updatePositionNext(item,Integer.parseInt(network.getId()));

                    updatePosition(
                        value.toString(), int.parse(dashboard.data.id));
                    updatePositionNext(
                        value.toString(), int.parse(dashboard.data.id));
                    dashboard.data.position_downline_setup_default =
                        value.toString();
                    dashboard.data.position_next = value.toString();
                  } else {
                    dashboard.data.position_downline_setup_default =
                        value.toString();
                    // networkFragment.updatePosition(item,Integer.parseInt(network.getId()));

                    updatePosition(
                        value.toString(), int.parse(dashboard.data.id));
                  }

                  Navigator.of(context).pop();
                  // }
                },
              ))
                  .toList(),
            ),
          ),
        ));
    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  Future<String> getLink() async {
    final datacount = await SharedPreferences.getInstance();
    String link = DataConstants.baseurl +
        "index.php/web/signup?sponserid=" +
        datacount.getString(DataConstants.userkey)!;

    return link;
  }

  updatePosition(String position, int id) async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
        Uri.parse(DataConstants.baseurl + DataConstants.updatePosition),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!,
          "position": position,
          "id": id.toString()
        },
        body: <String, String>{
          "position": position,
          "id": id.toString()
        });
    String response = dataasync.body;

    var json = jsonDecode(response);

    if (json['status'] == 1) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Position updated successfully"),
      ));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("failed"),
      ));
    }
  }

  updatePositionNext(String position, int id) async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
        Uri.parse(DataConstants.baseurl + DataConstants.updatePositionNext),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!,
          "position": position,
          "id": id.toString()
        },
        body: <String, String>{
          "position": position,
          "id": id.toString()
        });
    String response = dataasync.body;

    var json = jsonDecode(response);

    if (json['status'] == 1) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Position updated successfully"),
      ));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("failed"),
      ));
    }
  }



  getSettingsValue() async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl + DataConstants.getSettingsValue+"?timestamp="+date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!,

      },
    );
    String response = dataasync.body;

    print(response);

    var json = jsonDecode(response);

    if (json['status'] == 1) {

      var js=json['data'];

      String networkimage=js['network_image'];
      String url=DataConstants.linkimg+networkimage;


      setState(() {

        networkimgurl=url;
      });


    } else {

    }
  }
}