import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/views/PasswordManager.dart';

import '../design/ResponsiveInfo.dart';
import '../projectconstants/LanguageSections.dart';
import '../views/AccountSetup.dart';
import '../views/AssetList.dart';
import '../views/Bankvoucher.dart';
import '../views/Billvoucher.dart';
import '../views/BudgetScreen.dart';
import '../views/Cashbankstatement.dart';

import '../views/DocumentManager.dart';
import '../views/FlChartPage.dart';
import '../views/InsuranceList.dart';
import '../views/Investmentlist.dart';
import '../views/Liability.dart';
import '../views/MyDiary.dart';
import '../views/Payment.dart';
import '../views/Recipt.dart';
import '../views/TaskListPage.dart';
import '../views/Visitingcard.dart';
import '../views/Wallet.dart';
import '../projectconstants/LanguageSections.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml_parser/xml_parser.dart';
import 'package:page_indicator/page_indicator.dart';

import '../views/journalvoucher.dart';
import '../views/my_dream.dart';


class HomePage extends StatefulWidget {
  final String title;

  const HomePage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePage();

}



class _HomePage extends State<HomePage> with WidgetsBindingObserver {


  String viewchart="",payment="",recipt="",billing="",wallet="",cashbanck="",task="",asset="",liability="",insurance="",investment="",mydiary="",budget="";


  List<Widget> nsdwidget = [];

  List<String> imagesliderData = ["images/img1.jpg","images/img2.jpg","images/img3.jpg","images/img3.jpg"];

  @override
  void initState() {
    // TODO: implement initState

    //WidgetsBinding.instance?.addObserver(this);
    checkLanguage();


    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    WidgetsBinding.instance.addPostFrameCallback((_) => _animateSlider());
  }


  final PageController _controller = PageController();

  @override
  void dispose() {
    // TODO: implement dispose
 //   WidgetsBinding.instance?.removeObserver(this);

    super.dispose();
  }


  void _animateSlider() {
    Future.delayed(Duration(seconds: 2)).then((_) {
      int nextPage = _controller.page!.round() + 1;

      if (nextPage == imagesliderData.length) {
        nextPage = 0;
      }

      _controller
          .animateToPage(nextPage, duration: Duration(seconds: 1), curve: Curves.linear)
          .then((_) => _animateSlider());
    });
  }



  @override
  Widget build(BuildContext context) {

    for (var i = 0; i < imagesliderData.length; i++) {
      nsdwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.89349112,
        width: double.infinity,
        child: Card(
            child:
            Image.asset(imagesliderData[i],width:MediaQuery.of(context).size.width ,height: (MediaQuery.of(context).size.width)/1.89349112,fit: BoxFit.fill,),
      )));
    }




    return Scaffold(

      body: SingleChildScrollView(

        child:    Column(

          children: [


            Stack(

              children: [

                Align(
                  alignment: FractionalOffset.topCenter,

                  child: Padding(padding: EdgeInsets.all(5),

                    child:   Container(
                        height: (MediaQuery.of(context).size.width) / 1.89349112,
                        width: double.infinity,
                        child: PageIndicatorContainer(
                            length: imagesliderData.length,
                            align: IndicatorAlign.bottom,

                            indicatorSpace: 14.0,
                            padding: const EdgeInsets.all(10),
                            indicatorColor: Colors.black12,
                            indicatorSelectorColor: Colors.blueGrey,
                            shape: IndicatorShape.circle(size: 10),
                            child: PageView.builder(
                              scrollDirection: Axis.horizontal,
                              controller: _controller,
                              itemBuilder: (BuildContext context, int index) {
                                return getNetWorkWidget(index);
                              },
                              itemCount: imagesliderData.length,
                              // children: nsdwidget,
                            ))),


                  ),


                ),



                Align(

                    alignment: FractionalOffset.topCenter,
                    child: Padding(padding: EdgeInsets.only(top:(MediaQuery.of(context).size.width) / 1.79349112 ),

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,

                        children: [

                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                          child: Text(
                            "My Money",
                            style: TextStyle(
                                fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13 : 16:19, color: Colors.black,fontWeight: FontWeight.bold),
                          ),
                          ),


                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                            child: Card(


                              child:   GridView.count(
                                primary: false,
                                shrinkWrap: true,
                                padding: const EdgeInsets.all(3),
                                crossAxisSpacing: 5,
                                mainAxisSpacing: 3,
                                crossAxisCount: 3,
                                children: <Widget>[

                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                  child:  GestureDetector(

                                    child:     Column(
                                      children: [

                                        Center(
                                            child: Image.asset(
                                              'images/card.png',
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                              fit: BoxFit.cover,
                                            )),
                                        Container(

                                          child: Center(
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.push(
                                                      context, MaterialPageRoute(builder: (_) => PaymentPage(title: "Payment",)));


                                                },
                                                child: Text("Payments",textAlign: TextAlign.center,
                                                    style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                              )),
                                        )
                                      ],
                                    ),
                                    onTap: (){

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) => PaymentPage(title: "Payment",)));

                                    },
                                  )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/recipt.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => ReciptPage(title: "Payment",)));


                                                    },
                                                    child: Text("Receipts",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){

                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => ReciptPage(title: "Payment",)));

                                        },
                                      )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/wallet.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => WalletPage(title: "Payment",)));


                                                    },
                                                    child: Text("Wallet",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){

                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => WalletPage(title: "Payment",)));

                                        },
                                      )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/budget.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => Budgetpage(title: "Payment",)));


                                                    },
                                                    child: Text("Budget",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){
                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => Budgetpage(title: "Payment",)));


                                        },
                                      )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/bank.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => BankVoucherPage(title: "Payment",)));


                                                    },
                                                    child: Text("Bank",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){

                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => BankVoucherPage(title: "Payment",)));

                                        },
                                      )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/journal.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => JournalVoucherPage(title: "journal voucher")));



                                                    },
                                                    child: Text("Journal",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){

                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => JournalVoucherPage(title: "journal voucher")));


                                        },
                                      )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/invoice.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => BillPage(title: "Recipt",)));


                                                    },
                                                    child: Text("Billing",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){


                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => BillPage(title: "Recipt",)));

                                        },
                                      )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/dollar.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => CashbankstatementPage(title: "Recipt",)));

                                                    },
                                                    child: Text("Cash and Bank",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){


                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => CashbankstatementPage(title: "Recipt",)));
                                        },
                                      )



                                  ),
                                  Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                      child:  GestureDetector(

                                        child:     Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [

                                            Center(
                                                child: Image.asset(
                                                  'images/accounting.png',
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                  fit: BoxFit.cover,
                                                )),
                                            Container(

                                              child: Center(
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context, MaterialPageRoute(builder: (_) => AccountSettinglistpage(title: "Payment",accountType: "",)));



                                                    },
                                                    child: Text("Account Setup",textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                  )),
                                            )
                                          ],
                                        ),
                                        onTap: (){

                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) => AccountSettinglistpage(title: "Payment",accountType: "",)));


                                        },
                                      )



                                  ),





                                ]),


                            ),
                          ),


                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                            child: Text(
                              "My Belongings",
                              style: TextStyle(
                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13 : 16:19, color: Colors.black,fontWeight: FontWeight.bold),
                            ),
                          ),


                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                            child: Card(


                              child:   GridView.count(
                                  primary: false,
                                  shrinkWrap: true,
                                  padding: const EdgeInsets.all(3),
                                  crossAxisSpacing: 5,
                                  mainAxisSpacing: 3,
                                  crossAxisCount: 3,
                                  children: <Widget>[

                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/assets.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => AssetListPage(title: "Payment",)));


                                                      },
                                                      child: Text("Asset",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => AssetListPage(title: "Payment",)));

                                          },
                                        )



                                    ),
                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/recipt.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => LiabilityPage(title: "Payment",)));


                                                      },
                                                      child: Text("Liability",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => LiabilityPage(title: "Payment",)));

                                          },
                                        )



                                    ),
                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/lic.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => InsuranceListPage(title: "Payment",)));


                                                      },
                                                      child: Text("Insurance",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => InsuranceListPage(title: "Payment",)));

                                          },
                                        )



                                    ),
                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/profits.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => InvestmentListPage(title: "Payment",)));


                                                      },
                                                      child: Text("Investment",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){
                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => InvestmentListPage(title: "Payment",)));


                                          },
                                        )



                                    ),
                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/passcode.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => PasswordManagerpage(title: "Payment",)));


                                                      },
                                                      child: Text("Password Manager",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => PasswordManagerpage(title: "Payment",)));

                                          },
                                        )



                                    ),
                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/foldermanage.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => DocumentManagerPage(title: "passwordmanager")));




                                                      },
                                                      child: Text("Dcument Manager",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => DocumentManagerPage(title: "passwordmanager")));



                                          },
                                        )



                                    ),






                                  ]),


                            ),
                          ),



                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                            child: Text(
                              "My Life",
                              style: TextStyle(
                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13 : 16:19, color: Colors.black,fontWeight: FontWeight.bold),
                            ),
                          ),


                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                            child: Card(


                              child:   GridView.count(
                                  primary: false,
                                  shrinkWrap: true,
                                  padding: const EdgeInsets.all(3),
                                  crossAxisSpacing: 5,
                                  mainAxisSpacing: 3,
                                  crossAxisCount: 3,
                                  children: <Widget>[

                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/tasklist.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => TaskListPage(title: "Payment",)));


                                                      },
                                                      child: Text("Task",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => TaskListPage(title: "Payment",)));

                                          },
                                        )



                                    ),
                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/journal.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => MyDiarypage(title: "Payment",)));


                                                      },
                                                      child: Text("Diary",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => MyDiarypage(title: "Payment",)));

                                          },
                                        )



                                    ),
                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/aim.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => MyDream()));


                                                      },
                                                      child: Text("My Dream",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => MyDream()));

                                          },
                                        )



                                    ),







                                  ]),


                            ),
                          ),

                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                            child: Text(
                              "Utilities",
                              style: TextStyle(
                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13 : 16:19, color: Colors.black,fontWeight: FontWeight.bold),
                            ),
                          ),


                          Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?5:10:15),
                            child: Card(


                              child:   GridView.count(
                                  primary: false,
                                  shrinkWrap: true,
                                  padding: const EdgeInsets.all(3),
                                  crossAxisSpacing: 5,
                                  mainAxisSpacing: 3,
                                  crossAxisCount: 3,
                                  children: <Widget>[

                                    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?3:8:13),

                                        child:  GestureDetector(

                                          child:     Column(
                                            children: [

                                              Center(
                                                  child: Image.asset(
                                                    'images/idcard.png',
                                                    width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 30:35:45,
                                                    fit: BoxFit.cover,
                                                  )),
                                              Container(

                                                child: Center(
                                                    child: TextButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) => Visitcardpage(title: "Payment",)));


                                                      },
                                                      child: Text("Visiting Card",textAlign: TextAlign.center,
                                                          style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16)),
                                                    )),
                                              )
                                            ],
                                          ),
                                          onTap: (){

                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) => Visitcardpage(title: "Payment",)));

                                          },
                                        )



                                    ),








                                  ]),


                            ),
                          ),









                        ],

                      ) ,


                    )



                )
              ],
            )




              ,

               Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Image.asset(
                          "images/dropdownarrow.png",
                          width:ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? 10:12:14,
                          height: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? 10:12:14,
                        ),
                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () async {
                              // showAccountsDialog();
                              // Navigator.push(
                              //     context, MaterialPageRoute(builder: (_) => ChartPage(title: "passwordmanager")));

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) => FChartPage(title: "passwordmanager")));


                            },
                            child: Center(
                                child: Text(viewchart,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black38,
                                        fontSize: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)?12:14:16))),
                          ))
                    ],
                  ),
                )),



          ],

        ),
      )







    );
  }
  Widget getNetWorkWidget(int p) {
    return nsdwidget[p];
  }

  checkLanguage() async
  {


    final datacount = await SharedPreferences.getInstance();

    String? language =await LanguageSections.setLanguage();

    String? langdata= datacount.getString(LanguageSections.lan);

    if(langdata!=null)
    {


    }
    else{

      langdata="en";
    }









    updateLanguageValues(langdata);


  }


  updateLanguageValues(String l)async
  {
    String response = await    LanguageSections.getLanguageResponse(l);
    List<XmlElement> ?elements = XmlElement.parseString(
      response,
      returnElementsNamed: ['string'],

    );

    for(XmlElement xe in elements!)
    {


      if(xe.attributes![0].value.toString().compareTo("payment")==0)
      {

        setState(() {
          payment=xe.text.toString();

        });

      }
      if(xe.attributes![0].value.toString().compareTo("receipt")==0)
      {

        setState(() {
          recipt=xe.text.toString();

        });

      }
      if(xe.attributes![0].value.toString().compareTo("billing")==0)
      {

        setState(() {
          billing=xe.text.toString();

        });

      }
      if(xe.attributes![0].value.toString().compareTo("cash")==0)
      {

        setState(() {
          cashbanck=xe.text.toString();

        });

      }
      if(xe.attributes![0].value.toString().compareTo("task")==0)
      {

        setState(() {
          task=xe.text.toString();

        });

      }
      if(xe.attributes![0].value.toString().compareTo("asset")==0)
      {

        setState(() {
          asset=xe.text.toString();

        });

      }
      if(xe.attributes![0].value.toString().compareTo("liability")==0)
      {

        setState(() {
          liability=xe.text.toString();

        });

      }

      if(xe.attributes![0].value.toString().compareTo("insurance")==0)
      {

        setState(() {
          insurance=xe.text.toString();

        });

      }


      if(xe.attributes![0].value.toString().compareTo("investment")==0)
      {

        setState(() {
          investment=xe.text.toString();

        });

      }

      if(xe.attributes![0].value.toString().compareTo("diary")==0)
      {

        setState(() {
          mydiary=xe.text.toString();

        });

      }

      if(xe.attributes![0].value.toString().compareTo("budget")==0)
      {

        setState(() {
          budget=xe.text.toString();

        });

      }

      if(xe.attributes![0].value.toString().compareTo("viewchart")==0)
      {

        setState(() {
          viewchart=xe.text.toString();

        });

      }

      if(xe.attributes![0].value.toString().compareTo("wallet")==0)
      {

        setState(() {
          wallet=xe.text.toString();

        });

      }








    }



  }

  // void apptrackPermission() async
  // {
  //   final status = await AppTrackingTransparency.requestTrackingAuthorization();
  //
  //   // If the system can show an authorization request dialog
  //   if (status ==
  //       TrackingStatus.notDetermined) {
  //     // Show a custom explainer dialog before the system dialog
  //     await showCustomTrackingDialog(context);
  //     // Wait for dialog popping animation
  //     await Future.delayed(const Duration(milliseconds: 200));
  //     // Request system's tracking authorization dialog
  //     await AppTrackingTransparency.requestTrackingAuthorization();
  //   }
  //
  // }
  //
  // showCustomTrackingDialog(BuildContext context) {
  //
  //
  //   Widget yesButton = TextButton(
  //       child: Text("Allow tracking"),
  //       onPressed: () async {
  //
  //         final uuid = await AppTrackingTransparency.getAdvertisingIdentifier();
  //
  //
  //       });
  //
  //
  //
  //   Widget noButton = TextButton(
  //     child: Text("Ask app not to track"),
  //     onPressed: () {
  //       Navigator.pop(context);
  //     },
  //   );
  //
  //   // set up the AlertDialog
  //   AlertDialog alert = AlertDialog(
  //     title: Text("Save"),
  //     content: Text("'Save app' would like track your information"),
  //     actions: [yesButton, noButton],
  //   );
  //
  //   // show the dialog
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return alert;
  //     },
  //   );
  //
  //
  //
  // }



}