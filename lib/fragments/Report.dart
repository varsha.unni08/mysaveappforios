import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:saveappforios/design/ResponsiveInfo.dart';

import '../views/AccountReport.dart';
import '../views/BillRegister.dart';
import '../views/Cashbankstatement.dart';
import '../views/IncomeExpenditureStatement.dart';
import '../views/Ledger.dart';
import '../views/Reminds.dart';
import '../views/Transactions.dart';

class ReportPage extends StatefulWidget {
  final String title;

  const ReportPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ReportPage();

}

class _ReportPage extends State<ReportPage> {



  @override
  Widget build(BuildContext context) {
    return Scaffold(

       body: Container(

            height: double.infinity,
            width: double.infinity,

            // child:Stack(  alignment: Alignment.topLeft,  children: <Widget>[


            child: Container(
              margin: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(0, 5, 0, 5) :EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 10, 0, 10),

              child: SingleChildScrollView(
                  child:Column(

                      children: [

                        Card(



                          child: InkWell(


                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,

                              child:Padding(

                                padding: EdgeInsets.all(10),

                                child: Align(

                                  alignment: Alignment.centerLeft,
                                  child: TextButton(

                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          TransactionPage(title: "Transaction")));

                                    },
                                    child: Text("Transactions",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )




                                  ,),


                              )


                          ),


                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  TransactionPage(title: "Transaction")));
                            },
                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  LedgerPage(title: "Ledger")));
                            },



                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child: Align(

                                  alignment: Alignment.centerLeft,
                                  child: TextButton(

                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          LedgerPage(title: "Ledger")));

                                    },
                                    child: Text("Ledgers",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )




                                  ,),
                              )


                          ),


                          )
                        ),

                        Card(

                          child: InkWell(
                            onTap: (){
                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  CashbankstatementPage(title: "Cash bank statement")));

                            },



                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(


                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          CashbankstatementPage(title: "Cash bank statement")));

                                    },
                                    child: Text("Cash and Bank Statement",style: TextStyle(fontSize: 12,color: Colors.black)) ,
                                  )




                                  ,),
                              )


                          ),


                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  IncomeExpenditurePage(title: "IncomeExpenditure statement")));
                            },


                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:

                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(

                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          IncomeExpenditurePage(title: "IncomeExpenditure statement")));

                                    },
                                    child: Text("Income and Expenditure Statement",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )





                                  ,),
                              )


                          ),

                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  RemindsListPage(title: "Reminders")));
                            },




                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(

                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          RemindsListPage(title: "Reminders")));

                                    },
                                    child:  Text("Reminders",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )

                                  ,),
                              )


                          ),

                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  ReportPageAccount(title: "Report", accounttype: 'Asset account',)));


                            },




                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(


                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          ReportPageAccount(title: "Report", accounttype: 'Asset account',)));



                                    },
                                    child: Text("List of my Assets",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )

                                  ,),
                              )


                          ),

                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  ReportPageAccount(title: "Report", accounttype: 'Liability account',)));
                            },





                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(

                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          ReportPageAccount(title: "Report", accounttype: 'Liability account',)));
                                    },
                                    child: Text("List of my liabilities",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )

                                  ,),
                              )


                          ),


                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){
                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  ReportPageAccount(title: "Report", accounttype: 'Insurance',)));

                            },




                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(

                                    onPressed: () {
                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          ReportPageAccount(title: "Report", accounttype: 'Insurance',)));

                                    },
                                    child: Text("List of my insurance",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )

                                  ,),
                              )


                          ),


                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  ReportPageAccount(title: "Report", accounttype: 'Investment',)));
                            },




                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(


                                    onPressed: () {
                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          ReportPageAccount(title: "Report", accounttype: 'Investment',)));


                                    },
                                    child: Text("List of my investments",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )

                                  ,),
                              )


                          ),

                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) =>
                                  BillRegisterPage(title: "Bill register",)));
                            },




                       child:   Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80 ,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(onPressed: () {

                                    Navigator.push(
                                        context, MaterialPageRoute(builder: (_) =>
                                        BillRegisterPage(title: "Bill register",)));


                                  },
                                  child: Text("Bill register",style: TextStyle(fontSize: 12,color: Colors.black)),



                                  )

                                  ,),
                              )


                          ),)
                        )




                      ])



              ),




            )











          // ]),




        )


    );
  }
}