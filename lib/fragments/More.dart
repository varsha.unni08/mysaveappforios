import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:permission_handler/permission_handler.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Profiledata.dart';
import '../domain/ShareSlider.dart';
import '../projectconstants/DataConstants.dart';
import '../views/Feedback.dart';
import '../views/Usermanual.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:path_provider/path_provider.dart';



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: MorePage(title: 'registration'),
      debugShowCheckedModeBanner: false,
    );
  }
}


class MorePage extends StatefulWidget {
  final String title;

  const MorePage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MorePage();

}

class _MorePage extends State<MorePage> {


  String token="";
  String mobile="";


  String name="";
  String email="",phonenumber="";
  double amounttopay=0;
  String countryid="0",stateid="0";
  double d=0;
  double cgst=0,igst=0,sgst=0,othertaxamount=0,totalamount=0;





  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: Container(

            height: double.infinity,
            width: double.infinity,

            //  child:Stack(  alignment: Alignment.topLeft,  children: <Widget>[


            // child: Align(
            child: Container(
              margin: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(0, 5, 0, 5) :EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 10, 0, 10),

              child: SingleChildScrollView(
                  child:Column(

                      children: [

                        Card(

                          child: InkWell(

                  onTap: (){

                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => UsermanualPage(title: "How to use",)));
                  },



                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,

                              child:Padding(

                                padding: EdgeInsets.all(10),

                                child: Align(

                                  alignment: Alignment.centerLeft,
                                  child: TextButton(

                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) => UsermanualPage(title: "How to use",)));

                                    },
                                    child: Text("How to use",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )



                                  ,),


                              )

                        )
                          ),
                        ),

                        Card(

                          child: InkWell(

                            onTap: ()async{

                              String url="https://api.whatsapp.com/send?phone="+DataConstants.whatsapp;


                              if (await canLaunch(url)) {
                              await launch(url, forceWebView: true);
                              } else {
                              throw 'Could not launch $url';
                              }

                            },



                         child: Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child: Align(

                                  alignment: Alignment.centerLeft,
                                  child: TextButton(

                                    onPressed: () async{
                                      String url="https://api.whatsapp.com/send?phone="+DataConstants.whatsapp;


                                      if (await canLaunch(url)) {
                                      await launch(url, forceWebView: true);
                                      } else {
                                      throw 'Could not launch $url';
                                      }

                                    },
                                    child: Text("Help on whatsapp",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )



                                  ,),
                              )


                          ),


                          )
                        ),

                        Card(

                          child: InkWell(


                            onTap: (){

                              final Uri _emailLaunchUri = Uri(
                                  scheme: 'mailto',
                                  path: 'mail@integrasoftware.in',
                                  queryParameters: {
                                    'subject': ''
                                  }
                              );

// ...

// mailto:smith@example.com?subject=Example+Subject+%26+Symbols+are+allowed%21
                              launch(_emailLaunchUri.toString());
                            },



                         child: Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(onPressed: () {

                                    final Uri _emailLaunchUri = Uri(
                                        scheme: 'mailto',
                                        path: 'mail@integrasoftware.in',
                                        queryParameters: {
                                          'subject': ''
                                        }
                                    );

// ...

// mailto:smith@example.com?subject=Example+Subject+%26+Symbols+are+allowed%21
                                    launch(_emailLaunchUri.toString());

                                  },
                                  child: Text("Mail us",style: TextStyle(fontSize: 12,color: Colors.black)),




                                  )




                                  ,),
                              )


                          ),

                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: () async{

                              String privacypolicy=DataConstants.domain+"index.php/web/about";
                              if (await canLaunch(privacypolicy)) {
                              await launch(privacypolicy, forceWebView: true);
                              } else {
                              throw 'Could not launch $privacypolicy';
                              }
                            },




                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:

                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(


                                    onPressed: () async {

                                      String privacypolicy=DataConstants.domain+"index.php/web/about";
                                      if (await canLaunch(privacypolicy)) {
                                      await launch(privacypolicy, forceWebView: true);
                                      } else {
                                      throw 'Could not launch $privacypolicy';
                                      }


                                    },
                                    child: Text("About us",style: TextStyle(fontSize: 12,color: Colors.black)) ,
                                  )




                                  ,),
                              )


                          ),


                          )
                        ),

                        Card(

                          child: InkWell(

                            onTap: () async {

                              String privacypolicy=DataConstants.domain+"index.php/web/memTerms2";
                              if (await canLaunch(privacypolicy)) {
                              await launch(privacypolicy, forceWebView: true);
                              } else {
                              throw 'Could not launch $privacypolicy';
                              }
                            },



                         child: Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(

                                    onPressed: () async {
                                      String privacypolicy=DataConstants.domain+"index.php/web/memTerms2";
                                      if (await canLaunch(privacypolicy)) {
                                      await launch(privacypolicy, forceWebView: true);
                                      } else {
                                      throw 'Could not launch $privacypolicy';
                                      }


                                    },
                                    child: Text("Privacy policy",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )

                                  ,),
                              )


                          ),

                          )
                        ),

                        Card(

                          child: InkWell(
                            onTap: () async{

                              String privacypolicy=DataConstants.domain+"index.php/web/memTerms1";
                              if (await canLaunch(privacypolicy)) {
                              await launch(privacypolicy, forceWebView: true);
                              } else {
                              throw 'Could not launch $privacypolicy';
                              }
                            },




                         child: Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(


                                    onPressed: () async {

                                      String privacypolicy=DataConstants.domain+"index.php/web/memTerms1";
                                      if (await canLaunch(privacypolicy)) {
                                      await launch(privacypolicy, forceWebView: true);
                                      } else {
                                      throw 'Could not launch $privacypolicy';
                                      }

                                    },
                                    child: Text("Terms and conditions for use",style: TextStyle(fontSize: 12,color: Colors.black)) ,
                                  )

                                  ,),
                              )


                          ),)
                        ),

                        Card(

                          child: InkWell(
                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) => FeedbackPage(title: "Feedback",)));
                            },



                        child:  Container(
                              width: double.infinity,
                              height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,

                              child:Padding(

                                padding: EdgeInsets.all(10),
                                child:
                                Align(

                                  alignment: Alignment.centerLeft,

                                  child: TextButton(

                                    onPressed: () {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) => FeedbackPage(title: "Feedback",)));

                                    },
                                    child: Text("Feedback",style: TextStyle(fontSize: 12,color: Colors.black)),
                                  )

                                  ,),
                              )


                          ),

                          )
                        ),

                        // Card(
                        //
                        //     child: InkWell(
                        //       onTap: (){
                        //
                        //         Widget yesButton = TextButton(
                        //           child: Text("Ok"),
                        //           onPressed: () {
                        //
                        //             Navigator.pop(context);
                        //
                        //
                        //
                        //           },
                        //         );
                        //
                        //
                        //         // set up the AlertDialog
                        //         AlertDialog alert = AlertDialog(
                        //           title: Text("Save"),
                        //           content: SingleChildScrollView(
                        //
                        //             child: Column(
                        //
                        //               children: [
                        //
                        //                 Text("For activation of SAVE App transfer the bill amount of Rs. 2999/- to the bank given below and WhatsApp transaction details and ID/registered mobile number to 9946109501\n\n"
                        //                     "Century Gate Software Solutions Pvt Ltd\n Canara Bank \nAccount number : 45501400000018 \nMICR: 680015901 \nIFSC: CNRB0014550 \n"
                        //                     "\n\nSAVE App is licensed for lifetime usage of the activated version. However, the company is offering free version updates for one year. During this period users can update all newly released versions free of cost. \n"
                        //                     "After one year, a renewal cost of Rs. 600 (including GST) will be applicable for those who wish to continue the free updates for the coming year."
                        //                     " Reinstallation after one year will attract the renewal cost, if not renewed on time."
                        //                     " Limited period offer: This is a limited period discount for app purchases and may end at any time.")
                        //               ],
                        //
                        //
                        //             ),
                        //
                        //
                        //           )
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        //           ,
                        //           actions: [yesButton],
                        //         );
                        //
                        //         // show the dialog
                        //         showDialog(
                        //           context: context,
                        //           builder: (BuildContext context) {
                        //             return alert;
                        //           },
                        //         );
                        //       },
                        //
                        //
                        //
                        //       child:  Container(
                        //           width: double.infinity,
                        //           height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80,
                        //
                        //           child:Padding(
                        //
                        //             padding: EdgeInsets.all(10),
                        //             child:
                        //             Align(
                        //
                        //               alignment: Alignment.centerLeft,
                        //
                        //               child: TextButton(
                        //
                        //                 onPressed: () async{
                        //
                        //
                        //
                        //
                        //                       Widget yesButton = TextButton(
                        //                         child: Text("Ok"),
                        //                         onPressed: () {
                        //
                        //                           Navigator.pop(context);
                        //
                        //
                        //
                        //                         },
                        //                       );
                        //
                        //
                        //                       // set up the AlertDialog
                        //                       AlertDialog alert = AlertDialog(
                        //                         title: Text("Save"),
                        //                         content: SingleChildScrollView(
                        //
                        //                           child: Column(
                        //
                        //                             children: [
                        //
                        //                               Text("For activation of SAVE App transfer the bill amount of Rs. 2999/- to the bank given below and WhatsApp transaction details and ID/registered mobile number to 9946109501\n\n"
                        //                                   "Century Gate Software Solutions Pvt Ltd\n Canara Bank \nAccount number : 45501400000018 \nMICR: 680015901 \nIFSC: CNRB0014550 \n"
                        //                                   "\n\nSAVE App is licensed for lifetime usage of the activated version. However, the company is offering free version updates for one year. During this period users can update all newly released versions free of cost. \n"
                        //                              "After one year, a renewal cost of Rs. 600 (including GST) will be applicable for those who wish to continue the free updates for the coming year."
                        //                        " Reinstallation after one year will attract the renewal cost, if not renewed on time."
                        //                        " Limited period offer: This is a limited period discount for app purchases and may end at any time.")
                        //                             ],
                        //
                        //
                        //                           ),
                        //
                        //
                        //                         )
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        //                         ,
                        //                         actions: [yesButton],
                        //                       );
                        //
                        //                       // show the dialog
                        //                       showDialog(
                        //                         context: context,
                        //                         builder: (BuildContext context) {
                        //                           return alert;
                        //                         },
                        //                       );
                        //
                        //
                        //
                        //                 },
                        //                 child: Text("Payment Info",style: TextStyle(fontSize: 12,color: Colors.black)),
                        //               )
                        //
                        //               ,),
                        //           )
                        //
                        //
                        //       ),
                        //
                        //     )
                        // ),






                      ])



              ),




            ))



    );
  }


  getProfile() async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getUserDetails +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;

    print(response);
    var json = jsonDecode(response);

    Profiledata profile=Profiledata.fromJson(json);

    print(profile.data.full_name);


    mobile=profile.data.mobile;


    countryid=profile.data.country_id;
    stateid=profile.data.state_id;
    name=profile.data.full_name;
    email=profile.data.email_id;

    getSmemberData(mobile);



  }

  void getSmemberData(String mobilenumber) async
  {

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.showMemberDetails +
          "?mobile="+mobilenumber+"&timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;



    var json = jsonDecode(response);

    if(json['status']==1)
    {

      var js=json['data'];
      print(js['member_status']);

      if(js['member_status'].toString().compareTo("active")!=0)
      {

        // checkTrialPeriod();
        //
        // isactive=false;
        //
        // setState(() {
        //
        //   btntext="Purchase";
        // });

        showAlert();


      }
      else{

        shareLink();
      }



    }
    else{

      showAlert();
    }

  }

  shareLink() async {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,
        textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getSettingsSlider +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    ShareSlider shareSlider = ShareSlider.fromJson(jsonDecode(response));

    if (shareSlider.status == 1) {
      showShareDialog(shareSlider);
    }

    print(response);
  }
  showAlert()
  {
    Widget okButton = TextButton(
        child: Text("Yes"),
        onPressed: () {
          Navigator.of(context).pop();
          startPayment();

        }
    );


    Widget noButton = TextButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Save"),
      content: Text(
          "You are not purchased this application.do you want to purchase now ?"),
      actions: [
        okButton, noButton
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showShareDialog(ShareSlider shareSlider) {
    List<Widget> sliderwidget = [];

    PageController pageController = new PageController();

    for (var i = 0; i < shareSlider.data.length; i++) {
      sliderwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.89349112,
        width: double.infinity,
        child: Card(
            child: Image.network(
                DataConstants.sliderimageurl + shareSlider.data[i].image)),
      ));
    }

    Dialog sharedialog = Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        //this right here
        child: Container(
            height: double.infinity,
            width: double.infinity,
            child:SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                      width: 220,
                      height: 220 / 0.609375,
                      child: PageIndicatorContainer(
                          length: shareSlider.data.length,
                          align: IndicatorAlign.bottom,
                          indicatorSpace: 14.0,
                          padding: const EdgeInsets.all(10),
                          indicatorColor: Colors.black12,
                          indicatorSelectorColor: Colors.blueGrey,
                          shape: IndicatorShape.circle(size: 10),
                          child: PageView.builder(
                            scrollDirection: Axis.horizontal,
                            controller: pageController,
                            itemBuilder: (BuildContext context, int index) {
                              return sliderwidget[index];
                            },
                            itemCount: shareSlider.data.length,
                            // children: nsdwidget,
                          ))),
                  Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(5),
                          child: Container(
                              color: Colors.blueGrey,
                              child: TextButton(
                                onPressed: () {
                                  Clipboard.setData(ClipboardData(
                                      text: DataConstants.domain +
                                          "index.php/web/signup?sponserid=" +
                                          token));

                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text("Copied to clipboard"),
                                  ));
                                },
                                child: Text(
                                  "Copy link",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ))),
                      SizedBox(width: 170,child:Container(
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: new Theme(
                                  data: new ThemeData(hintColor: Colors.white),
                                  child: Text(
                                    DataConstants.domain +
                                        "index.php/web/signup?sponserid=" +
                                        token,
                                    maxLines: 2,
                                    style: TextStyle(color: Colors.blue),
                                  )))))
                    ],
                  ),
                  Container(
                      color: Colors.blueGrey,
                      child: TextButton(
                        onPressed: () async {

                            print("permission granted");

                            var date = new DateTime.now().toIso8601String();

                            String url = DataConstants.sliderimageurl +
                                shareSlider.data[pageController.page!.toInt()]
                                    .image; // <-- 1
                            var response = await http.get(Uri.parse(url)); // <--2
                            var documentDirectory =
                            await getApplicationDocumentsDirectory();
                            var firstPath = documentDirectory.path + "/images";
                            var filePathAndName = documentDirectory.path +
                                '/images/' +
                                date.toString() +
                                ".jpg";

                            await Directory(firstPath)
                                .create(recursive: true); // <-- 1
                            File file2 = new File(filePathAndName); // <-- 2
                            file2.writeAsBytesSync(response.bodyBytes);

                            bool fileExists = await file2.exists();

                            if (fileExists) {
                              print("file exists : " + file2.path);

                              //  Share.share('check out my website https://example.com');

                              Share.shareFiles([file2.path],
                                  text: shareSlider
                                      .data[pageController.page!.toInt()]
                                      .description);
                            }

                        },
                        child: Text(
                          "Share",
                          style: TextStyle(color: Colors.white),
                        ),
                      ))
                ],
              ),)
        ));

    showDialog(
        context: context, builder: (BuildContext context) => sharedialog);
  }

  startPayment() async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl + DataConstants.getSettingsValue+"?timestamp="+date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!,

      },
    );
    String response = dataasync.body;

    print(response);


    var json = jsonDecode(response);

    if (json['status'] == 1) {

      var js=json['data'];

      String networkimage=js['network_image'];
      String basicamount=js['one_bv_required_amount'];
      d=double.parse(basicamount);


      // cgst=double.parse(js['cgst']);
      //   igst=double.parse(js['igst']);
      //   sgst=double.parse(js['sgst']);
      //   othertaxamount=double.parse(js['other_tax']);


      if (countryid.compareTo("1")==0) {
        //layout_actualprice.setVisibility(View.GONE);

        if (stateid.compareTo("12")==0) {


          double cg=double.parse(js['cgst']);
          double  ig=double.parse(js['igst']);
          double  sg=double.parse(js['sgst']);
          double othertax=double.parse(js['other_tax']);

          cgst=d*(cg/100);
          igst=d*(ig/100);
          sgst=d*(sg/100);

          totalamount=d+cgst+sgst;



        }
        else{

          double cg=double.parse(js['cgst']);
          double  ig=double.parse(js['igst']);
          double  sg=double.parse(js['sgst']);
          double othertax=double.parse(js['other_tax']);

          cgst=d*(cg/100);
          igst=d*(ig/100);
          sgst=d*(sg/100);

          totalamount=d+igst;



        }
      }
      else{

        double cg=double.parse(js['cgst']);
        double  ig=double.parse(js['igst']);
        double  sg=double.parse(js['sgst']);
        double othertax=double.parse(js['other_tax']);

        cgst=d*(cg/100);
        igst=d*(ig/100);
        sgst=d*(sg/100);
        double ot=d*(othertax/100);

        totalamount=d+ot;

      }






      Dialog sharedialog = Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          //this right here
          child: Container(
              height: (MediaQuery.of(context).size.width),
              width:(MediaQuery.of(context).size.width)/1.9,
              child:SingleChildScrollView(
                child: Column(
                  children: [

                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Center(child:Text("Payment Confirmation"))
                    ),

                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Actual price  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              d.toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")==0)?     Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Sgst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              sgst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")==0)? Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Cgst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              cgst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")!=0)  ? Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Igst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              igst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Total price  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              totalamount.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),

                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Center(child:TextButton(onPressed: () {


                          amounttopay=totalamount;


                          Navigator.pop(context);

                          DateTime t=DateTime.now();
                          String orderstring=t.millisecond.toString();

                          //getDataFromPayment(orderstring);


                          showGeneralDialog(
                            barrierLabel: "Label",
                            barrierDismissible: true,
                            barrierColor: Colors.black.withOpacity(0.5),
                            transitionDuration: Duration(milliseconds: 700),
                            context: context,
                            pageBuilder: (context, anim1, anim2) {
                              return Align(
                                alignment: true ? Alignment.topCenter : Alignment.bottomCenter,
                                child: Container(
                                  height: (MediaQuery.of(context).size.height)/1.5,
                                  width: (MediaQuery.of(context).size.width),
                                  child: SingleChildScrollView(


                                      child: Column(

                                        children: [


                                          Container(child: Padding(
                                            padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Text("For activation of SAVE App transfer the bill amount of Rs. 2950/- to the bank given below and WhatsApp transaction details and ID/registered mobile number to 9946109501",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),),
                                          )),

                                          Divider(color: Colors.black38,
                                            thickness: 1,),

                                          Container(child: Padding(
                                            padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Text("Century Gate Software Solutions Pvt Ltd\n Canara Bank \nAccount number : 45501400000018 \nMICR: 680015901 \nIFSC: CNRB0014550 \n",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),),

                                          )),

                                          Container(child: Padding(
                                            padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child:TextButton(onPressed: () {

                                              Navigator.pop(context);

                                            }, child: Text("Ok",style: TextStyle( fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.blue),),



                                            ) ,
                                          )),



                                        ],


                                      )),
                                  margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                              );
                            },
                            transitionBuilder: (context, anim1, anim2, child) {
                              return SlideTransition(
                                position: Tween(begin: Offset(0, true ? -1 : 1), end: Offset(0, 0)).animate(anim1),
                                child: child,
                              );
                            },
                          );







                        }, child: Text("Confirm"),))
                    ),


                  ],
                ),)
          ));

      showDialog(
          context: context, builder: (BuildContext context) => sharedialog);
















    } else {

    }
  }
}


