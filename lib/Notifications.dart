import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:saveappforios/projectconstants/DataConstants.dart';
// import 'package:save_flutter/domain/MessageData.dart';
// import 'package:save_flutter/domain/NotificationMessage.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
 import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'NotificationDetails.dart';
import 'design/ResponsiveInfo.dart';
import 'domain/MessageData.dart';
import 'domain/NotificationMessage.dart';
// import 'mainviews/NotificationDetails.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: NotificationPage(title: 'registration'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class NotificationPage extends StatefulWidget {
  final String title;

  const NotificationPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotificationPage();

}

class _NotificationPage extends State<NotificationPage> {


  List<MessageData>messagedata=[];

  @override
  void initState() {
    // TODO: implement initState

    getNotifications();
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          toolbarHeight: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?60:70:90,
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Notifications",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
          centerTitle: false,
        ),

        body: Stack(

          children: [

            (messagedata.length>0)? Align(
    alignment: Alignment.topLeft,
    child: Padding(padding: EdgeInsets.all(5),

     child: ListView.builder(
        padding: const EdgeInsets.all(8),
            itemCount: messagedata.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                // color: Colors.amber[colorCodes[index]],
                child: Center(
                    child:InkWell(

                      onTap: ()async{

                        print("tapped");

                        Navigator.pushReplacement(context, MaterialPageRoute(
                            builder: (context) => NotificationDetailsPage(title: "Notification details",messagedata: messagedata[index],)
                        )
                        );



                      },

                      child:Card(
                        elevation: 5,
                        child: Container(
                          color: Colors.white,
                          width: double.infinity,


                            
                            // Image.asset("images/ic_launcher.png",width: 50,height: 50,),

                          child:
                          Padding(padding: EdgeInsets.all(8),
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [

                                      Expanded(
                                        child: Text(
                                          messagedata[index].message.title,
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [

                                      Expanded(
                                        child: Text(
                                          messagedata[index].message.message,
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [

                                      Expanded(
                                        child: Text(
                                          messagedata[index].date
                                          ,
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),



                              ],
                            ),

                          )



                          ,)
                          ,
                        ),
                      ) ,
                    )




                ,
              );
            }),
    )
        ) : Align(

              alignment: FractionalOffset.center,
              child: Text("No data found"),
            )



          ],


        )



    );

  }

  getNotifications() async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
        Uri.parse(DataConstants.baseurl +
            DataConstants.getNotifications +
            "?timestamp=" +
            date.toString()),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!
        },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

     var json = jsonDecode(response);;

    NotificationMessage notificationMessage=NotificationMessage.fromJson(json);

    if(notificationMessage.status==1)
      {



for(MessageData m in notificationMessage.data)
  {

    String createddate=m.message.created_date;

    if(createddate!=null)
      {

        DateTime trialdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(createddate);

      String   date1=trialdateparsed.day.toString()+"-"+trialdateparsed.month.toString()+"-"+trialdateparsed.year.toString();

      m.date=date1;

      }


  }

setState(() {

  messagedata.clear();
  messagedata.addAll(notificationMessage.data);
});


      }
    else{




    }




  }

}
