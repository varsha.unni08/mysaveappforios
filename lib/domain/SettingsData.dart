
import 'package:json_annotation/json_annotation.dart';
part 'SettingsData.g.dart';
@JsonSerializable(explicitToJson: true)
class SettingsData{

   String id="";
 String bv="";
 String referal_commission="";
 String one_bv_required_amount="";
 String renewal_charge="";
 String one_bv_equalant_value="";
  String daily_ceiling="";
 String Percent_tds="";
 String Percent_tds_nonpan="";
 String percent_admin_charges="";
String renewal_notification_days="";
 String bill_prefix="";
 String cgst="";
 String sgst="";
 String igst="";
  String other_tax="";
  String buffer_count="";
 String link_image="";
  String network_image="";

   SettingsData(
      this.id,
      this.bv,
      this.referal_commission,
      this.one_bv_required_amount,
      this.renewal_charge,
      this.one_bv_equalant_value,
      this.daily_ceiling,
      this.Percent_tds,
      this.Percent_tds_nonpan,
      this.percent_admin_charges,
      this.renewal_notification_days,
      this.bill_prefix,
      this.cgst,
      this.sgst,
      this.igst,
      this.other_tax,
       this.buffer_count,
      this.link_image,
      this.network_image);

   factory SettingsData.fromJson(Map<String,dynamic>data) => _$SettingsDataFromJson(data);


   Map<String,dynamic> toJson() => _$SettingsDataToJson(this);
}