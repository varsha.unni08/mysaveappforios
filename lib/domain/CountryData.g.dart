// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CountryData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryData _$CountryDataFromJson(Map<String, dynamic> json) {
  return CountryData(
    json['id'] as String,
    json['country_name'] as String,
    json['country_abr'] as String,
    json['country_code'] as String,
    json['currencyid'] as String,
    json['currency_code'] as String,
  );
}

Map<String, dynamic> _$CountryDataToJson(CountryData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'country_name': instance.country_name,
      'country_abr': instance.country_abr,
      'country_code': instance.country_code,
      'currencyid': instance.currencyid,
      'currency_code': instance.currency_code,
    };
