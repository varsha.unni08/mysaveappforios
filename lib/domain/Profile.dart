
import 'package:json_annotation/json_annotation.dart';

part 'Profile.g.dart';

@JsonSerializable()
class Profile{


   String id="";
  String full_name="";
  String regCode="";
  String country_id="";
  String state_id="";
  String mobile="";
   String email_id="";
 String currency="";
   String join_date="";
 String activation_date="";
   String activation_key="";
  String defaultLang="";
 String profile_image="";
 String gdrive_fileid="";
 String join_source="";
 String sp_reg_id="";


   Profile(
      this.id,
      this.full_name,
      this.regCode,
      this.country_id,
      this.state_id,
      this.mobile,
      this.email_id,
      this.currency,
      this.join_date,
      this.activation_date,
      this.activation_key,
      this.defaultLang,
      this.profile_image,
       this.gdrive_fileid,
      this.join_source,
      this.sp_reg_id);

  factory Profile.fromJson(Map<String,dynamic>data) => _$ProfileFromJson(data);


   Map<String,dynamic> toJson() => _$ProfileToJson(this);
}