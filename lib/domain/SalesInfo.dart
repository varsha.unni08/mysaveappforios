import 'SalesinfoData.dart';

import 'package:json_annotation/json_annotation.dart';

part 'SalesInfo.g.dart';

@JsonSerializable(explicitToJson: true)
class Salesinfo{

 int status=0;
 String message="";
late SalesinfoData data;

 Salesinfo(this.status, this.message, this.data);


 factory Salesinfo.fromJson(Map<String,dynamic>data) => _$SalesinfoFromJson(data);


 Map<String,dynamic> toJson() => _$SalesinfoToJson(this);
}