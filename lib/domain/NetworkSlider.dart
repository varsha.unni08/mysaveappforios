

import 'package:json_annotation/json_annotation.dart';

part 'NetworkSlider.g.dart';

@JsonSerializable()
class NetworkSlider{

   String id="";
   String imagepath="";
  String status="";


   NetworkSlider(this.id, this.imagepath, this.status);

  factory NetworkSlider.fromJson(Map<String,dynamic>data) => _$NetworkSliderFromJson(data);


   Map<String,dynamic> toJson() => _$NetworkSliderToJson(this);
}