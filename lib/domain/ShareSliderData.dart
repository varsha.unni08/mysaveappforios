
import 'package:json_annotation/json_annotation.dart';

part 'ShareSliderData.g.dart';

@JsonSerializable()
class ShareSliderData{

  String id="";
   String image="";
   String description="";

  ShareSliderData(this.id, this.image, this.description);

  factory ShareSliderData.fromJson(Map<String,dynamic>data) => _$ShareSliderDataFromJson(data);


  Map<String,dynamic> toJson() => _$ShareSliderDataToJson(this);
}