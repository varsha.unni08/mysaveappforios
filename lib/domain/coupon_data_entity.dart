import 'package:saveappforios/generated/json/base/json_field.dart';
import 'package:saveappforios/generated/json/coupon_data_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class CouponDataEntity {

	late int status;
	late String message;
	late CouponDataData data;
  
  CouponDataEntity();

  factory CouponDataEntity.fromJson(Map<String, dynamic> json) => $CouponDataEntityFromJson(json);

  Map<String, dynamic> toJson() => $CouponDataEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class CouponDataData {

	late String id;
	@JSONField(name: "full_name")
	late String fullName;
	@JSONField(name: "reg_code")
	late String regCode;
	@JSONField(name: "country_id")
	late String countryId;
	@JSONField(name: "state_id")
	late String stateId;
	late String mobile;
	@JSONField(name: "profile_image")
	late String profileImage;
	@JSONField(name: "email_id")
	late String emailId;
	late String currency;
	@JSONField(name: "join_date")
	late String joinDate;
	@JSONField(name: "activation_date")
	late String activationDate;
	@JSONField(name: "activation_key")
	late String activationKey;
	@JSONField(name: "join_source")
	late String joinSource;
	@JSONField(name: "used_link_for_registration")
	dynamic usedLinkForRegistration;
	@JSONField(name: "sp_reg_id")
	late String spRegId;
	@JSONField(name: "device_id")
	late String deviceId;
	@JSONField(name: "sp_reg_code")
	late String spRegCode;
	@JSONField(name: "default_lang")
	late String defaultLang;
	late String username;
	@JSONField(name: "encr_password")
	late String encrPassword;
	@JSONField(name: "gdrive_fileid")
	dynamic gdriveFileid;
	@JSONField(name: "unique_deviceId")
	late String uniqueDeviceid;
	@JSONField(name: "member_status")
	late String memberStatus;
	@JSONField(name: "reselling_partner")
	late String resellingPartner;
	late String coupon;
	@JSONField(name: "coup_stus")
	late String coupStus;
  
  CouponDataData();

  factory CouponDataData.fromJson(Map<String, dynamic> json) => $CouponDataDataFromJson(json);

  Map<String, dynamic> toJson() => $CouponDataDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}