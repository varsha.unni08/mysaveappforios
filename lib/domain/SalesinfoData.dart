

import 'package:json_annotation/json_annotation.dart';

part 'SalesinfoData.g.dart';

@JsonSerializable(explicitToJson: true)
class SalesinfoData{

 String id="";
  String billno_prefix="";
  String bill_no="";
 String reg_id="";
  String reg_code="";
  String product_id="";
   String sales_type="";
 String sales_date="";
  String expe_date="";
   String amt="";
  String cgst="";
  String sgst="";
 String igst="";
 String binary_val="";
 String currency="";
 String ex_rate="";
 String rupee_convertion_val="";
 String sponser_reg_id="";
 String sponser_reg_code="";
 String referal_commission_rupee="";
 String sponser_currency="";
   String sponser_cur_ex_rate="";
String sponser_conversion_value="";
 String binary_gen_status="";
 String cash_transaction_id="";

 SalesinfoData(
      this.id,
      this.billno_prefix,
      this.bill_no,
      this.reg_id,
      this.reg_code,
      this.product_id,
      this.sales_type,
      this.sales_date,
      this.expe_date,
      this.amt,
      this.cgst,
      this.sgst,
      this.igst,
      this.binary_val,
      this.currency,
      this.ex_rate,
      this.rupee_convertion_val,
      this.sponser_reg_id,
      this.sponser_reg_code,
      this.referal_commission_rupee,
      this.sponser_currency,
      this.sponser_cur_ex_rate,
      this.sponser_conversion_value,
      this.binary_gen_status,
      this.cash_transaction_id);



 factory SalesinfoData.fromJson(Map<String,dynamic>data) => _$SalesinfoDataFromJson(data);


 Map<String,dynamic> toJson() => _$SalesinfoDataToJson(this);

}