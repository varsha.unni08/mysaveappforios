// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Feedbackdata.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackList _$FeedbackListFromJson(Map<String, dynamic> json) {
  return FeedbackList(
    json['id'] as String,
    json['reg_id'] as String,
    json['reg_code'] as String,
    json['send_date'] as String,
    json['feedback_msg'] as String,
    json['reply_msg'] as String,
    json['reply_date'] as String,
    json['replied_userid'] as String,
    json['commit_status'] as String,
  );
}

Map<String, dynamic> _$FeedbackListToJson(FeedbackList instance) =>
    <String, dynamic>{
      'id': instance.id,
      'reg_id': instance.reg_id,
      'reg_code': instance.reg_code,
      'send_date': instance.send_date,
      'feedback_msg': instance.feedback_msg,
      'reply_msg': instance.reply_msg,
      'reply_date': instance.reply_date,
      'replied_userid': instance.replied_userid,
      'commit_status': instance.commit_status,
    };
