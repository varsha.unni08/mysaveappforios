// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RemindDateOfAsset.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RemindDateofAsset _$RemindDateofAssetFromJson(Map<String, dynamic> json) {
  return RemindDateofAsset()
    ..reminddate = json['reminddate'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic> _$RemindDateofAssetToJson(RemindDateofAsset instance) =>
    <String, dynamic>{
      'reminddate': instance.reminddate,
      'description': instance.description,
    };
