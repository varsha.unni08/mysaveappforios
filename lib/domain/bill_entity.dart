import 'package:saveappforios/generated/json/base/json_field.dart';
import 'package:saveappforios/generated/json/bill_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class BillEntity {

	late int status;
	late String message;
	late BillData data;
  
  BillEntity();

  factory BillEntity.fromJson(Map<String, dynamic> json) => $BillEntityFromJson(json);

  Map<String, dynamic> toJson() => $BillEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class BillData {

	late String id;
	@JSONField(name: "billno_prefix")
	late String billnoPrefix;
	@JSONField(name: "bill_no")
	late String billNo;
	@JSONField(name: "display_bill_no")
	late String displayBillNo;
	@JSONField(name: "reg_id")
	late String regId;
	@JSONField(name: "reg_code")
	late String regCode;
	@JSONField(name: "product_id")
	late String productId;
	@JSONField(name: "sales_type")
	late String salesType;
	@JSONField(name: "sales_date")
	late String salesDate;
	@JSONField(name: "expe_date")
	late String expeDate;
	late String amt;
	late String cgst;
	late String sgst;
	late String igst;
	@JSONField(name: "binary_val")
	late String binaryVal;
	late String currency;
	@JSONField(name: "ex_rate")
	late String exRate;
	@JSONField(name: "rupee_convertion_val")
	late String rupeeConvertionVal;
	@JSONField(name: "sponser_reg_id")
	late String sponserRegId;
	@JSONField(name: "sponser_reg_code")
	late String sponserRegCode;
	@JSONField(name: "referal_commission_rupee")
	late String referalCommissionRupee;
	@JSONField(name: "sponser_currency")
	late String sponserCurrency;
	@JSONField(name: "sponser_cur_ex_rate")
	late String sponserCurExRate;
	@JSONField(name: "sponser_conversion_value")
	late String sponserConversionValue;
	@JSONField(name: "binary_gen_status")
	late String binaryGenStatus;
	@JSONField(name: "cash_transaction_id")
	late String cashTransactionId;
	late String smode;
  
  BillData();

  factory BillData.fromJson(Map<String, dynamic> json) => $BillDataFromJson(json);

  Map<String, dynamic> toJson() => $BillDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}