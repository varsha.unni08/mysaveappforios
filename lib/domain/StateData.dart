import 'package:json_annotation/json_annotation.dart';
import 'Statelist.dart';

part 'StateData.g.dart';

@JsonSerializable(explicitToJson: true)
class StateData{


  int status=0;
 String message="";
  List<Statelist> data = [];

  StateData(this.status, this.message, this.data);

  factory StateData.fromJson(Map<String,dynamic>data) => _$StateDataFromJson(data);


  Map<String,dynamic> toJson() => _$StateDataToJson(this);
}