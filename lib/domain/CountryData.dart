import 'package:json_annotation/json_annotation.dart';

part 'CountryData.g.dart';

@JsonSerializable()
class CountryData{

 String id;
 String country_name;
 String country_abr;
 String country_code;
 String currencyid;
 String currency_code;


 CountryData(this.id, this.country_name, this.country_abr, this.country_code,
      this.currencyid, this.currency_code);

  factory CountryData.fromJson(Map<String,dynamic>data) => _$CountryDataFromJson(data);


  Map<String,dynamic> toJson() => _$CountryDataToJson(this);
}