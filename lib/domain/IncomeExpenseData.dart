class IncomeExpenseDataToChart {
  final String month;
  final double amount;

  IncomeExpenseDataToChart(this.month, this.amount);
}