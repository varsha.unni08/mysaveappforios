



import 'package:json_annotation/json_annotation.dart';

part 'Message.g.dart';

@JsonSerializable(explicitToJson: true)
class Message{
 String id="";
   String createdby_id="";
   String title="";
  String message="";
 String created_date="";
  String message_validity="";
 String image="";
 String verified_status="";
 String user_type="";
 String state_id="";
  String free_byuser="";
   Object active_byuser="";
  Object reg_code="";
   Object full_name="";


 Message(
      this.id,
      this.createdby_id,
      this.title,
      this.message,
      this.created_date,
      this.message_validity,
      this.image,
      this.verified_status,
      this.user_type,
      this.state_id,
      this.free_byuser,
      this.active_byuser,
      this.reg_code,
      this.full_name);

  factory Message.fromJson(Map<String,dynamic>data) => _$MessageFromJson(data);


   Map<String,dynamic> toJson() => _$MessageToJson(this);
}