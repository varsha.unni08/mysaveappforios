


import 'package:json_annotation/json_annotation.dart';

import 'Feedbackdata.dart';

part 'Feedbackfulldata.g.dart';

@JsonSerializable(explicitToJson: true)
class FeedbackFulldata{

  int status=0;

  String message="";

   List<FeedbackList> data=[];

  FeedbackFulldata(this.status, this.message, this.data);


  factory FeedbackFulldata.fromJson(Map<String,dynamic>data) => _$FeedbackFulldataFromJson(data);


  Map<String,dynamic> toJson() => _$FeedbackFulldataToJson(this);
}