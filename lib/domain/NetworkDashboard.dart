import 'package:json_annotation/json_annotation.dart';

import 'NetworkDashboardData.dart';


part 'NetworkDashboard.g.dart';

@JsonSerializable(explicitToJson: true)
class NetworkDashboard{

   int status=0;
  String message="";
   late NetWorkDashboardData data;

   NetworkDashboard(this.status, this.message, this.data);

   factory NetworkDashboard.fromJson(Map<String,dynamic>data) => _$NetworkDashboardFromJson(data);


   Map<String,dynamic> toJson() => _$NetworkDashboardToJson(this);
}