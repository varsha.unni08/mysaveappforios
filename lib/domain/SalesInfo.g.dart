// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SalesInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Salesinfo _$SalesinfoFromJson(Map<String, dynamic> json) {
  return Salesinfo(
    json['status'] as int,
    json['message'] as String,
    SalesinfoData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SalesinfoToJson(Salesinfo instance) => <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.toJson(),
    };
