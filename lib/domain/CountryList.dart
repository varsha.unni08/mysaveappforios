import 'CountryData.dart';
import 'package:json_annotation/json_annotation.dart';

 part 'CountryList.g.dart';

@JsonSerializable(explicitToJson: true)
class CountryList{


 int status=0;
 String message="";
   List<CountryData> data = [];

 CountryList(this.status, this.message, this.data);





 factory CountryList.fromJson(Map<String,dynamic>data) => _$CountryListFromJson(data);


 Map<String,dynamic> toJson() => _$CountryListToJson(this);

}