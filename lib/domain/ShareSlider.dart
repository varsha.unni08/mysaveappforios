import 'package:json_annotation/json_annotation.dart';
import 'ShareSliderData.dart';

part 'ShareSlider.g.dart';

@JsonSerializable(explicitToJson: true)
class ShareSlider{

  int status=0;
 String message="";
   late List<ShareSliderData> data ;

  ShareSlider(this.status, this.message, this.data);


  factory ShareSlider.fromJson(Map<String,dynamic>data) => _$ShareSliderFromJson(data);


  Map<String,dynamic> toJson() => _$ShareSliderToJson(this);
}