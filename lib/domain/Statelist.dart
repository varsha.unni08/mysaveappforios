import 'package:json_annotation/json_annotation.dart';

part 'Statelist.g.dart';

@JsonSerializable()
class Statelist{

  String id="0";
 String code="";
 String state_name="";
  String state_code="";

  Statelist(this.id, this.code, this.state_name, this.state_code);


  factory Statelist.fromJson(Map<String,dynamic>data) => _$StatelistFromJson(data);


  Map<String,dynamic> toJson() => _$StatelistToJson(this);
}