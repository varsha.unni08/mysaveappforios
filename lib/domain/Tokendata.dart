import 'package:json_annotation/json_annotation.dart';

import 'Head.dart';
import 'Body.dart';

part 'Tokendata.g.dart';

@JsonSerializable(explicitToJson: true)
class  Tokendata{

  late Head head;
  late Body body;

  Tokendata(this.head, this.body);


  factory Tokendata.fromJson(Map<String,dynamic>data) => _$TokendataFromJson(data);


  Map<String,dynamic> toJson() => _$TokendataToJson(this);


}