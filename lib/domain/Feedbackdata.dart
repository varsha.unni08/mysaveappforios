


import 'package:json_annotation/json_annotation.dart';

part 'Feedbackdata.g.dart';

@JsonSerializable()
class FeedbackList{


  String id="";

  String reg_id="";

  String reg_code="";

  String send_date="";

  String feedback_msg="";

  String reply_msg="";

  String reply_date="";

  String replied_userid="";

  String commit_status="";


  FeedbackList(
      this.id,
      this.reg_id,
      this.reg_code,
      this.send_date,
      this.feedback_msg,
      this.reply_msg,
      this.reply_date,
      this.replied_userid,
      this.commit_status);

  factory FeedbackList.fromJson(Map<String,dynamic>data) => _$FeedbackListFromJson(data);


  Map<String,dynamic> toJson() => _$FeedbackListToJson(this);

}