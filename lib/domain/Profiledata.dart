

import 'package:json_annotation/json_annotation.dart';

import 'Profile.dart';

part 'Profiledata.g.dart';

@JsonSerializable(explicitToJson: true)
class Profiledata{

  int status=0;
  String message="";
  late Profile data;

  Profiledata(this.status, this.message, this.data);


  factory Profiledata.fromJson(Map<String,dynamic>data) => _$ProfiledataFromJson(data);


  Map<String,dynamic> toJson() => _$ProfiledataToJson(this);
}