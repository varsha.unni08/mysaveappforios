// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Body _$BodyFromJson(Map<String, dynamic> json) {
  return Body(
    ResultInfo.fromJson(json['resultInfo'] as Map<String, dynamic>),
    json['txnToken'] as String,
    json['isPromoCodeValid'] as bool,
    json['authenticated'] as bool,
  );
}

Map<String, dynamic> _$BodyToJson(Body instance) => <String, dynamic>{
      'resultInfo': instance.resultInfo.toJson(),
      'txnToken': instance.txnToken,
      'isPromoCodeValid': instance.isPromoCodeValid,
      'authenticated': instance.authenticated,
    };
