// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CountryList.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryList _$CountryListFromJson(Map<String, dynamic> json) {
  return CountryList(
    json['status'] as int,
    json['message'] as String,
    (json['data'] as List<dynamic>)
        .map((e) => CountryData.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$CountryListToJson(CountryList instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
