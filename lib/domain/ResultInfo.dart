
import 'package:json_annotation/json_annotation.dart';

part 'ResultInfo.g.dart';

@JsonSerializable(explicitToJson: true)
class ResultInfo{

 String resultStatus="";
  String resultCode="";
  String resultMsg="";


 ResultInfo(this.resultStatus, this.resultCode, this.resultMsg);

  factory ResultInfo.fromJson(Map<String,dynamic>data) => _$ResultInfoFromJson(data);


 Map<String,dynamic> toJson() => _$ResultInfoToJson(this);

}