import 'package:json_annotation/json_annotation.dart';
import 'MessageData.dart';

part 'NotificationMessage.g.dart';
@JsonSerializable(explicitToJson: true)
class NotificationMessage{

 int status=0;
 String message="";
  List<MessageData> data = [];

 NotificationMessage(this.status, this.message, this.data);

 factory NotificationMessage.fromJson(Map<String,dynamic>data) => _$NotificationMessageFromJson(data);


 Map<String,dynamic> toJson() => _$NotificationMessageToJson(this);
}