// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ResultInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResultInfo _$ResultInfoFromJson(Map<String, dynamic> json) {
  return ResultInfo(
    json['resultStatus'] as String,
    json['resultCode'] as String,
    json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$ResultInfoToJson(ResultInfo instance) =>
    <String, dynamic>{
      'resultStatus': instance.resultStatus,
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };
