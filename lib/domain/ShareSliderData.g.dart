// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ShareSliderData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShareSliderData _$ShareSliderDataFromJson(Map<String, dynamic> json) {
  return ShareSliderData(
    json['id'] as String,
    json['image'] as String,
    json['description'] as String,
  );
}

Map<String, dynamic> _$ShareSliderDataToJson(ShareSliderData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'image': instance.image,
      'description': instance.description,
    };
