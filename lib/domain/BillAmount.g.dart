// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BillAmount.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BillAmount _$BillAmountFromJson(Map<String, dynamic> json) {
  return BillAmount(
    json['id'] as String,
    json['billno_prefix'] as String,
    json['bill_no'] as String,
    json['reg_id'] as String,
    json['reg_code'] as String,
    json['product_id'] as String,
    json['sales_type'] as String,
    json['sales_date'] as String,
    json['expe_date'] as String,
    json['amt'] as String,
    json['cgst'] as String,
    json['sgst'] as String,
    json['igst'] as String,
    json['binary_val'] as String,
    json['currency'] as String,
    json['ex_rate'] as String,
    json['rupee_convertion_val'] as String,
    json['sponser_reg_id'] as String,
    json['sponser_reg_code'] as String,
    json['referal_commission_rupee'] as String,
    json['sponser_currency'] as String,
    json['sponser_cur_ex_rate'] as String,
    json['sponser_conversion_value'] as String,
    json['binary_gen_status'] as String,
    json['cash_transaction_id'] as String,
  );
}

Map<String, dynamic> _$BillAmountToJson(BillAmount instance) =>
    <String, dynamic>{
      'id': instance.id,
      'billno_prefix': instance.billno_prefix,
      'bill_no': instance.bill_no,
      'reg_id': instance.reg_id,
      'reg_code': instance.reg_code,
      'product_id': instance.product_id,
      'sales_type': instance.sales_type,
      'sales_date': instance.sales_date,
      'expe_date': instance.expe_date,
      'amt': instance.amt,
      'cgst': instance.cgst,
      'sgst': instance.sgst,
      'igst': instance.igst,
      'binary_val': instance.binary_val,
      'currency': instance.currency,
      'ex_rate': instance.ex_rate,
      'rupee_convertion_val': instance.rupee_convertion_val,
      'sponser_reg_id': instance.sponser_reg_id,
      'sponser_reg_code': instance.sponser_reg_code,
      'referal_commission_rupee': instance.referal_commission_rupee,
      'sponser_currency': instance.sponser_currency,
      'sponser_cur_ex_rate': instance.sponser_cur_ex_rate,
      'sponser_conversion_value': instance.sponser_conversion_value,
      'binary_gen_status': instance.binary_gen_status,
      'cash_transaction_id': instance.cash_transaction_id,
    };
