

import 'package:json_annotation/json_annotation.dart';

part 'Head.g.dart';

@JsonSerializable(explicitToJson: true)
class Head{
   String responseTimestamp="";
 String version="";
  String signature="";

   Head(this.responseTimestamp, this.version, this.signature);

   factory Head.fromJson(Map<String,dynamic>data) => _$HeadFromJson(data);


   Map<String,dynamic> toJson() => _$HeadToJson(this);

}