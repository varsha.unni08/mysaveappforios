import 'package:json_annotation/json_annotation.dart';

import 'Message.dart';

part 'MessageData.g.dart';

@JsonSerializable(explicitToJson: true)
class MessageData{

   String id="";
  String reg_id="";
 String message_id="";
  late Message message;
  String date="";

   MessageData(this.id, this.reg_id, this.message_id, this.message);

    factory MessageData.fromJson(Map<String,dynamic>data) => _$MessageDataFromJson(data);


   Map<String,dynamic> toJson() => _$MessageDataToJson(this);
}