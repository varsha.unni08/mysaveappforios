

import 'package:json_annotation/json_annotation.dart';

part 'NetworkDashboardData.g.dart';

@JsonSerializable(explicitToJson: true)
class NetWorkDashboardData{

   String id="";
   String position="";
  String position_downline_setup_default="";
   String position_next="";
 String binary_left="";
  String binary_right="";
 String binary_matched="";
  String carry_left="";
   String member_status="";
  String carry_right="";
   String binary_amt="";
   String referral_commission_amt="";
   String level_amt="";
  String achievement_amt="";

   NetWorkDashboardData(
      this.id,
      this.position,
      this.position_downline_setup_default,
      this.position_next,
      this.binary_left,
      this.binary_right,
      this.binary_matched,
      this.carry_left,
      this.member_status,
      this.carry_right,
      this.binary_amt,
      this.referral_commission_amt,
      this.level_amt,
      this.achievement_amt);

   factory NetWorkDashboardData.fromJson(Map<String,dynamic>data) => _$NetWorkDashboardDataFromJson(data);


   Map<String,dynamic> toJson() => _$NetWorkDashboardDataToJson(this);


}