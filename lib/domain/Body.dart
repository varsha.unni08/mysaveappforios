import 'ResultInfo.dart';

import 'package:json_annotation/json_annotation.dart';

part 'Body.g.dart';

@JsonSerializable(explicitToJson: true)
class Body{

 late ResultInfo resultInfo;
   String txnToken="";
  bool isPromoCodeValid=false;
  bool authenticated=false;

 Body(this.resultInfo, this.txnToken, this.isPromoCodeValid,
      this.authenticated);

 factory Body.fromJson(Map<String,dynamic>data) => _$BodyFromJson(data);


 Map<String,dynamic> toJson() => _$BodyToJson(this);
}