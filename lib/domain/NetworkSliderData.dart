import 'package:json_annotation/json_annotation.dart';
import 'NetworkSlider.dart';


part 'NetworkSliderData.g.dart';

@JsonSerializable(explicitToJson: true)
class NetworkSliderData{

 int status=0;
 String message="";
  List<NetworkSlider> data = [];

 NetworkSliderData(this.status, this.message, this.data);

 factory NetworkSliderData.fromJson(Map<String,dynamic>data) => _$NetworkSliderDataFromJson(data);


 Map<String,dynamic> toJson() => _$NetworkSliderDataToJson(this);
}