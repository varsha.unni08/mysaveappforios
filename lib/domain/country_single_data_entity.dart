import 'package:saveappforios/generated/json/base/json_field.dart';
import 'package:saveappforios/generated/json/country_single_data_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class CountrySingleDataEntity {

	late int status;
	late String message;
	late CountrySingleDataData data;
  
  CountrySingleDataEntity();

  factory CountrySingleDataEntity.fromJson(Map<String, dynamic> json) => $CountrySingleDataEntityFromJson(json);

  Map<String, dynamic> toJson() => $CountrySingleDataEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class CountrySingleDataData {

	late String id;
	@JSONField(name: "country_name")
	late String countryName;
	@JSONField(name: "country_abr")
	late String countryAbr;
	@JSONField(name: "country_code")
	late String countryCode;
	late String currencyid;
	@JSONField(name: "currency_code")
	late String currencyCode;
  
  CountrySingleDataData();

  factory CountrySingleDataData.fromJson(Map<String, dynamic> json) => $CountrySingleDataDataFromJson(json);

  Map<String, dynamic> toJson() => $CountrySingleDataDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}