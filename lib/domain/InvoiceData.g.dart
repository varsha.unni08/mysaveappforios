// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'InvoiceData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InvoiceData _$InvoiceDataFromJson(Map<String, dynamic> json) {
  return InvoiceData(
    json['status'] as int,
    json['message'] as String,
    BillAmount.fromJson(json['data'] as Map<String, dynamic>),
    SettingsData.fromJson(json['settingsdata'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$InvoiceDataToJson(InvoiceData instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.toJson(),
      'settingsdata': instance.settingsdata.toJson(),
    };
