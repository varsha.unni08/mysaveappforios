

import 'BillAmount.dart';
import 'SettingsData.dart';
import 'package:json_annotation/json_annotation.dart';
part 'InvoiceData.g.dart';
@JsonSerializable(explicitToJson: true)
class InvoiceData{

   int status=0;
   String message="";
  late BillAmount data;

 late SettingsData settingsdata;

   InvoiceData(this.status, this.message, this.data, this.settingsdata);

   factory InvoiceData.fromJson(Map<String,dynamic>data) => _$InvoiceDataFromJson(data);


   Map<String,dynamic> toJson() => _$InvoiceDataToJson(this);
}