// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MessageData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageData _$MessageDataFromJson(Map<String, dynamic> json) {
  return MessageData(
    json['id'] as String,
    json['reg_id'] as String,
    json['message_id'] as String,
    Message.fromJson(json['message'] as Map<String, dynamic>),
  )..date = json['date'] as String;
}

Map<String, dynamic> _$MessageDataToJson(MessageData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'reg_id': instance.reg_id,
      'message_id': instance.message_id,
      'message': instance.message.toJson(),
      'date': instance.date,
    };
