// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Feedbackfulldata.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackFulldata _$FeedbackFulldataFromJson(Map<String, dynamic> json) {
  return FeedbackFulldata(
    json['status'] as int,
    json['message'] as String,
    (json['data'] as List<dynamic>)
        .map((e) => FeedbackList.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$FeedbackFulldataToJson(FeedbackFulldata instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
