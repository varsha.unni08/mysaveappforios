// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Tokendata.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tokendata _$TokendataFromJson(Map<String, dynamic> json) {
  return Tokendata(
    Head.fromJson(json['head'] as Map<String, dynamic>),
    Body.fromJson(json['body'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TokendataToJson(Tokendata instance) => <String, dynamic>{
      'head': instance.head.toJson(),
      'body': instance.body.toJson(),
    };
