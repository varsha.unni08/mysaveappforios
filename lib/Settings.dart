import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:saveappforios/projectconstants/DataConstants.dart';
import 'package:saveappforios/views/PurchaseOrRenewal.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Profilepage.dart';
import 'database/DBTables.dart';
import 'database/DatabaseHelper.dart';
import 'design/ResponsiveInfo.dart';
import 'domain/Profiledata.dart';
import 'login.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:googleapis/drive/v3.dart' as drive;
import 'package:file_picker/file_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class SettingsPage extends StatefulWidget {
  final String title;

  const SettingsPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SettingsPage();
}

class _SettingsPage extends State<SettingsPage> {

  bool _switchValue=false;
  DatabaseHelper dbhelper=new DatabaseHelper();
  Map <String,dynamic> m=new HashMap<String,dynamic>();

  TextEditingController usernamecontroller=new TextEditingController();
  TextEditingController passwordcontroller=new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    setLoginPassWordAvailableOrNot();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }




  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.pop(context),
          ),
          title: Text("Settings",style: TextStyle(fontSize: 14),),
          centerTitle: false,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Stack(alignment: Alignment.topLeft, children: <Widget>[

            // Align(
            Container(
              margin: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(0, 5, 0, 5) :EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 10, 0, 10),

              child:  SingleChildScrollView(

                  child: Column(

                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Card(
                            child:
                            InkWell(


                              child:  Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Profile",
                                          style: TextStyle(fontSize: 12, color: Colors.black)),
                                    ),
                                  )),
                              onTap: (){

                                Navigator.push(
                                    context, MaterialPageRoute(builder: (_) => ProfilePage(title: "profile",)));



                              },
                            )




                        ),
                        Card(
                            child:  InkWell(

                              onTap: (){


                                 showKeypad();
                              },



                              child: Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child:Row(
                                        children: [

                                          Expanded(

                                            child: Text("App lock",
                                                style: TextStyle(fontSize: 12, color: Colors.black)),
                                            flex: 2,
                                          )
                                          ,

                                    Expanded(

                                      child:   CupertinoSwitch(
                                            value: _switchValue,
                                            onChanged: (value) async{

                                              if(value) {
                                                final datacount = await SharedPreferences
                                                    .getInstance();
                                                datacount.setBool(
                                                    DataConstants.isappPin, true);
                                              }
                                              else{

                                                final datacount = await SharedPreferences
                                                    .getInstance();
                                                datacount.setBool(
                                                    DataConstants.isappPin, false);
                                              }
                                              setState(()  {
                                                _switchValue = value;

                                              });



                                            },
                                          ),
                                    flex:1
                                    )
                                        ],


                                      ) ,
                                    ),
                                  )),
                            )
                        ),
                        Card(

                            child:   InkWell(

                              onTap: (){

                                // ProgressDialog _progressDialog = ProgressDialog();
                                // _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

                                showOptions(0);

                              },



                              child: Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Data backup",
                                          style: TextStyle(fontSize: 12, color: Colors.black)),
                                    ),
                                  )),


                            )
                        ),
                        Card(
                            child:   InkWell(

                              onTap: (){

                                DataConstants.checkDBTablempty().then((value) => {

                                  if(!value)
                                    {
                                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                  content: Text("If you select 'Google Drive' ,Use your same Back up account of Google Drive"),
                                )),
                                      showOptions(1)
                                    }
                                  else{

                                    showOptions(1)
                                  }

                                });


                                // ProgressDialog _progressDialog = ProgressDialog();
                                // _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");


                              },



                              child:Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Restore data",
                                          style: TextStyle(fontSize: 12, color: Colors.black)),
                                    ),
                                  )),)
                        ),


//need to add app update feature in ios users

                        // Card(
                        //     child:
                        //     InkWell(
                        //
                        //       onTap: ()async{
                        //         PackageInfo packageInfo = await PackageInfo.fromPlatform();
                        //
                        //         String appName = packageInfo.appName;
                        //         String packageName = packageInfo.packageName;
                        //         String version = packageInfo.version;
                        //         String buildNumber = packageInfo.buildNumber;
                        //
                        //         Dialog forgotpasswordDialog = Dialog(
                        //           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                        //           child: Container(
                        //             height: double.infinity,
                        //             width: double.infinity,
                        //
                        //             decoration: new BoxDecoration(
                        //                 gradient: LinearGradient(
                        //                     begin: Alignment.topCenter,
                        //                     end: Alignment.center,
                        //                     colors: [Color(0xff096c6c), Color(0xff007a74)])),
                        //
                        //             child: Column(
                        //               mainAxisAlignment: MainAxisAlignment.center,
                        //               children: <Widget>[
                        //                 Padding(
                        //                   padding:  EdgeInsets.all(15.0),
                        //                   child: Text("My Save",style: TextStyle(color: Colors.white,fontSize: 15),),
                        //                 ),
                        //                 Padding(
                        //                   padding:  EdgeInsets.all(15.0),
                        //                   child: Center(child:Text("Version : "+version,style: TextStyle(color: Colors.white,fontSize: 15)) ,),
                        //                 ),
                        //                 Padding(
                        //                   padding: EdgeInsets.all(15.0),
                        //
                        //                   child: Container(
                        //
                        //                     width: 150,
                        //                     height: 55,
                        //                     decoration: BoxDecoration(
                        //
                        //                         color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                        //                     child:Align(
                        //                       alignment: Alignment.center,
                        //                       child: TextButton(
                        //
                        //                         onPressed:() async {
                        //
                        //                           String url="https://play.google.com/store/apps/details?id="+packageName;
                        //
                        //
                        //                           if (await canLaunch(url)) {
                        //                             await launch(url, forceWebView: true);
                        //                           } else {
                        //                             throw 'Could not launch $url';
                        //                           }
                        //
                        //
                        //                         },
                        //
                        //                         child: Text('Update', style: TextStyle(color: Colors.white,fontSize: 15) ,) ,),
                        //                     ),
                        //
                        //
                        //
                        //                     //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                        //                   ),
                        //
                        //
                        //                   // ,
                        //                 ),
                        //                 // Padding(padding: EdgeInsets.only(top: 50.0)),
                        //                 // TextButton(onPressed: () {
                        //                 //   Navigator.of(context).pop();
                        //                 // },
                        //                 //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                        //               ],
                        //             ),
                        //           ),
                        //         );
                        //
                        //
                        //
                        //         showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);
                        //
                        //       },
                        //
                        //
                        //
                        //
                        //       child: Container(
                        //           width: double.infinity,
                        //           height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                        //           child: Padding(
                        //             padding: EdgeInsets.all(10),
                        //             child: Align(
                        //               alignment: Alignment.centerLeft,
                        //               child: Text("App update",
                        //                   style: TextStyle(fontSize: 12, color: Colors.black)),
                        //             ),
                        //           )),)
                        // ),







                        // Card(
                        //
                        //   child:InkWell(
                        //     child: Container(
                        //         width: double.infinity,
                        //         height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                        //         child: Padding(
                        //           padding: EdgeInsets.all(10),
                        //           child: Align(
                        //             alignment: Alignment.centerLeft,
                        //             child: Text("Purchase or renewal",
                        //                 style: TextStyle(fontSize: 12, color: Colors.black)),
                        //           ),
                        //         )),
                        //
                        //     onTap: (){
                        //
                        //
                        //       Navigator.push(
                        //           context, MaterialPageRoute(builder: (_) => PurchasePage(title: "Purchase or Renewal",)));
                        //     },
                        //
                        //
                        //   )
                        //
                        //
                        //   ,
                        // ),
                        Card(
                            child:  InkWell(


                              child:  Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Logout",
                                          style: TextStyle(fontSize: 12, color: Colors.black)),
                                    ),
                                  )),
                              onTap: (){

                                Widget yesButton = TextButton(
                                    child: Text("Yes"),
                                    onPressed: () async {

                                      final datacount = await SharedPreferences.getInstance();

                                      datacount.setString(DataConstants.userkey, "");

                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(builder: (BuildContext context) => Loginpage(title: "login",)),
                                          ModalRoute.withName('/')
                                      );



                                    });



                                Widget noButton = TextButton(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                );

                                // set up the AlertDialog
                                AlertDialog alert = AlertDialog(
                                  title: Text("Save"),
                                  content: Text("Do you want to logout now ?"),
                                  actions: [yesButton, noButton],
                                );

                                // show the dialog
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return alert;
                                  },
                                );



                              },
                            )
                        ),

                        Card(
                            child:  InkWell(


                              child:  Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?50:60:80   ,
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Delete account",
                                          style: TextStyle(fontSize: 12, color: Colors.black)),
                                    ),
                                  )),
                              onTap: (){




                                Dialog forgotpasswordDialog = Dialog(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                                  child: Container(
                                    height: height/1.6,
                                    width: width,

                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                    Padding(
                                    padding:  EdgeInsets.all(10.0),
                                    child: new Theme(data: new ThemeData(
                                        hintColor: Colors.black38
                                    ), child: Text("Demo account/App License deletion \n\n",style: TextStyle(fontSize: ResponsiveInfo.isSmallMobile(context)
                                        ? ResponsiveInfo.isMobile(context) ? 14:17:20,fontWeight: FontWeight.bold,color: Colors.black),

                                    )),
                                  )

                                        ,
                                        Padding(
                                          padding:  EdgeInsets.all(10.0),
                                          child: new Theme(data: new ThemeData(
                                              hintColor: Colors.black38
                                          ), child: Text("If deleted , all your credentials will be removed from the database and you cannot login to the app and website anymore\n\nDo you want to contitnue ?",

                                          )),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(15.0),

                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [

                                              Container(



                                                child:Align(
                                                  alignment: Alignment.center,
                                                  child: TextButton(

                                                    onPressed:() {

                                                      Navigator.of(
                                                          context, rootNavigator: true)
                                                          .pop();


                                                      showDeleteConfirmationDialog();




                                                    },

                                                    child: Text('Yes', style: TextStyle(color: Colors.blue) ,) ,),
                                                ),



                                                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                              ),

                                              Container(

                                                child:Align(
                                                  alignment: Alignment.center,
                                                  child: TextButton(

                                                    onPressed:() {

                                                      Navigator.of(
                                                          context, rootNavigator: true)
                                                          .pop();



                                                    },

                                                    child: Text('No', style: TextStyle(color: Colors.blue
                                                    ) ,) ,),
                                                ),



                                                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                              )
                                            ],


                                          )






                                          // ,
                                        ),
                                        // Padding(padding: EdgeInsets.only(top: 50.0)),
                                        // TextButton(onPressed: () {
                                        //   Navigator.of(context).pop();
                                        // },
                                        //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                                      ],
                                    ),
                                  ),
                                );



                                showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);



                                // Widget yesButton = TextButton(
                                //     child: Text("Yes"),
                                //     onPressed: () async {
                                //
                                //
                               //
                                //
                                //
                                //
                                //
                                //
                                //
                                //
                                //     });
                                //
                                //
                                //
                                // Widget noButton = TextButton(
                                //   child: Text("No"),
                                //   onPressed: () {
                                //     Navigator.pop(context);
                                //   },
                                // );
                                //
                                // // set up the AlertDialog
                                // AlertDialog alert = AlertDialog(
                                //   title: Text("Save"),
                                //   content: Text("Do you want to delete you account now ?"),
                                //   actions: [yesButton, noButton],
                                // );
                                //
                                // // show the dialog
                                // showDialog(
                                //   context: context,
                                //   builder: (BuildContext context) {
                                //     return alert;
                                //   },
                                // );



                              },
                            )
                        )
                      ])),
            )
            //)
          ]),
        ));

  }



  showDeleteConfirmationDialog()
  {

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    Dialog forgotpasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: height/1.6,
        width: width,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: Text("Confirm your account ?",style: TextStyle(fontSize: 13,color: Colors.black,fontWeight: FontWeight.bold),),
            ),
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: usernamecontroller,
                keyboardType: TextInputType.number,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Mobile number',


                ),
              )),
            ),
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: passwordcontroller,
               obscureText: true,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Password',


                ),
              )),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      Navigator.pop(context);


                      userLogin(usernamecontroller.text.toString(), passwordcontroller.text.toString());





                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),
            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
    );



    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);

  }


  userLogin(String username,String password) async {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(DataConstants.baseurl+DataConstants.UserLogin),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        }, body: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      'mobile': username,
      'password': password,
      'uuid': date,
      'timestamp': date
    }

    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    int status = jsondata['status'];

    if(status!=0)
      {


        ProgressDialog _progressDialog = ProgressDialog();
        _progressDialog.showProgressDialog(
            context, textToBeDisplayed: "Please wait for a moment......");
        final datacount = await SharedPreferences.getInstance();
        var date = new DateTime.now().toIso8601String();
        var dataasync = await http.get(
          Uri.parse(DataConstants.baseurl +
              DataConstants.deleteAccount +
              "?timestamp=" +
              date.toString()),
          headers: <String, String>{
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': datacount.getString(DataConstants.userkey)!
          },
        );
        _progressDialog.dismissProgressDialog(context);
        String response = dataasync.body;

        print(response);
        var json = jsonDecode(response);

        if(json['status']==1)
        {

          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(
            content: Text("Account deleted "),
          ));
          datacount.setString(DataConstants.userkey, "");

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (BuildContext context) => Loginpage(title: "login",)),
              ModalRoute.withName('/')
          );

        }

        else{

          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(
            content: Text("Account deleted "),
          ));

        }


      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Account confirmation failed"),
      ));


    }


  }






  setLoginPassWordAvailableOrNot() async
  {
    final datacount = await SharedPreferences
        .getInstance();
   bool?  v= datacount.getBool(
        DataConstants.isappPin);

    setState(() {

      if(v!=null) {
        _switchValue = v;
      }
    });
  }



  typePin(String pin)async
  {

    List<Map<String,dynamic>>allaccountsettings=await dbhelper.queryAllRows(DatabaseTables.TABLE_APP_PIN);

    String data="";
    int id=0;

    for (Map ab in allaccountsettings) {
      print(ab);

      id = ab["keyid"];
      data = ab["data"];

    }

    if(allaccountsettings.length<=0) {
      m['data'] = pin;

      dbhelper.insert(m, DatabaseTables.TABLE_APP_PIN);
    }
    else{
      m['data'] = pin;
      dbhelper.update(m, DatabaseTables.TABLE_APP_PIN, id.toString());

    }
  }

  showKeypad(){

    showDialog(
        context: context,
        builder: (_) {
          return MyDialog();
        }).then((value) => {






      typePin(value['data'])






      // selectedmonth=value["selecteddata"].toString().split(",")[0],
      // selectedyear=value["selecteddata"].toString().split(",")[1],
      //
      //
      //
      //
      //
      //
      // showAccountDetails()




    });

  }


  showOptions(int code) {
    List<String> colorList = ['Google drive', 'Server'];


    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: (code == 0) ? Text('Back up data to ') : Text(
                "Restore data from"),
            content: Container(
              width: double.minPositive,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: colorList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(colorList[index]),
                    onTap: () {

                      if(code==0)
                      {


                        Navigator.pop(context);


                        prepareDataBackup(code,index);




                      }
                      else{

                        Navigator.pop(context);


                        getDataBackup(code,index);

                      }



                    },
                  );
                },
              ),
            ),
          );
        });
  }





  getDataBackup(int code,int index)
  {

    if(index==0)
    {
      getProfile(1);
    }
    else{

      getProfile(0);

    }

  }









  getBackupFromGoogleDrive(String fileid) async
  {

    String filename=new DateTime.now().millisecond.toString()+".json";



    final GoogleSignIn googleSignIn = GoogleSignIn( scopes: [
      'email',
      'https://www.googleapis.com/auth/drive.file'

    ],clientId: "458214687015-el4rrn8pke6c6g6e04oglgu5a4eb6jsu.apps.googleusercontent.com");

    final GoogleSignInAccount? googleSignInAccount =
    await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount!.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    var client = GoogleHttpClient(await googleSignInAccount!.authHeaders);
    var dri = drive.DriveApi(client);
    drive.Media file = await dri.files.get( fileid, downloadOptions: drive.DownloadOptions.fullMedia) as drive.Media;

    // var response = await dri.files.delete(fileId)

    final directory = await getApplicationDocumentsDirectory();
    print(directory.path);
    final saveFile = File('${directory.path}/${new DateTime.now().millisecondsSinceEpoch}$filename');

    List<int> dataStore = [];

    if(!saveFile.existsSync())
      {
        saveFile.create();

      }



    file.stream.listen((data) {
      print("DataReceived: ${data.length}");
      dataStore.insertAll(dataStore.length, data);
    }, onDone: () async {
      print("Task Done");
      saveFile.writeAsBytes(dataStore);
      print("File saved at ${saveFile.path}");

      // Share.shareFiles([saveFile.path],
      //     text: "File from google drive");

      // OpenFile.open(saveFile.path);

      String data=await saveFile.readAsString();


      var d=json.decode(data);

      List<dynamic>listdata=d;

      DatabaseHelper dbhelper=new DatabaseHelper();

      for(int i=0;i<listdata.length;i++)
      {

        if(i==0) {
          String m = listdata[i];

          dynamic accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_ACCOUNTS);
          }


          print("Account data : " + accountdata.toString());

        }

        else  if(i==1) {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.DIARYSUBJECT_table);
          }


          print("Account data : " + accountdata.toString());

        }
        else if(i==2)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.DIARY_table);
          }


          print("Account data : " + accountdata.toString());
        }
        else if(i==3)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_BUDGET);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==4)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.INVESTMENT_table);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==5)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_TASK);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==6)
        {

          dbhelper.deleteAll(DatabaseTables.TABLE_ACCOUNTSETTINGS);
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;



          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_ACCOUNTSETTINGS);
          }


          print("Account data : " + accountdata.toString());
        }
        else if(i==7)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_INSURANCE);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==8)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_LIABILITY);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==9)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_ASSET);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==10)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_ACCOUNTS_RECEIPT);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==11)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_APP_PIN);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==12)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_WALLET);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==13)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_DOCUMENT);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==14)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_PASSWORD);
          }


          print("Account data : " + accountdata.toString());
        }

        else if(i==15)
        {
          String m = listdata[i];

          var accountdata = json.decode(m);

          List<dynamic>dd=accountdata;

          for(Map<String,dynamic> sd in dd )
          {
            dbhelper.insert(sd, DatabaseTables.TABLE_VISITCARD);
          }


          print("Account data : " + accountdata.toString());
        }


      }






// print(data);















    }, onError: (error) {
      print("Some Error");
    });




  }










  getProfile(int code)async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getUserDetails +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;

    print(response);
    var json = jsonDecode(response);

    Profiledata profile=Profiledata.fromJson(json);

    if(profile.status==1)
    {

      String userid=  profile.data.id;

      if(code==0) {
        String downloadurl = DataConstants.backupbaseurl + userid.toString() +
            ".json";
        getDataFromServer(downloadurl);
      }
      else{

        String googledrivefileid=profile.data.gdrive_fileid;

        getBackupFromGoogleDrive(googledrivefileid);



      }

    }

  }








  getDataFromServer(String url) async
  {
    DatabaseHelper db=new DatabaseHelper();
    var dataasync = await http.get(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json',

      },
    );
    String response = dataasync.body;

    print("Response from server : "+response);


    List<dynamic> list = json.decode(response);

    for(int i=0;i<list.length;i++)
    {
      if(i==0)
      {


        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_ACCOUNTS);

        }


      }
      else if(i==1)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.DIARYSUBJECT_table);

        }

      }
      else if(i==2)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.DIARY_table);

        }

      }

      else if(i==3)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_BUDGET);

        }

      }

      else if(i==4)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.INVESTMENT_table);

        }

      }

      else if(i==5)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_TASK);

        }

      }

      else if(i==6)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_ACCOUNTSETTINGS);

        }

      }

      else if(i==7)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_INSURANCE);

        }

      }

      else if(i==8)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_LIABILITY);

        }

      }

      else if(i==9)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_ASSET);

        }

      }

      else if(i==10)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_ACCOUNTS_RECEIPT);

        }

      }

      else if(i==11)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_APP_PIN);

        }

      }

      else if(i==12)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_WALLET);

        }

      }

      else if(i==13)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_DOCUMENT);

        }

      }

      else if(i==14)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_PASSWORD);

        }

      }

      else if(i==15)
      {
        List<dynamic> li = json.decode(list[i]);
        print(li.length.toString()+"");

        for(int i=0;i<li.length;i++)
        {
          var a=li[i];

          Map<String,dynamic>ab=a;
          db.insert(ab, DatabaseTables.TABLE_VISITCARD);

        }

      }


    }



  }










  prepareDataBackup(int code,int index) async
  {


    List<dynamic>jsonarraydata=[];

    DatabaseHelper dbhelper=new DatabaseHelper();

    var dbdata=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);

    var jsaccount=json.encode(dbdata);
    jsonarraydata.add(jsaccount);

    var db_diary=await dbhelper.queryAllRows(DatabaseTables.DIARYSUBJECT_table);
    var jsdiary=json.encode(db_diary);
    jsonarraydata.add(jsdiary);

    var db_diarydata=await dbhelper.queryAllRows(DatabaseTables.DIARY_table);
    var jsdiarydata=json.encode(db_diarydata);
    jsonarraydata.add(jsdiarydata);
    var db_budget=await dbhelper.queryAllRows(DatabaseTables.TABLE_BUDGET);
    var jsdb_budget=json.encode(db_budget);
    jsonarraydata.add(jsdb_budget);

    var db_investment=await dbhelper.queryAllRows(DatabaseTables.INVESTMENT_table);
    var jsdb_investment=json.encode(db_investment);
    jsonarraydata.add(jsdb_investment);

    var db_task=await dbhelper.queryAllRows(DatabaseTables.TABLE_TASK);
    var jsdb_task=json.encode(db_task);
    jsonarraydata.add(jsdb_task);

    var db_accsettings=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    var jsdb_accsettings=json.encode(db_accsettings);
    jsonarraydata.add(jsdb_accsettings);

    var db_insurance=await dbhelper.queryAllRows(DatabaseTables.TABLE_INSURANCE);
    var jsdb_insurance=json.encode(db_insurance);
    jsonarraydata.add(jsdb_insurance);


    var db_liability=await dbhelper.queryAllRows(DatabaseTables.TABLE_LIABILITY);
    var jsdb_liability=json.encode(db_liability);
    jsonarraydata.add(jsdb_liability);

    var db_asset=await dbhelper.queryAllRows(DatabaseTables.TABLE_ASSET);
    var jsdb_asset=json.encode(db_asset);
    jsonarraydata.add(jsdb_asset);


    var db_recipt=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS_RECEIPT);
    var jsdb_recipt=json.encode(db_recipt);
    jsonarraydata.add(jsdb_recipt);



    var db_apppin=await dbhelper.queryAllRows(DatabaseTables.TABLE_APP_PIN);
    var jsdb_apppin=json.encode(db_apppin);
    jsonarraydata.add(jsdb_apppin);

    var db_wallet=await dbhelper.queryAllRows(DatabaseTables.TABLE_WALLET);
    var jsdb_wallet=json.encode(db_wallet);
    jsonarraydata.add(jsdb_wallet);


    var db_document=await dbhelper.queryAllRows(DatabaseTables.TABLE_DOCUMENT);
    var jsdb_document=json.encode(db_document);
    jsonarraydata.add(jsdb_document);

    var db_password=await dbhelper.queryAllRows(DatabaseTables.TABLE_PASSWORD);
    var jsdb_password=json.encode(db_password);
    jsonarraydata.add(jsdb_password);

    var db_visitcard=await dbhelper.queryAllRows(DatabaseTables.TABLE_VISITCARD);
    var jsdb_visitcard=json.encode(db_visitcard);
    jsonarraydata.add(jsdb_visitcard);


    var date = new DateTime.now().millisecond.toString();


    var jsonarraydata_=json.encode(jsonarraydata);
    print(jsonarraydata_);

    Directory tempDir = await getApplicationDocumentsDirectory();
    String tempPath = tempDir.path;
    var filePath = tempPath + '/'+date+'.json';

    File f= await  new File(filePath).create();


    f.writeAsStringSync(jsonarraydata_);


    bool fileExists = await f.existsSync();

    if(fileExists)
    {
      print("the file exists");

      if(index==0)
      {

        uploadFileToGoogleDrive(f.path);

      }
      else{


        uploadFileToServer(f.path);

      }
    }


    // var mjobject=new Map();
    // mjobject['Accountname']=s;
    // mjobject['Accounttype']="Expense account";
    // mjobject['Amount']="0";
    // mjobject['Type']="Debit";

    //




  }



  uploadFileToGoogleDrive(String path) async
  {
    File file=new File(path);
    final GoogleSignIn googleSignIn = GoogleSignIn( scopes: [
      'email',
      'https://www.googleapis.com/auth/drive.file'

    ],clientId: "458214687015-el4rrn8pke6c6g6e04oglgu5a4eb6jsu.apps.googleusercontent.com");
    final GoogleSignInAccount? googleSignInAccount =
    await googleSignIn.signIn();
    var client = GoogleHttpClient(await googleSignInAccount!.authHeaders);
    var dri = drive.DriveApi(client);
    drive.File fileToUpload = drive.File();
    fileToUpload.parents = ["root"];
    fileToUpload.mimeType="application/json";
    var response = await dri.files.create(
      fileToUpload,
      uploadMedia: drive.Media(file.openRead(), file.lengthSync()),
    );
    print(response.id);


    updateFileToGoogleDrive(response.id.toString());

  }



  updateFileToGoogleDrive(String fileid) async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
        Uri.parse(DataConstants.baseurl +
            DataConstants.updateGoogleDriveFileId +
            "?timestamp=" +
            date.toString()),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!
        },
        body: <String, String>{
          "gdrive_fileid":fileid,

          "timestamp":date
        }
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);
  }






  uploadFileToServer(String path)async
  {

    File file=new File(path);
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");


    final datacount = await SharedPreferences.getInstance();
    var request = http.MultipartRequest('POST', Uri.parse(DataConstants.baseurl+DataConstants.uploadBackupFile));
    request.headers.addAll( { "Authorization": datacount.getString(DataConstants.userkey)!,
      'Content-Type': 'application/x-www-form-urlencoded'});


    request.files.add(
        http.MultipartFile.fromBytes(
            'file',
            File(file.path).readAsBytesSync(),
            filename: file.path.split("/").last
        )
    );

    _progressDialog.dismissProgressDialog(context);
    var res = await request.send();
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    print(responseString);
  }

  }
class GoogleHttpClient extends http.BaseClient {
  Map<String, String> _headers;
  GoogleHttpClient(this._headers) : super();



  final http.Client _client = new http.Client();



  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return _client.send(request..headers.addAll(_headers));
  }


}

class MyDialog extends StatefulWidget {



  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  TextEditingController namecontroller=new TextEditingController();

  List<int> arr=[1,2,3,4,5,6,7,8,9,-1,0,-2];

  DatabaseHelper dbhelper=new DatabaseHelper();
  String data="";
  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

    // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    month=now.month.toString();
    year=now.year.toString();





    setState(() {
      int monthnumber=int.parse(month);
      currentmonthindex=monthnumber-1;

      currentmonth=months[monthnumber-1];
      currentyear=year;

      currentyearnumber=int.parse(currentyear);
    });

    showExistPin();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;

    double height = MediaQuery.of(context).size.height;





    return AlertDialog(
      title:  Text(
          "App lock"),
      content: Container(
        width: width,
        height: height,
        child: Column(

          children: [


            Padding(
              padding: const EdgeInsets.only(left:5.0,right: 5.0,top:5,bottom: 0),
              // padding: EdgeInsets.all(15),
              child: new Theme(data: new ThemeData(
                hintColor: Colors.black45,

              ), child: TextField(

                controller: namecontroller,
                keyboardType: TextInputType.none,
                maxLength: 6,
                obscureText: true,
                maxLines: 1,



                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width: 0.5),
                  ),
                  hintText: 'Enter 6 digit PIN',




                ),




              )),
            ),

            Padding(padding: EdgeInsets.all(5),
              child: GridView.builder(
                physics: BouncingScrollPhysics(),

                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 0.0,
                    childAspectRatio:1

                ),
                itemCount: arr.length,
                itemBuilder: (context, index) {
                  return(arr[index]==-2) ?
                  Padding(padding: EdgeInsets.all(5),

                    child: Container(

                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black45),borderRadius:    BorderRadius.all(
                            Radius.circular(5.0) //                 <--- border radius here
                        ),
                        ),width:60,height:60,

                        child:InkWell(
                          child:new Icon(Icons.keyboard_backspace,size: 40,),

                          onTap: (){

                            String a=namecontroller.text.toString();

                            String b=  a.substring(0,a.length-1);
                            namecontroller.text=b;


                          },



                        )



                    ),

                  )


                      : Padding(padding: EdgeInsets.all(5),child:Container(
                    child:InkWell(
                      child: Center(child:Text(arr[index].toString())),

                      onTap: (){

                        setState(() {
                          if(namecontroller.text.length<6) {
                            String a = namecontroller.text.toString();

                            a = a + arr[index].toString();

                            namecontroller.text = a;
                          }
                        });





                      },
                    ),

                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black45),borderRadius:    BorderRadius.all(
                        Radius.circular(5.0) //                 <--- border radius here
                    ),
                    ),width:60,height:60,

                  ));
                },
              ),),


            Padding(padding: EdgeInsets.all(10),

              child: TextButton(onPressed: () {


                if(namecontroller.text.toString().isNotEmpty)
                {



                  Navigator.of(context).pop({'data':namecontroller.text.toString()});
                }



              }, child: Center(
                child:Text("Submit",style: TextStyle(fontSize: 14),) ,
              ) ,),

            )


          ],


        ),
      ),
    );
  }


  showExistPin()async
  {
    List<Map<String,dynamic>>allaccountsettings=await dbhelper.queryAllRows(DatabaseTables.TABLE_APP_PIN);
    String dt ="";

    for (Map ab in allaccountsettings) {
      print(ab);

      int id = ab["keyid"];
      dt = ab["data"];

    }

    setState(() {

      namecontroller.text=dt;
    });
  }


}