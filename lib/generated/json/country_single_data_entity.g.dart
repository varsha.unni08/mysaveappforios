import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/country_single_data_entity.dart';

CountrySingleDataEntity $CountrySingleDataEntityFromJson(Map<String, dynamic> json) {
	final CountrySingleDataEntity countrySingleDataEntity = CountrySingleDataEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		countrySingleDataEntity.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		countrySingleDataEntity.message = message;
	}
	final CountrySingleDataData? data = jsonConvert.convert<CountrySingleDataData>(json['data']);
	if (data != null) {
		countrySingleDataEntity.data = data;
	}
	return countrySingleDataEntity;
}

Map<String, dynamic> $CountrySingleDataEntityToJson(CountrySingleDataEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] = entity.data.toJson();
	return data;
}

CountrySingleDataData $CountrySingleDataDataFromJson(Map<String, dynamic> json) {
	final CountrySingleDataData countrySingleDataData = CountrySingleDataData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		countrySingleDataData.id = id;
	}
	final String? countryName = jsonConvert.convert<String>(json['country_name']);
	if (countryName != null) {
		countrySingleDataData.countryName = countryName;
	}
	final String? countryAbr = jsonConvert.convert<String>(json['country_abr']);
	if (countryAbr != null) {
		countrySingleDataData.countryAbr = countryAbr;
	}
	final String? countryCode = jsonConvert.convert<String>(json['country_code']);
	if (countryCode != null) {
		countrySingleDataData.countryCode = countryCode;
	}
	final String? currencyid = jsonConvert.convert<String>(json['currencyid']);
	if (currencyid != null) {
		countrySingleDataData.currencyid = currencyid;
	}
	final String? currencyCode = jsonConvert.convert<String>(json['currency_code']);
	if (currencyCode != null) {
		countrySingleDataData.currencyCode = currencyCode;
	}
	return countrySingleDataData;
}

Map<String, dynamic> $CountrySingleDataDataToJson(CountrySingleDataData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['country_name'] = entity.countryName;
	data['country_abr'] = entity.countryAbr;
	data['country_code'] = entity.countryCode;
	data['currencyid'] = entity.currencyid;
	data['currency_code'] = entity.currencyCode;
	return data;
}