import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/SalesinfoData.dart';
import 'package:json_annotation/json_annotation.dart';


SalesinfoData $SalesinfoDataFromJson(Map<String, dynamic> json) {
	final SalesinfoData salesinfoData = SalesinfoData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		salesinfoData.id = id;
	}
	final String? billno_prefix = jsonConvert.convert<String>(json['billno_prefix']);
	if (billno_prefix != null) {
		salesinfoData.billno_prefix = billno_prefix;
	}
	final String? bill_no = jsonConvert.convert<String>(json['bill_no']);
	if (bill_no != null) {
		salesinfoData.bill_no = bill_no;
	}
	final String? reg_id = jsonConvert.convert<String>(json['reg_id']);
	if (reg_id != null) {
		salesinfoData.reg_id = reg_id;
	}
	final String? reg_code = jsonConvert.convert<String>(json['reg_code']);
	if (reg_code != null) {
		salesinfoData.reg_code = reg_code;
	}
	final String? product_id = jsonConvert.convert<String>(json['product_id']);
	if (product_id != null) {
		salesinfoData.product_id = product_id;
	}
	final String? sales_type = jsonConvert.convert<String>(json['sales_type']);
	if (sales_type != null) {
		salesinfoData.sales_type = sales_type;
	}
	final String? sales_date = jsonConvert.convert<String>(json['sales_date']);
	if (sales_date != null) {
		salesinfoData.sales_date = sales_date;
	}
	final String? expe_date = jsonConvert.convert<String>(json['expe_date']);
	if (expe_date != null) {
		salesinfoData.expe_date = expe_date;
	}
	final String? amt = jsonConvert.convert<String>(json['amt']);
	if (amt != null) {
		salesinfoData.amt = amt;
	}
	final String? cgst = jsonConvert.convert<String>(json['cgst']);
	if (cgst != null) {
		salesinfoData.cgst = cgst;
	}
	final String? sgst = jsonConvert.convert<String>(json['sgst']);
	if (sgst != null) {
		salesinfoData.sgst = sgst;
	}
	final String? igst = jsonConvert.convert<String>(json['igst']);
	if (igst != null) {
		salesinfoData.igst = igst;
	}
	final String? binary_val = jsonConvert.convert<String>(json['binary_val']);
	if (binary_val != null) {
		salesinfoData.binary_val = binary_val;
	}
	final String? currency = jsonConvert.convert<String>(json['currency']);
	if (currency != null) {
		salesinfoData.currency = currency;
	}
	final String? ex_rate = jsonConvert.convert<String>(json['ex_rate']);
	if (ex_rate != null) {
		salesinfoData.ex_rate = ex_rate;
	}
	final String? rupee_convertion_val = jsonConvert.convert<String>(json['rupee_convertion_val']);
	if (rupee_convertion_val != null) {
		salesinfoData.rupee_convertion_val = rupee_convertion_val;
	}
	final String? sponser_reg_id = jsonConvert.convert<String>(json['sponser_reg_id']);
	if (sponser_reg_id != null) {
		salesinfoData.sponser_reg_id = sponser_reg_id;
	}
	final String? sponser_reg_code = jsonConvert.convert<String>(json['sponser_reg_code']);
	if (sponser_reg_code != null) {
		salesinfoData.sponser_reg_code = sponser_reg_code;
	}
	final String? referal_commission_rupee = jsonConvert.convert<String>(json['referal_commission_rupee']);
	if (referal_commission_rupee != null) {
		salesinfoData.referal_commission_rupee = referal_commission_rupee;
	}
	final String? sponser_currency = jsonConvert.convert<String>(json['sponser_currency']);
	if (sponser_currency != null) {
		salesinfoData.sponser_currency = sponser_currency;
	}
	final String? sponser_cur_ex_rate = jsonConvert.convert<String>(json['sponser_cur_ex_rate']);
	if (sponser_cur_ex_rate != null) {
		salesinfoData.sponser_cur_ex_rate = sponser_cur_ex_rate;
	}
	final String? sponser_conversion_value = jsonConvert.convert<String>(json['sponser_conversion_value']);
	if (sponser_conversion_value != null) {
		salesinfoData.sponser_conversion_value = sponser_conversion_value;
	}
	final String? binary_gen_status = jsonConvert.convert<String>(json['binary_gen_status']);
	if (binary_gen_status != null) {
		salesinfoData.binary_gen_status = binary_gen_status;
	}
	final String? cash_transaction_id = jsonConvert.convert<String>(json['cash_transaction_id']);
	if (cash_transaction_id != null) {
		salesinfoData.cash_transaction_id = cash_transaction_id;
	}
	return salesinfoData;
}

Map<String, dynamic> $SalesinfoDataToJson(SalesinfoData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['billno_prefix'] = entity.billno_prefix;
	data['bill_no'] = entity.bill_no;
	data['reg_id'] = entity.reg_id;
	data['reg_code'] = entity.reg_code;
	data['product_id'] = entity.product_id;
	data['sales_type'] = entity.sales_type;
	data['sales_date'] = entity.sales_date;
	data['expe_date'] = entity.expe_date;
	data['amt'] = entity.amt;
	data['cgst'] = entity.cgst;
	data['sgst'] = entity.sgst;
	data['igst'] = entity.igst;
	data['binary_val'] = entity.binary_val;
	data['currency'] = entity.currency;
	data['ex_rate'] = entity.ex_rate;
	data['rupee_convertion_val'] = entity.rupee_convertion_val;
	data['sponser_reg_id'] = entity.sponser_reg_id;
	data['sponser_reg_code'] = entity.sponser_reg_code;
	data['referal_commission_rupee'] = entity.referal_commission_rupee;
	data['sponser_currency'] = entity.sponser_currency;
	data['sponser_cur_ex_rate'] = entity.sponser_cur_ex_rate;
	data['sponser_conversion_value'] = entity.sponser_conversion_value;
	data['binary_gen_status'] = entity.binary_gen_status;
	data['cash_transaction_id'] = entity.cash_transaction_id;
	return data;
}