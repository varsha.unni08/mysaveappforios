import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Tokendata.dart';
import 'package:json_annotation/json_annotation.dart';

import 'Head.dart';

import 'Body.dart';


Tokendata $TokendataFromJson(Map<String, dynamic> json) {
	final Tokendata tokendata = Tokendata();
	final Head? head = jsonConvert.convert<Head>(json['head']);
	if (head != null) {
		tokendata.head = head;
	}
	final Body? body = jsonConvert.convert<Body>(json['body']);
	if (body != null) {
		tokendata.body = body;
	}
	return tokendata;
}

Map<String, dynamic> $TokendataToJson(Tokendata entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['head'] = entity.head.toJson();
	data['body'] = entity.body.toJson();
	return data;
}