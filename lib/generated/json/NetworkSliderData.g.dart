import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/NetworkSliderData.dart';
import 'package:json_annotation/json_annotation.dart';

import 'NetworkSlider.dart';


NetworkSliderData $NetworkSliderDataFromJson(Map<String, dynamic> json) {
	final NetworkSliderData networkSliderData = NetworkSliderData();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		networkSliderData.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		networkSliderData.message = message;
	}
	final List<NetworkSlider>? data = jsonConvert.convertListNotNull<NetworkSlider>(json['data']);
	if (data != null) {
		networkSliderData.data = data;
	}
	return networkSliderData;
}

Map<String, dynamic> $NetworkSliderDataToJson(NetworkSliderData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}