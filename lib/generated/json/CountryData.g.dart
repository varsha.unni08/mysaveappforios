import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/CountryData.dart';
import 'package:json_annotation/json_annotation.dart';


CountryData $CountryDataFromJson(Map<String, dynamic> json) {
	final CountryData countryData = CountryData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		countryData.id = id;
	}
	final String? country_name = jsonConvert.convert<String>(json['country_name']);
	if (country_name != null) {
		countryData.country_name = country_name;
	}
	final String? country_abr = jsonConvert.convert<String>(json['country_abr']);
	if (country_abr != null) {
		countryData.country_abr = country_abr;
	}
	final String? country_code = jsonConvert.convert<String>(json['country_code']);
	if (country_code != null) {
		countryData.country_code = country_code;
	}
	final String? currencyid = jsonConvert.convert<String>(json['currencyid']);
	if (currencyid != null) {
		countryData.currencyid = currencyid;
	}
	final String? currency_code = jsonConvert.convert<String>(json['currency_code']);
	if (currency_code != null) {
		countryData.currency_code = currency_code;
	}
	return countryData;
}

Map<String, dynamic> $CountryDataToJson(CountryData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['country_name'] = entity.country_name;
	data['country_abr'] = entity.country_abr;
	data['country_code'] = entity.country_code;
	data['currencyid'] = entity.currencyid;
	data['currency_code'] = entity.currency_code;
	return data;
}