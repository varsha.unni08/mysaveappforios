import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/NetworkSlider.dart';
import 'package:json_annotation/json_annotation.dart';


NetworkSlider $NetworkSliderFromJson(Map<String, dynamic> json) {
	final NetworkSlider networkSlider = NetworkSlider();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		networkSlider.id = id;
	}
	final String? imagepath = jsonConvert.convert<String>(json['imagepath']);
	if (imagepath != null) {
		networkSlider.imagepath = imagepath;
	}
	final String? status = jsonConvert.convert<String>(json['status']);
	if (status != null) {
		networkSlider.status = status;
	}
	return networkSlider;
}

Map<String, dynamic> $NetworkSliderToJson(NetworkSlider entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['imagepath'] = entity.imagepath;
	data['status'] = entity.status;
	return data;
}