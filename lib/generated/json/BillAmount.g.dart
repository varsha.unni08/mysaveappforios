import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/BillAmount.dart';
import 'package:json_annotation/json_annotation.dart';


BillAmount $BillAmountFromJson(Map<String, dynamic> json) {
	final BillAmount billAmount = BillAmount();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		billAmount.id = id;
	}
	final String? billno_prefix = jsonConvert.convert<String>(json['billno_prefix']);
	if (billno_prefix != null) {
		billAmount.billno_prefix = billno_prefix;
	}
	final String? bill_no = jsonConvert.convert<String>(json['bill_no']);
	if (bill_no != null) {
		billAmount.bill_no = bill_no;
	}
	final String? reg_id = jsonConvert.convert<String>(json['reg_id']);
	if (reg_id != null) {
		billAmount.reg_id = reg_id;
	}
	final String? reg_code = jsonConvert.convert<String>(json['reg_code']);
	if (reg_code != null) {
		billAmount.reg_code = reg_code;
	}
	final String? product_id = jsonConvert.convert<String>(json['product_id']);
	if (product_id != null) {
		billAmount.product_id = product_id;
	}
	final String? sales_type = jsonConvert.convert<String>(json['sales_type']);
	if (sales_type != null) {
		billAmount.sales_type = sales_type;
	}
	final String? sales_date = jsonConvert.convert<String>(json['sales_date']);
	if (sales_date != null) {
		billAmount.sales_date = sales_date;
	}
	final String? expe_date = jsonConvert.convert<String>(json['expe_date']);
	if (expe_date != null) {
		billAmount.expe_date = expe_date;
	}
	final String? amt = jsonConvert.convert<String>(json['amt']);
	if (amt != null) {
		billAmount.amt = amt;
	}
	final String? cgst = jsonConvert.convert<String>(json['cgst']);
	if (cgst != null) {
		billAmount.cgst = cgst;
	}
	final String? sgst = jsonConvert.convert<String>(json['sgst']);
	if (sgst != null) {
		billAmount.sgst = sgst;
	}
	final String? igst = jsonConvert.convert<String>(json['igst']);
	if (igst != null) {
		billAmount.igst = igst;
	}
	final String? binary_val = jsonConvert.convert<String>(json['binary_val']);
	if (binary_val != null) {
		billAmount.binary_val = binary_val;
	}
	final String? currency = jsonConvert.convert<String>(json['currency']);
	if (currency != null) {
		billAmount.currency = currency;
	}
	final String? ex_rate = jsonConvert.convert<String>(json['ex_rate']);
	if (ex_rate != null) {
		billAmount.ex_rate = ex_rate;
	}
	final String? rupee_convertion_val = jsonConvert.convert<String>(json['rupee_convertion_val']);
	if (rupee_convertion_val != null) {
		billAmount.rupee_convertion_val = rupee_convertion_val;
	}
	final String? sponser_reg_id = jsonConvert.convert<String>(json['sponser_reg_id']);
	if (sponser_reg_id != null) {
		billAmount.sponser_reg_id = sponser_reg_id;
	}
	final String? sponser_reg_code = jsonConvert.convert<String>(json['sponser_reg_code']);
	if (sponser_reg_code != null) {
		billAmount.sponser_reg_code = sponser_reg_code;
	}
	final String? referal_commission_rupee = jsonConvert.convert<String>(json['referal_commission_rupee']);
	if (referal_commission_rupee != null) {
		billAmount.referal_commission_rupee = referal_commission_rupee;
	}
	final String? sponser_currency = jsonConvert.convert<String>(json['sponser_currency']);
	if (sponser_currency != null) {
		billAmount.sponser_currency = sponser_currency;
	}
	final String? sponser_cur_ex_rate = jsonConvert.convert<String>(json['sponser_cur_ex_rate']);
	if (sponser_cur_ex_rate != null) {
		billAmount.sponser_cur_ex_rate = sponser_cur_ex_rate;
	}
	final String? sponser_conversion_value = jsonConvert.convert<String>(json['sponser_conversion_value']);
	if (sponser_conversion_value != null) {
		billAmount.sponser_conversion_value = sponser_conversion_value;
	}
	final String? binary_gen_status = jsonConvert.convert<String>(json['binary_gen_status']);
	if (binary_gen_status != null) {
		billAmount.binary_gen_status = binary_gen_status;
	}
	final String? cash_transaction_id = jsonConvert.convert<String>(json['cash_transaction_id']);
	if (cash_transaction_id != null) {
		billAmount.cash_transaction_id = cash_transaction_id;
	}
	return billAmount;
}

Map<String, dynamic> $BillAmountToJson(BillAmount entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['billno_prefix'] = entity.billno_prefix;
	data['bill_no'] = entity.bill_no;
	data['reg_id'] = entity.reg_id;
	data['reg_code'] = entity.reg_code;
	data['product_id'] = entity.product_id;
	data['sales_type'] = entity.sales_type;
	data['sales_date'] = entity.sales_date;
	data['expe_date'] = entity.expe_date;
	data['amt'] = entity.amt;
	data['cgst'] = entity.cgst;
	data['sgst'] = entity.sgst;
	data['igst'] = entity.igst;
	data['binary_val'] = entity.binary_val;
	data['currency'] = entity.currency;
	data['ex_rate'] = entity.ex_rate;
	data['rupee_convertion_val'] = entity.rupee_convertion_val;
	data['sponser_reg_id'] = entity.sponser_reg_id;
	data['sponser_reg_code'] = entity.sponser_reg_code;
	data['referal_commission_rupee'] = entity.referal_commission_rupee;
	data['sponser_currency'] = entity.sponser_currency;
	data['sponser_cur_ex_rate'] = entity.sponser_cur_ex_rate;
	data['sponser_conversion_value'] = entity.sponser_conversion_value;
	data['binary_gen_status'] = entity.binary_gen_status;
	data['cash_transaction_id'] = entity.cash_transaction_id;
	return data;
}