import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Profiledata.dart';
import 'package:json_annotation/json_annotation.dart';

import 'Profile.dart';


Profiledata $ProfiledataFromJson(Map<String, dynamic> json) {
	final Profiledata profiledata = Profiledata();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		profiledata.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		profiledata.message = message;
	}
	final Profile? data = jsonConvert.convert<Profile>(json['data']);
	if (data != null) {
		profiledata.data = data;
	}
	return profiledata;
}

Map<String, dynamic> $ProfiledataToJson(Profiledata entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] = entity.data.toJson();
	return data;
}