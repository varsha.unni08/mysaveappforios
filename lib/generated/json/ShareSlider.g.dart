import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/ShareSlider.dart';
import 'package:json_annotation/json_annotation.dart';

import 'ShareSliderData.dart';


ShareSlider $ShareSliderFromJson(Map<String, dynamic> json) {
	final ShareSlider shareSlider = ShareSlider();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		shareSlider.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		shareSlider.message = message;
	}
	final List<ShareSliderData>? data = jsonConvert.convertListNotNull<ShareSliderData>(json['data']);
	if (data != null) {
		shareSlider.data = data;
	}
	return shareSlider;
}

Map<String, dynamic> $ShareSliderToJson(ShareSlider entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}