import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/SalesInfo.dart';
import 'SalesinfoData.dart';

import 'package:json_annotation/json_annotation.dart';


Salesinfo $SalesinfoFromJson(Map<String, dynamic> json) {
	final Salesinfo salesinfo = Salesinfo();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		salesinfo.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		salesinfo.message = message;
	}
	final SalesinfoData? data = jsonConvert.convert<SalesinfoData>(json['data']);
	if (data != null) {
		salesinfo.data = data;
	}
	return salesinfo;
}

Map<String, dynamic> $SalesinfoToJson(Salesinfo entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] = entity.data.toJson();
	return data;
}