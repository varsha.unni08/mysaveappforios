import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/CountryList.dart';
import 'CountryData.dart';

import 'package:json_annotation/json_annotation.dart';


CountryList $CountryListFromJson(Map<String, dynamic> json) {
	final CountryList countryList = CountryList();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		countryList.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		countryList.message = message;
	}
	final List<CountryData>? data = jsonConvert.convertListNotNull<CountryData>(json['data']);
	if (data != null) {
		countryList.data = data;
	}
	return countryList;
}

Map<String, dynamic> $CountryListToJson(CountryList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}