import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/InvoiceData.dart';
import 'BillAmount.dart';

import 'SettingsData.dart';

import 'package:json_annotation/json_annotation.dart';


InvoiceData $InvoiceDataFromJson(Map<String, dynamic> json) {
	final InvoiceData invoiceData = InvoiceData();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		invoiceData.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		invoiceData.message = message;
	}
	final BillAmount? data = jsonConvert.convert<BillAmount>(json['data']);
	if (data != null) {
		invoiceData.data = data;
	}
	final SettingsData? settingsdata = jsonConvert.convert<SettingsData>(json['settingsdata']);
	if (settingsdata != null) {
		invoiceData.settingsdata = settingsdata;
	}
	return invoiceData;
}

Map<String, dynamic> $InvoiceDataToJson(InvoiceData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] = entity.data.toJson();
	data['settingsdata'] = entity.settingsdata.toJson();
	return data;
}