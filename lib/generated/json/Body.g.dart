import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Body.dart';
import 'ResultInfo.dart';

import 'package:json_annotation/json_annotation.dart';


Body $BodyFromJson(Map<String, dynamic> json) {
	final Body body = Body();
	final ResultInfo? resultInfo = jsonConvert.convert<ResultInfo>(json['resultInfo']);
	if (resultInfo != null) {
		body.resultInfo = resultInfo;
	}
	final String? txnToken = jsonConvert.convert<String>(json['txnToken']);
	if (txnToken != null) {
		body.txnToken = txnToken;
	}
	final bool? isPromoCodeValid = jsonConvert.convert<bool>(json['isPromoCodeValid']);
	if (isPromoCodeValid != null) {
		body.isPromoCodeValid = isPromoCodeValid;
	}
	final bool? authenticated = jsonConvert.convert<bool>(json['authenticated']);
	if (authenticated != null) {
		body.authenticated = authenticated;
	}
	return body;
}

Map<String, dynamic> $BodyToJson(Body entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['resultInfo'] = entity.resultInfo.toJson();
	data['txnToken'] = entity.txnToken;
	data['isPromoCodeValid'] = entity.isPromoCodeValid;
	data['authenticated'] = entity.authenticated;
	return data;
}