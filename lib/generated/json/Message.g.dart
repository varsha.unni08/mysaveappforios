import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Message.dart';
import 'package:json_annotation/json_annotation.dart';


Message $MessageFromJson(Map<String, dynamic> json) {
	final Message message = Message();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		message.id = id;
	}
	final String? createdby_id = jsonConvert.convert<String>(json['createdby_id']);
	if (createdby_id != null) {
		message.createdby_id = createdby_id;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		message.title = title;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		message.message = message;
	}
	final String? created_date = jsonConvert.convert<String>(json['created_date']);
	if (created_date != null) {
		message.created_date = created_date;
	}
	final String? message_validity = jsonConvert.convert<String>(json['message_validity']);
	if (message_validity != null) {
		message.message_validity = message_validity;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		message.image = image;
	}
	final String? verified_status = jsonConvert.convert<String>(json['verified_status']);
	if (verified_status != null) {
		message.verified_status = verified_status;
	}
	final String? user_type = jsonConvert.convert<String>(json['user_type']);
	if (user_type != null) {
		message.user_type = user_type;
	}
	final String? state_id = jsonConvert.convert<String>(json['state_id']);
	if (state_id != null) {
		message.state_id = state_id;
	}
	final String? free_byuser = jsonConvert.convert<String>(json['free_byuser']);
	if (free_byuser != null) {
		message.free_byuser = free_byuser;
	}
	final Object? active_byuser = jsonConvert.convert<Object>(json['active_byuser']);
	if (active_byuser != null) {
		message.active_byuser = active_byuser;
	}
	final Object? reg_code = jsonConvert.convert<Object>(json['reg_code']);
	if (reg_code != null) {
		message.reg_code = reg_code;
	}
	final Object? full_name = jsonConvert.convert<Object>(json['full_name']);
	if (full_name != null) {
		message.full_name = full_name;
	}
	return message;
}

Map<String, dynamic> $MessageToJson(Message entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['createdby_id'] = entity.createdby_id;
	data['title'] = entity.title;
	data['message'] = entity.message;
	data['created_date'] = entity.created_date;
	data['message_validity'] = entity.message_validity;
	data['image'] = entity.image;
	data['verified_status'] = entity.verified_status;
	data['user_type'] = entity.user_type;
	data['state_id'] = entity.state_id;
	data['free_byuser'] = entity.free_byuser;
	data['active_byuser'] = entity.active_byuser.toJson();
	data['reg_code'] = entity.reg_code.toJson();
	data['full_name'] = entity.full_name.toJson();
	return data;
}