import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/RemindDateOfAsset.dart';
import 'package:json_annotation/json_annotation.dart';


RemindDateofAsset $RemindDateofAssetFromJson(Map<String, dynamic> json) {
	final RemindDateofAsset remindDateofAsset = RemindDateofAsset();
	final String? reminddate = jsonConvert.convert<String>(json['reminddate']);
	if (reminddate != null) {
		remindDateofAsset.reminddate = reminddate;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		remindDateofAsset.description = description;
	}
	return remindDateofAsset;
}

Map<String, dynamic> $RemindDateofAssetToJson(RemindDateofAsset entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['reminddate'] = entity.reminddate;
	data['description'] = entity.description;
	return data;
}