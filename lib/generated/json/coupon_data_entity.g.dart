import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/coupon_data_entity.dart';

CouponDataEntity $CouponDataEntityFromJson(Map<String, dynamic> json) {
	final CouponDataEntity couponDataEntity = CouponDataEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		couponDataEntity.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		couponDataEntity.message = message;
	}
	final CouponDataData? data = jsonConvert.convert<CouponDataData>(json['data']);
	if (data != null) {
		couponDataEntity.data = data;
	}
	return couponDataEntity;
}

Map<String, dynamic> $CouponDataEntityToJson(CouponDataEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] = entity.data.toJson();
	return data;
}

CouponDataData $CouponDataDataFromJson(Map<String, dynamic> json) {
	final CouponDataData couponDataData = CouponDataData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		couponDataData.id = id;
	}
	final String? fullName = jsonConvert.convert<String>(json['full_name']);
	if (fullName != null) {
		couponDataData.fullName = fullName;
	}
	final String? regCode = jsonConvert.convert<String>(json['reg_code']);
	if (regCode != null) {
		couponDataData.regCode = regCode;
	}
	final String? countryId = jsonConvert.convert<String>(json['country_id']);
	if (countryId != null) {
		couponDataData.countryId = countryId;
	}
	final String? stateId = jsonConvert.convert<String>(json['state_id']);
	if (stateId != null) {
		couponDataData.stateId = stateId;
	}
	final String? mobile = jsonConvert.convert<String>(json['mobile']);
	if (mobile != null) {
		couponDataData.mobile = mobile;
	}
	final String? profileImage = jsonConvert.convert<String>(json['profile_image']);
	if (profileImage != null) {
		couponDataData.profileImage = profileImage;
	}
	final String? emailId = jsonConvert.convert<String>(json['email_id']);
	if (emailId != null) {
		couponDataData.emailId = emailId;
	}
	final String? currency = jsonConvert.convert<String>(json['currency']);
	if (currency != null) {
		couponDataData.currency = currency;
	}
	final String? joinDate = jsonConvert.convert<String>(json['join_date']);
	if (joinDate != null) {
		couponDataData.joinDate = joinDate;
	}
	final String? activationDate = jsonConvert.convert<String>(json['activation_date']);
	if (activationDate != null) {
		couponDataData.activationDate = activationDate;
	}
	final String? activationKey = jsonConvert.convert<String>(json['activation_key']);
	if (activationKey != null) {
		couponDataData.activationKey = activationKey;
	}
	final String? joinSource = jsonConvert.convert<String>(json['join_source']);
	if (joinSource != null) {
		couponDataData.joinSource = joinSource;
	}
	final dynamic? usedLinkForRegistration = jsonConvert.convert<dynamic>(json['used_link_for_registration']);
	if (usedLinkForRegistration != null) {
		couponDataData.usedLinkForRegistration = usedLinkForRegistration;
	}
	final String? spRegId = jsonConvert.convert<String>(json['sp_reg_id']);
	if (spRegId != null) {
		couponDataData.spRegId = spRegId;
	}
	final String? deviceId = jsonConvert.convert<String>(json['device_id']);
	if (deviceId != null) {
		couponDataData.deviceId = deviceId;
	}
	final String? spRegCode = jsonConvert.convert<String>(json['sp_reg_code']);
	if (spRegCode != null) {
		couponDataData.spRegCode = spRegCode;
	}
	final String? defaultLang = jsonConvert.convert<String>(json['default_lang']);
	if (defaultLang != null) {
		couponDataData.defaultLang = defaultLang;
	}
	final String? username = jsonConvert.convert<String>(json['username']);
	if (username != null) {
		couponDataData.username = username;
	}
	final String? encrPassword = jsonConvert.convert<String>(json['encr_password']);
	if (encrPassword != null) {
		couponDataData.encrPassword = encrPassword;
	}
	final dynamic? gdriveFileid = jsonConvert.convert<dynamic>(json['gdrive_fileid']);
	if (gdriveFileid != null) {
		couponDataData.gdriveFileid = gdriveFileid;
	}
	final String? uniqueDeviceid = jsonConvert.convert<String>(json['unique_deviceId']);
	if (uniqueDeviceid != null) {
		couponDataData.uniqueDeviceid = uniqueDeviceid;
	}
	final String? memberStatus = jsonConvert.convert<String>(json['member_status']);
	if (memberStatus != null) {
		couponDataData.memberStatus = memberStatus;
	}
	final String? resellingPartner = jsonConvert.convert<String>(json['reselling_partner']);
	if (resellingPartner != null) {
		couponDataData.resellingPartner = resellingPartner;
	}
	final String? coupon = jsonConvert.convert<String>(json['coupon']);
	if (coupon != null) {
		couponDataData.coupon = coupon;
	}
	final String? coupStus = jsonConvert.convert<String>(json['coup_stus']);
	if (coupStus != null) {
		couponDataData.coupStus = coupStus;
	}
	return couponDataData;
}

Map<String, dynamic> $CouponDataDataToJson(CouponDataData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['full_name'] = entity.fullName;
	data['reg_code'] = entity.regCode;
	data['country_id'] = entity.countryId;
	data['state_id'] = entity.stateId;
	data['mobile'] = entity.mobile;
	data['profile_image'] = entity.profileImage;
	data['email_id'] = entity.emailId;
	data['currency'] = entity.currency;
	data['join_date'] = entity.joinDate;
	data['activation_date'] = entity.activationDate;
	data['activation_key'] = entity.activationKey;
	data['join_source'] = entity.joinSource;
	data['used_link_for_registration'] = entity.usedLinkForRegistration;
	data['sp_reg_id'] = entity.spRegId;
	data['device_id'] = entity.deviceId;
	data['sp_reg_code'] = entity.spRegCode;
	data['default_lang'] = entity.defaultLang;
	data['username'] = entity.username;
	data['encr_password'] = entity.encrPassword;
	data['gdrive_fileid'] = entity.gdriveFileid;
	data['unique_deviceId'] = entity.uniqueDeviceid;
	data['member_status'] = entity.memberStatus;
	data['reselling_partner'] = entity.resellingPartner;
	data['coupon'] = entity.coupon;
	data['coup_stus'] = entity.coupStus;
	return data;
}