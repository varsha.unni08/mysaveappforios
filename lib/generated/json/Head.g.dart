import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Head.dart';
import 'package:json_annotation/json_annotation.dart';


Head $HeadFromJson(Map<String, dynamic> json) {
	final Head head = Head();
	final String? responseTimestamp = jsonConvert.convert<String>(json['responseTimestamp']);
	if (responseTimestamp != null) {
		head.responseTimestamp = responseTimestamp;
	}
	final String? version = jsonConvert.convert<String>(json['version']);
	if (version != null) {
		head.version = version;
	}
	final String? signature = jsonConvert.convert<String>(json['signature']);
	if (signature != null) {
		head.signature = signature;
	}
	return head;
}

Map<String, dynamic> $HeadToJson(Head entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['responseTimestamp'] = entity.responseTimestamp;
	data['version'] = entity.version;
	data['signature'] = entity.signature;
	return data;
}