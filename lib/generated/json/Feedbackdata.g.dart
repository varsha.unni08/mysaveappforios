import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Feedbackdata.dart';
import 'package:json_annotation/json_annotation.dart';


FeedbackList $FeedbackListFromJson(Map<String, dynamic> json) {
	final FeedbackList feedbackList = FeedbackList();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		feedbackList.id = id;
	}
	final String? reg_id = jsonConvert.convert<String>(json['reg_id']);
	if (reg_id != null) {
		feedbackList.reg_id = reg_id;
	}
	final String? reg_code = jsonConvert.convert<String>(json['reg_code']);
	if (reg_code != null) {
		feedbackList.reg_code = reg_code;
	}
	final String? send_date = jsonConvert.convert<String>(json['send_date']);
	if (send_date != null) {
		feedbackList.send_date = send_date;
	}
	final String? feedback_msg = jsonConvert.convert<String>(json['feedback_msg']);
	if (feedback_msg != null) {
		feedbackList.feedback_msg = feedback_msg;
	}
	final String? reply_msg = jsonConvert.convert<String>(json['reply_msg']);
	if (reply_msg != null) {
		feedbackList.reply_msg = reply_msg;
	}
	final String? reply_date = jsonConvert.convert<String>(json['reply_date']);
	if (reply_date != null) {
		feedbackList.reply_date = reply_date;
	}
	final String? replied_userid = jsonConvert.convert<String>(json['replied_userid']);
	if (replied_userid != null) {
		feedbackList.replied_userid = replied_userid;
	}
	final String? commit_status = jsonConvert.convert<String>(json['commit_status']);
	if (commit_status != null) {
		feedbackList.commit_status = commit_status;
	}
	return feedbackList;
}

Map<String, dynamic> $FeedbackListToJson(FeedbackList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['reg_id'] = entity.reg_id;
	data['reg_code'] = entity.reg_code;
	data['send_date'] = entity.send_date;
	data['feedback_msg'] = entity.feedback_msg;
	data['reply_msg'] = entity.reply_msg;
	data['reply_date'] = entity.reply_date;
	data['replied_userid'] = entity.replied_userid;
	data['commit_status'] = entity.commit_status;
	return data;
}