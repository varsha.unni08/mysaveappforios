import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Feedbackfulldata.dart';
import 'package:json_annotation/json_annotation.dart';

import 'Feedbackdata.dart';


FeedbackFulldata $FeedbackFulldataFromJson(Map<String, dynamic> json) {
	final FeedbackFulldata feedbackFulldata = FeedbackFulldata();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		feedbackFulldata.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		feedbackFulldata.message = message;
	}
	final List<FeedbackList>? data = jsonConvert.convertListNotNull<FeedbackList>(json['data']);
	if (data != null) {
		feedbackFulldata.data = data;
	}
	return feedbackFulldata;
}

Map<String, dynamic> $FeedbackFulldataToJson(FeedbackFulldata entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}