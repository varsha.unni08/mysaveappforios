import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/NotificationMessage.dart';
import 'package:json_annotation/json_annotation.dart';

import 'MessageData.dart';


NotificationMessage $NotificationMessageFromJson(Map<String, dynamic> json) {
	final NotificationMessage notificationMessage = NotificationMessage();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		notificationMessage.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		notificationMessage.message = message;
	}
	final List<MessageData>? data = jsonConvert.convertListNotNull<MessageData>(json['data']);
	if (data != null) {
		notificationMessage.data = data;
	}
	return notificationMessage;
}

Map<String, dynamic> $NotificationMessageToJson(NotificationMessage entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}