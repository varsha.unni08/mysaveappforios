import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/SettingsData.dart';
import 'package:json_annotation/json_annotation.dart';


SettingsData $SettingsDataFromJson(Map<String, dynamic> json) {
	final SettingsData settingsData = SettingsData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		settingsData.id = id;
	}
	final String? bv = jsonConvert.convert<String>(json['bv']);
	if (bv != null) {
		settingsData.bv = bv;
	}
	final String? referal_commission = jsonConvert.convert<String>(json['referal_commission']);
	if (referal_commission != null) {
		settingsData.referal_commission = referal_commission;
	}
	final String? one_bv_required_amount = jsonConvert.convert<String>(json['one_bv_required_amount']);
	if (one_bv_required_amount != null) {
		settingsData.one_bv_required_amount = one_bv_required_amount;
	}
	final String? renewal_charge = jsonConvert.convert<String>(json['renewal_charge']);
	if (renewal_charge != null) {
		settingsData.renewal_charge = renewal_charge;
	}
	final String? one_bv_equalant_value = jsonConvert.convert<String>(json['one_bv_equalant_value']);
	if (one_bv_equalant_value != null) {
		settingsData.one_bv_equalant_value = one_bv_equalant_value;
	}
	final String? daily_ceiling = jsonConvert.convert<String>(json['daily_ceiling']);
	if (daily_ceiling != null) {
		settingsData.daily_ceiling = daily_ceiling;
	}
	final String? Percent_tds = jsonConvert.convert<String>(json['Percent_tds']);
	if (Percent_tds != null) {
		settingsData.Percent_tds = Percent_tds;
	}
	final String? Percent_tds_nonpan = jsonConvert.convert<String>(json['Percent_tds_nonpan']);
	if (Percent_tds_nonpan != null) {
		settingsData.Percent_tds_nonpan = Percent_tds_nonpan;
	}
	final String? percent_admin_charges = jsonConvert.convert<String>(json['percent_admin_charges']);
	if (percent_admin_charges != null) {
		settingsData.percent_admin_charges = percent_admin_charges;
	}
	final String? renewal_notification_days = jsonConvert.convert<String>(json['renewal_notification_days']);
	if (renewal_notification_days != null) {
		settingsData.renewal_notification_days = renewal_notification_days;
	}
	final String? bill_prefix = jsonConvert.convert<String>(json['bill_prefix']);
	if (bill_prefix != null) {
		settingsData.bill_prefix = bill_prefix;
	}
	final String? cgst = jsonConvert.convert<String>(json['cgst']);
	if (cgst != null) {
		settingsData.cgst = cgst;
	}
	final String? sgst = jsonConvert.convert<String>(json['sgst']);
	if (sgst != null) {
		settingsData.sgst = sgst;
	}
	final String? igst = jsonConvert.convert<String>(json['igst']);
	if (igst != null) {
		settingsData.igst = igst;
	}
	final String? other_tax = jsonConvert.convert<String>(json['other_tax']);
	if (other_tax != null) {
		settingsData.other_tax = other_tax;
	}
	final String? buffer_count = jsonConvert.convert<String>(json['buffer_count']);
	if (buffer_count != null) {
		settingsData.buffer_count = buffer_count;
	}
	final String? link_image = jsonConvert.convert<String>(json['link_image']);
	if (link_image != null) {
		settingsData.link_image = link_image;
	}
	final String? network_image = jsonConvert.convert<String>(json['network_image']);
	if (network_image != null) {
		settingsData.network_image = network_image;
	}
	return settingsData;
}

Map<String, dynamic> $SettingsDataToJson(SettingsData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['bv'] = entity.bv;
	data['referal_commission'] = entity.referal_commission;
	data['one_bv_required_amount'] = entity.one_bv_required_amount;
	data['renewal_charge'] = entity.renewal_charge;
	data['one_bv_equalant_value'] = entity.one_bv_equalant_value;
	data['daily_ceiling'] = entity.daily_ceiling;
	data['Percent_tds'] = entity.Percent_tds;
	data['Percent_tds_nonpan'] = entity.Percent_tds_nonpan;
	data['percent_admin_charges'] = entity.percent_admin_charges;
	data['renewal_notification_days'] = entity.renewal_notification_days;
	data['bill_prefix'] = entity.bill_prefix;
	data['cgst'] = entity.cgst;
	data['sgst'] = entity.sgst;
	data['igst'] = entity.igst;
	data['other_tax'] = entity.other_tax;
	data['buffer_count'] = entity.buffer_count;
	data['link_image'] = entity.link_image;
	data['network_image'] = entity.network_image;
	return data;
}