import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Profile.dart';
import 'package:json_annotation/json_annotation.dart';


Profile $ProfileFromJson(Map<String, dynamic> json) {
	final Profile profile = Profile();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		profile.id = id;
	}
	final String? full_name = jsonConvert.convert<String>(json['full_name']);
	if (full_name != null) {
		profile.full_name = full_name;
	}
	final String? regCode = jsonConvert.convert<String>(json['regCode']);
	if (regCode != null) {
		profile.regCode = regCode;
	}
	final String? country_id = jsonConvert.convert<String>(json['country_id']);
	if (country_id != null) {
		profile.country_id = country_id;
	}
	final String? state_id = jsonConvert.convert<String>(json['state_id']);
	if (state_id != null) {
		profile.state_id = state_id;
	}
	final String? mobile = jsonConvert.convert<String>(json['mobile']);
	if (mobile != null) {
		profile.mobile = mobile;
	}
	final String? email_id = jsonConvert.convert<String>(json['email_id']);
	if (email_id != null) {
		profile.email_id = email_id;
	}
	final String? currency = jsonConvert.convert<String>(json['currency']);
	if (currency != null) {
		profile.currency = currency;
	}
	final String? join_date = jsonConvert.convert<String>(json['join_date']);
	if (join_date != null) {
		profile.join_date = join_date;
	}
	final String? activation_date = jsonConvert.convert<String>(json['activation_date']);
	if (activation_date != null) {
		profile.activation_date = activation_date;
	}
	final String? activation_key = jsonConvert.convert<String>(json['activation_key']);
	if (activation_key != null) {
		profile.activation_key = activation_key;
	}
	final String? defaultLang = jsonConvert.convert<String>(json['defaultLang']);
	if (defaultLang != null) {
		profile.defaultLang = defaultLang;
	}
	final String? profile_image = jsonConvert.convert<String>(json['profile_image']);
	if (profile_image != null) {
		profile.profile_image = profile_image;
	}
	final String? gdrive_fileid = jsonConvert.convert<String>(json['gdrive_fileid']);
	if (gdrive_fileid != null) {
		profile.gdrive_fileid = gdrive_fileid;
	}
	final String? join_source = jsonConvert.convert<String>(json['join_source']);
	if (join_source != null) {
		profile.join_source = join_source;
	}
	final String? sp_reg_id = jsonConvert.convert<String>(json['sp_reg_id']);
	if (sp_reg_id != null) {
		profile.sp_reg_id = sp_reg_id;
	}
	return profile;
}

Map<String, dynamic> $ProfileToJson(Profile entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['full_name'] = entity.full_name;
	data['regCode'] = entity.regCode;
	data['country_id'] = entity.country_id;
	data['state_id'] = entity.state_id;
	data['mobile'] = entity.mobile;
	data['email_id'] = entity.email_id;
	data['currency'] = entity.currency;
	data['join_date'] = entity.join_date;
	data['activation_date'] = entity.activation_date;
	data['activation_key'] = entity.activation_key;
	data['defaultLang'] = entity.defaultLang;
	data['profile_image'] = entity.profile_image;
	data['gdrive_fileid'] = entity.gdrive_fileid;
	data['join_source'] = entity.join_source;
	data['sp_reg_id'] = entity.sp_reg_id;
	return data;
}