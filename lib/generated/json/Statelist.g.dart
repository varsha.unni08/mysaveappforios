import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/Statelist.dart';
import 'package:json_annotation/json_annotation.dart';


Statelist $StatelistFromJson(Map<String, dynamic> json) {
	final Statelist statelist = Statelist();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		statelist.id = id;
	}
	final String? code = jsonConvert.convert<String>(json['code']);
	if (code != null) {
		statelist.code = code;
	}
	final String? state_name = jsonConvert.convert<String>(json['state_name']);
	if (state_name != null) {
		statelist.state_name = state_name;
	}
	final String? state_code = jsonConvert.convert<String>(json['state_code']);
	if (state_code != null) {
		statelist.state_code = state_code;
	}
	return statelist;
}

Map<String, dynamic> $StatelistToJson(Statelist entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['code'] = entity.code;
	data['state_name'] = entity.state_name;
	data['state_code'] = entity.state_code;
	return data;
}