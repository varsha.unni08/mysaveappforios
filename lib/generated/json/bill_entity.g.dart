import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/bill_entity.dart';

BillEntity $BillEntityFromJson(Map<String, dynamic> json) {
	final BillEntity billEntity = BillEntity();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		billEntity.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		billEntity.message = message;
	}
	final BillData? data = jsonConvert.convert<BillData>(json['data']);
	if (data != null) {
		billEntity.data = data;
	}
	return billEntity;
}

Map<String, dynamic> $BillEntityToJson(BillEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] = entity.data.toJson();
	return data;
}

BillData $BillDataFromJson(Map<String, dynamic> json) {
	final BillData billData = BillData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		billData.id = id;
	}
	final String? billnoPrefix = jsonConvert.convert<String>(json['billno_prefix']);
	if (billnoPrefix != null) {
		billData.billnoPrefix = billnoPrefix;
	}
	final String? billNo = jsonConvert.convert<String>(json['bill_no']);
	if (billNo != null) {
		billData.billNo = billNo;
	}
	final String? displayBillNo = jsonConvert.convert<String>(json['display_bill_no']);
	if (displayBillNo != null) {
		billData.displayBillNo = displayBillNo;
	}
	final String? regId = jsonConvert.convert<String>(json['reg_id']);
	if (regId != null) {
		billData.regId = regId;
	}
	final String? regCode = jsonConvert.convert<String>(json['reg_code']);
	if (regCode != null) {
		billData.regCode = regCode;
	}
	final String? productId = jsonConvert.convert<String>(json['product_id']);
	if (productId != null) {
		billData.productId = productId;
	}
	final String? salesType = jsonConvert.convert<String>(json['sales_type']);
	if (salesType != null) {
		billData.salesType = salesType;
	}
	final String? salesDate = jsonConvert.convert<String>(json['sales_date']);
	if (salesDate != null) {
		billData.salesDate = salesDate;
	}
	final String? expeDate = jsonConvert.convert<String>(json['expe_date']);
	if (expeDate != null) {
		billData.expeDate = expeDate;
	}
	final String? amt = jsonConvert.convert<String>(json['amt']);
	if (amt != null) {
		billData.amt = amt;
	}
	final String? cgst = jsonConvert.convert<String>(json['cgst']);
	if (cgst != null) {
		billData.cgst = cgst;
	}
	final String? sgst = jsonConvert.convert<String>(json['sgst']);
	if (sgst != null) {
		billData.sgst = sgst;
	}
	final String? igst = jsonConvert.convert<String>(json['igst']);
	if (igst != null) {
		billData.igst = igst;
	}
	final String? binaryVal = jsonConvert.convert<String>(json['binary_val']);
	if (binaryVal != null) {
		billData.binaryVal = binaryVal;
	}
	final String? currency = jsonConvert.convert<String>(json['currency']);
	if (currency != null) {
		billData.currency = currency;
	}
	final String? exRate = jsonConvert.convert<String>(json['ex_rate']);
	if (exRate != null) {
		billData.exRate = exRate;
	}
	final String? rupeeConvertionVal = jsonConvert.convert<String>(json['rupee_convertion_val']);
	if (rupeeConvertionVal != null) {
		billData.rupeeConvertionVal = rupeeConvertionVal;
	}
	final String? sponserRegId = jsonConvert.convert<String>(json['sponser_reg_id']);
	if (sponserRegId != null) {
		billData.sponserRegId = sponserRegId;
	}
	final String? sponserRegCode = jsonConvert.convert<String>(json['sponser_reg_code']);
	if (sponserRegCode != null) {
		billData.sponserRegCode = sponserRegCode;
	}
	final String? referalCommissionRupee = jsonConvert.convert<String>(json['referal_commission_rupee']);
	if (referalCommissionRupee != null) {
		billData.referalCommissionRupee = referalCommissionRupee;
	}
	final String? sponserCurrency = jsonConvert.convert<String>(json['sponser_currency']);
	if (sponserCurrency != null) {
		billData.sponserCurrency = sponserCurrency;
	}
	final String? sponserCurExRate = jsonConvert.convert<String>(json['sponser_cur_ex_rate']);
	if (sponserCurExRate != null) {
		billData.sponserCurExRate = sponserCurExRate;
	}
	final String? sponserConversionValue = jsonConvert.convert<String>(json['sponser_conversion_value']);
	if (sponserConversionValue != null) {
		billData.sponserConversionValue = sponserConversionValue;
	}
	final String? binaryGenStatus = jsonConvert.convert<String>(json['binary_gen_status']);
	if (binaryGenStatus != null) {
		billData.binaryGenStatus = binaryGenStatus;
	}
	final String? cashTransactionId = jsonConvert.convert<String>(json['cash_transaction_id']);
	if (cashTransactionId != null) {
		billData.cashTransactionId = cashTransactionId;
	}
	final String? smode = jsonConvert.convert<String>(json['smode']);
	if (smode != null) {
		billData.smode = smode;
	}
	return billData;
}

Map<String, dynamic> $BillDataToJson(BillData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['billno_prefix'] = entity.billnoPrefix;
	data['bill_no'] = entity.billNo;
	data['display_bill_no'] = entity.displayBillNo;
	data['reg_id'] = entity.regId;
	data['reg_code'] = entity.regCode;
	data['product_id'] = entity.productId;
	data['sales_type'] = entity.salesType;
	data['sales_date'] = entity.salesDate;
	data['expe_date'] = entity.expeDate;
	data['amt'] = entity.amt;
	data['cgst'] = entity.cgst;
	data['sgst'] = entity.sgst;
	data['igst'] = entity.igst;
	data['binary_val'] = entity.binaryVal;
	data['currency'] = entity.currency;
	data['ex_rate'] = entity.exRate;
	data['rupee_convertion_val'] = entity.rupeeConvertionVal;
	data['sponser_reg_id'] = entity.sponserRegId;
	data['sponser_reg_code'] = entity.sponserRegCode;
	data['referal_commission_rupee'] = entity.referalCommissionRupee;
	data['sponser_currency'] = entity.sponserCurrency;
	data['sponser_cur_ex_rate'] = entity.sponserCurExRate;
	data['sponser_conversion_value'] = entity.sponserConversionValue;
	data['binary_gen_status'] = entity.binaryGenStatus;
	data['cash_transaction_id'] = entity.cashTransactionId;
	data['smode'] = entity.smode;
	return data;
}