import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/ResultInfo.dart';
import 'package:json_annotation/json_annotation.dart';


ResultInfo $ResultInfoFromJson(Map<String, dynamic> json) {
	final ResultInfo resultInfo = ResultInfo();
	final String? resultStatus = jsonConvert.convert<String>(json['resultStatus']);
	if (resultStatus != null) {
		resultInfo.resultStatus = resultStatus;
	}
	final String? resultCode = jsonConvert.convert<String>(json['resultCode']);
	if (resultCode != null) {
		resultInfo.resultCode = resultCode;
	}
	final String? resultMsg = jsonConvert.convert<String>(json['resultMsg']);
	if (resultMsg != null) {
		resultInfo.resultMsg = resultMsg;
	}
	return resultInfo;
}

Map<String, dynamic> $ResultInfoToJson(ResultInfo entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['resultStatus'] = entity.resultStatus;
	data['resultCode'] = entity.resultCode;
	data['resultMsg'] = entity.resultMsg;
	return data;
}