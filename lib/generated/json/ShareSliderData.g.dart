import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/ShareSliderData.dart';
import 'package:json_annotation/json_annotation.dart';


ShareSliderData $ShareSliderDataFromJson(Map<String, dynamic> json) {
	final ShareSliderData shareSliderData = ShareSliderData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		shareSliderData.id = id;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		shareSliderData.image = image;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		shareSliderData.description = description;
	}
	return shareSliderData;
}

Map<String, dynamic> $ShareSliderDataToJson(ShareSliderData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['image'] = entity.image;
	data['description'] = entity.description;
	return data;
}