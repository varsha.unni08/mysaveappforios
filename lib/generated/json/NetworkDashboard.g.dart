import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/NetworkDashboard.dart';
import 'package:json_annotation/json_annotation.dart';

import 'NetworkDashboardData.dart';


NetworkDashboard $NetworkDashboardFromJson(Map<String, dynamic> json) {
	final NetworkDashboard networkDashboard = NetworkDashboard();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		networkDashboard.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		networkDashboard.message = message;
	}
	final NetWorkDashboardData? data = jsonConvert.convert<NetWorkDashboardData>(json['data']);
	if (data != null) {
		networkDashboard.data = data;
	}
	return networkDashboard;
}

Map<String, dynamic> $NetworkDashboardToJson(NetworkDashboard entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] = entity.data.toJson();
	return data;
}