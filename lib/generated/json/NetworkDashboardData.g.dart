import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/NetworkDashboardData.dart';
import 'package:json_annotation/json_annotation.dart';


NetWorkDashboardData $NetWorkDashboardDataFromJson(Map<String, dynamic> json) {
	final NetWorkDashboardData netWorkDashboardData = NetWorkDashboardData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		netWorkDashboardData.id = id;
	}
	final String? position = jsonConvert.convert<String>(json['position']);
	if (position != null) {
		netWorkDashboardData.position = position;
	}
	final String? position_downline_setup_default = jsonConvert.convert<String>(json['position_downline_setup_default']);
	if (position_downline_setup_default != null) {
		netWorkDashboardData.position_downline_setup_default = position_downline_setup_default;
	}
	final String? position_next = jsonConvert.convert<String>(json['position_next']);
	if (position_next != null) {
		netWorkDashboardData.position_next = position_next;
	}
	final String? binary_left = jsonConvert.convert<String>(json['binary_left']);
	if (binary_left != null) {
		netWorkDashboardData.binary_left = binary_left;
	}
	final String? binary_right = jsonConvert.convert<String>(json['binary_right']);
	if (binary_right != null) {
		netWorkDashboardData.binary_right = binary_right;
	}
	final String? binary_matched = jsonConvert.convert<String>(json['binary_matched']);
	if (binary_matched != null) {
		netWorkDashboardData.binary_matched = binary_matched;
	}
	final String? carry_left = jsonConvert.convert<String>(json['carry_left']);
	if (carry_left != null) {
		netWorkDashboardData.carry_left = carry_left;
	}
	final String? member_status = jsonConvert.convert<String>(json['member_status']);
	if (member_status != null) {
		netWorkDashboardData.member_status = member_status;
	}
	final String? carry_right = jsonConvert.convert<String>(json['carry_right']);
	if (carry_right != null) {
		netWorkDashboardData.carry_right = carry_right;
	}
	final String? binary_amt = jsonConvert.convert<String>(json['binary_amt']);
	if (binary_amt != null) {
		netWorkDashboardData.binary_amt = binary_amt;
	}
	final String? referral_commission_amt = jsonConvert.convert<String>(json['referral_commission_amt']);
	if (referral_commission_amt != null) {
		netWorkDashboardData.referral_commission_amt = referral_commission_amt;
	}
	final String? level_amt = jsonConvert.convert<String>(json['level_amt']);
	if (level_amt != null) {
		netWorkDashboardData.level_amt = level_amt;
	}
	final String? achievement_amt = jsonConvert.convert<String>(json['achievement_amt']);
	if (achievement_amt != null) {
		netWorkDashboardData.achievement_amt = achievement_amt;
	}
	return netWorkDashboardData;
}

Map<String, dynamic> $NetWorkDashboardDataToJson(NetWorkDashboardData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['position'] = entity.position;
	data['position_downline_setup_default'] = entity.position_downline_setup_default;
	data['position_next'] = entity.position_next;
	data['binary_left'] = entity.binary_left;
	data['binary_right'] = entity.binary_right;
	data['binary_matched'] = entity.binary_matched;
	data['carry_left'] = entity.carry_left;
	data['member_status'] = entity.member_status;
	data['carry_right'] = entity.carry_right;
	data['binary_amt'] = entity.binary_amt;
	data['referral_commission_amt'] = entity.referral_commission_amt;
	data['level_amt'] = entity.level_amt;
	data['achievement_amt'] = entity.achievement_amt;
	return data;
}