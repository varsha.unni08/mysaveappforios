import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/StateData.dart';
import 'package:json_annotation/json_annotation.dart';

import 'Statelist.dart';


StateData $StateDataFromJson(Map<String, dynamic> json) {
	final StateData stateData = StateData();
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		stateData.status = status;
	}
	final String? message = jsonConvert.convert<String>(json['message']);
	if (message != null) {
		stateData.message = message;
	}
	final List<Statelist>? data = jsonConvert.convertListNotNull<Statelist>(json['data']);
	if (data != null) {
		stateData.data = data;
	}
	return stateData;
}

Map<String, dynamic> $StateDataToJson(StateData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['status'] = entity.status;
	data['message'] = entity.message;
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}