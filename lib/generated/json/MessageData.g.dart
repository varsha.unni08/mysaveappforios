import 'package:saveappforios/generated/json/base/json_convert_content.dart';
import 'package:saveappforios/domain/MessageData.dart';
import 'package:json_annotation/json_annotation.dart';

import 'Message.dart';


MessageData $MessageDataFromJson(Map<String, dynamic> json) {
	final MessageData messageData = MessageData();
	final String? id = jsonConvert.convert<String>(json['id']);
	if (id != null) {
		messageData.id = id;
	}
	final String? reg_id = jsonConvert.convert<String>(json['reg_id']);
	if (reg_id != null) {
		messageData.reg_id = reg_id;
	}
	final String? message_id = jsonConvert.convert<String>(json['message_id']);
	if (message_id != null) {
		messageData.message_id = message_id;
	}
	final Message? message = jsonConvert.convert<Message>(json['message']);
	if (message != null) {
		messageData.message = message;
	}
	final String? date = jsonConvert.convert<String>(json['date']);
	if (date != null) {
		messageData.date = date;
	}
	return messageData;
}

Map<String, dynamic> $MessageDataToJson(MessageData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['reg_id'] = entity.reg_id;
	data['message_id'] = entity.message_id;
	data['message'] = entity.message.toJson();
	data['date'] = entity.date;
	return data;
}