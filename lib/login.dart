import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/projectconstants/ServerMessageType.dart';
import 'package:saveappforios/registration.dart';
// import 'package:save_flutter/main.dart';
// import 'package:save_flutter/projectconstants/ServerMessageType.dart';
import 'dart:io';
import 'forgotpassword.dart';
import 'home.dart';
//import 'registration.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'projectconstants/DataConstants.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
//import 'forgotpassword.dart';
import 'connection/DataConnection.dart';

void main() {
  // runApp(MyApp());


  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: Loginpage(title: 'dashboard'),
      debugShowCheckedModeBanner: false,
    );
  }
}


class Loginpage extends StatefulWidget{


  Loginpage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}



class _MyLoginPageState extends State<Loginpage> {
  int _counter = 0;



  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      // _counter++;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    // sleep(const Duration(seconds:6));
   // startTime();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  // route() {
  //   Navigator.pushReplacement(context, MaterialPageRoute(
  //       builder: (context) => _MyLoginPageState(title: "dashboard",)
  //   )
  //   );
  // }



  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    TextEditingController mobilenumbercontroller = new TextEditingController();
    TextEditingController passwordcontroller = new TextEditingController();
    TextEditingController otpcodecontroller = new TextEditingController();
    TextEditingController forgotpasswordmobilecontroller = new TextEditingController();


    return Scaffold(
      backgroundColor: Color(0xff007a74),
      body: Stack(

          children: <Widget>[

            Container(
              decoration: new BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.center,
                      colors: [Color(0xff096c6c), Color(0xff007a74)])),
            ),

      Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        width: 200,
                        height: 120,

                        child: Image.asset('images/loginpageiconimg.png')),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Align(

                    alignment: Alignment.topCenter,
                    child: Container(
                        alignment: Alignment.topCenter,

                        child: Text("My Personal App", style: TextStyle(color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.w100),)),
                  ),
                ),

                Padding(
                  //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                  padding: const EdgeInsets.only(top: 20.0,left:10.0,right: 10.0,bottom: 0),

                  child:Align(
                    alignment: Alignment.center,
                    child: new Theme(data: new ThemeData(
                        hintColor: Colors.white
                    ), child: TextField(
                      controller: mobilenumbercontroller,
                      keyboardType: TextInputType.number,

                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white, width: 0.5),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white, width: 0.5),
                        ),
                        hintText: 'Mobile number',

                      ),
                    )),),
                ),


                Padding(
                  padding: const EdgeInsets.only(top: 10.0,left:10.0,right: 10.0,bottom: 0),

                  //padding: EdgeInsets.symmetric(horizontal: 15),

                  child:Align(
                    alignment: Alignment.center,
                    child: new Theme(data: new ThemeData(
                        hintColor: Colors.white
                    ), child: TextField(
                      controller: passwordcontroller,
                      obscureText: true,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white, width: 0.5),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white, width: 0.5),
                        ),
                        hintText: 'Password',
                      ),
                    )),
                  ),),


                Padding(padding: const EdgeInsets.only(top: 10.0,left:15.0,right: 15.0,bottom: 0),

                  child: Align(
                    alignment: Alignment.centerRight,

                    child: TextButton(
                      onPressed:() {

                        Dialog forgotpasswordDialog = Dialog(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                          child: Container(
                            height: 300.0,
                            width: 500.0,

                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding:  EdgeInsets.all(15.0),
                                  child: new Theme(data: new ThemeData(
                                      hintColor: Colors.black38
                                  ), child: TextField(
                                    controller: forgotpasswordmobilecontroller,
                                    keyboardType: TextInputType.number,

                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black38, width: 0.5),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black38, width: 0.5),
                                      ),
                                      hintText: 'Enter the mobile number',


                                    ),
                                  )),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(15.0),

                                  child: Container(

                                    width: 150,
                                    height: 55,
                                    decoration: BoxDecoration(

                                        color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                                    child:Align(
                                      alignment: Alignment.center,
                                      child: TextButton(

                                        onPressed:() {


                                          if(forgotpasswordmobilecontroller.text.toString().isNotEmpty) {
                                            Navigator.of(
                                                context, rootNavigator: true)
                                                .pop();

                                            DataConnectionStatus.check().then((value)
                                            {

                                              if (value != null && value) {
                                                // Internet Present Case

                                                checkMobileNumber(forgotpasswordmobilecontroller.text.toString(),otpcodecontroller);

                                              }
                                              else{

                                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                                  content: Text("Check your internet connection"),
                                                ));

                                              }
                                            }
                                            );

                                          }
                                          else{



                                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                              content: Text("Please enter the mobile number"),
                                            ));
                                          }


                                        },

                                        child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                                    ),



                                    //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                  ),


                                  // ,
                                ),
                                // Padding(padding: EdgeInsets.only(top: 50.0)),
                                // TextButton(onPressed: () {
                                //   Navigator.of(context).pop();
                                // },
                                //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                              ],
                            ),
                          ),
                        );



                        showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);






                      },

                      child:Text('Forgot password ?',style: TextStyle(color: Colors.white, fontSize: 15)



                      ),
                    ),
                  ),),

                Padding(
                  padding: const EdgeInsets.only(top: 5.0,left:15.0,right: 15.0,bottom: 0),

                  child: Align(
                    alignment: Alignment.bottomCenter,

                    child: Container(
                      height: 50,
                      width: 150,
                      decoration: BoxDecoration(
                          color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                      child: TextButton(
                        onPressed: () {
                          // Navigator.push(
                          //     context, MaterialPageRoute(builder: (_) => Registrationpage(title: "registration",)));


                          DataConnectionStatus.check().then((value)
                          {

                            if (value != null && value) {
                              // Internet Present Case

                              userLogin(mobilenumbercontroller.text.toString(),passwordcontroller.text.toString());

                            }
                            else{

                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Check your internet connection"),
                              ));

                            }
                          }
                          );


                        },
                        child: Text(
                          'Login',
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ),
                    ),),),

                Padding(
                  padding: const EdgeInsets.only(top: 10.0,left:15.0,right: 15.0,bottom: 0),





                  child:Align(
                    alignment: Alignment.bottomCenter,

                    child:TextButton(
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                      ),
                      onPressed: () {

                        Navigator.push(
                            context, MaterialPageRoute(builder: (_) => Registrationpage(title: "registration",)));
                      },
                      child: Text('New user ? Create Account'),
                    ),),)

                // Container(
                //     padding: EdgeInsets.only(top: 5),
                //     width: MediaQuery.of(context).size.width * 0.80,
                //     child: RaisedButton(
                //         onPressed: () => loginTapped(),
                //         child: Text("Login"),
                //         color: Colors.blue))
              ],
            ),
          ),
        ),
      ),
   ] )  );





  }


  userLogin(String username,String password) async {

    final datacount = await SharedPreferences.getInstance();
    //
    // String ? a=datacount.getString(DataConstants.firstuser);
    //
    // if(a==null)
    //   {
    //     a="";
    //   }
    //
    // if(username.compareTo(a)==0)

    if(username.isNotEmpty&&password.isNotEmpty) {

     // String encoded = base64.encode(utf8.encode(password));

      // datacount.setString(DataConstants.userkey, token);
      // datacount.setString(DataConstants.backuptoken, token);
      // datacount.setString(DataConstants.firstuser, username);
      // datacount.setString(DataConstants.firstpassword, encoded);


      var connectivityResult = await (Connectivity().checkConnectivity());

      if(connectivityResult==ConnectivityResult.none)
        {

          if(datacount.getString(DataConstants.firstuser)!=null&&datacount.getString(DataConstants.firstpassword)!=null)
            {

              if(datacount.getString(DataConstants.firstuser).toString().compareTo(username)==0)
                {

                  String encoded = base64.encode(utf8.encode(password));


                  if(datacount.getString(DataConstants.firstpassword).toString().compareTo(encoded)==0)
                    {


                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) => MyDashboardPage(title: "dashboard",)
                      )
                      );

                    }
                  else{


                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Wrong password"),
                    ));
                  }





                }
              else{

                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Sorry , This device is already authenticated by another number"),
                ));
              }







            }


        }
      else{



        ProgressDialog _progressDialog = ProgressDialog();
        _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
        var date = new DateTime.now().toIso8601String();

        var dataasync = await http.post(
            Uri.parse(DataConstants.baseurl+DataConstants.UserLogin),

            headers: <String, String>{
              'Content-Type': 'application/x-www-form-urlencoded',

            }, body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'mobile': username,
          'password': password,
          'uuid': date,
          'timestamp': date
        }

        );

        _progressDialog.dismissProgressDialog(context);
        String response = dataasync.body;
        if(datacount.getString(DataConstants.firstuser)!=null) {
          if (datacount.getString(DataConstants.firstuser).toString().compareTo(
              username) == 0) {



            parseLoginData(response,username,password);

          }
          else{

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Sorry , This device is already authenticated by another number"),
            ));

          }
        }
        else{

          parseLoginData(response,username,password);
          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          //   content: Text(""),
          // ));


        }


      }




















     // print(response);
    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Please ente the username and password"),
      ));
    }




  }


  parseLoginData(String response,String username,String password) async
  {


    final datacount = await SharedPreferences.getInstance();
    dynamic jsondata = jsonDecode(response);
    String token = jsondata['token'];
    int status = jsondata['status'];

    String message = jsondata['message'];

    // Navigator.of(context).pop();
    //
    // String encoded = base64.encode(utf8.encode(password));
    //
    // datacount.setString(DataConstants.userkey, token);
    // datacount.setString(DataConstants.backuptoken, token);
    // datacount.setString(DataConstants.firstuser, username);
    // datacount.setString(DataConstants.firstpassword, encoded);
    //
    // Navigator.pushReplacement(context, MaterialPageRoute(
    //     builder: (context) => MyDashboardPage(title: "dashboard",)
    // )
    // );


    // print(datacount.read(DataConstants.tokenkey));


     if (status != 0) {
      String encoded = base64.encode(utf8.encode(password));

      datacount.setString(DataConstants.userkey, token);
      datacount.setString(DataConstants.backuptoken, token);
      datacount.setString(DataConstants.firstuser, username);
      datacount.setString(DataConstants.firstpassword, encoded);

      Navigator.pushReplacement(context, MaterialPageRoute(
          builder: (context) => MyDashboardPage(title: "dashboard",)
      )
      );
    }

    else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Login failed"),
      ));
    }
  }



  checkMobileNumber(String mobilenumber ,TextEditingController otpcodecontroller) async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.get(
        Uri.parse(DataConstants.baseurl+DataConstants.getUserByMobile+'?mobile='+mobilenumber+'&timestamp='+date),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        },

    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

   // print(jsondata);

    int status = jsondata['status'];

    if(status==1)
      {

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("This mobile number  exists"),
        ));

        var rng = new Random();
        var code = rng.nextInt(9000) + 1000;

        print('otp code is : '+code.toString());

        DataConnectionStatus.check().then((value)
        {

          if (value != null && value) {
            // Internet Present Case

            sendOtpCode(code,mobilenumber);

          }
          else{

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Check your internet connection"),
            ));

          }
        }
        );

        showOtpCodeDialog(code,otpcodecontroller,mobilenumber);
      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("This mobile number does not exist"),
      ));
    }




  }

  showOtpCodeDialog(int code,TextEditingController otpcodeController,String mobilenumber)
  {

    Dialog forgotpasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: 300.0,
        width: 600.0,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: otpcodeController,
                keyboardType: TextInputType.number,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Enter OTP code',


                ),
              )),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {


                      if(otpcodeController.text.toString().isNotEmpty) {

                        if(otpcodeController.text.toString().compareTo(code.toString())==0)
{
  // Navigator.of(
  //     context, rootNavigator: true)
  //     .pop();

  Navigator.push(
      context, MaterialPageRoute(builder: (_) => ForgotpasswordPage(title: "forgotpassword",mobile: mobilenumber,)));

}



                     //   checkMobileNumber(otpcodeController.text.toString());
                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Please enter the mobile number"),
                        ));
                      }


                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),

            Padding(
              padding: const EdgeInsets.all(15),
              child:TextButton(
                onPressed: (){
                  //TODO FORGOT PASSWORD SCREEN GOES HERE
                  var rng = new Random();
                  var c = rng.nextInt(9000) + 1000;

                  code=c;

                  sendOtpCode(c,mobilenumber);
                },
                child: Align(
                  alignment: Alignment.center,

                  child: Text('Did not get OTP code ?  Resend')
                ),
              ),),








            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
    );



    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);


  }


  sendOtpCode(int code,String mobilenumber) async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();


    String message=DataConstants.buildServerMessage(ServerMessageType.forgot_password,code.toString(),"","");

    String u=message.replaceAll(" ", "%20");


    print('the message is '+u);

    String url=DataConstants.smsbaseurl+DataConstants.smsMethode+"?token="+DataConstants.apikey+
        "&sender="+DataConstants.sender+
        "&number="+mobilenumber+
        "&route="+DataConstants.route+
        "&type="+DataConstants.type+
        "&sms="+u+"&templateid="+DataConstants.forgotpasstemplateid;



    var dataasync = await http.get(
      Uri.parse(url),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

      //     body: <String, String>{
      //   'Content-Type': 'application/x-www-form-urlencoded',
      //
      //   'uuid': date,
      //   'timestamp': date
      // }

    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);




  }


}

