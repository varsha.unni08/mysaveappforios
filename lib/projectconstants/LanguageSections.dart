import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
class LanguageSections{

  static final String lan="en";




  static Future<String> getLanguageResponse(String lan) async
  {
    String response="";
    try {

      if(lan.compareTo("en")==0) {
        response = await rootBundle.loadString('assets/strings_en.xml');
      }
      else if(lan.compareTo("ml")==0)
        {
          response = await rootBundle.loadString('assets/strings_ml.xml');

        }
      else if(lan.compareTo("ar")==0)
      {
        response = await rootBundle.loadString('assets/strings_ar.xml');

      }
      else if(lan.compareTo("hi")==0)
      {
        response = await rootBundle.loadString('assets/strings_hi.xml');

      }
      else if(lan.compareTo("kn")==0)
      {
        response = await rootBundle.loadString('assets/strings_kn.xml');

      }
      else if(lan.compareTo("mr")==0)
      {
        response = await rootBundle.loadString('assets/strings_mr.xml');

      }
      else if(lan.compareTo("ta")==0)
      {
        response = await rootBundle.loadString('assets/strings_ta.xml');

      }
      else if(lan.compareTo("te")==0)
      {
        response = await rootBundle.loadString('assets/strings_te.xml');

      }


    } on Exception catch (_) {
      print('not connected');

      response="";
    }

    return      response;;
  }

  static Future<String> setLanguage() async
  {
    String? item="",languagedata="";
    final datacount = await SharedPreferences.getInstance();
    item= await datacount.getString(LanguageSections.lan);

    if(item!=null) {
      if (item.toString().compareTo("te") == 0) {
        languagedata = "తెలుగు";
      }
      else if (item.toString().compareTo("en") == 0) {
        languagedata = "English";
      }

      else if (item.toString().compareTo("mr") == 0) {
        languagedata = "मराठी";
      }
      else if (item.toString().compareTo("ta") == 0) {
        languagedata = "தமிழ்";
      }

      else if (item.toString().compareTo("kn") == 0) {
        languagedata = "ಕನ್ನಡ";
      }

      else if (item.toString().compareTo("hi") == 0) {
        languagedata = "हिंदी";
      }


      else if (item.toString().compareTo("ml") == 0) {
        languagedata = "മലയാളം";
      }

      else {
        languagedata = "اَلْعَرَبِيَّةُ";
      }
    }
    else{

      languagedata = "English";

    }

    return languagedata;
  }





}