import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import 'ServerMessageType.dart';

class DataConstants{

 static List<String> expenses=["Paper and periodicals","Books and knowledge","Education expenses",
  "Child care expenses","Entertainments","Hospital and healthcare","Household items",
  "Utensils and kitchen items","Furniture and fixtures","Grocery and stationery",
  "Vegetables and meats","Milk, bakery and snacks","Agriculture expenses","Telephone Expenses and recharges"
        "Electricity charges","TV and internet charges","Dress/clothing and footwear","Personal care expenses",
  "Bank charges","Interest paid expenses","Tax/duties and levies","Defaults and penalties",
  "Rent paid","House maintenance expenses","Conveyance","Diesel/petrol charges","Vehicle maintenance",
  "Tour and travelling expenses","Hotel and outing Expenses","Drinks and beverages",
  "Club and recreations","Donations","Charity","Miscellaneous expenses"];



static  List<String>reciptAccount=["Salary","Agriculture income","Income from business","Interest from investments",
  "Dividend from share and debentures","Rent received","Professional income","Miscellaneous income"];


 static List<String>status=["Initial","Completed","Postponed"];



   static final String sender="CGSAVE";
   static final String forgotpasstemplateid="1007856104698741987";
   static final String registrationtemplateid="1007625690429475781";
   static final String registration_Confirm_templateid="1007134283594642980";

   static final String route="2";

   static final String type="1";

   static final String apikey="bf25917c3254cfe9f50694f24884f23a";













  static final String tokenkey="tokenkey";

   static final String baseurl="https://mysaving.in/IntegraAccount/api/";

 //static final String baseurl="https://mysaving.in/TestApi/IntegraAccount/api/";

   static final String smsbaseurl="http://eapoluenterprise.in/httpapi/";
   static final String sliderimageurl="https://mysaving.in/images/";

    static final  String profileimgbaseurl="https://mysaving.in/uploads/profile/";
  static final String linkimg="https://mysaving.in/images/";

  static final   String backupbaseurl="https://mysaving.in/IntegraAccount/backups/";

 //static final   String backupbaseurl="https://mysaving.in/TestApi/IntegraAccount/backups/";

  // uploadBackupFile.php check this also





   static final String backuptoken="BACKUPTOKEN";

   static final String userkey="userkey";

 static final String isappPin="isappPin";

   static final String firstuser="FIRSTUSER";

   static final String firstpassword="FIRSTPassword";
    static final  String AccSettingsFirst="AccSettingsFirst";
 static final String userDataPermissionkey="userDataPermissionkey";
 static final String ApptrackPermissionkey="ApptrackPermissionkey";











   static final String UserLogin='UserLogin.php';
   static final String getUserByMobile="getUserByMobile.php";
    static final String smsMethode="httpapi";
    static final String changePassword="changePassword.php";
    static final String getCountry="getCountry.php";
 static final String getSingleCountry="getSingleCountry.php";

    static final String getState="getState.php";
    static final String checkSponsor="getSponserDetailsByMobile.php";
  static final String getCouponcodeData="getCouponcodeData.php";
    static final String userAuthenticate="UserAuthenticate.php";
    static final String getUserDetails="getUserDetails.php";
    static final String getSliderImages="getSlideBanner.php";
    static  final String showMemberDetails="getDSTByPhoneNumber.php";
    static final String updatePosition="updatePosition.php";
    static final String updatePositionNext="updatePositionNext.php";
    static final String getSettingsSlider="getSettingsSlider.php";
   static final String updateProfile="UserProfileUpdate.php";
   static final String uploadUserProfile="uploadUserProfile.php";
  static final String addFeedback="addFeedback.php";
 static final String getFeedback="getFeedback.php";
  static final String checkSalesInfo="getpaymentDetails.php";

  static final String validateExpirydate="validateExpirydate.php";

  static final String validateTrialPeriod="validateTrialPeriod.php";
  static final String getSettingsValue="getSettingsValue.php";
  static final String addSalesInfo="addSalesInfo.php";
  static final String updateGoogleDriveFileId="updateGoogleDriveFileId.php";

  static final String uploadBackupFile="uploadBackupFile.php";
  static final String getNotifications="getNotifications.php";
  static final String updateNotificationStatus="updateNotificationStatus.php";

  static final String getbill="getDSTSales.php";
  static final String deleteAccount="deleteAccount.php";








   static String buildServerMessage(int Type, String otp,String username,String password)
  {
    String message="";

    if(Type==ServerMessageType.forgot_password)
    {

      message="Your OTP for forgot Password is "+otp+" .CGSAVE";
    }

    if(Type==ServerMessageType.registration_Confirm_password)
    {

      message="Your registration is successful. Your Registration ID "+username+" Passowrd "+password+" .CGSAVE";
    }

    if(Type==ServerMessageType.registration)
    {

      message="Dear "+username+" Welcome to SAVE App - My Personal App. Your OTP is "+otp+" CGSAVE";
    }


    return message;
  }


  static final List<String> arrofLanguages = ['Select your language','English','हिंदी','മലയാളം','தமிழ்','ಕನ್ನಡ','తెలుగు','मराठी','اَلْعَرَبِيَّةُ'];

  //  static class ServerMessage{
  //
  // public static String sender="CGSAVE";
  // public static String forgotpasstemplateid="1007856104698741987";
  // public static String registrationtemplateid="1007625690429475781";
  // public static String registration_Confirm_templateid="1007134283594642980";
  //
  // public static String route="2";
  //
  // public static String type="1";
  //
  // public static String apikey="bf25917c3254cfe9f50694f24884f23a";
  //
  // }


static  bool checkEmailPattern(var email)
{

  bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);

  return emailValid;
}





static String bankAccountType="Bank";
 static String cashAccountType="Cash";
 static String allAccountType="";
 static String investmentType="Investment";
 static String customerAccountType="Customers";
 static String incomeAccountType="Income account";
 static String billnumber="Billnumber";
 static String billvoucherNumber="Save_Bill_000";


  static int credit=1;
  static int debit=0;






  static int paymentvoucher=1;
  static int receiptvoucher=2;

  static int billvoucher=3;

  static int bankvoucher=5;

  static int journalvoucher=4;

  static int wallet=6;


 static Image imageFromBase64String(String base64String) {
   return Image.memory(
     base64Decode(base64String),
     fit: BoxFit.fill,
   );
 }

 static Uint8List dataFromBase64String(String base64String) {
   return base64Decode(base64String);
 }

 static String base64String(Uint8List data) {
   return base64Encode(data);
 }


 static String whatsapp="919846290789";
 static String domain="https://mysaving.in/";



 //Test credentials

       static  final String TestMerchantID=
              "eAoUUb21380278750445";
         static final String TestMerchantKey=
        "@q%YFqxqTmAsmuWi";
         static final String Website=
        "WEBSTAGING";
         static final String IndustryType=
       "Retail";

        static final String ChannelID =
        "WAP";
        static final String callbackUrl="https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=";

        static final String initiatetransactionurl="https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction";









 //live credentials
 //  static final String TestMerchantID=
 //     "GDaxgY06469545723565";
 //  static final String TestMerchantKey=
 //     "z2qhcZY4%W5uMgeK";
 //  static final String Website=
 //     "DEFAULT";
 //  static final String IndustryType=
 //     "Retail";
 //
 //  static String ChannelID =
 //     "WAP";
 //static final String callbackUrl="https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

 //static final String initiatetransactionurl="https://securegw.paytm.in/theia/api/v1/initiateTransaction";





  static Future<bool> checkDBTablempty() async
 {

   bool isdbEmpty=false;
   List<Map<String, dynamic>> accounts = await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_ACCOUNTS);
   List<Map<String, dynamic>>commonData_diarysubject=await new DatabaseHelper().queryAllRows(DatabaseTables.DIARYSUBJECT_table);
   List<Map<String, dynamic>> commonData_diary=await new DatabaseHelper().queryAllRows(DatabaseTables.DIARY_table);
   List<Map<String, dynamic>> commonData_budget=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_BUDGET);
   List<Map<String, dynamic>> commonData_invest=await new DatabaseHelper().queryAllRows(DatabaseTables.INVESTMENT_table);
   List<Map<String, dynamic>> commonData_task=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_TASK);
   // List<CommonData>commonData_acc=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
   List<Map<String, dynamic>>commonData_insu=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_INSURANCE);
   List<Map<String, dynamic>>commonData_iliab=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_LIABILITY);
   List<Map<String, dynamic>>commonData_asset=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_ASSET);
   List<Map<String, dynamic>>commonData_accrecipt=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_ACCOUNTS_RECEIPT);
   List<Map<String, dynamic>>commonData_appin=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_APP_PIN);
   List<Map<String, dynamic>>commonData_wallet=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_WALLET);
   List<Map<String, dynamic>>commonData_doc=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_DOCUMENT);
   List<Map<String, dynamic>>commonData_pass=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_PASSWORD);
   List<Map<String, dynamic>>commonData_visit=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_VISITCARD);


   if(accounts.length>0||commonData_diarysubject.length>0||commonData_diary.length>0||
       commonData_budget.length>0||commonData_invest.length>0||commonData_task.length>0||
       commonData_insu.length>0||commonData_iliab.length>0||commonData_asset.length>0
       ||commonData_accrecipt.length>0||commonData_appin.length>0||commonData_wallet.length>0||commonData_doc.length>0||
       commonData_pass.length>0||commonData_visit.length>0 )
   {



     isdbEmpty=false;



   }
   else {

     isdbEmpty=true;
   }

   return isdbEmpty;
 }


}