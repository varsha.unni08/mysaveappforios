

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/AssetData.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/mainviews/AddAsset.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';
import 'package:saveappforios/database/DBTables.dart';

import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AssetData.dart';
import '../utils/Tutorials.dart';
import 'AddAsset.dart';




class AssetListPage extends StatefulWidget {
  final String title;



  const AssetListPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AssetListPage();

}

  class _AssetListPage extends State<AssetListPage> {

    String diaryid = "0";

     DatabaseHelper dbhelper=new DatabaseHelper();


    String date = "",
        month = "",
        year = "";


    List<String>subject = ["Select subject"];

    String subjectdata = "Select subject";


    String datetxt1 = "Select start date";

    String datetxt2 = "Select end date";

    String languagedropdown = 'Select your language';

    TextEditingController commentcontroller = new TextEditingController();
    TextEditingController feedbackcontroller = new TextEditingController();

     final dbHelper = new DatabaseHelper();

    // DatabaseHelper dbhelper = new DatabaseHelper();

    String stdate="",endate="";


    List<AssetData> assetdata = [];



  @override
  void initState() {
  // TODO: implement initState

    Tutorial.showTutorial(Tutorial.assetTutorial, context, Tutorial.assettutorial);
getAssetData();
SystemChrome.setPreferredOrientations([
  DeviceOrientation.portraitDown,
  DeviceOrientation.portraitUp,
]);

  super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
  return Scaffold( resizeToAvoidBottomInset: true,
    appBar:  AppBar(
      backgroundColor: Color(0xFF096c6c),
      leading: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.white),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Text("Asset",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
      centerTitle: false,
    ),


    body:  Stack(

        children:[



          Align(

              alignment: Alignment.topCenter,
              child:   Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 10): EdgeInsets.fromLTRB(18, 12, 18, 12):EdgeInsets.fromLTRB(21, 15, 21, 15),


                child: Container(child:
                MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),

                      shrinkWrap: true,

                      itemCount: assetdata.length,
                      itemBuilder: (context, index) {
                        return  Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) : EdgeInsets.all(6):EdgeInsets.all(8) ,

                            child: Card(
                          elevation: 6,
                          child: Container(


                            width: double.infinity,


                            child: Padding(



                           padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12):EdgeInsets.all(15),
                           child: Column(

                              children: [

                           Padding(



                           padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) : EdgeInsets.all(11),
                              child:
                                Row(
                                  children: [

                                    Expanded(
                                      child: Text("Account name",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
                                      flex: 2,
                                    ),
                                    Expanded(
                                      child: Text(":",style:TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19) ,),
                                      flex: 1,
                                    ),
                                    Expanded(
                                      child: Text(
                                        assetdata[index].name,
                                        style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),
                                      ),
                                      flex: 2,
                                    )


                                  ],

                                )),

                                Padding(



                                    padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) : EdgeInsets.all(11),

                                    child:
                                    Row(
                                      children: [

                                        Expanded(
                                          child: Text("Amount",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),),
                                          flex: 2,
                                        ),
                                        Expanded(
                                          child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19)),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Text(
                                            assetdata[index].amount,
                                            style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),
                                          ),
                                          flex: 2,
                                        )


                                      ],

                                    )),


                                Divider(
                                  thickness: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 1:1.1:1.3,
                                  color: Colors.black26,
                                ),

                                Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4) :EdgeInsets.all(6),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: TextButton(
                                            onPressed: () async {



                                              Map results = await Navigator.of(context)
                                                  .push(new MaterialPageRoute<dynamic>(
                                                builder: (BuildContext context) {
                                                  return new AddAssetListPage(
                                                    title: "AddAsset", assetid:assetdata[index].id,
                                                  );
                                                },
                                              ));

                                              if (results != null &&
                                                  results.containsKey('accountsetupdata')) {
                                                setState(() {
                                                  var acc_selected =
                                                  results['accountsetupdata'];

                                                  int acs =
                                                  acc_selected as int;

                                                  if(acs!=0) {



                                                    getAssetData();



                                                  }




                                                });
                                              }






                                            },
                                            child: Text(
                                              "Edit",
                                              style: TextStyle(
                                                fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19,
                                                  color: Colors.lightGreen),
                                            ),
                                          ),
                                          flex: 2,
                                        ),
                                        Container(
                                          width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 1:1.1:1.3,
                                          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:50:60,
                                          color: Colors.grey,
                                        ),
                                        Expanded(
                                          child: TextButton(
                                            onPressed: () {

                                              Widget yesButton = TextButton(
                                                  child: Text("Yes"),
                                                  onPressed: () async {

                                                    Navigator.pop(context);

                                                    deleteAssetData( assetdata[index].id,index);


                                                  });



                                              Widget noButton = TextButton(
                                                child: Text("No"),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                              );

                                              // set up the AlertDialog
                                              AlertDialog alert = AlertDialog(
                                                title: Text("Save"),
                                                content: Text("Do you want to delete now ?"),
                                                actions: [yesButton, noButton],
                                              );

                                              // show the dialog
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return alert;
                                                },
                                              );





                                            },
                                            child: Text(
                                              "Delete",
                                              style: TextStyle(
                                                  color: Colors.redAccent),
                                            ),
                                          ),
                                          flex: 2,
                                        ),
                                      ],
                                    ))
                              ],



                            )),
                          ),
                        ));
                      },
                    ))
                ),)

          ),










          Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(12) : EdgeInsets.all(18) : EdgeInsets.all(24),
                  child: FloatingActionButton(
                    onPressed: () async {

                      Map results = await Navigator.of(context)
                          .push(new MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) {
                          return new AddAssetListPage(
                            title: "AddAsset", assetid: '0',
                          );
                        },
                      ));

                      if (results != null &&
                          results.containsKey('accountsetupdata')) {
                        setState(() {
                          var acc_selected =
                          results['accountsetupdata'];

                          int acs =
                          acc_selected as int;

                          if(acs!=0) {



getAssetData();



                          }




                        });
                      }



                    },
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 29:35:40,
                    ),
                    backgroundColor: Colors.blue,
                    elevation: 5,
                    splashColor: Colors.grey,
                  ))),
        ]




      ),






  );

  }


  getAssetData() async
  {

    // assetdata['name']=accountid;
    // assetdata['amount']=amountcontroller.text.toString();
    // assetdata['purchase_date']=datetxt;
    // assetdata['remind_date']=rminddates;
    // assetdata['remarks']=remarkcontroller.text.toString();

    List<AssetData>asd=[];




    List<Map<String, dynamic>> a =
        await dbHelper.queryAllRows(DatabaseTables.TABLE_ASSET);
    List<AssetData> acsdata = [];


    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["name"];
      String amount=jsondata["amount"];
      AssetData asdata=new AssetData();
      asdata.id=id.toString();
      asdata.name=await getDataById(Accountname);
      asdata.amount=amount;

      acsdata.add(asdata);



    }

    setState(() {
      assetdata.clear();

      assetdata.addAll(acsdata);
    });

  }


   Future<String> getDataById(String id) async
    {
      List<Map<String, dynamic>> a =
      await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
      // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
      List<String> accountsetupdata = [];
      String Accountname = "";

      accountsetupdata.add(subjectdata);
      for (Map ab in a) {
        print(ab);

        int subid = ab["keyid"];
        String data = ab["data"];

        var jsondata = jsonDecode(data);


        if (subid.toString().compareTo(id) == 0) {
           Accountname = jsondata["Accountname"];
        }
      }

      return Accountname;
    }

    void deleteAssetData(String id,int index)
    {
      dbhelper.deleteDataByid(id, DatabaseTables.TABLE_ASSET);

      setState(() {

        assetdata.removeAt(index);
      });
    }
  }
