import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../projectconstants/DataConstants.dart';
import '../utils/Tutorials.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';

import 'ReportAccounts.dart';

class ReportPageAccount extends StatefulWidget {
  final String title;
  final String accounttype;

  const ReportPageAccount({Key? key, required this.title, required this.accounttype}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ReportPage(accounttype);
}

class _ReportPage extends State<ReportPageAccount> {


  String accounttype;


  _ReportPage(this.accounttype);

  String date = "",
      month = "",
      year = "";

  String datetxt = "Select date";

  List<String>accountsdata = [];

  List<String> accountsetupdata = [];

  List<DataRow>accountsdatarow = [];

  List<String>months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  String currentyear = "";

  List<String>tableheaddata = [

    "Account",
    "Closing balance",

    "Action"
  ];


  List<DataColumn>tableheadings = [];

  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;
  String currentmonth = "";

  int currentmonthindex = 0;

  String selectedmonth = "",
      selectedyear = "";

  DatabaseHelper dbhelper=new DatabaseHelper();


  @override
  void initState() {
    // TODO: implement initState
    
    if(accounttype.compareTo("Asset account")==0)
      {
        Tutorial.showTutorial(Tutorial.listofmyassets_tutorial, context, Tutorial.listofmyassetstutorial);


      }

    if(accounttype.compareTo("Liability account")==0)
    {
      Tutorial.showTutorial(Tutorial.listofliabilities_tutorial, context, Tutorial.listofliabilitiestutorial);


    }

    if(accounttype.compareTo("Insurance")==0)
    {
      Tutorial.showTutorial(Tutorial.listofmyinsurance_tutorial, context, Tutorial.listofmyinsurancetutorial);


    }

    if(accounttype.compareTo("Investment")==0)
    {
      Tutorial.showTutorial(Tutorial.listofmyinvestment_tutorial, context, Tutorial.listofmyinvestmenttutorial);


    }

loadAccountHeads();
    // loadTableHeading();

    super.initState();
SystemChrome.setPreferredOrientations([
  DeviceOrientation.portraitDown,
  DeviceOrientation.portraitUp,
]);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("List of my "+accounttype,style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

    body: Stack(
    children: [

      (accountsetupdata.length>0)?    Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 0),




          child: MediaQuery.removePadding(
            context: context,
            removeTop: true, child:GridView.builder(
            physics: BouncingScrollPhysics(),

            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 0.0,
                mainAxisSpacing: 0.0,
                childAspectRatio: 2.0

            ),
            itemCount: tableheaddata.length,
            itemBuilder: (context, index) {
              return  Container(
                  height: 50,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black54,
                      width: 0.3,
                    ),
                  ),

                  child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),maxLines: 2,)));


            },
          )
            ,)



      ):Container(),

      (accountsetupdata.length>0)?    Padding(padding:EdgeInsets.fromLTRB(10, 72, 10, 0)

          ,child:MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child:GridView.builder(
                physics: BouncingScrollPhysics(),

                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 0.0,
                    childAspectRatio:2.0

                ),
                itemCount: accountsetupdata.length,
                itemBuilder: (context, index) {
                  return Container(

                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black54,
                        width: 0.3,
                      ),
                    ),

                    child:

                    ((index+1)%3==0)?
                    Center(child:TextButton(onPressed: ()async {


                      String accountid=accountsetupdata[index].split(",")[1];

                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) =>
                          ReportAccounts(title: "AccountReport", accid: accountid,)));

                    },
                        child:Text(accountsetupdata[index].split(",")[0],style: TextStyle(fontSize: 13,color: Colors.lightGreen),)))
                        :Center(child:Text(accountsetupdata[index],style: TextStyle(fontSize: 13,color: Colors.black),)),

                  );
                },
              ))):Container(),

    ]

    )


    );
  }


  void loadAccountHeads() async{

    List<Map<String, dynamic>> a =
        await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupd = [];
    String Accountname = "";

    // accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int subid = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);



        Accountname = jsondata["Accountname"];
      String Accounttype = jsondata["Accounttype"];
      String Amount=jsondata['Amount'];

      if(Amount.isEmpty)
        {
          Amount="0";
        }

      if(Accounttype.compareTo(accounttype)==0)
        {

          // AccountReport acr=new AccountReport();
          // acr.id=subid.toString();
          // acr.accountname=Accountname;
          double a= await getClosingBalance(subid, double.parse(Amount));
          accountsetupd.add(Accountname);
          accountsetupd.add(a.toString());
          accountsetupd.add("View,"+subid.toString());




          //acr.closingbalance=a.toString();
         // accountsetupd.add(acr);


        }

    }

    setState(() {

      accountsetupdata.clear();
          accountsetupdata.addAll(accountsetupd);
    });

  }


  Future<double>getClosingBalance(int accountid,double openingbalance) async
  {
    double closingbalance=openingbalance;
    var m=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);
    List<Map<String, dynamic>> v=m;

    for(Map a in v) {
      String setupidaccount = a[DatabaseTables.ACCOUNTS_setupid];
      String ab=accountid.toString();
      String accountsamount = a[DatabaseTables.ACCOUNTS_amount];
      if(setupidaccount.compareTo(ab)==0)
        {

          double amount=double.parse(accountsamount);

          String accountstype = a[DatabaseTables.ACCOUNTS_type].toString();
          if (accountstype.compareTo(DataConstants.debit.toString()) == 0) {
            closingbalance=closingbalance+amount;
          }
          else {
            closingbalance=closingbalance-amount;
          }



        }
    }





    return closingbalance;
  }

}