import 'dart:collection';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/services.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/IncomeExpenseData.dart';
import '../projectconstants/LanguageSections.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml_parser/xml_parser.dart';

import '../utils/Tutorials.dart';

class FChartPage extends StatefulWidget {
  final String title;

  const FChartPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FChartPage();

}

class _FChartPage extends State<FChartPage> {

  List<BarChartRodData> br=[];
  List<BarChartGroupData>brg=[];

  List<String> years=[];
  String year="";
  List<int> months=[1,2,3,4,5,6,7,8,9,10,11,12];

  List<String> arrmonth=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  DatabaseHelper dbhelper=new DatabaseHelper();

  List<Map<String,dynamic>> allaccountsettings=[];
  List<AccountSetupData> incomeaccount = [];
  List<AccountSetupData> expenseaccount = [];
  List<Map<String,dynamic>>accounts=[];

  int id=0;
  String data ="";
  String amountdata="";
  var jsondata =null;
  String Accounttype = "";

  String Accountname = "";

  double Amount = 0;
  String Type = "";
  AccountSetupData dataaccsetup=new AccountSetupData();
  Map<String, dynamic> mapval =new HashMap();
  String accountdate = "";

  String yearaccount="";

  String month="";

  String nodatafound="No data found",viewchart="View chart",totalexpense="Total expense",totalincome="Total income";

  List<IncomeExpenseDataToChart>incomedata=[];
  List<IncomeExpenseDataToChart>expensedata=[];

  bool isvalueExists=false;

  @override
  void initState() {
    // TODO: implement initState
    Tutorial.showTutorial(Tutorial.viewcharttutorial, context, Tutorial.viewchart);


    var now = DateTime.now();
    int y=now.year;

    year = y.toString();

    years.add(year);

    for(int i=0;i<5;i++)
    {

      int a=y+1;

      years.add(a.toString());
      y=a;


    }

    createChartData();
    checkLanguage();
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }


  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(viewchart, style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  14:16:18),),
        centerTitle: false,
      ),
      body:Stack(

        children: [

          Align(

            alignment: FractionalOffset.topCenter,

            child:   Column(

              children: [
                new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [

                      Expanded(
                        child:Container(
                          margin: const EdgeInsets.fromLTRB(2, 20, 0, 0),



                          child:   Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,

                            children: [




                              (isvalueExists) ?  Row(
                                children: [

                                  SizedBox(width: 12,height: 12,child: Container(color: Colors.amber,),),

                                  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(6, 0, 0, 0) : EdgeInsets.fromLTRB(9, 0, 0, 0) : EdgeInsets.fromLTRB(13, 0, 0, 0),

                                    child: Text(
                                      totalincome,
                                      style: TextStyle(
                                          fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 10:12:14, color: Colors.black),
                                    ),
                                  ),

                                  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(6, 0, 0, 0) : EdgeInsets.fromLTRB(9, 0, 0, 0) : EdgeInsets.fromLTRB(13, 0, 0, 0),

                                      child: SizedBox(width: 12,height: 12,child: Container(color: Colors.green,),)),

                                  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(6, 0, 0, 0) : EdgeInsets.fromLTRB(9, 0, 0, 0) : EdgeInsets.fromLTRB(13, 0, 0, 0),

                                    child: Text(
                                      totalexpense,
                                      style: TextStyle(
                                          fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 10:12:14, color: Colors.black),
                                    ),
                                  )




                                ],

                              ) :Container()



                            ],

                          ),),
                        flex: 5,
                      ),
                    ]),

                Padding(
                    padding: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(8):EdgeInsets.all(12),
                    child: Container(
                      width: double.infinity,
                      height:ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? 45 :55:75,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          // red as border color
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: InputDecorator(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                value: year,
                                items: years.map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                onChanged: (String? newValue) {




                                  setState(() {
                                    year=newValue.toString();
                                    createChartData();

                                    //_createSampleData();
                                  });



                                },
                                //  value: dropdownValue,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )),


              ],


            )




          ),

          (isvalueExists)?  Align(

          alignment: FractionalOffset.center,

          child:

              SizedBox(

                width: double.infinity,
                height: 500,

                child:   Padding(
                  padding: EdgeInsets.fromLTRB(10, 100, 10, 10),
                  child: BarChart(BarChartData(
                      borderData: FlBorderData(
                          border: const Border(
                            top: BorderSide.none,
                            right: BorderSide.none,
                            left: BorderSide(width: 1),
                            bottom: BorderSide(width: 1),
                          )),
                      groupsSpace: 10,
                      barGroups: brg)),
                ),

              )


        ,) :

          Container(

            width: double.infinity,
       height: double.infinity,

       child:   Column(

            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,

            children: [

              Padding(
                padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:10.0,right: 10.0,top:10,bottom: 0):EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 0):EdgeInsets.only(left:18.0,right: 18.0,top:18,bottom: 0),
                // padding: EdgeInsets.all(15),
                child: new Theme(data: new ThemeData(
                    hintColor: Colors.white
                ), child: Text(

             nodatafound, style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?12:15:18 ),



                )),
              ),

            ],


          )
          )



        ],

      )








    );
  }

  createChartData() async
  {
    incomeaccount.clear();
    expenseaccount.clear();

    incomedata.clear();
    expensedata.clear();
    brg.clear();
    br.clear();


    allaccountsettings= await dbhelper.queryAllRows( DatabaseTables.TABLE_ACCOUNTSETTINGS);

    for (Map ab in allaccountsettings) {


      id = ab["keyid"];
    data = ab["data"];

    jsondata = jsonDecode(data);
    Accounttype = jsondata["Accounttype"];

    Accountname = jsondata["Accountname"];

    // Amount = jsondata["Amount"],
    Type = jsondata["Type"];



    if(Accounttype.compareTo("Income account")==0)
    {
    dataaccsetup = new AccountSetupData();
    dataaccsetup.id = id.toString();
    dataaccsetup.Accountname = Accountname;
    dataaccsetup.Accounttype = Accounttype;
    dataaccsetup.Amount = jsondata["Amount"];
    dataaccsetup.Type = Type;

    incomeaccount.add(dataaccsetup);

    }

    else if(Accounttype.compareTo("Expense account")==0)
    {
    dataaccsetup = new AccountSetupData();
    dataaccsetup.id = id.toString();
    dataaccsetup.Accountname = Accountname;
    dataaccsetup.Accounttype = Accounttype;
    dataaccsetup.Amount = jsondata["Amount"];
    dataaccsetup.Type = Type;

    expenseaccount.add(dataaccsetup);

    }


    };




    for(int m in months)
    {
  double   Amount=0;

  List<BarChartRodData> br=[];

    Amount=await  getAmountBymonthIncome(m,incomeaccount);




   double exAmount=0;

  exAmount=await  getAmountBymonthExpense(m,expenseaccount);

  if(Amount==0&&exAmount==0)
    {
      setState(() {

        isvalueExists=false;
      });

    }
  else{

    setState(() {

      isvalueExists=true;
    });

  }


  BarChartRodData brc=new BarChartRodData( width: 15, color: Colors.amber, toY: Amount);
  BarChartRodData brc1=new BarChartRodData( width: 15, color: Colors.green, toY: exAmount);


  br.add(brc);
  br.add(brc1);




  setState(() {
    brg.add(new BarChartGroupData(x: m,barRods:br ));

    //_createSampleData();
  });

    //     .
    //
    //
    // then((value) => {
    //
    //   expensedata.add(IncomeExpenseDataToChart(arrmonth[m-1], value))
    //
    //
    //
    //
    //
    // });


    }











  }

  checkLanguage() async
  {


    final datacount = await SharedPreferences.getInstance();

    String? language =await LanguageSections.setLanguage();

    String? langdata= datacount.getString(LanguageSections.lan);

    if(langdata!=null)
    {


    }
    else{

      langdata="en";
    }









    updateLanguageValues(langdata);


  }


  updateLanguageValues(String l)async
  {
    String response = await    LanguageSections.getLanguageResponse(l);
    List<XmlElement> ?elements = XmlElement.parseString(
      response,
      returnElementsNamed: ['string'],

    );

    for(XmlElement xe in elements!)
    {


      if(xe.attributes![0].value.toString().compareTo("nodatafound")==0)
      {

        setState(() {
          nodatafound=xe.text.toString();

        });

      }
      if(xe.attributes![0].value.toString().compareTo("viewchart")==0)
      {

        setState(() {
          viewchart=xe.text.toString();

        });

      }







      if(xe.attributes![0].value.toString().compareTo("totalincome")==0)
      {

        setState(() {
          totalincome=xe.text.toString();

        });

      }

      if(xe.attributes![0].value.toString().compareTo("totalexpense")==0)
      {

        setState(() {
          totalexpense=xe.text.toString();

        });

      }





    }



  }


  Future<double> getAmountBymonthIncome(int m,List<AccountSetupData> acc)async
  {
    Amount=0;
    var m1 =await new DatabaseHelper().queryAllRows( DatabaseTables.TABLE_ACCOUNTS);

    if(m1!=null) {
      List<Map<String, dynamic>> v = m1;

      for (AccountSetupData ac in acc) {
        for (Map ab in v) {
          yearaccount = ab[DatabaseTables.ACCOUNTS_year].toString();
          month = ab[DatabaseTables.ACCOUNTS_month].toString();
          amountdata = ab[DatabaseTables.ACCOUNTS_amount].toString();
          String setupid = ab[DatabaseTables.ACCOUNTS_setupid].toString();

          double a = double.parse(
              ab[DatabaseTables.ACCOUNTS_amount].toString());


          if (setupid.compareTo(ac.id) == 0) {
            if (year.compareTo(yearaccount) == 0) {
              if (m == int.parse(month)) {
                Amount = Amount + a;
              }
            }
          }
        }
      }
    }
    else{

      Amount=0;
    }





    return Amount;

  }

  Future<double> getAmountBymonthExpense(int m,List<AccountSetupData> acc)async
  {
    Amount=0;

    var m1 =await new DatabaseHelper().queryAllRows( DatabaseTables.TABLE_ACCOUNTS);
    if(m1!=null) {
      List<Map<String, dynamic>> v = m1;

      for (AccountSetupData ac in acc) {
        for (Map ab in v) {
          yearaccount = ab[DatabaseTables.ACCOUNTS_year].toString();
          month = ab[DatabaseTables.ACCOUNTS_month].toString();
          amountdata = ab[DatabaseTables.ACCOUNTS_amount].toString();
          String setupid = ab[DatabaseTables.ACCOUNTS_setupid].toString();

          double a = double.parse(
              ab[DatabaseTables.ACCOUNTS_amount].toString());


          if (setupid.compareTo(ac.id) == 0) {
            if (year.compareTo(yearaccount) == 0) {
              if (m == int.parse(month)) {
                Amount = Amount + a;
              }
            }
          }
        }
      }
    }
    else{

      Amount=0;
    }



    return Amount;

  }


}