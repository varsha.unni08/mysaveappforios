import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:saveappforios/domain/MileStoneData.dart';
import 'package:saveappforios/views/view_mile_stone_added_amount_details.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';

class ViewMileStones extends StatefulWidget {
  String targetid;

   ViewMileStones(this.targetid) : super();

  @override
  _ViewMileStonesState createState() => _ViewMileStonesState(targetid);
}

class _ViewMileStonesState extends State<ViewMileStones> {

  String targetid;

  List<MileStoneData>milestonedata=[];

  _ViewMileStonesState(this.targetid);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMileStoneDetails();
  }

  String date="",datetxt="";

  String month="0",year="0";

  TextEditingController amountcontroller=new TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(


      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () =>    Navigator.of(context).pop({'mydreamadded':1 }),
        ),
        title: Text("View Milestones",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),

      body: Stack(

        children: [


          Align(
            alignment: FractionalOffset.topCenter,

            child: ListView.builder(
                itemCount: milestonedata.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                      elevation: 4,

                      child: Column(

                        children: [

                          Row(


                            children: [


                              Expanded(child:    Column(

                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,

                                children: [

                                  Padding(padding: EdgeInsets.all(7),


                                    child: Text("Start Date : "+milestonedata[index].start_date,style: TextStyle(fontSize: 13,color: Colors.black),),



                                  ),


                                  Padding(padding: EdgeInsets.all(7),


                                    child: Text("End Date : "+milestonedata[index].end_date,style: TextStyle(fontSize: 13,color: Colors.black),),



                                  ),

                                  Padding(padding: EdgeInsets.all(7),


                                    child: Text("Amount : "+milestonedata[index].amount,style: TextStyle(fontSize: 13,color: Colors.black),),



                                  )




                                ],

                              ),flex: 2, ),


                              Expanded(child: Container(

                                  child:  GestureDetector(
                                    onTap: () {


                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) => ViewMileStoneAddedAmountDetails(milestonedata[index])));

                                    },
                                    child:

                                    Icon(Icons.arrow_forward_ios,size: 25,color: Colors.black12,),)

                              ),flex: 1,)



                            ],


                          ),

                          Row(


                            children: [

                              Expanded(



                                child: Padding(padding: EdgeInsets.all(5),

                                  child: TextButton(

                                    onPressed: (){





    showDialog(
    context: context,
    builder: (BuildContext context) {
      return MyDialog(milestonedata[index]);
    }
    ).then((value) => {


     if(value["updated"].toString().compareTo("1")==0){

       getMileStoneDetails()
     }







      // showAccountDetails()




    });



















                                    },

                                    child: Text("Edit",style: TextStyle(color: Colors.green,fontSize: 14),),




                                  ),



                                ),

                              ),

                              Expanded(



                                child: Padding(padding: EdgeInsets.all(5),

                                  child: TextButton(

                                    onPressed: (){

                                      Widget yesButton = TextButton(
                                          child: Text("Yes"),
                                          onPressed: () async {

                                            Navigator.pop(context);

                                            new DatabaseHelper().deleteDataByid(milestonedata[index].milestoneid, DatabaseTables.TABLE_MILESTONE);


                                            milestonedata.removeAt(index);

                                            // deleteLiabilityData( lbdata[index].id,index);


                                          });



                                      Widget noButton = TextButton(
                                        child: Text("No"),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      );

                                      // set up the AlertDialog
                                      AlertDialog alert = AlertDialog(
                                        title: Text("Save"),
                                        content: Text("Do you want to delete milestone data now ?"),
                                        actions: [yesButton, noButton],
                                      );

                                      // show the dialog
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return alert;
                                        },
                                      );



                                    },

                                    child: Text("Delete",style: TextStyle(color: Colors.red,fontSize: 14),),




                                  ),



                                ),
                                flex: 1,
                              )


                            ],

                          )

                        ],

                      )






                  );
                }),



          )




        ],


      ),

    );
  }

  getMileStoneDetails()
  async {

    List<Map<String,dynamic>>mp= await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_MILESTONE);


    print(mp.length);

    // mobject_milestone['amount'] =
    //     ml.amount;
    // mobject_milestone['start_date'] =
    //     ml.start_date;
    // mobject_milestone['end_date'] =
    //     ml.end_date;
    // mobject_milestone['targetid'] =
    //     id.toString();

    List<MileStoneData>mldata=[];
    for(int i=0;i<mp.length;i++) {
      Map<String, dynamic> ab = mp[i];
      int id = ab["keyid"];
      String data = ab["data"];
      Map jsondata = jsonDecode(data);

      String milestoneid=id.toString();
      String tid=jsondata['targetid'];
      String end_date=jsondata['end_date'];
      String start_date=jsondata['start_date'];
      String amount=jsondata['amount'];

      if(targetid.compareTo(tid)==0) {
        MileStoneData mld = new MileStoneData();
        mld.targetid = tid;
        mld.amount = amount;
        mld.start_date = start_date;
        mld.end_date = end_date;
        mld.milestoneid = milestoneid;
        mldata.add(mld);
      }


    }

    setState(() {
      milestonedata.clear();
     milestonedata.addAll(mldata);
    });


  }
}

class MyDialog extends StatefulWidget {
  MileStoneData mileStoneData;

  MyDialog(this.mileStoneData);


  @override
  _MyDialogState createState() => new _MyDialogState(this.mileStoneData);
}

class _MyDialogState extends State<MyDialog> {

  String date="",datetxt="",datetxt1="";

  String month="0",year="0";

  MileStoneData mileStoneData;

  _MyDialogState(this.mileStoneData);
  @override
  void initState() {
    // TODO: implement initState


    super.initState();

    setMileStoneData();
  }

  TextEditingController amountcontroller=new TextEditingController();


  setMileStoneData()
  {

    setState(() {
      datetxt=mileStoneData.start_date;
      datetxt1=mileStoneData.end_date;
      amountcontroller.text=mileStoneData.amount;


    });
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content:  Container(

          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height/2,
          child: Column(children: [
            Padding(
                padding: const EdgeInsets.all(5),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,color: Colors.black12,),
                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () async {
                              // showAccountsDialog();

                              var now = DateTime.now();

                              date = now.day.toString() +
                                  "-" +
                                  now.month.toString() +
                                  "-" +
                                  now.year.toString();

                              month = now.month.toString();
                              year = now.year.toString();

                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {
                                                date = newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();

                                                month = newDateTime.month
                                                    .toString();
                                                year = newDateTime.year
                                                    .toString();

                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height: 50,
                                              width: 150,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });




                            },
                            child: Center(
                                child: Text(datetxt,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black38,
                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                          ))
                    ],
                  ),
                )),
            Padding(
                padding: const EdgeInsets.all(5),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,color: Colors.black12,),
                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () async {
                              // showAccountsDialog();

                              var now = DateTime.now();

                              date = now.day.toString() +
                                  "-" +
                                  now.month.toString() +
                                  "-" +
                                  now.year.toString();

                              month = now.month.toString();
                              year = now.year.toString();

                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {
                                                date = newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();

                                                month = newDateTime.month
                                                    .toString();
                                                year = newDateTime.year
                                                    .toString();

                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height: 50,
                                              width: 150,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt1 = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });




                            },
                            child: Center(
                                child: Text(datetxt1,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black38,
                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                          ))
                    ],
                  ),
                )),
            Padding(
              padding: EdgeInsets.all(5),

              child: TextField(
                keyboardType: TextInputType.number,
                controller: amountcontroller,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Amount',
                ),
              ),

            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                height: 50,
                width: 150,
                decoration: BoxDecoration(
                    color: Color(0xF0233048),
                    borderRadius:
                    BorderRadius.circular(
                        10)),
                child: TextButton(
                  onPressed: () {

                    var mobject_milestone = new Map();
                    mobject_milestone['amount'] =
                        amountcontroller.text;
                    mobject_milestone['start_date'] =
                        datetxt;
                    mobject_milestone['end_date'] =
                        datetxt1;
                    mobject_milestone['targetid'] =
                        mileStoneData.targetid;
                    var js2 = json.encode(mobject_milestone);
                    Map<String,
                        dynamic> data_To_Table2 = new Map();
                    data_To_Table2['data'] = js2.toString();

                    new DatabaseHelper().update(
                        data_To_Table2,
                        DatabaseTables.TABLE_MILESTONE,mileStoneData.milestoneid);




                    Navigator.of(context).pop({'updated':1});

                    setState(() {
                      datetxt = date;
                    });
                  },
                  child: Text(
                    'Update',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15),
                  ),
                ),
              ),
            )
          ])),
      // actions: <Widget>[
      //   FlatButton(
      //       child: Text('Switch'),
      //       onPressed: () => setState(() {
      //         _c == Colors.redAccent
      //             ? _c = Colors.blueAccent
      //             : _c = Colors.redAccent;
      //       }))
      // ],
    );
  }





}
