import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:saveappforios/database/DBTables.dart';
import 'package:saveappforios/database/DatabaseHelper.dart';
import 'package:saveappforios/views/add_my_dream.dart';
import 'package:saveappforios/views/dream_details.dart';

import '../design/ResponsiveInfo.dart';
import '../domain/MyDreamEntity.dart';
import '../projectconstants/DataConstants.dart';

class MyDream extends StatefulWidget {
  const MyDream() : super();

  @override
  _MyDreamState createState() => _MyDreamState();
}

class _MyDreamState extends State<MyDream> {


  List<MyDreamEntity>mydreamList=[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMyDreamData();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("My Dream",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),

      body: Stack(
        
        children: [

          (mydreamList.length>0)?

              Align(

                alignment: FractionalOffset.topCenter,

                child: ListView.builder(
                    itemCount: mydreamList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return

                      GestureDetector(

                        child: Card(

                          child: Padding(


                              padding: EdgeInsets.all(5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [

                                  Expanded(child:  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,

                                    children: [

                                      Padding(padding: EdgeInsets.all(6),

                                        child:  Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,

                                          children: [

                                            Expanded(child:Text("Name",style: TextStyle(fontSize: 14),),flex: 2, ),
                                            Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                                            Expanded(child:Text(mydreamList[index].targetname,style: TextStyle(fontSize: 14),),flex: 2, )



                                          ],



                                        ),

                                      ),



                                      Padding(padding: EdgeInsets.all(6),

                                          child:        Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [

                                              Expanded(child:Text("Category",style: TextStyle(fontSize: 14),),flex: 2, ),
                                              Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                                              Expanded(child:Text(mydreamList[index].target_categoryname,style: TextStyle(fontSize: 14),),flex: 2, )



                                            ],



                                          )),


                                      (mydreamList[index].investment_id.compareTo("0")!=0)?  Padding(padding: EdgeInsets.all(6),

                                          child:        Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [

                                              Expanded(child:Text("Investment",style: TextStyle(fontSize: 14),),flex: 2, ),
                                              Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                                              Expanded(child:Text(mydreamList[index].investmentaccount,style: TextStyle(fontSize: 14),),flex: 2, )



                                            ],



                                          )) :Container(),


                                      (mydreamList[index].investment_id.compareTo("0")!=0)?  Padding(padding: EdgeInsets.all(6),

                                          child:        Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [

                                              Expanded(child:Text("Closing balance",style: TextStyle(fontSize: 14),),flex: 2, ),
                                              Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                                              Expanded(child:Text(mydreamList[index].Closingbalance,style: TextStyle(fontSize: 14),),flex: 2, )



                                            ],



                                          )) :Container(),

                                      (mydreamList[index].investment_id.compareTo("0")!=0)?  Padding(padding: EdgeInsets.all(6),

                                          child:        Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [

                                              Expanded(child:Text("Added Amount",style: TextStyle(fontSize: 14),),flex: 2, ),
                                              Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                                              Expanded(child:Text(mydreamList[index].totalamountadded,style: TextStyle(fontSize: 14),),flex: 2, )



                                            ],



                                          )) :Container(),

                                      Padding(padding: EdgeInsets.all(6),

                                          child:        Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [

                                              Expanded(child:Text("Saved Amount",style: TextStyle(fontSize: 14),),flex: 2, ),
                                              Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                                              Expanded(child:Text(mydreamList[index].savedamount,style: TextStyle(fontSize: 14),),flex: 2, )



                                            ],



                                          )) ,

                                      Padding(padding: EdgeInsets.all(6),

                                          child:        Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [

                                              Expanded(child:Text("Target Amount",style: TextStyle(fontSize: 14),),flex: 2, ),
                                              Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                                              Expanded(child:Text(mydreamList[index].targetamount,style: TextStyle(fontSize: 14),),flex: 2, )



                                            ],



                                          )),


                                      Padding(padding: EdgeInsets.all(6),

                                          child:        Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [

                                              Expanded(child:Text(mydreamList[index].percent.toStringAsFixed(2),style: TextStyle(fontSize: 14),),flex: 2, ),


                                              Expanded(child:  LinearProgressIndicator(
                                                backgroundColor:   Color(0xff7fe1e1),
                                                valueColor: new AlwaysStoppedAnimation<Color>(Color(
                                                    0xFF096c6c)),
                                                value: (mydreamList[index].percent/100),
                                              ),  flex: 2, )



                                            ],



                                          ))



                                    ],



                                  ),flex: 3,),

                                  Expanded(child: GestureDetector(
                                    onTap: () async {

                                      // Navigator.push(
                                      //     context, MaterialPageRoute(builder: (_) =>
                                      //     DreamDetails(mydreamList[index])));


                                      Map results = await Navigator.of(context)
                                          .push(new MaterialPageRoute<dynamic>(
                                        builder: (BuildContext context) {
                                          return new DreamDetails(mydreamList[index]);
                                        },
                                      ));

                                      if (results != null &&
                                          results.containsKey('mydreamadded')) {

                                        getMyDreamData();

                                      }



                                    },

                                    child: Icon(Icons.arrow_forward_ios,color: Colors.black12,size: 30,),



                                  ),flex: 1,)



                                ],


                              )





                          ),





                          elevation: 3,
                        ),

                        onTap: () async {

                          Map results = await Navigator.of(context)
                              .push(new MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) {
                              return new DreamDetails(mydreamList[index]);
                            },
                          ));

                          if (results != null &&
                              results.containsKey('mydreamadded')) {

                            getMyDreamData();

                          }


                        },
                      )

                        ;



                    }),


              )




          :Align(
            alignment: FractionalOffset.center,
            
            child: Text("No data found",style: TextStyle(fontSize: 12,color: Colors.black),),
            
            
          ),
          
          
          
          Align(
            
            alignment: FractionalOffset.bottomRight,
            
            child: Padding(
              
              
              padding: EdgeInsets.all(12),
              child:     FloatingActionButton(

                tooltip: 'add',
                onPressed: () async {


                  // Navigator.push(
                  //     context, MaterialPageRoute(builder: (_) => AddMyDream( )));

                  Map results = await Navigator.of(context)
                      .push(new MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) {
                      return new AddMyDream();
                    },
                  ));

                  if (results != null &&
                      results.containsKey('mydreamadded')) {

                    getMyDreamData();

                  }

                },
                child: const Icon(Icons.add),
              ),
            )
            
            
        
            
            
          )
          
        ],
        
        
      ),


    );
  }

  getMyDreamData()async
  {
    List<Map<String,dynamic>>mp= await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_TARGET);


    print(mp.length);

    List<MyDreamEntity>mde=[];

    for(int i=0;i<mp.length;i++)
      {
        Map<String,dynamic> ab=mp[i];

         int id = ab["keyid"];
       String data = ab["data"];
        Map jsondata = jsonDecode(data);
       String targetname = jsondata["targetname"];
        String savedamount = jsondata["savedamount"];
        String target_date = jsondata["target_date"];
        String note = jsondata["note"];
        String target_categoryid = jsondata["target_categoryid"];
        String investment_id=jsondata["investment_id"];
        String targetamount=jsondata["targetamount"];
        String goalreached="0";

        if(ab.containsKey("reached"))
          {
            goalreached=ab["reached"];

          }

        String categoryname=await getCategoryname(target_categoryid);

        String investmentdata=await getInvestmentClosingBalance(investment_id);

        String invaccname="",closingbalance="0";

        double adedamount=await getAddedAmount(id.toString());

        double dreamamount=double.parse(targetamount);

        if(investmentdata.isNotEmpty)
          {
            invaccname=investmentdata.split(",")[0];
            closingbalance=investmentdata.split(",")[1];

          }


        MyDreamEntity myDreamEntity=new MyDreamEntity();
        myDreamEntity.target_categoryid=target_categoryid;
        myDreamEntity.note=note;
        myDreamEntity.target_date=target_date;
        myDreamEntity.savedamount=savedamount;
        myDreamEntity.targetname=targetname;
        myDreamEntity.id=id.toString();
        myDreamEntity.target_categoryname=categoryname;
        myDreamEntity.goalreached=goalreached;

        if(investment_id.compareTo("0")!=0) {
          myDreamEntity.Closingbalance = closingbalance;
          myDreamEntity.investmentaccount = invaccname;
          myDreamEntity.investment_id = investment_id;
          myDreamEntity.totalamountadded = adedamount.toString();

          double d_closingbalance = double.parse(closingbalance);
          double d = adedamount + d_closingbalance;

          myDreamEntity.savedamount = d.toString();

          double a=(d/dreamamount)*100;


          myDreamEntity.percent=a;
        }
        else{

          myDreamEntity.savedamount = adedamount.toString();

          double a=(adedamount/dreamamount)*100;
          myDreamEntity.percent=a;

        }
        myDreamEntity.targetamount=targetamount;













        mde.add(myDreamEntity);

      }






    setState(() {
      mydreamList.clear();

      mydreamList.addAll(mde);
    });


  }


  Future<double>getAddedAmount(String targetid)async{

    double amount=0;

    List<Map<String,dynamic>>mpd= await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_ADDEDAMOUNT_MILESTONE);

    if(mpd.length>0)
      {

        for(int i=0;i<mpd.length;i++) {
          Map<String, dynamic> ab = mpd[i];

          int id = ab["keyid"];
          String data = ab["data"];
          var jsondata = jsonDecode(data);
          String target_id=jsondata['targetid'];

          String amount1=jsondata['savedamount'];

          if(target_id.compareTo(targetid)==0)
            {
              double d=double.parse(amount1);
              amount=amount+d;




            }





        }

      }



    return amount;
  }


  Future<String>getCategoryname(String id) async
  {
    String name="";

    List<Map<String,dynamic>>mpd= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_TARGETCATEGORY,id);


    if(mpd.length>0)
    {


        Map ab=mpd[0];
        int id = ab["keyid"];
        String data = ab["data"];

      name=data;





    }





    return name;
  }



  Future<String>getInvestmentClosingBalance(String id) async
  {
    String name="";

    List<Map<String,dynamic>>mpd=await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, id);


    if(mpd.length>0)
    {


        Map ab=mpd[0];
        int id = ab["keyid"];
        String data = ab["data"];

        var jsondata = jsonDecode(data);

         String Accountname = jsondata["Accountname"];
        String  savedamount=jsondata['Amount'];

        double svedmount=await getClosingBalance(id, double.parse(savedamount));

        name=Accountname+","+svedmount.toString();





    }





    return name;
  }

  Future<double>getClosingBalance(int accountid,double openingbalance) async
  {
    double closingbalance=openingbalance;
    var m=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_ACCOUNTS);
    List<Map<String, dynamic>> v=m;

    for(Map a in v) {
      String setupidaccount = a[DatabaseTables.ACCOUNTS_setupid];
      String ab=accountid.toString();
      String accountsamount = a[DatabaseTables.ACCOUNTS_amount];
      if(setupidaccount.compareTo(ab)==0)
      {

        double amount=double.parse(accountsamount);

        String accountstype = a[DatabaseTables.ACCOUNTS_type].toString();
        if (accountstype.compareTo(DataConstants.debit.toString()) == 0) {
          closingbalance=closingbalance+amount;
        }
        else {
          closingbalance=closingbalance-amount;
        }



      }
    }





    return closingbalance;
  }

}
