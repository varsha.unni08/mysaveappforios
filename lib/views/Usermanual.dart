import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart' show DeviceOrientation, SystemChrome, rootBundle;
import 'package:xml_parser/xml_parser.dart';
import '../projectconstants/LanguageSections.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UsermanualPage extends StatefulWidget {
  final String title;

  const UsermanualPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UsermanualPage();

}

class _UsermanualPage extends State<UsermanualPage> {

  String data="";

  @override
  void initState() {
    // TODO: implement initState
checkLanguage();
   // setupUsermanual();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    super.initState();
  }


  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("How to use",style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

      body: SingleChildScrollView(

        child:
        Padding(padding: EdgeInsets.all(15),
        child: Text(data,style: TextStyle(fontSize: 14,color: Colors.black),))

       ,
      ),

    );
  }


  setupUsermanual()async
  {
    String message=await rootBundle.loadString('assets/usermanual.txt');


    setState(() {

      data=message;
    });

  }


  checkLanguage() async
  {


    final datacount = await SharedPreferences.getInstance();

    String? language =await LanguageSections.setLanguage();

    String? langdata= datacount.getString(LanguageSections.lan);

    if(langdata!=null)
    {


    }
    else{

      langdata="en";
    }









    updateLanguageValues(langdata);


  }


  updateLanguageValues(String l)async
  {
    String response = await LanguageSections.getLanguageResponse(l);
    List<XmlElement> ?elements = XmlElement.parseString(
      response,
      returnElementsNamed: ['string'],

    );

    for (XmlElement xe in elements!) {
      if (xe.attributes![0].value.toString().compareTo("usermanualtxt") == 0) {
        
        String a=data = xe.text.toString();
        
        String d=a.replaceAll(r'\n', '\n');
        
        setState(() {
          data = d;
        });
      }
    }
  }
  }