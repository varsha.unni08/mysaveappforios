import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/mainviews/CashbankAccountDetails.dart';
//
//
// import 'package:save_flutter/domain/Paymentdata.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:save_flutter/mainviews/AddRecipt.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
//
// import 'AddAccountSetup.dart';
import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import 'AddAccountSetup.dart';
import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;
import 'package:numberpicker/numberpicker.dart';


class AddInsurancePage extends StatefulWidget {
  final String title;
  final String liabilityid;

  const AddInsurancePage({Key? key, required this.title, required this.liabilityid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddInsurancePage(liabilityid);

}

class _AddInsurancePage extends State<AddInsurancePage> {


  TextEditingController amountcontroller = new TextEditingController();

  TextEditingController remarkcontroller = new TextEditingController();

  TextEditingController emiamountcontroller = new TextEditingController();

  TextEditingController nofemiamountcontroller = new TextEditingController();

  String date = "",
      date1 = "";
  String datetxt1 = "Select date of payment";
  String datetxt2 = "Select closing date";
  List<String>instype = ["Select insurance type", "Quarterly","Half yearly","Monthly","Yearly"];

  String accountid = "";

  List<String>subject = ["Select insurance account"];

  String subjectdata = "Select insurance account";
  String insurancetype = "Select insurance type";

   DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<Cashbankaccount> cashbankdata = [];

  String accountType = "Insurance";
  String liabilityid;
  String selecteddate = "";


  _AddInsurancePage(this.liabilityid);


  @override
  void initState() {
    // TODO: implement initState
    // setupAccountData();
    //
    if (liabilityid.compareTo("0") != 0) {
      setupInsuranceDataForEdit(liabilityid);
    }

    setupAccountData();

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }






  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Insurance entry",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18),),
        centerTitle: false,
      ),

      body: Stack(
          children: <Widget>[


            SingleChildScrollView(
                child: Column(

                    children: <Widget>[


                      Padding(padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11):EdgeInsets.all(14)

                          ,child:Row(children: [
                            Expanded(

                                flex: 3,
                                child:Padding(padding: EdgeInsets.all(0),
                                  child: Container(
                                    height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,

                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black38)),

                                    child:  DropdownButtonHideUnderline(

                                      child: ButtonTheme(
                                        alignedDropdown: true,
                                        child: InputDecorator(
                                          decoration: const InputDecoration(border: OutlineInputBorder()),
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButton(

                                              isExpanded: true,
                                              value: subjectdata,
                                              items: subject
                                                  .map<DropdownMenuItem<String>>((String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(value),
                                                );
                                              }).toList(),
                                              onChanged: (String? newValue) {
                                                setState(() {
                                                  subjectdata = newValue!;

                                                  // getBudgetData();

                                                  setDataByAccountName(subjectdata);




                                                });
                                              },
                                              style: Theme.of(context).textTheme.bodyText1,

                                            ),
                                          ),
                                        ),
                                      ),
                                    ),


                                  ),


                                )





                            ),

                            Expanded(child: Padding(
                                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(10) : EdgeInsets.all(13),

                                child: FloatingActionButton(
                                  onPressed: () async{


                                    //   showBubjectDialog();

                                    Map results = await Navigator.of(context)
                                        .push(new MaterialPageRoute<dynamic>(
                                      builder: (BuildContext context) {
                                        return new AddAccountSettinglistpage(title: "account setup",accountType: accountType, accountsetupid: '0',);
                                      },
                                    ));

                                    if (results != null &&
                                        results.containsKey('accountsetupdata')) {
                                      setState(() {
                                        var accountsetupdata =
                                        results['accountsetupdata'];

                                        int acs =
                                        accountsetupdata as int;
                                        //
                                        if(acs>0)
                                        {
                                          setupAccountData();
                                        }


                                      });
                                    }

                                  },
                                  child: Icon(Icons.add, color: Colors.white, size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 29:35:40,),
                                  backgroundColor: Colors.blue,

                                  elevation: 5,
                                  splashColor: Colors.grey,
                                )),flex: 1,)


                          ],)),

                      Padding(
                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 0) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 0),
                        // padding: EdgeInsets.all(15),
                        child: new Theme(data: new ThemeData(
                            hintColor: Colors.black
                        ), child: TextField(
                          controller: amountcontroller,

                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black26, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black26, width: 0.5),
                            ),
                            hintText: 'Amount',
                          ),

                          onChanged: (text) {
                            //TemRegData.email=text;

                          },
                        )),
                      ),

                      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8): EdgeInsets.all(11) :EdgeInsets.all(14),
                        child: Container(
                          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:70,

                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black38)),

                          child:  DropdownButtonHideUnderline(

                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: InputDecorator(
                                decoration: const InputDecoration(border: OutlineInputBorder()),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(

                                    isExpanded: true,
                                    value: insurancetype,
                                    items: instype
                                        .map<DropdownMenuItem<String>>((String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    onChanged: (String? newValue) {
                                      setState(() {
                                        insurancetype = newValue!;


                                        // getBudgetData();






                                      });
                                    },
                                    style: Theme.of(context).textTheme.bodyText1,

                                  ),
                                ),
                              ),
                            ),
                          ),


                        ),


                      ),



                      Padding(
                          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black38)),
                            child: Row(
                              textDirection: ui.TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                                    child: Icon(Icons.calendar_month,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,)),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () {



                                          String d="";

                                          var now = DateTime.now();

                                          d = now.day.toString() +
                                              "-" +
                                              now.month.toString() +
                                              "-" +
                                              now.year.toString();



                                          // showCustomDatePicker(build(context));

                                          showModalBottomSheet(
                                              context: context,
                                              builder: (context) {
                                                return Container(
                                                    child: Column(children: [
                                                      Expanded(
                                                        child: CupertinoDatePicker(
                                                          mode: CupertinoDatePickerMode
                                                              .date,
                                                          initialDateTime: DateTime(
                                                              now.year,
                                                              now.month,
                                                              now.day),
                                                          onDateTimeChanged:
                                                              (DateTime newDateTime) {
                                                            d = newDateTime.day
                                                                .toString() +
                                                                "-" +
                                                                newDateTime.month
                                                                    .toString() +
                                                                "-" +
                                                                newDateTime.year
                                                                    .toString();



                                                            //print(date);
                                                            // Do something
                                                          },
                                                        ),
                                                        flex: 2,
                                                      ),
                                                      Padding(
                                                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4):EdgeInsets.all(6),
                                                        child: Container(
                                                          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                          width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                                          decoration: BoxDecoration(
                                                              color: Color(0xF0233048),
                                                              borderRadius:
                                                              BorderRadius.circular(
                                                                  10)),
                                                          child: TextButton(
                                                            onPressed: () {
                                                              Navigator.pop(context);

                                                              setState(() {
                                                                datetxt1 = d;
                                                              });
                                                            },
                                                            child: Text(
                                                              'Ok',
                                                              style: TextStyle(
                                                                  color: Colors.white,
                                                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    ]));
                                              });








                                      },
                                      child: Text(
                                        datetxt1,
                                        style: TextStyle(
                                            color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                                      ),
                                    ))
                              ],
                            ),
                          )),

                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black38)),
                            child: Row(
                              textDirection: ui.TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                    padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12):EdgeInsets.all(14),
                                    child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26 ,)),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () {


                                        String d="";

                                        var now = DateTime.now();

                                        d = now.day.toString() +
                                            "-" +
                                            now.month.toString() +
                                            "-" +
                                            now.year.toString();



                                        // showCustomDatePicker(build(context));

                                        showModalBottomSheet(
                                            context: context,
                                            builder: (context) {
                                              return Container(
                                                  child: Column(children: [
                                                    Expanded(
                                                      child: CupertinoDatePicker(
                                                        mode: CupertinoDatePickerMode
                                                            .date,
                                                        initialDateTime: DateTime(
                                                            now.year,
                                                            now.month,
                                                            now.day),
                                                        onDateTimeChanged:
                                                            (DateTime newDateTime) {
                                                          d = newDateTime.day
                                                              .toString() +
                                                              "-" +
                                                              newDateTime.month
                                                                  .toString() +
                                                              "-" +
                                                              newDateTime.year
                                                                  .toString();



                                                          //print(date);
                                                          // Do something
                                                        },
                                                      ),
                                                      flex: 2,
                                                    ),
                                                    Padding(
                                                      padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4):EdgeInsets.all(6),
                                                      child: Container(
                                                        height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                        width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                                        decoration: BoxDecoration(
                                                            color: Color(0xF0233048),
                                                            borderRadius:
                                                            BorderRadius.circular(
                                                                10)),
                                                        child: TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(context);

                                                            setState(() {
                                                              datetxt2 = d;
                                                            });
                                                          },
                                                          child: Text(
                                                            'Ok',
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ]));
                                            });


                                      },
                                      child: Text(
                                        datetxt2,
                                        style: TextStyle(
                                            color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                                      ),
                                    ))
                              ],
                            ),
                          )),

                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 15) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 18) : EdgeInsets.only(left:21.0,right: 21.0,top:14,bottom: 21),
                          // padding: EdgeInsets.all(15),
                          child:Container(
                            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 80 : 100:120,


                            child:  Theme(data: new ThemeData(
                                hintColor: Colors.black26
                            ), child: TextField(
                              maxLines: 4,
                              controller: remarkcontroller,

                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black26, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black26, width: 0.5),
                                ),
                                hintText: 'Enter remarks',
                              ),

                              onChanged: (text) {
                                //TemRegData.email=text;

                              },
                            )),
                          )),

                      Align(

                        alignment: Alignment.topCenter,
                        child:   Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10) : EdgeInsets.all(15):EdgeInsets.all(20),
                          child:  Container(
                            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50 :60:70,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Color(0xFF006b6b),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () async {
                                
                                if(subjectdata.compareTo("Select Insurance account")!=0)
                                  {

                                    if(insurancetype.compareTo("Select insurance type")!=0)
                                    {


                                      if(datetxt1.compareTo("Select date of payment")!=0)
                                      {

                                        if(datetxt1.compareTo("Select closing date")!=0)
                                        {


                                          Map<String, dynamic> assetdata = new Map();
                                          assetdata['name'] = accountid;
                                          assetdata['amount'] =
                                              amountcontroller.text.toString();
                                          assetdata['insurancetype'] = insurancetype;
                                          assetdata['paymentdate'] = datetxt1;
                                          assetdata['closingdate'] = datetxt2;
                                          var js = json.encode(assetdata);
                                          Map<String, dynamic> data_To_Table = new Map();
                                          data_To_Table['data'] = js.toString();

                                          if (liabilityid.compareTo("0") == 0) {
                                            dbhelper.insert(
                                                data_To_Table, DatabaseTables.TABLE_INSURANCE);
                                          }
                                          else{

                                            dbhelper.update(data_To_Table, DatabaseTables.TABLE_INSURANCE, liabilityid);
                                          }

                                          Navigator.pop(context, {"accountsetupdata": 1});



                                        }
                                        else{

                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                            content: Text("Select closing date"),
                                          ));


                                        }



                                      }
                                      else{

                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                          content: Text("Select date of payment"),
                                        ));


                                      }



                                    }
                                    else{

                                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                        content: Text("Select insurance type"),
                                      ));


                                    }


                                  }
                                else{

                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                    content: Text("Select Insurance account"),
                                  ));


                                }





















                              },
                              child: Text(
                                'Save',
                                style:
                                TextStyle(color: Colors.white, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),
                              ),
                            ),
                          ),
                        ),),

                    ]
                )
            )
          ]
      ),


    );
  }


  void setupInsuranceDataForEdit(String id) async
  {
    List<Map<String, dynamic>> a =
    await dbhelper.getDataByid(DatabaseTables.TABLE_INSURANCE,id);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));

    var ab=a[0];


    // print(ab);

    int dataid = ab["keyid"];
    String data=ab["data"];

    var jsondata = jsonDecode(data);

    String name=jsondata['name'];

    String amount=jsondata['amount'];

    String insurancet=jsondata['insurancetype'];

    String paymentdate=jsondata['paymentdate'];

    String closingdate=jsondata['closingdate'];

    var v1 = await dbhelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, name);
    List<Map<String, dynamic>> ab1 = v1;
    Map<String, dynamic> mapdata1 = ab1[0];
    String d1 = mapdata1['data'];
    var jsd = jsonDecode(d1);

    String Accountname = jsd["Accountname"];
    for(String ah in subject)
    {
      if(ah.compareTo(Accountname)==0)
      {
        setState(() {
          subjectdata=        ah;
        });
        break;
      }
    }


    setState(() {
      accountid=name;
      amountcontroller.text=amount;
      insurancetype=insurancet;
      datetxt1=paymentdate;
      datetxt2=closingdate;

    });

  }





  void setupAccountData() async {
    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Amount ="";
    accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      String Accounttype = jsondata["Accounttype"];
      Amount = jsondata["Amount"];
      String Type = jsondata["Type"];

      if(Accounttype.compareTo("Insurance")==0)
      {
        accountsetupdata.add(Accountname);
      }

    }


    setState(() {

      subject.clear();

      subjectdata=accountsetupdata[0];
      amountcontroller.text="0";

      subject.addAll(accountsetupdata);

    });
  }


  setDataByAccountName(String name) async
  {

    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Amount ="0";
    String accid="0";
    accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      if(Accountname.compareTo(name)==0) {
        String Accounttype = jsondata["Accounttype"];
        Amount = jsondata["Amount"];
        accid=id.toString();
      }
    }
    setState(() {



      accountid=accid;

      amountcontroller.text=Amount;



    });

  }

}