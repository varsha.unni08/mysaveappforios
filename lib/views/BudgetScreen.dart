import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/mainviews/BudgetEdit.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import '../utils/Tutorials.dart';
import 'BudgetEdit.dart';


class Budgetpage extends StatefulWidget {
  final String title;



  const Budgetpage(
      {Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _Budgetpage();
}

class _Budgetpage extends State<Budgetpage> {

  String date="";
  String date1="";
  String datetxt1="Select start date";
  String datetxt2="Select end date";
  List<String>tableheaddata=["Month","Amount","Action"];

   DatabaseHelper dbhelper=new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<Cashbankaccount> cashbankdata = [];

  String dropdownyear="";

  List<String>yeardatadropdown=[];

  TextEditingController amountcontroller=new TextEditingController();

  List<String> arrmonth=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  final dbHelper = new DatabaseHelper();

  List<String>expenseaccount=[];

  List<String>tableaccount=[];

  String expense="";

  @override
  void initState() {
    // TODO: implement initState
    Tutorial.showTutorial(Tutorial. budgetutorial, context, Tutorial.budgettutorial);
    setupYearDataForDropDown();
    setupAccountData();


    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    // var now=DateTime.now();
    // int y=now.year;
    //
    // List<String>yeardata=[];
    //
    //
    // for(int i=0;i<4;i++)
    // {
    //   int y1=y+1;
    //   yeardata.add(y1.toString());
    //
    // }
    //
    // yeardatadropdown.clear();
    // yeardatadropdown.addAll(yeardata);
    //
    // dropdownyear=y.toString();

    return Scaffold(

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Budget" , style: TextStyle(fontSize:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
        centerTitle: false,
      ),

      body: Stack(

        children: [

          Align(

            alignment: Alignment.topCenter,
            child: SizedBox(
              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 280:320:360,




              child: Stack(

                children: [

                  Align(
                    alignment: FractionalOffset.topCenter,
                    child:Column(

                      children: [
                        Row(
                          children: [

                            Expanded(child:(yeardatadropdown.length>0)? Padding(
                                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8): EdgeInsets.all(11),
                                child:Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:80,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.white,
                                      // red as border color
                                    ),
                                  ),
                                  child: DropdownButtonHideUnderline(

                                    child: ButtonTheme(
                                      alignedDropdown: true,
                                      child: InputDecorator(
                                        decoration: const InputDecoration(border: OutlineInputBorder()),
                                        child: DropdownButtonHideUnderline(
                                          child: DropdownButton(

                                            isExpanded: true,
                                            value: dropdownyear,
                                            items: yeardatadropdown
                                                .map<DropdownMenuItem<String>>((String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(value),
                                              );
                                            }).toList(),
                                            onChanged: (String? newValue) {
                                              setState(() {
                                                dropdownyear = newValue!;

                                                getBudgetData();



                                              });
                                            },
                                            style: Theme.of(context).textTheme.bodyText1,

                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )):Container(),flex: 1,),
                            Expanded(child: (expenseaccount.length>0)? Padding(
                                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) : EdgeInsets.all(11),
                                child:Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:80,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.white,
                                      // red as border color
                                    ),
                                  ),
                                  child: DropdownButtonHideUnderline(

                                    child: ButtonTheme(
                                      alignedDropdown: true,
                                      child: InputDecorator(
                                        decoration: const InputDecoration(border: OutlineInputBorder()),
                                        child: DropdownButtonHideUnderline(
                                          child: DropdownButton(

                                            isExpanded: true,
                                            value: expense,
                                            items: expenseaccount
                                                .map<DropdownMenuItem<String>>((String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(value),
                                              );
                                            }).toList(),
                                            onChanged: (String? newValue) {
                                              setState(() {
                                                expense = newValue!;

                                                getBudgetData();




                                              });
                                            },
                                            style: Theme.of(context).textTheme.bodyText1,

                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )):Container(),flex: 1,)


                          ],

                        ),
                        Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 5, 0, 0) : EdgeInsets.fromLTRB(0, 8, 0, 0) : EdgeInsets.fromLTRB(0, 11, 0, 0),
                          child: Row(
                            textDirection: TextDirection.ltr,
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 0, 10, 0) : EdgeInsets.fromLTRB(15, 0, 15, 0) : EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(

                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      controller: amountcontroller,
                                      decoration: InputDecoration(
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black38, width: 0.5),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black38, width: 0.5),
                                        ),
                                        hintText: 'Amount',
                                      ),
                                    ),
                                  ),
                                ),
                                flex: 3,
                              ),

                            ],
                          ),
                        ),
                        Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(0, 10, 0, 0) : EdgeInsets.fromLTRB(0, 15, 0, 0) :EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Container(
                            height: 50,
                            width: 150,
                            decoration: BoxDecoration(
                                color: Color(0xF0233048),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () async{

                                int setupid=await getAccountIndex(expense);

                                if(amountcontroller.text.isNotEmpty)
                                {

                                  for(int i=0;i<arrmonth.length;i++)
                                  {
                                    var mjobject=new Map();
                                    mjobject['year']=dropdownyear;
                                    mjobject['month']=arrmonth[i];
                                    mjobject['amount']=amountcontroller.text;
                                    mjobject['accountname']=setupid;

                                    var js=json.encode(mjobject);

                                    Map<String, dynamic> data_To_Table=new Map();
                                    data_To_Table['data']=js.toString();
                                    int id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_BUDGET);


                                  }

                                  getBudgetData();






                                }








                              },
                              child: Text(
                                'Submit',
                                style:
                                TextStyle(color: Colors.white, fontSize: 15),
                              ),
                            ),
                          ),
                        ),

                      ],
                    ) ,

                  ),

                  Align(

                    alignment: FractionalOffset.bottomCenter,
                    child: (tableaccount.length>0) ?  Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(0, 10, 0, 0) : EdgeInsets.fromLTRB(0, 15, 0, 0) : EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: MediaQuery.removePadding(
                          context: context,
                          removeTop: true,
                          removeBottom: true,
                          child:GridView.builder(
                        physics: BouncingScrollPhysics(),

                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: 0.0,
                            mainAxisSpacing: 0.0,
                            childAspectRatio: 3
                        ),
                        itemCount: tableheaddata.length,
                        itemBuilder: (context, index) {
                          return

                            Container(
                                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:80,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black54,
                                    width: 0.3,
                                  ),
                                ),

                                child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19,color: Colors.black),)));


                        },
                      )),
                    ):Container(),

                  )

                ],

              )



              ,
            )


          ),












          (tableaccount.length>0) ? Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0,280, 0, 0)  :EdgeInsets.fromLTRB(0,320, 0, 0) : EdgeInsets.fromLTRB(0,360, 0, 0),
                      child: MediaQuery.removePadding(
                          context: context,
                          removeTop: true,child: GridView.builder(
                        physics: BouncingScrollPhysics(),

                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 0.0,
                          mainAxisSpacing: 0.0,
                          childAspectRatio: 3,
                        ),
                        itemCount: tableaccount.length,
                        itemBuilder: (context, index) {
                          return



                              Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.black54,
                                      width: 0.3,
                                    ),
                                  ),

                                  child:

                                  (tableaccount[index].contains("Edit"))?


                                  Center(

                                      child:TextButton(onPressed: ()async {

                                        // Navigator.push(
                                        //     context, MaterialPageRoute(builder: (_) =>
                                        //     BudgetEditpage(title: "Edit budget",id:tableaccount[index].split(",")[1], )));

                                        Map results = await Navigator.of(context)
                                            .push(new MaterialPageRoute<dynamic>(
                                          builder: (BuildContext context) {
                                            return new BudgetEditpage(title: "Edit budget",id:tableaccount[index].split(",")[1], );
                                          },
                                        ));

                                        if (results != null &&
                                            results.containsKey('updated')) {
                                          setState(() {
                                            var accountsetupdata =
                                            results['updated'];

                                            int acs =
                                            accountsetupdata as int;
                                            //
                                            // if(acs>0)
                                            //   {
                                            getBudgetData();
                                            //  }


                                          });
                                        }





                                      },
                                        child: Text(tableaccount[index].split(",")[0],style: TextStyle(fontSize:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19,color: Colors.green),),)


                                  ):
                                  Center(child:Text(tableaccount[index],style: TextStyle(fontSize: 15,color: Colors.black),))

                              );






                        },
                      )),
                    ):Container(),









        ],


      ),
    );

  }


  void setupYearDataForDropDown()
  {
    var now=DateTime.now();
    int y=now.year;

    List<String>yeardata=[];


    for(int i=0;i<6;i++)
      {
        int y1=y+i;
        yeardata.add(y1.toString());

      }

    setState(() {

      yeardatadropdown.clear();
      yeardatadropdown.addAll(yeardata);

      dropdownyear=y.toString();

    });



  }

  void setupAccountData() async {
    List<Map<String, dynamic>> a =
    await dbHelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      String Accounttype = jsondata["Accounttype"];
      String Amount = jsondata["Amount"];
      String Type = jsondata["Type"];

      if(Accounttype.compareTo("Expense account")==0)
      {
        accountsetupdata.add(Accountname);
      }

    }


    setState(() {

      expenseaccount.clear();

      expense=accountsetupdata[0];

      expenseaccount.addAll(accountsetupdata);
      getBudgetData();
    });
  }

  Future<int> getAccountIndex(String accountname)
  async {

    int id =0;
        List<Map<String, dynamic>> a =
        await dbHelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    for (Map ab in a) {
      print(ab);


      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];


      if(Accountname.compareTo(accountname)==0)
      {
         id = ab["keyid"];
         break;

      }

    }

    return id;
  }

  getBudgetData() async
  {



    List<String>tabledata=[];

    int id= await getAccountIndex(expense);

    List<Map<String, dynamic>> a =
    await dbHelper.queryAllRows(DatabaseTables.TABLE_BUDGET);
    for (Map ab in a) {

     int aid = ab["keyid"];
     String data = ab["data"];
     var jsondata = jsonDecode(data);

     String Accountname = jsondata["accountname"].toString();

     String amount = jsondata["amount"];

     String year = jsondata["year"];
     String month = jsondata["month"];

     if(Accountname.compareTo(id.toString())==0)
       {

         if(year.compareTo(dropdownyear)==0)
           {


             tabledata.add(month);
             tabledata.add(amount);
             tabledata.add("Edit,"+aid.toString());



           }



       }






    }


    setState(() {

      tableaccount.clear();
      tableaccount.addAll(tabledata);
    });




  }

}