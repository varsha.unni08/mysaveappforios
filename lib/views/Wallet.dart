import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
//
// import 'package:save_flutter/domain/Paymentdata.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:save_flutter/mainviews/AddRecipt.dart';
// import 'package:save_flutter/mainviews/AddWallet.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../utils/Tutorials.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';

import 'AddWallet.dart';





class WalletPage extends StatefulWidget {
  final String title;

  const WalletPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WalletPage();
}

class _WalletPage extends State<WalletPage> {

  String date="",month="",year="";

  String datetxt="Select date";

  List<String>accountsdata=[];

  List<DataRow>accountsdatarow=[];

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";

  List<String>tableheaddata=["Date","Account name","Amount"];


  List<DataColumn>tableheadings=[];

  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;
  String currentmonth="";

  int currentmonthindex=0;

  String selectedmonth="",selectedyear="";
  String openingbalance="0";

  final dbHelper = new DatabaseHelper();



  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

    Tutorial.showTutorial(Tutorial.wallettitorial, context, Tutorial.wallettutorial);

    selectedmonth=now.month.toString();
    selectedyear=now.year.toString();


    setState(() {

      datetxt=selectedmonth+"/"+selectedyear;
    });

    showAccountDetails();
    // loadTableHeading();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child:



      Scaffold(
        resizeToAvoidBottomInset: true,

          appBar:  AppBar(
            backgroundColor: Color(0xFF096c6c),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text("Wallet",style: TextStyle(fontSize: 14),),
            centerTitle: false,
          ),




        body: Stack(
          children: [




            Column(

              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: [

                SizedBox(
                  height:
                  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?190:230:290,

                  child:
                  Stack(

                    children: [

                      Align(

                        alignment: FractionalOffset.topCenter,
                        child: Padding(

                          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5) :EdgeInsets.fromLTRB(15, 10, 15, 10) :EdgeInsets.fromLTRB(20, 15, 20, 15),
                          child:        Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black38)),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[

                                Expanded(child:

                                InkWell(
                                  child:Padding(
                                      padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(25),
                                      child: Icon(
                                        Icons.calendar_month,
                                        color: Colors.black54,
                                        size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:35:45,

                                      )) ,
                                  onTap: (){
                                    var now = DateTime.now();

                                    date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                                    month=now.month.toString();
                                    year=now.year.toString();

                                    int monthnumber=int.parse(month);
                                    currentmonthindex=monthnumber-1;

                                    currentmonth=months[monthnumber-1];
                                    currentyear=year;




                                    showDialog(
                                        context: context,
                                        builder: (_) {
                                          return MyDialog();
                                        }).then((value) => {


                                      selectedmonth=value["selecteddata"].toString().split(",")[0],
                                      selectedyear=value["selecteddata"].toString().split(",")[1],






                                      showAccountDetails()




                                    });
                                  },

                                )


                                  ,flex: 1, )
                                ,
                                Expanded(child:  Container(
                                    child: TextButton(
                                      onPressed: () async {
                                        var now = DateTime.now();

                                        date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                                        month=now.month.toString();
                                        year=now.year.toString();

                                        int monthnumber=int.parse(month);
                                        currentmonthindex=monthnumber-1;

                                        currentmonth=months[monthnumber-1];
                                        currentyear=year;




                                        showDialog(
                                            context: context,
                                            builder: (_) {
                                              return MyDialog();
                                            }).then((value) => {


                                          selectedmonth=value["selecteddata"].toString().split(",")[0],
                                          selectedyear=value["selecteddata"].toString().split(",")[1],






                                          showAccountDetails()




                                        });


                                      },
                                      child: Text(
                                        datetxt,
                                        style: TextStyle(
                                            color: Colors.black38, fontSize: 12),
                                      ),
                                    )),flex: 2,  )
                              ],
                            ),
                          )),
                        ),


                      Align(

                        alignment: FractionalOffset.bottomCenter,
                        child:   (accountsdata.length>0)?

                        SizedBox(

                          width: double.infinity,


                          child:

                          Padding(

                              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 0) :EdgeInsets.fromLTRB(15, 10, 15, 0) :EdgeInsets.fromLTRB(20, 15, 20, 0),

                              child:    MediaQuery.removePadding(
                                removeBottom: true,
                                context: context,
                                removeTop: true, child:GridView.builder(
                                primary: false,


                                physics: ScrollPhysics(),

                                shrinkWrap: true,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    crossAxisSpacing: 0.0,
                                    mainAxisSpacing: 0.0,


                                ),
                                itemCount: tableheaddata.length,
                                itemBuilder: (context, index) {
                                  return  Container(

                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.black54,
                                          width: 0.3,
                                        ),
                                      ),

                                      child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),maxLines: 2,)));


                                },
                              )
                                ,) ),
                        )





                            :Container(),
                      )


                    ],
                  ),


                )









                ,






              ],

            ),






            Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 190, 10, 0) :EdgeInsets.fromLTRB(15, 230, 15, 0) :EdgeInsets.fromLTRB(20, 290, 20, 0),

                child:
                SingleChildScrollView(

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,

                    children: [

                      MediaQuery.removePadding(
                          context: context,
                          removeTop: true,
                          child:GridView.builder(
                            physics: ScrollPhysics(),
                            primary: false,
                            shrinkWrap: true,
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 0.0,
                                mainAxisSpacing: 0.0,


                            ),
                            itemCount: accountsdata.length,
                            itemBuilder: (context, index) {
                              return Container(

                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black54,
                                    width: 0.3,
                                  ),
                                ),

                                child:

                             Center(child:Text(accountsdata[index],style: TextStyle(fontSize: 13,color: Colors.black),)),

                              );
                            },
                          )),




                    ],
                  ),
                )),

            Align(alignment: Alignment.bottomRight,

                child:Padding(
                    padding: EdgeInsets.all(12),

                    child: FloatingActionButton(
                      onPressed: () async{



                        Map results = await Navigator.of(context)
                            .push(new MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) {
                            return new AddWalletPage(
                              title: "Add money to wallet", balance: "0",
                            );
                          },
                        ));

                        if (results != null &&
                            results.containsKey('accountadded')) {


                          int code=results['accountadded'];
                          if(code==1)
                          {

                            showAccountDetails();


                          }
                        }

                      },
                      child: Icon(Icons.add, color: Colors.white, size: 29,),
                      backgroundColor: Colors.blue,

                      elevation: 5,
                      splashColor: Colors.grey,
                    ))),
          ],







        )),


        onWillPop: ()async{




      return true;

    });

  }












  void showAccountDetails() async
  {

    List<String>tabledata=[];
    List<Map<String, dynamic>> a =
    await dbHelper.queryAllRows(DatabaseTables.TABLE_WALLET);
    String amountdata="";

    int i=0;

    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);
      String date = jsondata["date"];
      String accountname="Money added to wallet";

      String amount = jsondata["amount"];

      String month = jsondata["month"];

      String year = jsondata["year"];

      if(month.compareTo(selectedmonth)==0&&year.compareTo(selectedyear)==0) {
        tabledata.add(date);
        tabledata.add(accountname);
        tabledata.add(amount);

        if(i==0)
          {
            amountdata=amount;

          }

        i++;
      }






    }

    setState(() {
      accountsdata.clear();
      accountsdata=tabledata;

      datetxt=selectedmonth+"/"+selectedyear;

      openingbalance=amountdata;
    });







  }










}


class MyDialog extends StatefulWidget {



  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

    // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    month=now.month.toString();
    year=now.year.toString();



    setState(() {
      int monthnumber=int.parse(month);
      currentmonthindex=monthnumber-1;

      currentmonth=months[monthnumber-1];
      currentyear=year;

      currentyearnumber=int.parse(currentyear);
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: 300.0,
        width: 600.0,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: EdgeInsets.all(15.0),
              child: Container(
                  width: 150,

                  child:Row(

                    children: [

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {

                            if(currentmonthindex>0) {
                              currentmonthindex =
                                  currentmonthindex -1;
                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });
                            }



                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child: Text(currentmonth,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {

                            if(currentmonthindex<11) {
                              currentmonthindex =
                                  currentmonthindex + 1;

                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });

                            }



                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {


                            currentyearnumber =
                                currentyearnumber - 1;

                            setState(() {
                              currentyear=currentyearnumber.toString();
                            });




                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child:  Text(currentyear,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {
                            currentyearnumber =
                                currentyearnumber + 1;

                            setState(() {
                              currentyear=currentyearnumber.toString();
                            });
                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),


                    ],


                  )


              ),
            ),







            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      int ab=currentmonthindex+1;


                      String currentyearmonth=ab.toString()+","+currentyearnumber.toString();



                      //  Navigator.pop(context,'selecteddata':currentyearmonth);

                      Navigator.of(context).pop({'selecteddata':currentyearmonth});


                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),


            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
      // actions: <Widget>[
      //   FlatButton(
      //       child: Text('Switch'),
      //       onPressed: () => setState(() {
      //         _c == Colors.redAccent
      //             ? _c = Colors.blueAccent
      //             : _c = Colors.redAccent;
      //       }))
      // ],
    );
  }





}