

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';




class AddTaskListPage extends StatefulWidget {
  final String title;
  final String taskid;






  const AddTaskListPage(
      {Key? key, required this.title, required this.taskid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddTaskListPage(taskid);
}

class _AddTaskListPage extends State<AddTaskListPage> {
String taskid="0";


_AddTaskListPage(this.taskid);

  TextEditingController namecontroller=new TextEditingController();


   DatabaseHelper dbhelper = new DatabaseHelper();

  String datetxt1="Select date";
  String tasktime="Select task time";
  String reminddate="Select remind date";

  List<String> arrstatus=["Initial","Completed","Postponed"];

  String status="Initial";

  bool isStatusvisible=false;

  String taskdate="Select task date";
  String    date="";

  String time="";


  String    rmdate="";



  @override
  void initState() {
    // TODO: implement initState

    if(taskid.compareTo("0")!=0)
      {
        setupDataForEditing( taskid);
      }



    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }



  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold( resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () =>  Navigator.pop(context,{"accountsetupdata":1}),
        ),
        title: Text("Add Task",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
        centerTitle: false,
      ),

      body: SingleChildScrollView(


        child:Column(

          children: [



            Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0): EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 0) :EdgeInsets.only(left:21.0,right: 21.0,top:15,bottom: 0),
              // padding: EdgeInsets.all(15),
              child: new Theme(data: new ThemeData(
                hintColor: Colors.black,

              ), child: TextField(

                controller: namecontroller,


                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width: 0.5),
                  ),
                  hintText: 'Name',



                ),




              )),
            ),

            Container(child:     Padding(
                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 10) : EdgeInsets.fromLTRB(18, 12, 18, 12):EdgeInsets.fromLTRB(21, 15, 21, 15),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[

                      Expanded(child: Padding(
                          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12):EdgeInsets.all(15),
                          child: InkWell(
                            child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,),

                            onTap: (){

                              var now=DateTime.now();

                              date= now.day
                                  .toString() +
                                  "-" +
                                  now.month
                                      .toString() +
                                  "-" +
                                  now.year
                                      .toString();


                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {



                                                date= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();


                                                // taskdate=date;



                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4):EdgeInsets.all(6),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    taskdate = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:16:19),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });
                            },
                          )



                      ),flex: 1,)
                      ,
                      Expanded(child: Container(
                        child: TextButton(
                          onPressed: () async {

                            var now=DateTime.now();

                            date= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              date= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4):EdgeInsets.all(6),
                                          child: Container(
                                            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                            width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  taskdate = date;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:16:19),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });




                          },
                          child: Text(
                              taskdate,
                              style: TextStyle(
                                  color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:17)),
                        ),
                      ),flex: 3,)
                    ],
                  ),
                ))),
            Container(child:     Padding(
                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 10) : EdgeInsets.fromLTRB(18, 12, 18, 12):EdgeInsets.fromLTRB(21, 15, 21, 15),

                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[

                      Expanded(child: Padding(
                          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12):EdgeInsets.all(15),
                          child: Icon(Icons.punch_clock_outlined,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,)),flex: 1,)
                      ,

                      Expanded(child: Container(
                          child: TextButton(
                            onPressed: () async {

                              TimeOfDay selectedTime = TimeOfDay.now();
                              final TimeOfDay? timeOfDay = await showTimePicker(
                                context: context,
                                initialTime: selectedTime,
                                initialEntryMode: TimePickerEntryMode.dial,

                              );
                              if(timeOfDay != null && timeOfDay != selectedTime)
                              {


                                setState(() {
                                  selectedTime = timeOfDay;

                                  tasktime=timeOfDay.format(context);
                                });
                              }
                              else{

                                setState(() {


                                  tasktime=selectedTime.format(context);
                                });
                              }





                            },
                            child: Text(
                                tasktime,
                                style: TextStyle(
                                    color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:17)),

                          )),flex: 3,)

                    ],
                  ),
                ))),



            Container(child:     Padding(
                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 10) : EdgeInsets.fromLTRB(18, 12, 18, 12):EdgeInsets.fromLTRB(21, 15, 21, 15),

                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[

                      Expanded(child: Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
                          child: InkWell(

                            child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,),

                            onTap: ()
                            {

                              var now=DateTime.now();

                              rmdate= now.day
                                  .toString() +
                                  "-" +
                                  now.month
                                      .toString() +
                                  "-" +
                                  now.year
                                      .toString();


                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {



                                                rmdate= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();


                                                // taskdate=date;



                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2):EdgeInsets.all(4):EdgeInsets.all(6),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    reminddate = rmdate;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });
                            },
                          )


                      ),flex: 1,)
                      ,

                      Expanded(child: Container(
                          child: TextButton(
                            onPressed: () async {


                              var now=DateTime.now();

                              rmdate= now.day
                                  .toString() +
                                  "-" +
                                  now.month
                                      .toString() +
                                  "-" +
                                  now.year
                                      .toString();


                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {



                                                rmdate= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();


                                                // taskdate=date;



                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2):EdgeInsets.all(4):EdgeInsets.all(6),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    reminddate = rmdate;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });





                            },
                            child: Text(
                                reminddate,
                                style: TextStyle(
                                    color: Colors.black38, fontSize: 12)),

                          )),flex: 3,)

                    ],
                  ),
                ))),



            (isStatusvisible)?  Padding(
                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11):EdgeInsets.all(14),
                child:Container(
                  width: double.infinity,
                  height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  60.0:70:80,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                      // red as border color
                    ),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: InputDecorator(
                        decoration: const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(

                            value: status,
                            items: arrstatus
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            onChanged: (String? newValue) {
                              setState(() {
                                status = newValue!;

                                //TemRegData.language=languagedropdown;
                              });
                            },
                            style: Theme.of(context).textTheme.bodyText1,

                          ),
                        ),
                      ),
                    ),
                  ),
                )):Container(),












            Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 20, 15, 15) : EdgeInsets.fromLTRB(20, 25, 20, 20):EdgeInsets.fromLTRB(25, 30, 25, 25),

              child: Container(

                width: double.infinity,
                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 55:60:65,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {


                      // JSONObject jsonObject = new JSONObject();
                      // jsonObject.put("name", edtName.getText().toString());
                      // jsonObject.put("date", date);
                      // jsonObject.put("time", timeselected);
                      // jsonObject.put("status", 0);
                      // jsonObject.put("reminddate",reminddate);

                      if(namecontroller.text.isNotEmpty)
                      {
                        if(taskdate.isNotEmpty)
                        {
                          if(reminddate.isNotEmpty)
                          {
                            var m=new Map();
                            m['name']=namecontroller.text.toString();
                            m['date']=taskdate;

                            if(tasktime.compareTo("Select task time")!=0) {
                              m['time'] = tasktime;
                            }
                            else{
                              m['time'] = "";

                            }
                            m['status']=status;
                            m['reminddate']=rmdate;

                            var js=json.encode(m);

                            Map<String, dynamic> data_To_Table=new Map();
                            data_To_Table['data']=js.toString();

                            if(taskid.compareTo("0")==0)
                            {
                              dbhelper.insert(data_To_Table, DatabaseTables.TABLE_TASK);
                            }
                            else{

                              dbhelper.update(data_To_Table, DatabaseTables.TABLE_TASK,taskid);
                            }



                            Navigator.pop(context,{"accountsetupdata":1});

                          }
                          else{

                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Select remind date"),
                            ));
                          }

                        }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Select task date"),
                          ));
                        }

                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Enter name"),
                        ));
                      }








                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),
          ],



        ) ,
      )


      ,
    );
  }





setupDataForEditing(String taskid) async
{

  var v = await dbhelper.getDataByid(
      DatabaseTables.TABLE_TASK, taskid.toString());

  List<Map<String, dynamic>> ab = v;

  Map<String, dynamic> mapdata = ab[0];
  String d = mapdata['data'];

  var jsondata = jsonDecode(d);


  String tdate=jsondata['date'];

  String name=jsondata['name'];
  String time=jsondata['time'];
  String sts=jsondata['status'];
  String rddate=jsondata['reminddate'];

  setState(() {

    namecontroller.text=name;
    reminddate=rddate;
    status=sts;
    tasktime=time;
    taskdate=tdate;

  });


}





}