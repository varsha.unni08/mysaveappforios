

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'dart:ui' as ui;
import 'package:intl/intl.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../domain/Taskdata.dart';
import '../utils/Tutorials.dart';







class RemindsListPage extends StatefulWidget {
  final String title;






  const RemindsListPage(
      {Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _RemindsListPage();
}

class _RemindsListPage extends State<RemindsListPage> {

  String diaryid = "0";

  DatabaseHelper dbhelper=new DatabaseHelper();


  String date = "",
      month = "",
      year = "";


  List<String>subject = ["Select subject"];

  String subjectdata = "Select subject";


  String datetxt1 = "Select start date";

  String datetxt2 = "Select end date";

  String languagedropdown = 'Select your language';

  TextEditingController commentcontroller = new TextEditingController();
  TextEditingController feedbackcontroller = new TextEditingController();

  List<Task> fulltaskdata = [];

  // DatabaseHelper dbhelper = new DatabaseHelper();

  String stdate="",endate="";

  @override
  void initState() {
    // TODO: implement initState
    Tutorial.showTutorial(Tutorial.reminders_tutorial, context, Tutorial.remindsss);
    setupDateData();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;

    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Reminds",style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

      body: Stack(

        children: [

          Container(child:     Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Image.asset(
                          "images/calendar.png",
                          width: 25,
                          height: 25,
                        )),
                    Container(
                        child: TextButton(
                          onPressed: () async {


                            var now=DateTime.now();

                            stdate= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              stdate= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: Container(
                                            height: 50,
                                            width: 150,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  datetxt1 = stdate;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });





                          },
                          child:SizedBox(width: 290,child: Text(
                              datetxt1,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12)),
                          ),
                        ))
                  ],
                ),
              ))),

          Container(child:     Padding(
              padding: EdgeInsets.fromLTRB(10, 80, 10, 10),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Image.asset(
                          "images/calendar.png",
                          width: 25,
                          height: 25,
                        )),
                    Container(
                        child: TextButton(
                          onPressed: () async {


                            var now=DateTime.now();

                            endate= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              endate= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: Container(
                                            height: 50,
                                            width: 150,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  datetxt2 = endate;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });





                          },
                          child:SizedBox(width: 290,child: Text(
                              datetxt2,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12)),
                          ),
                        ))
                  ],
                ),
              ))),

          Padding(
            padding: EdgeInsets.fromLTRB(15, 140, 15, 15),

            child:Align(

              alignment: Alignment.topCenter,

            child: Container(

              width: width/2,
              height: 55,
              decoration: BoxDecoration(

                  color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
              child:Align(
                alignment: Alignment.center,
                child: TextButton(

                  onPressed:() {


                    // showTasks();

                    DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(datetxt1);

                    DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(datetxt2);

                    if(accountsdateselected.isAfter(accountsdateparsed)||accountsdateselected.compareTo(accountsdateparsed)==0)
                    {

                      showTasks();

                    }

                  },

                  child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
              ),



              //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
            ) ,
            )



           ,


            // ,
          ),


          Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.fromLTRB(5, 200, 5, 5),
                child: ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount: fulltaskdata.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        // color: Colors.amber[colorCodes[index]],
                        child: Center(
                            child:InkWell(

                              onTap: ()async{

                                print("tapped");


                                // Map results = await Navigator.of(context)
                                //     .push(new MaterialPageRoute<dynamic>(
                                //   builder: (BuildContext context) {
                                //     return new AddTaskListPage(
                                //       title: "Accountsettings", taskid: fulltaskdata[index].id,
                                //     );
                                //   },
                                // ));
                                //
                                // if (results != null &&
                                //     results.containsKey('accountsetupdata')) {
                                //   setState(() {
                                //     var acc_selected =
                                //     results['accountsetupdata'];
                                //
                                //     int acs =
                                //     acc_selected as int;
                                //
                                //     if(acs!=0) {
                                //       // setupAccountData();
                                //       showTasks();
                                //     }
                                //
                                //
                                //
                                //
                                //   });
                                // }


                              },

                              child:Card(
                                elevation: 5,
                                child: Container(
                                  color: Colors.white,
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.all(6),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Name"),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":"),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].task,
                                                style: TextStyle(fontSize: 13),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(6),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("date"),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":"),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].date,
                                                style: TextStyle(fontSize: 13),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(6),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Time"),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":"),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index]
                                                    .time,
                                                style: TextStyle(fontSize: 13),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(6),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Remind date"),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":"),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].reminddate,
                                                style: TextStyle(fontSize: 13),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(6),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Status"),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":"),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].status,
                                                style: TextStyle(fontSize: 13),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),

                                    ],
                                  ),
                                ),
                              ) ,
                            )




                        ),
                      );
                    }),
              )),










        ],
      ),




    );

  }



  setupDateData()
  {
    var now =DateTime.now();

    setState(() {

      datetxt1=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();
      datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    });


    showTasks();



  }


  showTasks()async{

    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_TASK);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<Task> taskdata = [];


    DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(datetxt1);

    DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(datetxt2);
    for (Map ab in a) {
      print(ab);
      String id = ab["keyid"].toString();

      String data = ab["data"];

      var jsondata = jsonDecode(data);


      String taskdate=jsondata['date'];

      String name=jsondata['name'];
      String time=jsondata['time'];
      String status=jsondata['status'];
      String reminddate=jsondata['reminddate'];



      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(taskdate);
      if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
          accountsdateparsed.isBefore(accountsdatend)) {

        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);
      }
      else if (accountsdateparsed.isAfter(accountsdatestart) &&
          accountsdateparsed.compareTo(accountsdatend) == 0) {
        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);

      }
      else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
          accountsdateparsed.compareTo(accountsdatend) == 0) {

        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);
      }

      else if (accountsdateparsed.isAfter(accountsdatestart) &&
          accountsdateparsed.isBefore(accountsdatend)) {

        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);
      }

    }

    setState(() {
      fulltaskdata.clear();

      fulltaskdata.addAll(taskdata);
    });


  }

}