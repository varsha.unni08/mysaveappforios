import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:saveappforios/views/view_mile_stones.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/MyDreamEntity.dart';
import 'calculators/cala_culator_page.dart';

class DreamDetails extends StatefulWidget {

  MyDreamEntity myDreamEntity;
  
   DreamDetails(this.myDreamEntity) : super();

  @override
  _DreamDetailsState createState() => _DreamDetailsState(myDreamEntity);
}

class _DreamDetailsState extends State<DreamDetails> {

  MyDreamEntity myDreamEntity;

  _DreamDetailsState(this.myDreamEntity);

 late  Uint8List image=new Uint8List.fromList([0, 2, 5, 7, 42, 255]) ;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCategoryname(myDreamEntity.target_categoryid);
  }
  
  
  
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () =>    Navigator.of(context).pop({'mydreamadded':1 }),
        ),
        title: Text("View Details",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),
      body: SingleChildScrollView(
        
        
        child: Column(
          
          
          children: [
            
            
            
            Row(
              
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                
                
                Expanded(child:


            (image!=null) ? SizedBox(

          child:Image.memory(image) ,
              width: 50,
              height: 50,
        )  : Container()


                  ,



                  flex: 1,),
                
                Expanded(child: Column(
                  
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  
                  children: [

                    Padding(padding: EdgeInsets.all(6),

                      child:  Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,

                        children: [

                          Expanded(child:Text("Name",style: TextStyle(fontSize: 14),),flex: 2, ),
                          Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                          Expanded(child:Text(myDreamEntity.targetname,style: TextStyle(fontSize: 14),),flex: 2, )



                        ],



                      ),

                    ),



                    Padding(padding: EdgeInsets.all(6),

                        child:        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Expanded(child:Text("Category",style: TextStyle(fontSize: 14),),flex: 2, ),
                            Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                            Expanded(child:Text(myDreamEntity.target_categoryname,style: TextStyle(fontSize: 14),),flex: 2, )



                          ],



                        )),


                    (myDreamEntity.investment_id.compareTo("0")!=0)?  Padding(padding: EdgeInsets.all(6),

                        child:        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Expanded(child:Text("Investment",style: TextStyle(fontSize: 14),),flex: 2, ),
                            Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                            Expanded(child:Text(myDreamEntity.investmentaccount,style: TextStyle(fontSize: 14),),flex: 2, )



                          ],



                        )) :Container(),


                    (myDreamEntity.investment_id.compareTo("0")!=0)?  Padding(padding: EdgeInsets.all(6),

                        child:        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Expanded(child:Text("Closing balance",style: TextStyle(fontSize: 14),),flex: 2, ),
                            Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                            Expanded(child:Text(myDreamEntity.Closingbalance,style: TextStyle(fontSize: 14),),flex: 2, )



                          ],



                        )) :Container(),

                    (myDreamEntity.investment_id.compareTo("0")!=0)?  Padding(padding: EdgeInsets.all(6),

                        child:        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Expanded(child:Text("Added Amount",style: TextStyle(fontSize: 14),),flex: 2, ),
                            Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                            Expanded(child:Text(myDreamEntity.totalamountadded,style: TextStyle(fontSize: 14),),flex: 2, )



                          ],



                        )) :Container(),

                    Padding(padding: EdgeInsets.all(6),

                        child:        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Expanded(child:Text("Saved Amount",style: TextStyle(fontSize: 14),),flex: 2, ),
                            Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                            Expanded(child:Text(myDreamEntity.savedamount,style: TextStyle(fontSize: 14),),flex: 2, )



                          ],



                        )) ,

                    Padding(padding: EdgeInsets.all(6),

                        child:        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Expanded(child:Text("Target Amount",style: TextStyle(fontSize: 14),),flex: 2, ),
                            Expanded(child:Text(":",style: TextStyle(fontSize: 14),),flex: 2, ),

                            Expanded(child:Text(myDreamEntity.targetamount,style: TextStyle(fontSize: 14),),flex: 2, )



                          ],



                        )),





                    
                    
                  ],
                  
                  
                  
                  
                ),flex: 3,)
                
                
                
                
              ],
              
              
            ),


            Padding(padding: EdgeInsets.all(6),

                child:
                SizedBox(

                  width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 230 : 250:275,
                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 230 : 250:275,

                  child:  Stack(

                    children: [

                      Align(

                        alignment: FractionalOffset.center,

                        child:   SizedBox(

                          width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 210 : 230:250,
                          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 210 : 230:250,

                          child: CircularProgressIndicator(
                            backgroundColor: Color(0xff7fe1e1),
                            valueColor: AlwaysStoppedAnimation(Color(
                                0xFF096c6c)),
                            strokeWidth: 4,
                            value: myDreamEntity.percent/100,
                          ),

                        ),
                      ),

                      Align(

                        alignment: FractionalOffset.center,

                        child: Text(myDreamEntity.percent.toStringAsFixed(2)+" %"+"\n "+myDreamEntity.savedamount+"/"+myDreamEntity.targetamount,style: TextStyle(fontSize: 14),textAlign: TextAlign.center,),

                      )


                    ],


                  ),


                )

            ),

            (  myDreamEntity.goalreached==null)?    Padding(padding: EdgeInsets.all(6),


            child: Container(
              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 : 60:70,
              width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150 : 175:200,
              decoration: BoxDecoration(
                  color: Color(0xFF1D535A), borderRadius: BorderRadius.circular(10)),
              child: TextButton(
                onPressed: () async {
                  // Navigator.push(
                  //     context, MaterialPageRoute(builder: (_) => Registrationpage(title: "registration",)));




                  Map results = await Navigator.of(context)
                      .push(new MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) {
                      return new CalaCulatorPage();
                    },
                  ));

                  if (results != null &&
                      results.containsKey('amountadded')) {


                    setState(() {
                      var accountsetupdata =
                      results['amountadded'];

                      String acs =
                      accountsetupdata as String;
                      var now = DateTime.now();

                      String date = now.day.toString() +
                          "-" +
                          now.month.toString() +
                          "-" +
                          now.year.toString();
                      var mobject1 = new Map();
                      mobject1['date'] =
                          date;
                      mobject1['savedamount'] = acs;
                      mobject1['targetid'] = myDreamEntity.id;

                      var js1 = json.encode(mobject1);
                      Map<String,
                          dynamic> data_To_Table1 = new Map();
                      data_To_Table1['data'] = js1.toString();

                      new DatabaseHelper().insert(
                          data_To_Table1,
                          DatabaseTables
                              .TABLE_ADDEDAMOUNT_MILESTONE);

                      String addedamount=myDreamEntity.totalamountadded;

                      double d=double.parse(addedamount);
                      double dsaved=double.parse(myDreamEntity.savedamount);
                      double enteredamount=double.parse(acs);

                      d=d+enteredamount;
                      myDreamEntity.totalamountadded=d.toStringAsFixed(2);

                      double totalsaved=dsaved+enteredamount;

                      myDreamEntity.savedamount=totalsaved.toStringAsFixed(2);

                      double targetamount=double.parse(myDreamEntity.targetamount);

                      double a=(totalsaved/targetamount)*100;
                      myDreamEntity.percent=a;










                    });
                  }



                },
                child: Text(
                  'Add Amount',
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              ),
            ),

            ) : Container(),


            (  myDreamEntity.goalreached==null)?  Padding(padding: EdgeInsets.all(6),


              child: Container(
                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 : 60:70,
                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150 : 175:200,
                decoration: BoxDecoration(
                    color: Color(0xF08292B0), borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => ViewMileStones(myDreamEntity.id)));





                  },
                  child: Text(
                    'View Milestones',
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ),
              ),

            ) : Container(),


            (  myDreamEntity.goalreached==null)?   Padding(padding: EdgeInsets.all(6),


              child: Container(
                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 : 60:70,
                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150 : 175:200,
                decoration: BoxDecoration(
                    color: Color(0xF08292B0), borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                  onPressed: () async {


                    List<Map<String,dynamic>>mp= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_TARGET,myDreamEntity.id);

                    if(mp.length>0)
                      {

                        Map<String,dynamic> mpdata=mp[0];

                        final mapdatatoUpdate = Map.of(mpdata);

                        mapdatatoUpdate["reached"]="1";

                        new DatabaseHelper().update(mapdatatoUpdate, DatabaseTables.TABLE_TARGET, mapdatatoUpdate["keyid"].toString());

                        setState(() {

                          myDreamEntity.goalreached="1";

                        });




                      }







                  },
                  child: Text(
                    'Set Goal Reached',
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ),
              ),

            ) : Container(),

            



          ],
          
          
        ),
        
        
      ),
      
      
    );
  }

  Future<String>getCategoryname(String id) async
  {
    String name="";

    List<Map<String,dynamic>>mpd= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_TARGETCATEGORY,id);


    if(mpd.length>0)
    {


      Map ab=mpd[0];
      int id = ab["keyid"];
      String data = ab["data"];

      name=data;

      setState(() {
         image = ab["iconimage"];
      });





    }





    return name;
  }

}
