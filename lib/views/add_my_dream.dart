import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/database/DatabaseHelper.dart';
import 'package:saveappforios/domain/MileStoneEntity.dart';
import 'package:saveappforios/utils/StaticValues.dart';
import 'package:saveappforios/views/calculators/cala_culator_page.dart';
import 'package:saveappforios/views/mile_stone.dart';

import '../database/DBTables.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Accountsettings.dart';
import '../domain/TargetCategory.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

import '../projectconstants/DataConstants.dart';
import 'DropdownlistPage.dart';

class AddMyDream extends StatefulWidget {
   AddMyDream() : super();

  @override
  _AddMyDreamState createState() => _AddMyDreamState();
}

class _AddMyDreamState extends State<AddMyDream> {


  TextEditingController targetcontroller=new TextEditingController();

  TextEditingController notecontroller=new TextEditingController();

List<TargetCategory>targetcategory=[];
  late TargetCategory selected_targetcategory;

  String categoryname="Select Target";
  String targetamount="Target Amount",savedamount="Saved Amount",targetdate="Select Target Date";

  List<String>targetdata=["Vehicle","New home","Education","Emergency","Healthcare","Party","Charity"];
  List<String>targetimagedata=["images/car.png","images/home.png","images/education.png","images/emergency.png","images/healthcare.png","images/party.png","images/charity.png"];

bool _fromTop=true;

int count=0;

  String date = "", month = "", year = "";
late String targetfile="";
  String datetxt = "Select target date";

  late Accountsettings _accountsettings;
  String accountdata="Select Investment";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    addTargetCategory();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("My Dream",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,

      ),

      body: SingleChildScrollView(

        child: Column(

          children: [

            Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(Icons.arrow_drop_down,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,color: Colors.black12,),
                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () async {
                              // showAccountsDialog();



                              showTargetDialog();





                            },
                            child: Center(
                                child: Text(categoryname,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black38,
                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                          ))
                    ],
                  ),
                )),

            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),

              child: TextField(
                keyboardType: TextInputType.text,
                controller: targetcontroller,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Target Name',
                ),
              ),

            ),

            Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[

                      Stack(

                        children: [

                          Align(

                            alignment: FractionalOffset.centerRight,

                            child:      TextButton(
                              onPressed: () async {
                                // showAccountsDialog();

                                Map results = await Navigator.of(context)
                                    .push(new MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) {
                                    return new CalaCulatorPage();
                                  },
                                ));

                                if (results != null &&
                                    results.containsKey('amountadded')) {


                                  setState(() {
                                    var accountsetupdata =
                                    results['amountadded'];

                                    String acs =
                                    accountsetupdata as String;
                                    //
                                    // if(acs>0)
                                    // {
                                      //setupAccountData();

                                      setState(() {

                                        targetamount=acs.toString();
                                    });







                                  });
                                }



                              },
                              child: Center(
                                  child: Text(targetamount,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          color: Colors.black38,
                                          fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                            ),

                          )
                        ],




                      )
                    ],
                  ),
                )),

            Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(Icons.arrow_drop_down,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,color: Colors.black12,),
                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () async {
                              // showAccountsDialog();
                              Map results = await Navigator.of(context)
                                  .push(new MaterialPageRoute<dynamic>(
                                builder: (BuildContext context) {
                                  return new Dropdownlistpage(
                                    title: "dropdownlist",
                                    accountType:
                                    DataConstants.investmentType,
                                  );
                                },
                              ));

                              if (results != null &&
                                  results.containsKey('selecteddata')) {

                                var acc_selected =
                                results['selecteddata'];

                                Accountsettings acs =
                                acc_selected as Accountsettings;

                                _accountsettings = acs;
                                accountdata = acs.data;

                                List<Map<String,dynamic>>mpd=await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, acs.id);


                                if(mpd.length>0)
                                {

                                  setState(() {
                                    Map ab=mpd[0];
                                    int id = ab["keyid"];
                                    String data = ab["data"];

                                    var jsondata = jsonDecode(data);

                                    // String Accountname = jsondata["Accountname"];
                                    savedamount=jsondata['Amount'];
                                  });




                                }



                              }
                            },
                            child: Center(
                                child: Text(accountdata,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black38,
                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                          ))
                    ],
                  ),
                )),

            Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[

                      Stack(

                        children: [

                          Align(

                            alignment: FractionalOffset.centerRight,

                            child:      TextButton(
                              onPressed: () async {
                                // showAccountsDialog();
                                Map results = await Navigator.of(context)
                                    .push(new MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) {
                                    return new CalaCulatorPage();
                                  },
                                ));

                                if (results != null &&
                                    results.containsKey('amountadded')) {


                                  setState(() {
                                    var accountsetupdata =
                                    results['amountadded'];

                                    String acs =
                                    accountsetupdata as String;
                                    //
                                    // if(acs>0)
                                    // {
                                    //setupAccountData();

                                    setState(() {

                                      savedamount=acs.toString();
                                    });







                                  });
                                }


                              },
                              child: Center(
                                  child: Text(savedamount,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          color: Colors.black38,
                                          fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                            ),

                          )
                        ],




                      )
                    ],
                  ),
                )),
            Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,color: Colors.black12,),
                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () async {
                              // showAccountsDialog();

                              var now = DateTime.now();

                              date = now.day.toString() +
                                  "-" +
                                  now.month.toString() +
                                  "-" +
                                  now.year.toString();

                              month = now.month.toString();
                              year = now.year.toString();

                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {
                                                date = newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();

                                                month = newDateTime.month
                                                    .toString();
                                                year = newDateTime.year
                                                    .toString();

                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height: 50,
                                              width: 150,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });




                            },
                            child: Center(
                                child: Text(datetxt,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black38,
                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                          ))
                    ],
                  ),
                )),

            Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black38,
                      // red as border color
                    ),
                  ),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(Icons.arrow_drop_down,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,color: Colors.black12,),
                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () async {
                              // showAccountsDialog();
                              Map results = await Navigator.of(context)
                                  .push(new MaterialPageRoute<dynamic>(
                                builder: (BuildContext context) {
                                  return new MileStone(


                                  );
                                },
                              ));

                              if (results != null &&
                                  results.containsKey('accountsetupdata')) {
                                setState(() {
                                  count=StaticValues.milestoneadded.length;
                                });
                              }



                            },
                            child: Center(
                                child: Text( (count==0)? "Add Milestone" : "View Milestone",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black38,
                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16))),
                          ))
                    ],
                  ),
                )),

            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),

              child: Container(



                child:  TextField(
                  keyboardType: TextInputType.text ,
                  maxLines: 3,
                  controller: notecontroller,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.black38, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.black38, width: 0.5),
                    ),
                    hintText: 'Notes',
                  ),
                ),

              )




            ),

            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                height: 50,
                width: 150,
                decoration: BoxDecoration(
                    color: Color(0xF0233048),
                    borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                  onPressed: () async {


                    if(selected_targetcategory!=null)
                      {

                        if(targetcontroller.text.isNotEmpty)
                        {

                          if(targetamount.isNotEmpty)
                          {

                            if(savedamount.isNotEmpty)
                            {

                              double d_target=double.parse(targetamount);
                              double d_saved=double.parse(savedamount);

                              if(d_target>d_saved) {
                                if (targetdate.isNotEmpty) {
                                  if (StaticValues.milestoneadded.length > 0) {
                                    var mjobject = new Map();
                                    mjobject['targetname'] =
                                        targetcontroller.value.text;
                                    mjobject['savedamount'] = "0";
                                    mjobject['target_date'] = datetxt;
                                    mjobject['note'] =
                                        notecontroller.text.toString();
                                    mjobject['targetamount'] = targetamount;


                                    if (accountdata.compareTo(
                                        "Select Investment") != 0) {
                                      mjobject["investment_id"] =
                                          _accountsettings.id;
                                    }
                                    else {
                                      mjobject["investment_id"] = "0";
                                    }
                                    mjobject["target_categoryid"] =
                                        selected_targetcategory.id;

                                    var js = json.encode(mjobject);
                                    Map<String,
                                        dynamic> data_To_Table = new Map();
                                    data_To_Table['data'] = js.toString();
                                    int id = await new DatabaseHelper().insert(
                                        data_To_Table,
                                        DatabaseTables.TABLE_TARGET);


                                    if (accountdata.compareTo(
                                        "Select Investment") == 0) {
                                      var now = DateTime.now();

                                      String date = now.day.toString() +
                                          "-" +
                                          now.month.toString() +
                                          "-" +
                                          now.year.toString();
                                      var mobject1 = new Map();
                                      mobject1['date'] =
                                          date;
                                      mobject1['savedamount'] = savedamount;
                                      mobject1['targetid'] = id.toString();

                                      var js1 = json.encode(mobject1);
                                      Map<String,
                                          dynamic> data_To_Table1 = new Map();
                                      data_To_Table1['data'] = js1.toString();

                                      new DatabaseHelper().insert(
                                          data_To_Table1,
                                          DatabaseTables
                                              .TABLE_ADDEDAMOUNT_MILESTONE);
                                    }


                                    // js2.put("amount", amount);
                                    // js2.put("start_date", start_date);
                                    // js2.put("end_date", end_date);
                                    // js2.put("targetid", isid + "");

                                    for (MileStoneEntity ml in StaticValues
                                        .milestoneadded) {
                                      var mobject_milestone = new Map();
                                      mobject_milestone['amount'] =
                                          ml.amount;
                                      mobject_milestone['start_date'] =
                                          ml.start_date;
                                      mobject_milestone['end_date'] =
                                          ml.end_date;
                                      mobject_milestone['targetid'] =
                                          id.toString();
                                      var js2 = json.encode(mobject_milestone);
                                      Map<String,
                                          dynamic> data_To_Table2 = new Map();
                                      data_To_Table2['data'] = js2.toString();

                                      new DatabaseHelper().insert(
                                          data_To_Table2,
                                          DatabaseTables.TABLE_MILESTONE);
                                    }


                                    StaticValues.milestoneadded.clear();
                                    Navigator.of(context).pop({'mydreamadded':1 });
                                  }
                                  else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(content: Text("Add milestone")),
                                    );
                                  }
                                }
                                else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content: Text("Select target date")),
                                  );
                                }
                              }
                              else{

                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text("Target amount must be greater than saved amount")),
                                );

                              }



                            }
                            else{

                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text("Enter saved amount")),
                              );

                            }




                          }
                          else{

                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Enter target amount")),
                            );

                          }


                        }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("Enter target name")),
                          );

                        }



                      }
                    else{

                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("Select target category")),
                      );

                    }






                  },
                  child: Text(
                    'Add',
                    style:
                    TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
              ),
            ),

          ],


        )


      )






    );
  }






  getAllTarget()
  async {

    List<Map<String,dynamic>>mp=await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_TARGETCATEGORY);

    print("My Dream size : "+mp.length.toString());
    targetcategory.clear();

    for (Map ab in mp) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];
      Uint8List image = ab["iconimage"];

      TargetCategory targetCategory=new TargetCategory();
      targetCategory.id=id.toString();
      targetCategory.name=data;
      targetCategory.image=image;



      targetcategory.add(targetCategory);






    }






  }

  addTargetCategory()async
  {

    // new DatabaseHelper().deleteAll(DatabaseTables.TABLE_TARGETCATEGORY);
    // for(int i=0;i<targetdata.length;i++)
    //   {
    //
    //     final ByteData bytes = await rootBundle.load(targetimagedata[i]);
    //     final Uint8List list = bytes.buffer.asUint8List();
    //
    //     Map<String,dynamic>mp=new HashMap();
    //
    //     mp["data"]=targetdata[i];
    //     mp["iconimage"]=list;
    //
    //     new DatabaseHelper().insert(mp, DatabaseTables.TABLE_TARGETCATEGORY);
    //
    //
    //   }

    getAllTarget();

  }


  showTargetDialog()
  {

    showGeneralDialog(
      barrierLabel: "Label",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return Align(
          alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
          child: Container(
            height: (MediaQuery.of(context).size.height)/1.3,
            width: (MediaQuery.of(context).size.width),
            child: SingleChildScrollView(


                child: Stack(

                  children:[

                    Align(

                      alignment: FractionalOffset.topCenter,

                      child: Padding(
                        
                        padding: EdgeInsets.only(top: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:110:160),
                        child:  GridView.count(
                          shrinkWrap: true,
                          primary: false,
                          crossAxisCount: 3,
                          childAspectRatio: 1,
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5,
                          //physics:BouncingScrollPhysics(),
                          padding: EdgeInsets.all(10.0),
                          children: targetcategory
                              .map(
                                (data) => GestureDetector(
                                onTap: () {

                                  Navigator.pop(context);

                                  setState(() {
                                    selected_targetcategory=data;

                                    categoryname=data.name;
                                  });





                                },
                                child: Container(
                                  padding: const EdgeInsets.all(10),

                                  //  margin:EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                                  //color:data.color,

                                  child: Column(
                                    children: [
                                      Image.memory(data.image,width: 50,height: 50,fit: BoxFit.fill,)

                                      ,
                                      Text(data.name,
                                        style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:13:16, color: Colors.black),
                                        textAlign: TextAlign.center,)
                                    ],
                                  ),
                                )),
                          )
                              .toList(),
                        ),
                      )
                      
                      
                     
                    ),



                    Align(

                        alignment: FractionalOffset.bottomRight,

                        child: Padding(


                          padding: EdgeInsets.all(12),
                          child:     FloatingActionButton(

                            tooltip: 'add',
                            onPressed: () {

                              Navigator.pop(context);

  showCreateTargetDialog();

                            },
                            child: const Icon(Icons.add),
                          ),
                        )





                    )


                  ]
                )





                

            ),
            margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        );
      },
      transitionBuilder: (context, anim1, anim2, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0)).animate(anim1),
          child: child,
        );
      },
    );
  }

  showCreateTargetDialog()
  async {

    Map results = await Navigator.of(context)
        .push(new MaterialPageRoute<dynamic>(
      builder: (BuildContext context) {
        return new MyDialog();
      },
    ));

    if (results != null &&
        results.containsKey('dream')) {
      setState(() {
        var accountsetupdata =
        results['dream'];

        int acs =
        accountsetupdata as int;
        //
        if(acs>0)
        {
        getAllTarget();
        }


      });
    }

    // showGeneralDialog(
    //   barrierLabel: "Label",
    //   barrierDismissible: true,
    //   barrierColor: Colors.black.withOpacity(0.5),
    //   transitionDuration: Duration(milliseconds: 700),
    //   context: context,
    //   pageBuilder: (context, anim1, anim2) {
    //     return MyDialog();
    //   },
    //   transitionBuilder: (context, anim1, anim2, child) {
    //     return SlideTransition(
    //       position: Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0)).animate(anim1),
    //       child: child,
    //     );
    //   },
    // );
  }
}


class MyDialog extends StatefulWidget{

  @override
  _MyDialogState createState() => new _MyDialogState();



}

class _MyDialogState extends State<MyDialog> {



  _MyDialogState();



  TextEditingController titlecontroller=new TextEditingController();


  final dbHelper = new DatabaseHelper();

  String targetfile="";

  @override
  void initState()  {
    // TODO: implement initState

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
          width: MediaQuery.of(context).size.width ,
          height: MediaQuery.of(context).size.height/1.3,

          child:Column(

              children:[

                Align(
                  alignment: FractionalOffset.topCenter,

                  child: Padding(
                      padding: EdgeInsets.all(2),
                      child: Container(

                        height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?120:150:180,
                        width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 120:150:180,

                        child:  Stack(children: [

                          Align(
                              alignment: FractionalOffset.bottomCenter,

                              child:

                              Container(
                                  height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?100:130:160,
                                  width: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? 100:130:160,

                                  child: (targetfile.isNotEmpty)?  CircleAvatar(
                                    backgroundImage:


                                    FileImage(
                                        new File(targetfile)  ,
                                        scale: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?100:130:160


                                    ),




                                    // child: Image.network(
                                    //   DataConstants.profileimgbaseurl+profileimage,
                                    //   fit: BoxFit.fill,
                                    //   errorBuilder: (context, url, error) => new Icon(Icons.account_circle_rounded,size: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?90:120:160,),
                                    // ),


                                  ) : CircleAvatar(

                                    backgroundColor: Colors.blue,


                                  )

                                // child:Image.asset("images/user.png")
                              ) ),



                          Align(
                              alignment: FractionalOffset.bottomCenter,
                              child: Padding(

                                padding:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(45, 0, 0, 0) : EdgeInsets.fromLTRB(60, 0, 0, 0):EdgeInsets.fromLTRB(80, 0, 0, 0),

                                child: FloatingActionButton(
                                  onPressed: () async {

                                    Widget yesButton = TextButton(
                                        child: Text("Yes",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
                                        onPressed: () async {

                                          Navigator.pop(context);

                                          final ImagePicker _picker = ImagePicker();
                                          // Pick an image
                                          final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

                                          // print(image!.path) ;

                                          if(image!=null) {


                                            CroppedFile? croppedFile = await ImageCropper()
                                                .cropImage(
                                              sourcePath: image.path,
                                              aspectRatioPresets: [
                                                CropAspectRatioPreset.square,
                                                CropAspectRatioPreset.ratio3x2,
                                                CropAspectRatioPreset.original,
                                                CropAspectRatioPreset.ratio4x3,
                                                CropAspectRatioPreset.ratio16x9
                                              ],
                                              // androidUiSettings: AndroidUiSettings(
                                              //     toolbarTitle: 'Cropper',
                                              //     toolbarColor: Colors.deepOrange,
                                              //     toolbarWidgetColor: Colors.white,
                                              //     initAspectRatio: CropAspectRatioPreset
                                              //         .original,
                                              //     lockAspectRatio: false),
                                              // iosUiSettings: IOSUiSettings(
                                              //   minimumAspectRatio: 1.0,
                                              // )
                                            );


                                            setState(() {

                                              targetfile=croppedFile!.path;
                                            });


                                           // uploadImage(croppedFile);
                                          }


                                        });



                                    Widget noButton = TextButton(
                                      child: Text("No",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    );

                                    // set up the AlertDialog
                                    AlertDialog alert = AlertDialog(
                                      title: Text("Save",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                                      content: Text("We will keep your Target Category photo secure, We do not going to share outside.Do you want to continue ?",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                                      actions: [yesButton, noButton],
                                    );

                                    // show the dialog
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return alert;
                                      },
                                    );







                                  },
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                    size: 29,
                                  ),
                                  backgroundColor: Color(0xffbfefcc),
                                  tooltip: 'Capture Picture',
                                  elevation: 5,
                                  splashColor: Colors.grey,
                                ),
                              )



                          )
                        ]),
                      )




                  ),





                ),

                Padding(
                  padding:   ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(4):EdgeInsets.all(8):EdgeInsets.all(11),

                  // padding: EdgeInsets.all(15),
                  child: new Theme(data: new ThemeData(
                      hintColor: Colors.black38
                  ), child: TextField(

                    controller: titlecontroller,

                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black38, width: 0.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black38, width: 0.5),
                      ),
                      hintText: 'Target name',
                    ),

                    onChanged: (text) {


                    },
                  )),
                ),

                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Container(
                    height: 50,
                    width: 150,
                    decoration: BoxDecoration(
                        color: Color(0xF0233048),
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                      onPressed: () {


                        if(targetfile.isNotEmpty)
                          {
                            if(titlecontroller.text.isNotEmpty)
                            {

                              Uint8List bytes = new File(targetfile).readAsBytesSync();
                              final Uint8List list = bytes.buffer.asUint8List();

                              Map<String,dynamic>mp=new HashMap();

                              mp["data"]=titlecontroller.text;
                              mp["iconimage"]=list;

                              new DatabaseHelper().insert(mp, DatabaseTables.TABLE_TARGETCATEGORY);

                              Navigator.of(context).pop({'dream': 1});




                            }
                            else{

                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text("Enter your target category title")),
                              );

                            }


                          }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("Upload Target category Image")),
                          );

                        }



                      },
                      child: Text(
                        'Add',
                        style:
                        TextStyle(color: Colors.white, fontSize: 15),
                      ),
                    ),
                  ),
                ),




              ]
          )




      ),

    );
  }







}
