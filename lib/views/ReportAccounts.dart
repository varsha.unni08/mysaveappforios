import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:open_file/open_file.dart';


import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../projectconstants/DataConstants.dart';





class ReportAccounts extends StatefulWidget {

  final String title;


  final String accid;

  const ReportAccounts({Key? key, required this.title, required this.accid})
      : super(key: key);

  @override
  State<StatefulWidget> createState()  => _ReportAccountsDetailsPage(title,accid);

}


  class _ReportAccountsDetailsPage extends State<ReportAccounts> {

     String title;


     String accid;

     String accountname="";

     DatabaseHelper dbhelper=new DatabaseHelper();

     List<String>accdata=[];

     List<String>tableheaddata = [

       "Date",
       "Name",
       "Amount",

       "Debit/Credit"
     ];


     _ReportAccountsDetailsPage(this.title, this.accid);

  @override
  void initState() {
    // TODO: implement initState

    loadAccountHeads();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Ledger : "+accountname,style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

   body: Stack(
    children: [

    (accdata.length>0)?    Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 0),




        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          removeBottom: true,
          child:GridView.builder(
          physics: BouncingScrollPhysics(),

          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              crossAxisSpacing: 0.0,
              mainAxisSpacing: 0.0,
              childAspectRatio: 2.0

          ),
          itemCount: tableheaddata.length,
          itemBuilder: (context, index) {
            return  Container(

                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black54,
                    width: 0.3,
                  ),
                ),

                child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),maxLines: 2,)));


          },
        )
          ,)



    ):Container(),

    (accdata.length>0)?    Padding(padding:EdgeInsets.fromLTRB(10, 55, 10, 0)

    ,child:MediaQuery.removePadding(
    context: context,
    removeTop: true,
    child:GridView.builder(
    physics: BouncingScrollPhysics(),

    shrinkWrap: true,
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 4,
    crossAxisSpacing: 0.0,
    mainAxisSpacing: 0.0,
    childAspectRatio:2.0

    ),
    itemCount: accdata.length,
    itemBuilder: (context, index) {
    return Container(

    decoration: BoxDecoration(
    border: Border.all(
    color: Colors.black54,
    width: 0.3,
    ),
    ),

    child:

    Center(child:Text(accdata[index],style: TextStyle(fontSize: 13,color: Colors.black),)),

    );
    },
    ))):Container(),

    ]

    ),





    );
  }

     void loadAccountHeads() async {
       List<Map<String, dynamic>> a =
       await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
       // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
       List<String> accountsetupd = [];
       String Accountname = "";

       // accountsetupdata.add(subjectdata);
       for (Map ab in a) {
         print(ab);

         int subid = ab["keyid"];

         if(subid.toString().compareTo(accid)==0) {
           String data = ab["data"];

           var jsondata = jsonDecode(data);


           Accountname = jsondata["Accountname"];
           String Accounttype = jsondata["Accounttype"];
           String Amount = jsondata['Amount'];
           String Type = jsondata['Type'];

           if (Amount.isEmpty) {
             Amount = "0";
           }

           double am=double.parse(Amount);

           if(Type.compareTo("Credit")==0)
             {

               am=am*-1;


             }


           setState(() {

             accountname=Accountname;

           });

        List<String >s=await   loadDataFromAccounts(subid,am);

        setState(() {
          accdata.clear();

          accdata.addAll(s);
        });
         }
       }
     }




     Future<List<String>> loadDataFromAccounts(int accountid,double openingbalance)async
     {


       List<String>accountdata=[];

       DateTime dt=DateTime.now();
       String y=dt.year.toString();
       double op=openingbalance*-1;

       accountdata.add("1-1-"+y);
       accountdata.add("Opening balance");
       accountdata.add(op.toString());
      // accountdata.add("Credit");

       if(openingbalance>0)
         {
           accountdata.add("Debit");
         }
       else{

         accountdata.add("Credit");
       }


       double closingbalance=openingbalance;
       var m=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);
       List<Map<String, dynamic>> v=m;

       for(Map a in v) {

         String accountid = a[DatabaseTables.ACCOUNTS_id];
         String date = a[DatabaseTables.ACCOUNTS_date];
         String setupidaccount = a[DatabaseTables.ACCOUNTS_setupid];
         String ab=accountid.toString();
         String accountsamount = a[DatabaseTables.ACCOUNTS_amount];
         String accountsentryid = a[DatabaseTables.ACCOUNTS_entryid];

         accountdata.add(date);



         if(setupidaccount.compareTo(ab)==0)
         {



           double amount=double.parse(accountsamount);




           double closingbalance=openingbalance;
           var m1=await dbhelper.getAccounDataByEntryid(accountid);
           var m2=await dbhelper.getAccountDataByID(accountsentryid);
           List<Map<String, dynamic>> v1=m1;

           List<Map<String, dynamic>> v2=m2;
           String Accountname ="";

           if(v1.length>0)
             {
               Map<String, dynamic> a1=v1[0];

               String setupidaccount = a1[DatabaseTables.ACCOUNTS_setupid];

               var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupidaccount);


               List<Map<String, dynamic>> accountsettings=value;

               Map ab=accountsettings[0];

               // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

               int id = ab["keyid"];
               String data = ab["data"];

               var jsondata = jsonDecode(data);

                Accountname = jsondata["Accountname"];





             }
           if(v2.length>0)
           {
             Map<String, dynamic> a2=v2[0];

             String setupidaccount = a2[DatabaseTables.ACCOUNTS_setupid];



             var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupidaccount);


             List<Map<String, dynamic>> accountsettings=value;

             Map ab=accountsettings[0];

             // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

             int id = ab["keyid"];
             String data = ab["data"];

             var jsondata = jsonDecode(data);

             Accountname = jsondata["Accountname"];
           }


           accountdata.add(Accountname);
           accountdata.add(amount.toString());


           String accountstype = a[DatabaseTables.ACCOUNTS_type].toString();
           if (accountstype.compareTo(DataConstants.debit.toString()) == 0) {
             accountdata.add("Debit");

           }
           else {

             accountdata.add("Credit");
           }



         }
       }



       return accountdata;



     }




  }

