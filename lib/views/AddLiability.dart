import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/mainviews/CashbankAccountDetails.dart';
//
//
// import 'package:save_flutter/domain/Paymentdata.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:save_flutter/mainviews/AddRecipt.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
//
// import 'AddAccountSetup.dart';
import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import 'AddAccountSetup.dart';
import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;
import 'package:numberpicker/numberpicker.dart';


class AddLiabilityPage extends StatefulWidget {
  final String title;
  final String liabilityid;

  const AddLiabilityPage({Key? key, required this.title, required this.liabilityid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddLiabilityPage(liabilityid);

}

class _AddLiabilityPage extends State<AddLiabilityPage> {



  TextEditingController amountcontroller=new TextEditingController();

  TextEditingController emiamountcontroller=new TextEditingController();

  TextEditingController nofemiamountcontroller=new TextEditingController();

  String date = "",
      date1 = "";
  String datetxt1 = "Select date of payment";
  String datetxt2 = "Select closing date";
  List<String>emiclassification = ["EMI","NON EMI"];

  String accountid="";

  List<String>subject = ["Select liability account"];

  String subjectdata = "Select liability account";
  String eminon="EMI";

  DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<Cashbankaccount> cashbankdata = [];

  String accountType="Liability account";
  String liabilityid;
  String selecteddate="";




  _AddLiabilityPage(this.liabilityid);



  @override
  void initState() {
    // TODO: implement initState
    setupAccountData();

    if(liabilityid.compareTo("0")!=0)
      {

        showLiabilityDataByID(liabilityid);
      }

    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);


  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }






  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Opening/new liabilities",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),


      body: Stack(
      children: <Widget>[


    SingleChildScrollView(
    child: Column(

    children: <Widget>[

      Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(8) : EdgeInsets.all(11) :EdgeInsets.all(14),
        child: Container(
          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,

          decoration: BoxDecoration(
              border: Border.all(color: Colors.black38)),

          child:  DropdownButtonHideUnderline(

            child: ButtonTheme(
              alignedDropdown: true,
              child: InputDecorator(
                decoration: const InputDecoration(border: OutlineInputBorder()),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(

                    isExpanded: true,
                    value: eminon,
                    items: emiclassification
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        eminon = newValue!;

                        if(eminon.compareTo("NON EMI")==0)
                          {



                              datetxt1="Select closing date";
                              datetxt2="Set remind dates";

                          }
                        else {
                           datetxt1 = "Select date of payment";
                           datetxt2 = "Select closing date";

                        }

                        // getBudgetData();






                      });
                    },
                    style: Theme.of(context).textTheme.bodyText1,

                  ),
                ),
              ),
            ),
          ),


        ),


      ),
      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11) : EdgeInsets.all(14)

          ,child:Row(children: [
            Expanded(

                flex: 3,
                child:Padding(padding: EdgeInsets.all(0),
                  child: Container(
                    height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,

                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black38)),

                    child:  DropdownButtonHideUnderline(

                      child: ButtonTheme(
                        alignedDropdown: true,
                        child: InputDecorator(
                          decoration: const InputDecoration(border: OutlineInputBorder()),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(

                              isExpanded: true,
                              value: subjectdata,
                              items: subject
                                  .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (String? newValue) {
                                setState(() {
                                  subjectdata = newValue!;

                                  // getBudgetData();

                                  setDataByAccountName(subjectdata);




                                });
                              },
                              style: Theme.of(context).textTheme.bodyText1,

                            ),
                          ),
                        ),
                      ),
                    ),


                  ),


                )





            ),

            Expanded(child: Padding(
                padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11) : EdgeInsets.all(14),

                child: FloatingActionButton(
                  onPressed: () async{


                    //   showBubjectDialog();

                    Map results = await Navigator.of(context)
                        .push(new MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) {
                        return new AddAccountSettinglistpage(title: "account setup",accountType: accountType, accountsetupid: '0',);
                      },
                    ));

                    if (results != null &&
                        results.containsKey('accountsetupdata')) {
                      setState(() {
                        var accountsetupdata =
                        results['accountsetupdata'];

                        int acs =
                        accountsetupdata as int;
                        //
                        if(acs>0)
                        {
                          setupAccountData();
                        }


                      });
                    }

                  },
                  child: Icon(Icons.add, color: Colors.white, size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 29:35:40,),
                  backgroundColor: Colors.blue,

                  elevation: 5,
                  splashColor: Colors.grey,
                )),flex: 1,)


          ],)),

      Padding(
        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 0) : EdgeInsets.only(left:21.0,right: 21.0,top:16,bottom: 0),
        // padding: EdgeInsets.all(15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.black
        ), child: TextField(
          controller: amountcontroller,

          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            hintText: 'Amount',
          ),

          onChanged: (text) {
            //TemRegData.email=text;

          },
        )),
      ),

      ((eminon.compareTo("EMI")==0))? Padding(
        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 0) : EdgeInsets.only(left:21.0,right: 21.0,top:16,bottom: 0),

        // padding: EdgeInsets.all(15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.black
        ), child: TextField(
          controller: emiamountcontroller,

          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            hintText: 'EMI Amount',
          ),

          onChanged: (text) {
            //TemRegData.email=text;

          },
        )),
      ):Container(),

      ((eminon.compareTo("EMI")==0))?  Padding(
        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 0) : EdgeInsets.only(left:21.0,right: 21.0,top:16,bottom: 0),

        // padding: EdgeInsets.all(15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.black
        ), child: TextField(
          controller: nofemiamountcontroller,

          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            hintText: 'No. of EMI',
          ),

          onChanged: (text) {
            //TemRegData.email=text;

            if(selecteddate.isNotEmpty) {
              calculateClosingDate(nofemiamountcontroller.text.toString(), selecteddate);
            }

          },
        )),
      ):Container(),

      Padding(
          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10) : EdgeInsets.all(12) :EdgeInsets.all(13),
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black38)),
            child: Row(
              textDirection: ui.TextDirection.rtl,
              children: <Widget>[
                Padding(
                    padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12):EdgeInsets.all(14),
                    child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26 ,)),
                Expanded(
                    child: TextButton(
                      onPressed: () {


                        if(eminon.compareTo("NON EMI")==0)
                        {



                          // datetxt1="Select closing date";
                          // datetxt2="Set remind dates";

                          String d="";

                          var now = DateTime.now();

                          d = now.day.toString() +
                              "-" +
                              now.month.toString() +
                              "-" +
                              now.year.toString();



                          // showCustomDatePicker(build(context));

                          showModalBottomSheet(
                              context: context,
                              builder: (context) {
                                return Container(
                                    child: Column(children: [
                                      Expanded(
                                        child: CupertinoDatePicker(
                                          mode: CupertinoDatePickerMode
                                              .date,
                                          initialDateTime: DateTime(
                                              now.year,
                                              now.month,
                                              now.day),
                                          onDateTimeChanged:
                                              (DateTime newDateTime) {
                                                d = newDateTime.day
                                                .toString() +
                                                "-" +
                                                newDateTime.month
                                                    .toString() +
                                                "-" +
                                                newDateTime.year
                                                    .toString();



                                            //print(date);
                                            // Do something
                                          },
                                        ),
                                        flex: 2,
                                      ),
                                      Padding(
                                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4) :EdgeInsets.all(6),
                                        child: Container(
                                          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                          width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                          decoration: BoxDecoration(
                                              color: Color(0xF0233048),
                                              borderRadius:
                                              BorderRadius.circular(
                                                  10)),
                                          child: TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);

                                              setState(() {
                                                datetxt1 = d;
                                              });
                                            },
                                            child: Text(
                                              'Ok',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                            ),
                                          ),
                                        ),
                                      )
                                    ]));
                              });




                        }
                        else {
                          // datetxt1 = "Select date of payment";
                          // datetxt2 = "Select closing date";


                          showDialog(
                              context: context,
                              builder: (_) {
                                return NumberDialog();
                              }).then((value) => {

                            selecteddate =value["option"].toString(),

                            setDateofpaymentValue(selecteddate)


                          });

                        }




                      },
                      child: Text(
                        datetxt1,
                        style: TextStyle(
                            color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                      ),
                    ))
              ],
            ),
          )),

      Padding(
          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black38)),
            child: Row(
              textDirection: ui.TextDirection.rtl,
              children: <Widget>[
                Padding(
                    padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
                    child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26 ,)),
                Expanded(
                    child: TextButton(
                      onPressed: () {


                        String d="";

                        var now = DateTime.now();

                        d = now.day.toString() +
                            "-" +
                            now.month.toString() +
                            "-" +
                            now.year.toString();



                        // showCustomDatePicker(build(context));

                        showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return Container(
                                  child: Column(children: [
                                    Expanded(
                                      child: CupertinoDatePicker(
                                        mode: CupertinoDatePickerMode
                                            .date,
                                        initialDateTime: DateTime(
                                            now.year,
                                            now.month,
                                            now.day),
                                        onDateTimeChanged:
                                            (DateTime newDateTime) {
                                          d = newDateTime.day
                                              .toString() +
                                              "-" +
                                              newDateTime.month
                                                  .toString() +
                                              "-" +
                                              newDateTime.year
                                                  .toString();



                                          //print(date);
                                          // Do something
                                        },
                                      ),
                                      flex: 2,
                                    ),
                                    Padding(
                                      padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) :EdgeInsets.all(4):EdgeInsets.all(6),
                                      child: Container(
                                        height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                        width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150:160:170,
                                        decoration: BoxDecoration(
                                            color: Color(0xF0233048),
                                            borderRadius:
                                            BorderRadius.circular(
                                                10)),
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);

                                            setState(() {
                                              datetxt2 = d;
                                            });
                                          },
                                          child: Text(
                                            'Ok',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                          ),
                                        ),
                                      ),
                                    )
                                  ]));
                            });


                      },
                      child: Text(
                        datetxt2,
                        style: TextStyle(
                            color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                      ),
                    ))
              ],
            ),
          )),

      Align(

        alignment: Alignment.topCenter,
        child:   Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
          child:  Container(
            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xFF006b6b),
                borderRadius: BorderRadius.circular(10)),
            child: TextButton(
              onPressed: () async {





                if(eminon.compareTo("NON EMI")==0)
                  {


                    if(subjectdata.compareTo("Select liability account")!=0) {

                      if(datetxt1.compareTo("Select closing date")!=0) {

                        if(datetxt2.compareTo("Set remind dates")!=0) {
                          Map<String, dynamic> assetdata = new Map();
                          assetdata['name'] = accountid;
                          assetdata['amount'] =
                              amountcontroller.text.toString();
                          assetdata['closing_date'] = datetxt1;
                          assetdata['remind_date'] = datetxt2;

                          assetdata['nofemi'] = "";

                          assetdata['payment_date'] = "";
                          var js = json.encode(assetdata);
                          Map<String, dynamic> data_To_Table = new Map();
                          data_To_Table['data'] = js.toString();

                          if (liabilityid.compareTo("0") == 0) {
                            dbhelper.insert(
                                data_To_Table, DatabaseTables.TABLE_LIABILITY);
                          }
                          else{

                            dbhelper.update(data_To_Table, DatabaseTables.TABLE_LIABILITY, liabilityid);
                          }

                          Navigator.pop(context, {"accountsetupdata": 1});
                        }

                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Select remind date"),
                          ));

                        }




                      }

                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Select closing date"),
                        ));

                      }



                    }
                    else{

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Select liability account"),
                      ));

                    }


                  }
                else {
                  if (subjectdata.compareTo("Select liability account") != 0) {

                    if(emiamountcontroller.text.isNotEmpty) {

                      if(nofemiamountcontroller.text.isNotEmpty) {

                        if(datetxt1.isNotEmpty) {
                          Map<String, dynamic> assetdata = new Map();
                          assetdata['name'] = accountid;
                          assetdata['amount'] =
                              amountcontroller.text.toString();
                          assetdata['emiamount'] =
                              emiamountcontroller.text.toString();
                          assetdata['nofemi'] =
                              nofemiamountcontroller.text.toString();

                          assetdata['payment_date'] = datetxt1;
                          assetdata['closing_date'] = datetxt2;
                          assetdata['remind_date'] = "";
                          var js = json.encode(assetdata);
                          Map<String, dynamic> data_To_Table = new Map();
                          data_To_Table['data'] = js.toString();

                          if (liabilityid.compareTo("0") == 0) {
                            dbhelper.insert(
                                data_To_Table, DatabaseTables.TABLE_LIABILITY);
                          }
                          else{

                            dbhelper.update(data_To_Table, DatabaseTables.TABLE_LIABILITY, liabilityid);
                          }


                          Navigator.pop(context, {"accountsetupdata": 1});



                        }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Select payment date"),
                          ));
                        }

                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Enter number of emi"),
                        ));
                      }


                    }
                    else{

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Enter emi amount"),
                      ));
                    }


                  }
                  else{

                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Select liability account"),
                    ));

                  }
                }
















              },
              child: Text(
                'Save',
                style:
                TextStyle(color: Colors.white, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  15:17:19),
              ),
            ),
          ),
        ),),

    ]
      )
    )
    ]
      ),
    );
  }


  void showLiabilityDataByID(String id) async
  {

    List<Map<String, dynamic>> a =
        await dbhelper.getDataByid(DatabaseTables.TABLE_LIABILITY,id);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));

    var ab=a[0];


    // print(ab);

    int dataid = ab["keyid"];
    String data=ab["data"];

    var jsondata = jsonDecode(data);

    // Map<String, dynamic> assetdata = new Map();
    // assetdata['name'] = accountid;
    // assetdata['amount'] =
    //     amountcontroller.text.toString();
    // assetdata['emiamount'] =
    //     emiamountcontroller.text.toString();
    // assetdata['nofemi'] =
    //     nofemiamountcontroller.text.toString();
    //
    // assetdata['payment_date'] = datetxt1;
    // assetdata['closing_date'] = datetxt2;
    // assetdata['remind_date'] = "";

    String name=jsondata['name'];
    String amount=jsondata['amount'];

    var v1 = await dbhelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, name);
    List<Map<String, dynamic>> ab1 = v1;
    Map<String, dynamic> mapdata1 = ab1[0];
    String d1 = mapdata1['data'];
    var jsd = jsonDecode(d1);

    String Accountname = jsd["Accountname"];
    for(String ah in subject)
    {
      if(ah.compareTo(Accountname)==0)
      {
        setState(() {
          subjectdata=        ah;
        });
        break;
      }
    }




    setState(() {

      accountid=name;
      amountcontroller.text=amount;
    });


    String emiamount=jsondata['emiamount'];

    String nofemi=jsondata['nofemi'];
    String payment_date=jsondata['payment_date'];

    String closing_date=jsondata['closing_date'];
    String remind_date=jsondata['remind_date'];


    if(nofemi.isNotEmpty)
      {

        setState(() {
          eminon="EMI";

          emiamountcontroller.text=emiamount;
          nofemiamountcontroller.text=nofemi;

     datetxt1=payment_date;
        datetxt2=closing_date;

        });


      }
    else{

      setState(() {

        eminon="NON EMI";
      });

       datetxt1=closing_date;
    datetxt2=remind_date;

      // assetdata['nofemi'] = "";
      //
      // assetdata['payment_date'] = "";

    }






    print(jsondata);

  }



void setDateofpaymentValue(String v)
{
  setState(() {
    datetxt1=v;
  });
if(nofemiamountcontroller.text.isNotEmpty) {
  calculateClosingDate(nofemiamountcontroller.text.toString(), selecteddate);
}
}

void calculateClosingDate(String numofemi,String currentdate)
{

  DateTime newDateTime=new DateTime.now();
  date= currentdate +
      "-" +
      newDateTime.month
          .toString() +
      "-" +
      newDateTime.year
          .toString();

  int month=int.parse(numofemi);
  int a=month*30;

  int m=0;

  if(month>=12)
    {
      m=month~/12;

    }

  int y=newDateTime.year;
  int year=y+m;

  DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(date);
  accountsdateparsed.add(Duration(days: a));


  setState(() {

    datetxt2=accountsdateparsed.day.toString()+"-"+accountsdateparsed.month.toString()+"-"+year.toString();



  });



}




  void setupAccountData() async {
    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Amount ="";
    accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      String Accounttype = jsondata["Accounttype"];
      Amount = jsondata["Amount"];
      String Type = jsondata["Type"];

      if(Accounttype.compareTo("Liability account")==0)
      {
        accountsetupdata.add(Accountname);
      }

    }


    setState(() {

      subject.clear();

      subjectdata=accountsetupdata[0];
      amountcontroller.text="0";

      subject.addAll(accountsetupdata);

    });
  }


  setDataByAccountName(String name) async
  {

    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Amount ="0";
    String accid="0";
    accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      if(Accountname.compareTo(name)==0) {
        String Accounttype = jsondata["Accounttype"];
        Amount = jsondata["Amount"];
        accid=id.toString();
      }
    }
    setState(() {



      accountid=accid;

      amountcontroller.text=Amount;



    });

  }








}



class NumberDialog extends StatefulWidget{




  NumberDialog();

  @override
  _NumberDialogDialogState createState() => new _NumberDialogDialogState();



}
class _NumberDialogDialogState extends State<NumberDialog> {


  int _currentValue = 1;

  _NumberDialogDialogState();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context, {'option': _currentValue.toString()});
      },
    );


    return AlertDialog(
      actions: [
        okButton,
      ],
      content: Container(
        height: 150, // Change as per your requirement
        width: MediaQuery
            .of(context)
            .size
            .width, // Change as per your requirement
        child: NumberPicker(
          value: _currentValue,
          minValue: 1,
          maxValue: 31,
          onChanged: (value) => setState(() => _currentValue = value),
        ),
      ),

    );
  }
}