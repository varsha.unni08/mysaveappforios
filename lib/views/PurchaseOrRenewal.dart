import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:saveappforios/views/Payment.dart';
import 'package:share_extend/share_extend.dart';
import 'package:page_indicator/page_indicator.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:ui' as ui;

import 'package:share/share.dart';

import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
// import 'package:paytm_allinonesdk/paytm_allinonesdk.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/NetworkDashboard.dart';
import '../domain/Profiledata.dart';
import '../domain/SalesInfo.dart';
import '../domain/SalesinfoData.dart';
import '../domain/ShareSlider.dart';
import '../domain/Tokendata.dart';
import '../projectconstants/DataConstants.dart';
import 'InvoicePage.dart';
import 'openpay/PaymentWebView.dart';

class PurchasePage extends StatefulWidget {
  final String title;

  const PurchasePage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PurchasePage();
}


class _PurchasePage extends State<PurchasePage> {

  String btntext="";

  String date1="",date2="";

  String date1Title="Date of activation ",date2title="Date of expiry ";

  bool isactive=false;

  String mobile="";
  String token="";

  String name="";
  String email="",phonenumber="";
  double amounttopay=0;
  String countryid="0",stateid="0";
  double d=0;
  double cgst=0,igst=0,sgst=0,othertaxamount=0,totalamount=0;


  @override
  void initState() {
    // TODO: implement initState

    getProfile();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("App Renewal",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ?14:16:18),),
        centerTitle: false,
      ),

      body:Container(
      width: double.infinity,
    height: double.infinity,
    child: Column(

      children: [

        Padding(
          padding:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(15):EdgeInsets.all(20):EdgeInsets.all(25),
          child: Row(
            children: [
              Expanded(
                child: Text(date1Title,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 14:16:18),),
                flex: 2,
              ),
              Expanded(
                child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 14:16:18),),
                flex: 1,
              ),
              Expanded(
                child: Text(
                  date1,
                  style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 14:16:18),
                ),
                flex: 2,
              )
            ],
          ),
        ),

        Padding(
          padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(15):EdgeInsets.all(20):EdgeInsets.all(25),
          child: Row(
            children: [
              Expanded(
                child: Text(date2title,style:  TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 14:16:18),),
                flex: 2,
              ),
              Expanded(
                child: Text(":",style:  TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 14:16:18)),
                flex: 1,
              ),
              Expanded(
                child: Text(
                  date2,
                  style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 14:16:18),
                ),
                flex: 2,
              )
            ],
          ),
        ),



        (!isactive)? Padding(
          padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(15):EdgeInsets.all(20):EdgeInsets.all(25),

          child: Align(
            alignment: Alignment.bottomCenter,

            child: Container(
              height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 50 :60:70,
              width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 150 :160:170,
              decoration: BoxDecoration(
                  color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
              child: TextButton(
                onPressed: () {
                  // Navigator.push(
                  //     context, MaterialPageRoute(builder: (_) => Registrationpage(title: "registration",)));


                  if(isactive)
                    {
                      if(btntext.compareTo("Renew")==0)
                        {

                          startPayment(true);
                        }
                      else {
                        getNetWorkData(mobile);
                      }

                    }
                  else{

                    if(btntext.compareTo("Renew")==0)
                    {

                      startPayment(true);
                    }
                    else {
                      startPayment(false);
                    }




                  }



                },
                child: Text(
                  btntext,
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              ),
            ),),) :Container(),










      ],




      )

      )



    );
  }

  getNetWorkData(String mobilenumber) async {
    print("mobile : " + mobilenumber);
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.showMemberDetails +
          "?timestamp=" +
          date.toString() +
          "&mobile=" +
          mobilenumber),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;

    var json = jsonDecode(response);

    NetworkDashboard ndashboard = NetworkDashboard.fromJson(json);

    if (ndashboard.status == 1) {

        shareLink();
      }

  }

  shareLink() async {


    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,
        textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
     token=datacount.getString(DataConstants.userkey)!;
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getSettingsSlider +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    ShareSlider shareSlider = ShareSlider.fromJson(jsonDecode(response));

    if (shareSlider.status == 1) {
      showShareDialog(shareSlider);
    }

    print(response);
  }

  showShareDialog(ShareSlider shareSlider) {
    List<Widget> sliderwidget = [];

    PageController pageController = new PageController();

    for (var i = 0; i < shareSlider.data.length; i++) {
      sliderwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.89349112,
        width: double.infinity,
        child: Card(
            child: Image.network(
                DataConstants.sliderimageurl + shareSlider.data[i].image)),
      ));
    }

    Dialog sharedialog = Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        //this right here
        child: Container(
            height: double.infinity,
            width: double.infinity,
            child:SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                      width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? 200 :220:250,
                      height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? (200 / 0.609375) :(220 / 0.609375)   :(250 / 0.609375),
                      child: PageIndicatorContainer(
                          length: shareSlider.data.length,
                          align: IndicatorAlign.bottom,
                          indicatorSpace: 14.0,
                          padding: const EdgeInsets.all(10),
                          indicatorColor: Colors.black12,
                          indicatorSelectorColor: Colors.blueGrey,
                          shape: IndicatorShape.circle(size: 10),
                          child: PageView.builder(
                            scrollDirection: Axis.horizontal,
                            controller: pageController,
                            itemBuilder: (BuildContext context, int index) {
                              return sliderwidget[index];
                            },
                            itemCount: shareSlider.data.length,
                            // children: nsdwidget,
                          ))),
                  Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[

                      Expanded(child: Padding(
                          padding:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(5) : EdgeInsets.all(10):EdgeInsets.all(15),
                          child: Container(
                              color: Colors.blueGrey,
                              child: TextButton(
                                onPressed: () {
                                  Clipboard.setData(ClipboardData(
                                      text: DataConstants.baseurl +
                                          "index.php/web/signup?sponserid=" +
                                          token));

                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text("Copied to clipboard"),
                                  ));
                                },
                                child: Text(
                                  "Copy link",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ))),flex: 1,)
                      ,
                      Expanded(child:  Container(
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: new Theme(
                                  data: new ThemeData(hintColor: Colors.white),
                                  child: Text(
                                    DataConstants.baseurl +
                                        "index.php/web/signup?sponserid=" +
                                        token,
                                    maxLines: 2,
                                    style: TextStyle(color: Colors.blue),
                                  )))),flex: 2,)



                    ],
                  ),
                  Container(
                      color: Colors.blueGrey,
                      child: TextButton(
                        onPressed: () async {
                          // var status =
                          // await Permission.manageExternalStorage.status;
                          // if (status.isDenied) {
                          //   // We didn't ask for permission yet or the permission has been denied before but not permanently.
                          //
                          //   print("permission not granted");
                          //   await Permission.manageExternalStorage.request();
                          // } else {
                          //   print("permission granted");

                            var date = new DateTime.now().toIso8601String();

                            String url = DataConstants.sliderimageurl +
                                shareSlider.data[pageController.page!.toInt()]
                                    .image; // <-- 1
                            var response = await http.get(Uri.parse(url)); // <--2
                            var documentDirectory =
                            await getApplicationDocumentsDirectory();
                            var firstPath = documentDirectory.path + "/images";
                            var filePathAndName = documentDirectory.path +
                                '/images/' +
                                date.toString() +
                                ".jpg";

                            await Directory(firstPath)
                                .create(recursive: true); // <-- 1
                            File file2 = new File(filePathAndName); // <-- 2
                            file2.writeAsBytesSync(response.bodyBytes);

                            bool fileExists = await file2.exists();

                            if (fileExists) {
                              print("file exists : " + file2.path);

                              //  Share.share('check out my website https://example.com');


                              // ShareExtend.share(file2.path, "file",subject: shareSlider
                              //     .data[pageController.page!.toInt()]
                              //     .description,extraText:shareSlider
                              //     .data[pageController.page!.toInt()]
                              //     .description );

                              Share.shareFiles([file2.path],
                                  text: shareSlider
                                      .data[pageController.page!.toInt()]
                                      .description);
                            }
                         // }
                        },
                        child: Text(
                          "Share",
                          style: TextStyle(color: Colors.white),
                        ),
                      ))
                ],
              ),)
        ));

    showDialog(
        context: context, builder: (BuildContext context) => sharedialog);
  }

  getProfile() async {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,
        textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getUserDetails +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);
    var json = jsonDecode(response);

    Profiledata profile=Profiledata.fromJson(json);

    print(profile.data.full_name);


     mobile=profile.data.mobile;


    countryid=profile.data.country_id;
    stateid=profile.data.state_id;
    name=profile.data.full_name;
    email=profile.data.email_id;

    getSmemberData(mobile);

    // TemRegData.name=profile.data.full_name;
    // TemRegData.email=profile.data.email_id;
    //
    //
    //
    //
    // namecontroller.text=profile.data.full_name;
    // emailcontroller.text=profile.data.email_id;
    //
    // setState(() {
    //   TemRegData.mobilenumber=profile.data.mobile;
    //   profileimage=profile.data.profile_image;
    // });
    //
    // getCountryData(profile.data.country_id,profile.data.state_id);

  }

  startPayment(bool isrenew) async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl + DataConstants.getSettingsValue+"?timestamp="+date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!,

      },
    );
    String response = dataasync.body;

    print(response);


    var json = jsonDecode(response);

    if (json['status'] == 1) {

      var js=json['data'];

      String networkimage=js['network_image'];
      String basicamount=js['one_bv_required_amount'];
      String renewal_charge=js['renewal_charge'];
      if (isrenew) {
        d = double.parse(renewal_charge);
      }
      else {
        d = double.parse(basicamount);
      }


      // cgst=double.parse(js['cgst']);
      //   igst=double.parse(js['igst']);
      //   sgst=double.parse(js['sgst']);
      //   othertaxamount=double.parse(js['other_tax']);


      if (countryid.compareTo("1")==0) {
        //layout_actualprice.setVisibility(View.GONE);

        if (stateid.compareTo("12")==0) {


          double cg=double.parse(js['cgst']);
          double  ig=double.parse(js['igst']);
          double  sg=double.parse(js['sgst']);
          double othertax=double.parse(js['other_tax']);

          cgst=d*(cg/100);
          igst=d*(ig/100);
          sgst=d*(sg/100);

          totalamount=d+cgst+sgst;



        }
        else{

          double cg=double.parse(js['cgst']);
          double  ig=double.parse(js['igst']);
          double  sg=double.parse(js['sgst']);
          double othertax=double.parse(js['other_tax']);

          cgst=d*(cg/100);
          igst=d*(ig/100);
          sgst=d*(sg/100);

          totalamount=d+igst;



        }
      }
      else{

        double cg=double.parse(js['cgst']);
        double  ig=double.parse(js['igst']);
        double  sg=double.parse(js['sgst']);
        double othertax=double.parse(js['other_tax']);

        cgst=d*(cg/100);
        igst=d*(ig/100);
        sgst=d*(sg/100);
        double ot=d*(othertax/100);

        totalamount=d+ot;

      }






      Dialog sharedialog = Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          //this right here
          child: Container(
              height: (MediaQuery.of(context).size.width),
              width:(MediaQuery.of(context).size.width)/1.9,
              child:SingleChildScrollView(
                child: Column(
                  children: [

                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Center(child:Text("Payment Confirmation"))
                    ),

                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Actual price  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              d.toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")==0)?     Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Sgst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              sgst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")==0)? Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Cgst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              cgst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    (countryid.compareTo("1")==0&&stateid.compareTo("12")!=0)  ? Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Igst  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              igst.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ):Container(),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Total price  "),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              totalamount.roundToDouble().toString(),
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),

                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Center(child:TextButton(onPressed: () {


                          amounttopay=totalamount;


                          Navigator.pop(context);

                          DateTime t=DateTime.now();
                          String orderstring=t.millisecond.toString();

                          getDataFromPayment(orderstring);







                        }, child: Text("Confirm"),))
                    ),


                  ],
                ),)
          ));

      showDialog(
          context: context, builder: (BuildContext context) => sharedialog);
















    } else {

    }
  }



  getDataFromPayment(String orderstring) async
  {

   int a= amounttopay.round();

  // String initialurl="https://mysaving.in/IntegraAccount/openpay/index.php?name="+name+"&email="+email+"&phone="+mobile+"&amount=1";
   String url="https://mysaveapp.com/purchase";
   // String initialurl="https://mysaving.in/IntegraAccount/openpay/index.php?name="+name+"&email="+email+"&phone="+mobile+"&amount="+a.toString();

   Map results = await Navigator.of(context)
       .push(new MaterialPageRoute<dynamic>(
     builder: (BuildContext context) {
       return new PaymentExplorer(url);
     },
   ));

   if (results != null &&
       results.containsKey('paymentdata')) {
     setState(() {
       var accountsetupdata =
       results['paymentdata'];

       String acs =
       accountsetupdata.toString() ;
       //
       if(acs.isNotEmpty)
       {

         getProfile();
         //print("Transaction id : "+acs);
         //setupAccountData();
       }


     });
   }



    // Navigator.push(
    //     context, MaterialPageRoute(builder: (_) => PaymentExplorer(initialurl)));



    // showGeneralDialog(
    //   barrierLabel: "Label",
    //   barrierDismissible: true,
    //   barrierColor: Colors.black.withOpacity(0.5),
    //   transitionDuration: Duration(milliseconds: 700),
    //   context: context,
    //   pageBuilder: (context, anim1, anim2) {
    //     return Align(
    //       alignment: true ? Alignment.topCenter : Alignment.bottomCenter,
    //       child: Container(
    //         height: (MediaQuery.of(context).size.height)/1.2,
    //         width: (MediaQuery.of(context).size.width),
    //         child: SingleChildScrollView(
    //
    //
    //             child: Column(
    //
    //               children: [
    //
    //
    //                 Container(child: Padding(
    //                   padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
    //
    //                   //padding: EdgeInsets.symmetric(horizontal: 15),
    //                   child: Text("For activation of SAVE App transfer the bill amount of Rs. "+amounttopay.toString()+"/- to the bank given below and WhatsApp transaction details and ID/registered mobile number to 9946109501\n\n",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),),
    //                 )),
    //
    //                 Divider(color: Colors.black38,
    //                   thickness: 1,),
    //
    //                 Container(child: Padding(
    //                   padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
    //
    //                   //padding: EdgeInsets.symmetric(horizontal: 15),
    //                   child: Text("Century Gate Software Solutions Pvt Ltd\n Canara Bank \nAccount number : 45501400000018 \nMICR: 680015901 \nIFSC: CNRB0014550 \n",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),),
    //
    //                 )),
    //
    //                 Container(child: Padding(
    //                   padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
    //
    //                   //padding: EdgeInsets.symmetric(horizontal: 15),
    //                   child:TextButton(onPressed: () {
    //
    //                     Navigator.pop(context);
    //
    //                   }, child: Text("Ok",style: TextStyle( fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.blue),),
    //
    //
    //
    //                   ) ,
    //                 )),
    //
    //
    //
    //               ],
    //
    //
    //             )),
    //         margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
    //         decoration: BoxDecoration(
    //           color: Colors.white,
    //           borderRadius: BorderRadius.circular(10),
    //         ),
    //       ),
    //     );
    //   },
    //   transitionBuilder: (context, anim1, anim2, child) {
    //     return SlideTransition(
    //       position: Tween(begin: Offset(0, true ? -1 : 1), end: Offset(0, 0)).animate(anim1),
    //       child: child,
    //     );
    //   },
    // );

//     ProgressDialog _progressDialog = ProgressDialog();
//     _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
//
//     var date = new DateTime.now().millisecond;
//
//     String orderid=date.toString();
//
//     var dataasync = await http.post(
//         Uri.parse("https://mysaving.in/IntegraAccount/"+"paytmapi/initTransaction.php?timestamp="+orderid),
//
//         headers: <String, String>{
//           'Content-Type': 'application/x-www-form-urlencoded',
//
//         }, body: <String, String>{
//       'Content-Type': 'application/x-www-form-urlencoded',
//       'requestType':'Payment',
//       'mid':DataConstants.TestMerchantID,
//       'websiteName':DataConstants.Website,
//       'orderId':orderid,
//       'callbackUrl':DataConstants.callbackUrl,
//       'value':amounttopay.roundToDouble().toString(),
//       'custId':'cust_001',
//       'inittrans':DataConstants.initiatetransactionurl,
//       'merchantkey':DataConstants.TestMerchantKey,
//       'timestamp':orderid
//     }
//
//     );
//
//     _progressDialog.dismissProgressDialog(context);
//
//
//     String response=dataasync.body;
//     var js=jsonDecode(response);
//     Tokendata td=Tokendata.fromJson(js);
//
//     if(td.body!=null)
//     {
//
//       String host = "https://securegw.paytm.in/";
//
// //        String orderDetails = "MID: " + Utils.PaytmCredentials.TestMerchantID + ", OrderId: " + "ORDERID_98765" + ", TxnToken: " + txnTokenString
// //                + ", Amount: " + "1.00";
//
//       //Log.e(TAG, "order details "+ orderDetails);
//
//       String callBackUrl = host + "theia/paytmCallback?ORDER_ID="+orderid;
//
//       print("Transaction id : "+td.body.txnToken);
//
//       // var response = AllInOneSdk.startTransaction(
//       //     DataConstants.TestMerchantID, orderid, amounttopay.roundToDouble().toString(), td.body.txnToken, callBackUrl, true, true);
//       // response.then((value) {
//       //   print(value);
//       //
//       //   String  result = value.toString();
//       //
//       //   dynamic m=value;
//       //
//       //   Map<dynamic, dynamic> ab1 =m ;
//       //
//       //   print("Result Data is : "+result);
//       //
//       //   // {CURRENCY: INR, GATEWAYNAME: SBI, RESPMSG: Txn Success, BANKNAME: State Bank of India, PAYMENTMODE: NB, MID: eAoUUb21380278750445, RESPCODE: 01, TXNAMOUNT: 2950.00, TXNID: 20220113111212800110168076403338727, ORDERID: 784, STATUS: TXN_SUCCESS, BANKTXNID: 13090179464, TXNDATE: 2022-01-13 15:21:18.0, CHECKSUMHASH: 69NGywshzGCJ02+Pmx1em2UGab4eEyQQGveu9oeBOOXqVk6LR4lq+DyjU9ctwZBC+uFictNSEcexbtCybxtlgBpQB4Ku8QQ2Pn9cqQM4ZJM=}
//       //
//       //   // var js=jsonDecode(result);
//       //
//       //   //String transactionid=js['TXNID'];
//       //
//       //
//       //   purchaseApp(ab1['TXNID']);
//       //
//       //
//       //
//       //
//       //
//       //
//       //
//       //
//       // }).catchError((onError) {
//       //   if (onError is PlatformException) {
//       //     setState(() {
//       //       String  result = onError.message.toString() + " \n  " + onError.details.toString();
//       //
//       //       print("Result Data is Error : "+result);
//       //     });
//       //   } else {
//       //     setState(() {
//       //       String  result = onError.toString();
//       //
//       //       print("Result Data is Error : "+result);
//       //     });
//       //   }
//       // });
//
//
//     }
//
//
//
//     _progressDialog.dismissProgressDialog(context);



  }


  purchaseApp(String transactionid) async
  {

    print("transaction id data : "+transactionid);

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

    var date = new DateTime.now().millisecond;
    final datacount = await SharedPreferences.getInstance();
    String orderid=date.toString();

    var dataasync = await http.post(
        Uri.parse(DataConstants.baseurl+DataConstants.addSalesInfo),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!

        }, body: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      'cash_transaction_id':transactionid,
      'timestamp':orderid,
    }

    );

    _progressDialog.dismissProgressDialog(context);


    String response=dataasync.body;

    print("Add sales info : "+response);
    dynamic json = jsonDecode(response);


    Salesinfo salesinfo=Salesinfo.fromJson(json);

    if(salesinfo.status==1)
    {

      // var jsonObject_data=json["data"];
      // var json1 = jsonDecode(jsonObject_data.toString());


      // print("Bill no : "+billno+" , "+"billprefix : "+billprefix);

      SalesinfoData data=salesinfo.data;
      String billno=data.bill_no;
      String billprefix=data.billno_prefix;

      String bill=billprefix+""+billno;

      DateTime dt=DateTime.now();

      String date=dt.day.toString()+"-"+dt.month.toString()+"-"+dt.year.toString();


      // const InvoicePage(
      //     {Key? key, required this.title, required this.date,required this.billno,required  this.buyer,required this.transactions, required this.amount,required this.sgst,required this.cgst, required this.igst,required this.amounttopay})
      // : super(key: key);

      Navigator.push(
          context, MaterialPageRoute(builder: (_) => InvoicePage(title: "Payment",date:date,billno:bill,buyer:name+"\n"+email,transactions: transactionid,amount: d.toString(),sgst: sgst.toString(),cgst: cgst
          .toString(),igst: igst.toString(),amounttopay: amounttopay.toString(),countryid: countryid,stateid: stateid,)));




      getSmemberData(mobile);


    }
    else{

      print("Error");

    }




  }



  void getSmemberData(String mobilenumber) async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,
        textToBeDisplayed: "Please wait for a moment......");

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.showMemberDetails +
          "?mobile="+mobilenumber+"&timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;



    var json = jsonDecode(response);

    if(json['status']==1)
      {

        var js=json['data'];
        print(js['member_status']);
        
        if(js['member_status'].toString().compareTo("active")!=0)
          {

            checkTrialPeriod();

            isactive=false;

            setState(() {

              btntext="Purchase";
            });


          }
        else{

          checkSalesInfo();

          isactive=true;

          setState(() {

            btntext="Share link";
          });
        }



      }
    else{

      checkTrialPeriod();

      isactive=false;

      setState(() {

        btntext="Purchase";
      });
    }

  }




  void checkTrialPeriod() async{

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,
        textToBeDisplayed: "Please wait for a moment......");

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.validateTrialPeriod +
          "?"+"timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    var json = jsonDecode(response);

    if(json['status']==1) {
      var jsdata = json['data'];

     // String join_date=jsdata['join_date'].toString();

     // DateTime salesdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(join_date);

      String trial_date=jsdata['trialstart_date'].toString();

      DateTime trialdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(trial_date);

      final datenow = DateTime.now();
      int difference = datenow.difference(trialdateparsed).inDays;
      int dif=0;

      if(difference<0)
        {
          dif=difference*-1;
        }
      else{
        dif=difference;

      }

      setState(() {
        date1Title="Date of Registration";
        date2title="Trial days";

        date1=trialdateparsed.day.toString()+"-"+trialdateparsed.month.toString()+"-"+trialdateparsed.year.toString();
        date2=dif.toString()+" days left";



      });



    }

  }





  void checkSalesInfo() async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,
        textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl +
          DataConstants.checkSalesInfo +
          "?"+"timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    var json = jsonDecode(response);

    if(json['status']==1)
    {

      var jsdata=json['data'];

      String sales_date=jsdata['sales_date'].toString();
      String expe_date =jsdata['expe_date'].toString();

      DateTime salesdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(sales_date);



      DateTime expdateparsed = new DateFormat("yyyy-MM-dd").parse(expe_date);

      if(expdateparsed.compareTo(new DateTime.now())<0)


        {


          setState(() {
            isactive=false;
          btntext="Renew";

          });
        }
      else{
        isactive=true;
      }



setState(() {

  date1=salesdateparsed.day.toString()+"-"+salesdateparsed.month.toString()+"-"+salesdateparsed.year.toString();

  date2=expdateparsed.day.toString()+"-"+expdateparsed.month.toString()+"-"+expdateparsed.year.toString();

});




    }

    else{


    }
  }

}