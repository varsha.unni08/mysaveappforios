import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:saveappforios/domain/AddedAmountMileStone.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/MileStoneData.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class ViewMileStoneAddedAmountDetails extends StatefulWidget {


  MileStoneData mileStoneData;


   ViewMileStoneAddedAmountDetails(this.mileStoneData) ;

  @override
  _ViewMileStoneAddedAmountDetailsState createState() =>
      _ViewMileStoneAddedAmountDetailsState(this.mileStoneData);
}

class _ViewMileStoneAddedAmountDetailsState
    extends State<ViewMileStoneAddedAmountDetails> {

  MileStoneData mileStoneData;

  _ViewMileStoneAddedAmountDetailsState(this.mileStoneData);
  List<AddedAmountMileStone>addedmilestonesToAdd=[];

  double percentage=0;
  String percentageString="";


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getSavedAmounts();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(


      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () =>    Navigator.pop(context),
        ),
        title: Text("View Milestone Details",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),

      body: SingleChildScrollView(


        child: Column(

          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,

          children: [


            Padding(padding: EdgeInsets.all(7),


              child: Text("Start Date : "+mileStoneData.start_date,style: TextStyle(fontSize: 13,color: Colors.black),),



            ),

            Padding(padding: EdgeInsets.all(7),


              child: Text("End Date : "+mileStoneData.end_date,style: TextStyle(fontSize: 13,color: Colors.black),),



            ),

            Padding(padding: EdgeInsets.all(7),


              child: Text("Target Amount : "+mileStoneData.amount,style: TextStyle(fontSize: 13,color: Colors.black),),



            ),


            Padding(padding: EdgeInsets.only(left: 37),


              child:  CircularPercentIndicator(

                    radius: (MediaQuery.of(context).size.width)/3,
                    lineWidth: 8.0,

                    percent: percentage,
                    center: new Text(percentageString,style: TextStyle(fontSize: 13,color: Colors.black)),
                    progressColor: Color(0xFF096c6c),
                  )


              ,



            ),

            Padding(padding: EdgeInsets.all(7),


              child: Text("Saved Amount",style: TextStyle(fontSize: 13,color: Colors.black),),



            ),

            Padding(padding: EdgeInsets.all(7),


              child: ListView.builder(
                shrinkWrap: true,
                  primary: false,
                  itemCount: addedmilestonesToAdd.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(

                      child: ListTile(

                          title:  Text(
                          "Date : "+  addedmilestonesToAdd[index].date,
                            style: TextStyle(color: Colors.black, fontSize: 15),
                          ),
                          subtitle: Text(
                          "Amount: "+  addedmilestonesToAdd[index].amount1,
                            style: TextStyle(color: Colors.black, fontSize: 12),
                          )),
                    )


                      ;
                  })




            ),




          ],



        ),



      ),







    );
  }





  getSavedAmounts()
  async {

    double amount=0;

    List<Map<String,dynamic>>mpd= await new DatabaseHelper().queryAllRows(DatabaseTables.TABLE_ADDEDAMOUNT_MILESTONE);
List<AddedAmountMileStone>addedmilestones=[];
    if(mpd.length>0)
    {

      for(int i=0;i<mpd.length;i++) {
        Map<String, dynamic> ab = mpd[i];

        int id = ab["keyid"];
        String data = ab["data"];
        var jsondata = jsonDecode(data);
        String target_id=jsondata['targetid'];

        String amount1=jsondata['savedamount'];

        String datemilestoneadded=jsondata['date'];



        DateTime amountaddeddate = new DateFormat("dd-MM-yyyy").parse(datemilestoneadded);

        DateTime milestonestart_ddate = new DateFormat("dd-MM-yyyy").parse(mileStoneData.start_date);

        DateTime milestoneend_ddate = new DateFormat("dd-MM-yyyy").parse(mileStoneData.end_date);



        if(target_id.compareTo(mileStoneData.targetid)==0)
        {



          if (amountaddeddate.compareTo(milestonestart_ddate)==0) {

            AddedAmountMileStone addedAmountMileStone=new AddedAmountMileStone();
            addedAmountMileStone.id=id.toString();
            addedAmountMileStone.date=datemilestoneadded;
            addedAmountMileStone.target_id=mileStoneData.targetid;
            addedAmountMileStone.amount1=amount1;
            addedmilestones.add(addedAmountMileStone);

            double d=double.parse(amount1);
            amount=amount+d;
          }
          else if(amountaddeddate.isAfter(milestonestart_ddate))
            {
              AddedAmountMileStone addedAmountMileStone=new AddedAmountMileStone();
              addedAmountMileStone.id=id.toString();
              addedAmountMileStone.date=datemilestoneadded;
              addedAmountMileStone.target_id=mileStoneData.targetid;
              addedAmountMileStone.amount1=amount1;
              addedmilestones.add(addedAmountMileStone);

              double d=double.parse(amount1);
              amount=amount+d;
            }

         else if (amountaddeddate.compareTo(milestoneend_ddate)==0) {
            AddedAmountMileStone addedAmountMileStone=new AddedAmountMileStone();
            addedAmountMileStone.id=id.toString();
            addedAmountMileStone.date=datemilestoneadded;
            addedAmountMileStone.target_id=mileStoneData.targetid;
            addedAmountMileStone.amount1=amount1;
            addedmilestones.add(addedAmountMileStone);
            double d=double.parse(amount1);
            amount=amount+d;
          }

         else if(amountaddeddate.isBefore(milestoneend_ddate))
           {

             AddedAmountMileStone addedAmountMileStone=new AddedAmountMileStone();
             addedAmountMileStone.id=id.toString();
             addedAmountMileStone.date=datemilestoneadded;
             addedAmountMileStone.target_id=mileStoneData.targetid;
             addedAmountMileStone.amount1=amount1;
             addedmilestones.add(addedAmountMileStone);
             double d=double.parse(amount1);
             amount=amount+d;

           }




          setState(() {






            addedmilestonesToAdd.addAll(addedmilestones);
          });


        }





      }

      setState(() {

        double milestoneamount= double.parse(mileStoneData.amount)  ;
        double amount=0;

   for(int i=0;i<addedmilestonesToAdd.length;i++)
     {
       double am= double.parse(addedmilestonesToAdd[i].amount1)  ;

       amount=amount+am;
     }


   double p=(amount/milestoneamount);
   if(p>=1)
     {
       percentage=1;
     }
   else{

     percentage=p;
   }

   double ap=percentage*100;

   percentageString=amount.toString()+"/"+milestoneamount.toString()+"\n\n"+ap.toString()+" %";




      });

    }

  }



}
