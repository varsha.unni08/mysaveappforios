

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/domain/Taskdata.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/mainviews/AddTaskPage.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';
import 'package:saveappforios/database/DBTables.dart';

import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Taskdata.dart';
import 'AddTaskPage.dart';







class TaskListPage extends StatefulWidget {
  final String title;






  const TaskListPage(
      {Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _TaskListPage();
}

class _TaskListPage extends State<TaskListPage> {

  String diaryid = "0";

 DatabaseHelper dbhelper=new DatabaseHelper();


  String date = "",
      month = "",
      year = "";


  List<String>subject = ["Select subject"];

  String subjectdata = "Select subject";


  String datetxt1 = "Select start date";

  String datetxt2 = "Select end date";

  String languagedropdown = 'Select your language';

  TextEditingController commentcontroller = new TextEditingController();
  TextEditingController feedbackcontroller = new TextEditingController();

  List<Task> fulltaskdata = [];

 // DatabaseHelper dbhelper = new DatabaseHelper();

  String stdate="",endate="";

  @override
  void initState() {
    // TODO: implement initState

setupDateData();
SystemChrome.setPreferredOrientations([
  DeviceOrientation.portraitDown,
  DeviceOrientation.portraitUp,
]);

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Tasks",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),

      body: Stack(

        children: [

          Container(child:     Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12) :EdgeInsets.all(14),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  children: <Widget>[

                    Expanded(child:  Padding(
                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12) :EdgeInsets.all(14),
                        child: InkWell(

                          child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,),

                          onTap: (){
                            var now=DateTime.now();

                            stdate= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              stdate= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4) :EdgeInsets.all(6),
                                          child: Container(
                                            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:70,
                                            width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170 ,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  datetxt1 = stdate;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });

                          },
                        )


                        ),flex: 1, )
                  ,

                    Expanded(child:Container(
                        child: TextButton(
                          onPressed: () async {


                            var now=DateTime.now();

                            stdate= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              stdate= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) :EdgeInsets.all(4):EdgeInsets.all(6),
                                          child: Container(
                                            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                            width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  150:160:170,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  datetxt1 = stdate;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  14:16:19),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });





                          },
                          child: Text(
                              datetxt1,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12)),
                          ),
                        ),flex: 3, )

                  ],
                ),
              ))),

          Container(child:     Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 80, 10, 10) : EdgeInsets.fromLTRB(15, 100, 15, 15) :EdgeInsets.fromLTRB(20, 150, 20, 20),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  children: <Widget>[
                    Expanded(child: Padding(
                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15) : EdgeInsets.all(20),
                        child: InkWell(

                          child: Icon(Icons.calendar_month,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,color: Colors.black26,),

                          onTap: (){

                            var now=DateTime.now();

                            endate= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              endate= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4) :EdgeInsets.all(6),
                                          child: Container(
                                            height:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  50:60:70,
                                            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  datetxt2 = endate;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });

                          },
                        )


                        ),flex: 1,)

                    ,
                    Expanded(child:  Container(
                        child: TextButton(
                          onPressed: () async {


                            var now=DateTime.now();

                            endate= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              endate= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4) :EdgeInsets.all(6),
                                          child: Container(
                                            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  50:60:70,
                                            width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  datetxt2 = endate;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });





                          },
                       child: Text(
                              datetxt2,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:18)),

                        )),flex: 3,)


                  ],
                ),
              ))),

          Padding(
            padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 140, 15, 15) : EdgeInsets.fromLTRB(20, 200, 20, 20) :EdgeInsets.fromLTRB(25, 260, 25, 25),

            child: Container(

              width: double.infinity,
              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 55:65:75,
              decoration: BoxDecoration(

                  color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
              child:Align(
                alignment: Alignment.center,
                child: TextButton(

                  onPressed:() {


                   // showTasks();

                    DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(datetxt1);

                    DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(datetxt2);

                    if(accountsdateselected.isAfter(accountsdateparsed)||accountsdateselected.compareTo(accountsdateparsed)==0)
                    {

                      showTasks();

                    }

                  },

                  child: Text('Submit', style: TextStyle(color: Colors.white,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19) ,) ,),
              ),



              //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
            ),


            // ,
          ),


          Align(
              alignment: FractionalOffset.topRight,
              child: Padding(
                padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(5, 200, 5, 5) : EdgeInsets.fromLTRB(10, 260, 10, 10) :EdgeInsets.fromLTRB(15, 310, 15, 15),
                child: ListView.builder(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11) :EdgeInsets.all(14),
                    itemCount: fulltaskdata.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        // color: Colors.amber[colorCodes[index]],
                        child: Center(
                            child:InkWell(

                              onTap: ()async{

                                print("tapped");


                                Map results = await Navigator.of(context)
                                    .push(new MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) {
                                    return new AddTaskListPage(
                                      title: "Accountsettings", taskid: fulltaskdata[index].id,
                                    );
                                  },
                                ));

                                if (results != null &&
                                    results.containsKey('accountsetupdata')) {
                                  setState(() {
                                    var acc_selected =
                                    results['accountsetupdata'];

                                    int acs =
                                    acc_selected as int;

                                    if(acs!=0) {
                                      // setupAccountData();
                                      showTasks();
                                    }




                                  });
                                }


                              },

                              child:Card(
                                elevation: 5,
                                child: Container(
                                  color: Colors.white,
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(6) : EdgeInsets.all(10):EdgeInsets.all(14),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Name",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19)),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].task,
                                                style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(6) : EdgeInsets.all(10):EdgeInsets.all(14),

                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Date",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19)),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19)),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].date,
                                                style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(6) : EdgeInsets.all(10):EdgeInsets.all(14),

                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Time",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19)),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19)),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index]
                                                    .time,
                                                style:  TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(6) : EdgeInsets.all(10):EdgeInsets.all(14),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Remind date",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].reminddate,
                                                style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(6) : EdgeInsets.all(10):EdgeInsets.all(14),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text("Status",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19)),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19)),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Text(
                                                fulltaskdata[index].status,
                                                style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),
                                              ),
                                              flex: 2,
                                            )
                                          ],
                                        ),
                                      ),

                                    ],
                                  ),
                                ),
                              ) ,
                            )




                            ),
                      );
                    }),
              )),





          Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                  padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(12) : EdgeInsets.all(18):EdgeInsets.all(24),
                  child: FloatingActionButton(
                    onPressed: () async {



                      Map results = await Navigator.of(context)
                          .push(new MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) {
                          return new AddTaskListPage(
                            title: "Accountsettings", taskid: '0',
                           );
                        },
                      ));

                      if (results != null &&
                          results.containsKey('accountsetupdata')) {
                        setState(() {
                          var acc_selected =
                          results['accountsetupdata'];

                          int acs =
                          acc_selected as int;

                          if(acs!=0) {
                           // setupAccountData();

                            showTasks();

                          }




                        });
                      }

                    },
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 29:35:45,
                    ),
                    backgroundColor: Colors.blue,
                    elevation: 5,
                    splashColor: Colors.grey,
                  ))),




        ],
      ),




    );

  }



  setupDateData()
  {
    var now =DateTime.now();

    setState(() {

      datetxt1=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();
      datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    });


    showTasks();



  }


  showTasks()async{

    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_TASK);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<Task> taskdata = [];


    DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(datetxt1);

    DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(datetxt2);
    for (Map ab in a) {
      print(ab);
      String id = ab["keyid"].toString();

      String data = ab["data"];

      var jsondata = jsonDecode(data);


      String taskdate=jsondata['date'];

      String name=jsondata['name'];
      String time=jsondata['time'];
      String status=jsondata['status'];
      String reminddate=jsondata['reminddate'];



      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(taskdate);
      if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
          accountsdateparsed.isBefore(accountsdatend)) {

        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);
      }
      else if (accountsdateparsed.isAfter(accountsdatestart) &&
          accountsdateparsed.compareTo(accountsdatend) == 0) {
        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);

      }
      else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
          accountsdateparsed.compareTo(accountsdatend) == 0) {

        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);
      }

      else if (accountsdateparsed.isAfter(accountsdatestart) &&
          accountsdateparsed.isBefore(accountsdatend)) {

        Task t=new Task();
        t.id=id;
        t.reminddate=reminddate;
        t.status=status;
        t.time=time;
        t.date=taskdate;
        t.task=name;

        taskdata.add(t);
      }

    }

    setState(() {
      fulltaskdata.clear();

      fulltaskdata.addAll(taskdata);
    });


  }

}