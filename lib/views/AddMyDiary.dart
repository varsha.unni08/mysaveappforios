

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../projectconstants/DataConstants.dart';







class AddMyDiarypage extends StatefulWidget {
  final String title;

  final String diaryid;




  const AddMyDiarypage(
      {Key? key, required this.title, required this.diaryid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddMyDiarypage(diaryid);
}

class _AddMyDiarypage extends State<AddMyDiarypage> {

  String diaryid="0";


  _AddMyDiarypage(this.diaryid);

  String date="",month="",year="";


List<String>subject=["Select subject"];

String subjectdata="Select subject";


  String datetxt="Select date";

  String languagedropdown='Select your language';

  TextEditingController commentcontroller=new TextEditingController();
  TextEditingController feedbackcontroller=new TextEditingController();

  DatabaseHelper dbhelper=new DatabaseHelper();

  @override
  void initState() {
    // TODO: implement initState

    showDiarySubjectData();
    showCurrentDate();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    super.initState();

  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Write my diary",style: TextStyle(fontSize:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
        centerTitle: false,
      ),

      body: Stack(
          children: <Widget>[SingleChildScrollView(


       child: Column(

        children: [
          // Padding(
          //     padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11):EdgeInsets.all(14),
          //     child:Container(
          //       width: double.infinity,
          //       height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:70,
          //       decoration: BoxDecoration(
          //         border: Border.all(
          //           color: Colors.white,
          //           // red as border color
          //         ),
          //       ),
          //       child: DropdownButtonHideUnderline(
          //         child: ButtonTheme(
          //           alignedDropdown: true,
          //           child: InputDecorator(
          //             decoration: const InputDecoration(border: OutlineInputBorder()),
          //             child: DropdownButtonHideUnderline(
          //               child: DropdownButton(
          //
          //                 value: languagedropdown,
          //                 items: DataConstants.arrofLanguages
          //                     .map<DropdownMenuItem<String>>((String value) {
          //                   return DropdownMenuItem<String>(
          //                     value: value,
          //                     child: Text(value),
          //                   );
          //                 }).toList(),
          //                 onChanged: (String? newValue) {
          //                   setState(() {
          //                     languagedropdown = newValue!;
          //
          //                     //TemRegData.language=languagedropdown;
          //                   });
          //                 },
          //                 style: Theme.of(context).textTheme.bodyText1,
          //
          //               ),
          //             ),
          //           ),
          //         ),
          //       ),
          //     )),

          Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11): EdgeInsets.all(14),
          child:   SizedBox(

              width: double.infinity,
              child: Container(child:     Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black38)),
                    child: Row(
                      textDirection: ui.TextDirection.rtl,
                      children: <Widget>[

                        Expanded(child:  Padding(
                            padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10) : EdgeInsets.all(13) : EdgeInsets.all(16),
                            child: Icon(Icons.calendar_month,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35, color: Colors.black26,)


                        ),flex: 1,)
                       ,
                        Expanded(flex: 3,
                            child: TextButton(
                              onPressed: () async {
                                var now = DateTime.now();

                                date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                                month=now.month.toString();
                                year=now.year.toString();

                                showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return Container(
                                          child: Column(children: [
                                            Expanded(
                                              child: CupertinoDatePicker(
                                                mode: CupertinoDatePickerMode
                                                    .date,
                                                initialDateTime: DateTime(
                                                    now.year,
                                                    now.month,
                                                    now.day),
                                                onDateTimeChanged:
                                                    (DateTime newDateTime) {



                                                  date= newDateTime.day
                                                      .toString() +
                                                      "-" +
                                                      newDateTime.month
                                                          .toString() +
                                                      "-" +
                                                      newDateTime.year
                                                          .toString();


                                                  datetxt=date;



                                                  //print(date);
                                                  // Do something
                                                },
                                              ),
                                              flex: 2,
                                            ),
                                            Padding(
                                              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4) : EdgeInsets.all(6),
                                              child: Container(
                                                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50 : 60:70,
                                                width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150 : 160:190,

                                                child: TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);

                                                    setState(() {
                                                      datetxt = date;
                                                    });
                                                  },
                                                  child: Text(
                                                    'Ok',
                                                    style: TextStyle(
                                                        color: Colors.blue,
                                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15 : 18:21),
                                                  ),
                                                ),
                                              ),
                                            )
                                          ]));
                                    });







                              },
                              child: Text(
                                datetxt,
                                style: TextStyle(
                                    color: Colors.black, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12 : 14:16),
                              ),
                            ))
                      ],
                    ),
                  )))),),

          Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11): EdgeInsets.all(14)

              ,child:Row(children: [
                Expanded(

                    flex: 3,
                    child:Padding(padding: EdgeInsets.all(0),
                      child: Container(
                        height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,

                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black38)),

                        child:  DropdownButtonHideUnderline(

                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: InputDecorator(
                              decoration: const InputDecoration(border: OutlineInputBorder()),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(

                                  isExpanded: true,
                                  value: subjectdata,
                                  items: subject
                                      .map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (String? newValue) {
                                    setState(() {
    subjectdata = newValue!;

                                      // getBudgetData();




                                    });
                                  },
                                  style: Theme.of(context).textTheme.bodyText1,

                                ),
                              ),
                            ),
                          ),
                        ),


                      ),


                    )





                ),

                Expanded(child: Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11): EdgeInsets.all(14)
,

    child: FloatingActionButton(
                      onPressed: () async{


                        showBubjectDialog();

                      },
                      child: Icon(Icons.add, color: Colors.white, size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  29:34:41,),
                      backgroundColor: Colors.blue,

                      elevation: 5,
                      splashColor: Colors.grey,
                    )),flex: 1,)


              ],)),


          Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11) : EdgeInsets.all(14),
          child: Container(


            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 1,
              ),
            ),



              child:
    SizedBox(

      height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?120 : 150:180,

    child: TextField(
      maxLines: 10,
      controller: commentcontroller,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 0.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 0.5),
        ),
        hintText: 'Comments',
      ),

      onChanged: (text) {
        // TemRegData.email=text;

      },
    ),
    )



              ,
          ),







          ),

          Align(

            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(5, 10, 5, 0) : EdgeInsets.fromLTRB(10, 15, 10, 0) : EdgeInsets.fromLTRB(15, 20, 15, 0),
              child: Container(
                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 :60:70,
                width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150 : 170:190,

                child: TextButton(
                  onPressed: () async{


                    // if(languagedropdown.compareTo("Select your language")!=0)
                    //   {
                        
                        if(datetxt.compareTo("Select date")!=0)
                          {

                            if(subjectdata.compareTo("Select subject")!=0)
                            {

                              if(commentcontroller.text.isNotEmpty)
                              {

                               var m=new Map();
                                m['date']=datetxt;
                                m['content']=commentcontroller.text.toString();
                                m['subject']=await getInFromSubjectname(subjectdata);

                                var js=json.encode(m);

                                Map<String, dynamic> data_To_Table=new Map();
                                data_To_Table['data']=js.toString();


                                if(diaryid.compareTo("0")!=0)
                                  {
                                    dbhelper.update(data_To_Table, DatabaseTables.DIARY_table,diaryid);

                                  }
                                else{
                                  dbhelper.insert(data_To_Table, DatabaseTables.DIARY_table);

                                }


                                Navigator.pop(context,{"accountadded":1});




                              }
                              else{

                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                  content: Text("Enter comments"),
                                ));
                              }


                            }
                            else{

                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Select subject"),
                              ));
                            }


                          }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Select date"),
                          ));
                        }


                    //   }
                    // else{
                    //
                    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    //     content: Text("Select your language"),
                    //   ));
                    // }












                  },
                  child: Text(
                    'Submit',
                    style:
                    TextStyle(color: Colors.blue, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:16:18),
                  ),
                ),
              ),
            ),),

        ]

       )
      )]),
    );


  }


  void showCurrentDate()
  {
    var now = DateTime.now();


    setState(() {
      datetxt=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

      month=now.month.toString();
      year=now.year.toString();
    });

    if(diaryid.compareTo("0")!=0) {
      getDiaryDataByID();
    }


  }

  void showBubjectDialog()
  {

    Dialog feedbackdialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        color: Colors.white,
        height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?300.0 : 400:500,
        width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 800.0:900:1000,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: feedbackcontroller,
                keyboardType: TextInputType.text,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Enter the subject',


                ),
              )),
            ),
            Padding(
              padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(15.0): EdgeInsets.all(18.0):EdgeInsets.all(20.0),

              child: Container(

                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150 : 170:190,
                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?55 : 65:75,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() async {


                      if(feedbackcontroller.text.toString().isNotEmpty) {


                        Map<String, dynamic> m1 = Map();
                        m1['data'] = feedbackcontroller.text.toString();
                        dbhelper.insert(m1, DatabaseTables.DIARYSUBJECT_table);


                        showDiarySubjectData();




                      //  submitFeedback(feedbackcontroller.text.toString());



                        Navigator.pop(context);

                        //   checkMobileNumber(otpcodeController.text.toString());
                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Enter your feedback"),
                        ));
                      }


                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),










            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),







      ),
    );



    showDialog(context: context, builder: (BuildContext context) => feedbackdialog);
  }

  void showDiarySubjectData() async
  {
  List<Map<String,dynamic>>m=await  dbhelper.queryAllRows( DatabaseTables.DIARYSUBJECT_table);
  List<String>subjectdata1=[];
  subjectdata1.add("Select subject");

  for(Map a in m)
    {

      subjectdata1.add(a['data'].toString());

    }


  setState(() {

    subject.clear();
    subject.addAll(subjectdata1);

    subjectdata=subjectdata1[0];


  });




  }



  getInFromSubjectname(String subject) async
  {
    List<Map<String,dynamic>>m=await  dbhelper.queryAllRows( DatabaseTables.DIARYSUBJECT_table);
String datasub="";
    //subjectdata1.add("Select subject");

    for(Map a in m)
    {

      String id=a['keyid'].toString();
      String sub=a['data'].toString();

      if(sub.compareTo(subject)==0)
        {
          datasub=id;
          break;
        }


     // subjectdata1.add();

    }

    return datasub;

  }


  getDiaryDataByID() async
  {
    var v1 = await dbhelper.getDataByid(
        DatabaseTables.DIARY_table, diaryid.toString());

    List<Map<String, dynamic>> ab1 = v1;

    Map ab=ab1[0];

    // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

    int id = ab["keyid"];
    String data = ab["data"];

    var jsondata = jsonDecode(data);

    String date = jsondata["date"];
    String content = jsondata["content"];
    String subject = jsondata["subject"];


    //fetching subject data

     var v1sub = await dbhelper.getDataByid(
         DatabaseTables.DIARYSUBJECT_table, subject.toString());

    List<Map<String, dynamic>> absub = v1sub;

    Map absubject=absub[0];

    // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

    int id1 = absubject["keyid"];
    String subjectname = absubject["data"];




    var now=new DateFormat("dd-MM-yyyy").parse(date);

    setState(() {
      datetxt=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

      month=now.month.toString();
      year=now.year.toString();

      commentcontroller.text=content;
      languagedropdown="English";

      subjectdata=subjectname;
    });




  }


}