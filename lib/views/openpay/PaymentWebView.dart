import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;
import '../../projectconstants/DataConstants.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
// Import for iOS features.
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';



class PaymentExplorer extends StatefulWidget {

   final String url;



   // WebView wv1;


   PaymentExplorer(
       this.url);

  @override
  _PaymentExplorerState createState() => _PaymentExplorerState( this.url);
}

class _PaymentExplorerState extends State<PaymentExplorer> {


   String url;


  _PaymentExplorerState(
      this.url);

   String responseurl="https://mysaveapp.com";


  //String responseurl="https://mysaving.in/IntegraAccount/openpay/loadedresponse.php?data=";

   late WebViewController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
    WebViewController.fromPlatformCreationParams(params);
    // #enddocregion platform_features

    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
Page resource error:
  code: ${error.errorCode}
  description: ${error.description}
  errorType: ${error.errorType}
  isForMainFrame: ${error.isForMainFrame}
          ''');
          },
          onNavigationRequest: (NavigationRequest request) {

            if(url.toString().compareTo(responseurl)==0)
            {

              print(url.toString());

             // getResponseData(url.toString());

              Navigator.of(context).pop({'paymentdata': "1"});

              //


            }

            // if (request.url.startsWith('https://www.youtube.com/')) {
            //   debugPrint('blocking navigation to ${request.url}');
            //   return NavigationDecision.prevent;
            // }
            // debugPrint('allowing navigation to ${request.url}');
            return NavigationDecision.navigate;
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      )
      ..loadRequest(Uri.parse(url));

    // #docregion platform_features
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }
    // #enddocregion platform_features

    _controller = controller;
  }


  @override
  Widget build(BuildContext context) {


    print(url);

    String u=url.replaceAll(" ", "%20");


    return Scaffold(
      appBar: AppBar(
        title: const Text('Save app payment'),
        // This drop down menu demonstrates that Flutter widgets can be shown over the web view.

      ),
      // body: WebView(
      //
      //   onPageStarted: (url) {
      //
      //     if(url.toString().contains(responseurl))
      //     {
      //
      //       print(url.toString());
      //
      //       getResponseData(url.toString());
      //
      //       //
      //
      //
      //     }
      //
      //   },
      //   initialUrl: u,
      //   javascriptMode: JavascriptMode.unrestricted,
      //
      //
      //
      // ),

      body: WebViewWidget(controller: _controller),

      );
      // floatingActionButton: _bookmarkButton(),

  }



   getResponseData(String url) async
   {
     ProgressDialog _progressDialog = ProgressDialog();
     _progressDialog.showProgressDialog(
         context, textToBeDisplayed: "Please wait for a moment......");

     var dataasync = await http.get(
       Uri.parse(url),
       headers: <String, String>{
         'Content-Type': 'application/x-www-form-urlencoded',

       },

     );
     _progressDialog.dismissProgressDialog(context);
     String response = dataasync.body;

     print(response);

     //var json = jsonDecode(response);;

     List<String> d=response.split(",");

     if(d.length>0)
       {
         if(d[0].contains("Your payment is success")==0)
           {

             String a = d[1];
             List<String>b= a.split(":");
             if(b.length>0) {
               String transactionid = b[2];

               Navigator.of(context).pop({'paymentdata': transactionid});
             }

           }
         else{

           Navigator.of(context).pop({'paymentdata': ""});

         }



       }
     else{

       Navigator.of(context).pop({'paymentdata': ""});
     }






   }




}




