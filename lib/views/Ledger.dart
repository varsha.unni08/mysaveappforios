import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';


import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import '../projectconstants/DataConstants.dart';
import '../utils/Tutorials.dart';
import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;

import 'CashbankAccountDetails.dart';


class LedgerPage extends StatefulWidget {
  final String title;

  const LedgerPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _LedgerPage();

}


class _LedgerPage extends State<LedgerPage> {


  String date = "",date1="";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Account name", "Debit", "Credit", "Action"];

  DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<Cashbankaccount> cashbankdata = [];


  @override
  void initState() {
    // TODO: implement initState
    showCurrentMonthAndYear();

    Tutorial.showTutorial(Tutorial.ledger_tutorial, context, Tutorial.ledgertutorial);

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(

      resizeToAvoidBottomInset: true,

      appBar: AppBar(
        automaticallyImplyLeading: false,

        flexibleSpace: Container(

            color: Color(0xFF096c6c),
            width: double.infinity,
            padding: EdgeInsets.all(10),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(2, 20, 0, 0),
                        alignment: Alignment.center,
                        child: Center(
                            child: new InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Image.asset(
                                  'images/rightarrow.png',
                                  width: 24,
                                  height: 24,
                                  fit: BoxFit.contain,
                                )))),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(10, 20, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Ledger",
                          style: TextStyle(
                              fontSize: 13, color: Colors.white),
                        )),
                    flex: 5,
                  ),
                ])) ,
        backgroundColor: Color(0xFF096c6c),
        centerTitle: false,

      ),

      body: Stack(


        children: [

          SizedBox(
            height:
            ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?160:190:250,

            child:Stack(

              children: [


                Align(


                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,

                    children: [
                      Row(
                        children: [

                          Expanded(child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black38)),
                                child: Row(
                                  textDirection: ui.TextDirection.rtl,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Image.asset(
                                          "images/calendar.png",
                                          width: 25,
                                          height: 25,
                                        )),
                                    Expanded(
                                        child: TextButton(
                                          onPressed: () {
                                            var newDateTime = DateTime.now();

                                            date= newDateTime.day
                                                .toString() +
                                                "-" +
                                                newDateTime.month
                                                    .toString() +
                                                "-" +
                                                newDateTime.year
                                                    .toString();



                                            // showCustomDatePicker(build(context));

                                            showModalBottomSheet(
                                                context: context,
                                                builder: (context) {
                                                  return Container(
                                                      child: Column(children: [
                                                        Expanded(
                                                          child: CupertinoDatePicker(
                                                            mode: CupertinoDatePickerMode
                                                                .date,
                                                            initialDateTime: DateTime(
                                                                newDateTime.year,
                                                                newDateTime.month,
                                                                newDateTime.day),
                                                            onDateTimeChanged:
                                                                (DateTime newDateTime) {


                                                              date= newDateTime.day
                                                                  .toString() +
                                                                  "-" +
                                                                  newDateTime.month
                                                                      .toString() +
                                                                  "-" +
                                                                  newDateTime.year
                                                                      .toString();






                                                              //print(date);
                                                              // Do something
                                                            },
                                                          ),
                                                          flex: 2,
                                                        ),
                                                        Padding(
                                                          padding: const EdgeInsets.all(2),
                                                          child: Container(
                                                            height: 50,
                                                            width: 150,
                                                            decoration: BoxDecoration(
                                                                color: Color(0xF0233048),
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    10)),
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(context);

                                                                setState(() {
                                                                  datetxt1 = date;
                                                                });
                                                              },
                                                              child: Text(
                                                                'Ok',
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontSize: 15),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ]));
                                                });



                                            // showCustomDatePicker(build(context));


                                          },
                                          child: Text(
                                            datetxt1,
                                            style: TextStyle(
                                                color: Colors.black38, fontSize: 12),
                                          ),
                                        ))
                                  ],
                                ),
                              )),flex: 1,),
                          Expanded(child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black38)),
                                child: Row(
                                  textDirection: ui.TextDirection.rtl,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Image.asset(
                                          "images/calendar.png",
                                          width: 25,
                                          height: 25,
                                        )),
                                    Expanded(
                                        child: TextButton(
                                          onPressed: () {
                                            var newDateTime = DateTime.now();

                                            date1= newDateTime.day
                                                .toString() +
                                                "-" +
                                                newDateTime.month
                                                    .toString() +
                                                "-" +
                                                newDateTime.year
                                                    .toString();



                                            // showCustomDatePicker(build(context));

                                            showModalBottomSheet(
                                                context: context,
                                                builder: (context) {
                                                  return Container(
                                                      child: Column(children: [
                                                        Expanded(
                                                          child: CupertinoDatePicker(
                                                            mode: CupertinoDatePickerMode
                                                                .date,
                                                            initialDateTime: DateTime(
                                                                newDateTime.year,
                                                                newDateTime.month,
                                                                newDateTime.day),
                                                            onDateTimeChanged:
                                                                (DateTime newDateTime) {


                                                              date1= newDateTime.day
                                                                  .toString() +
                                                                  "-" +
                                                                  newDateTime.month
                                                                      .toString() +
                                                                  "-" +
                                                                  newDateTime.year
                                                                      .toString();






                                                              //print(date);
                                                              // Do something
                                                            },
                                                          ),
                                                          flex: 2,
                                                        ),
                                                        Padding(
                                                          padding: const EdgeInsets.all(2),
                                                          child: Container(
                                                            height: 50,
                                                            width: 150,
                                                            decoration: BoxDecoration(
                                                                color: Color(0xF0233048),
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    10)),
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(context);

                                                                setState(() {
                                                                  datetxt2 = date1;
                                                                });
                                                              },
                                                              child: Text(
                                                                'Ok',
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontSize: 15),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ]));
                                                });


                                            // showCustomDatePicker(build(context));


                                          },
                                          child: Text(
                                            datetxt2,
                                            style: TextStyle(
                                                color: Colors.black38, fontSize: 12),
                                          ),
                                        ))
                                  ],
                                ),
                              )),flex: 1,)


                        ],

                      ),

                      Stack(

                          children: [

                            Align(
                              alignment: FractionalOffset.center,
                              child: Padding(
                                padding: const EdgeInsets.all(2),
                                child: Container(
                                  height: 50,
                                  width: 150,
                                  decoration: BoxDecoration(
                                      color: Color(0xF0233048),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: TextButton(
                                    onPressed: () {

                                      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(datetxt1);

                                      DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(datetxt2);

                                      if(accountsdateselected.isAfter(accountsdateparsed)||accountsdateselected.compareTo(accountsdateparsed)==0)
                                      {

                                        showData();

                                      }

                                    },
                                    child: Text(
                                      'Submit',
                                      style:
                                      TextStyle(color: Colors.white, fontSize: 15),
                                    ),
                                  ),
                                ),
                              ),

                            )


                          ])


                    ],



                  ),
                  alignment: FractionalOffset.topCenter,
                ),


                Align(
                  alignment: FractionalOffset.bottomCenter,

                  child:(cashbankaccountata.length>0)?Padding(padding:

                  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?   EdgeInsets.fromLTRB(10, 0, 10, 0) : EdgeInsets.fromLTRB(15, 0, 15, 0):EdgeInsets.fromLTRB(20, 0, 20, 0),



                   child: MediaQuery.removePadding(
                        context: context,
                        removeTop: true,
                        removeBottom: true,
                        child:GridView.builder(
                          physics: BouncingScrollPhysics(),

                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              crossAxisSpacing: 0.0,
                              mainAxisSpacing: 0.0,
                              childAspectRatio: 2
                          ),
                          itemCount: tableheaddata.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding:EdgeInsets.all(0),

                              child: Container(

                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.black54,
                                      width: 0.3,
                                    ),
                                  ),


                                  child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  13:15:17,color: Colors.black),))),

                            );
                          },
                        ),)



                    ,






                  ):Container(),

                )







              ],



            ),



          )








        ,




          (cashbankaccountata.length>0)?Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 160, 10, 0) :  EdgeInsets.fromLTRB(15, 190, 15, 0): EdgeInsets.fromLTRB(20, 250, 20, 0),

            child:

            Align(alignment: Alignment.topCenter,child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child:GridView.builder(
                  physics: BouncingScrollPhysics(),

                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 0.0,
                    childAspectRatio: 0.7
                  ),
                  itemCount: cashbankaccountata.length,
                  itemBuilder: (context, index) {
                    return Container(

                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black54,
                            width: 0.3,
                          ),
                        ),


                        child:


                            (cashbankaccountata[index].contains("View"))?

                            Center(child:TextButton(
                                onPressed: () {

                                  int a=index-1;
                                  int b=a~/4;

                                  Cashbankaccount cb=cashbankdata[b];

                                  print(cb.accountname);


                                  Navigator.push(
                                      context, MaterialPageRoute(builder: (_) =>
                                      CashbankstatementDetailsPage(title: "Cashbankstatement details", accid: cashbankaccountata[index].split(",")[1], cb: cb, Startdate: datetxt1, Enddate: datetxt2,)));

                                },
                                child: Text(cashbankaccountata[index].split(",")[0],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.green)
                                  ,))):Center(child:Text(cashbankaccountata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.black),)));
                  },
                )),)



            ,






          ):new Container(),















        ],


      ),





    );
  }



  void showCurrentMonthAndYear()
  async{

    List<String> cashbankaccountatanow = [];

    var now=DateTime.now();

    setState(() {

      datetxt1="1"+"-"+now.month.toString()+"-"+now.year.toString();
      datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    });

    showData();




  }

  showData() async
  {
    List<String> cashbankaccountatanow = [];
    List<Cashbankaccount> cashbank = [];

    var dbdata=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);

    List<Map<String,dynamic>>allaccountsettings=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);

    List<AccountSetupData> accountsetupdata = [];
    for (Map ab in allaccountsettings) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);
      String Accounttype = jsondata["Accounttype"];

      if(Accounttype.compareTo("Bank")!=0 &&Accounttype.compareTo("Cash")!=0) {
        String Accountname = jsondata["Accountname"];

        String Amount = jsondata["Amount"];
        String Type = jsondata["Type"];

        AccountSetupData dataaccsetup = new AccountSetupData();
        dataaccsetup.id = id.toString();
        dataaccsetup.Accountname = Accountname;
        dataaccsetup.Accounttype = Accounttype;
        dataaccsetup.Amount = Amount;
        dataaccsetup.Type = Type;

        accountsetupdata.add(dataaccsetup);
      }


    }

    setState(() {

      accsetupdata.clear();
      accsetupdata.addAll(accountsetupdata);
    });

    List<Map<String,dynamic>>accounts=dbdata;

    List<Map<String,dynamic>>sortedaccounts=[];



    for(int i=0;i<accounts.length;i++)
    {
      Map<String, dynamic> mapval=accounts[i];
      String setupid = mapval[DatabaseTables.ACCOUNTS_setupid];



      if(!checkDataExists(sortedaccounts, setupid))
      {
        sortedaccounts.add(accounts[i]);
      }

    }

    for(AccountSetupData acc in accountsetupdata) {
      for (int i = 0; i < sortedaccounts.length; i++) {
        Map<String, dynamic> mapval = sortedaccounts[i];
        String setupid = mapval[DatabaseTables.ACCOUNTS_setupid];

        if (setupid.compareTo(acc.id) == 0) {
          Cashbankaccount cashbankaccount = new Cashbankaccount();
          String openingbalance = acc.Amount;
          String type = acc.Type;

          double openingbal = double.parse(openingbalance);

          if (openingbal > 0) {
            if (type.compareTo("Credit") == 0) {
              openingbal = openingbal * -1;
            }
          }

          double finalbalance = await getFinalOpeningBalance(
              openingbal, acc.id);

          cashbankaccount.amount = finalbalance;
          cashbankaccount.accountname = acc.Accountname;
          cashbankaccount.accountid = acc.id;

          cashbank.add(cashbankaccount);

          cashbankaccountatanow.add(acc.Accountname);


          if (finalbalance >= 0) {
            cashbankaccountatanow.add(finalbalance.toString());
            cashbankaccountatanow.add("");
          }
          else if (finalbalance < 0) {
            finalbalance = finalbalance * -1;
            cashbankaccountatanow.add("");
            cashbankaccountatanow.add(finalbalance.toString());
          }
          cashbankaccountatanow.add("View," + acc.id);
        }
      }
    }

      setState(() {

        cashbankaccountata.clear();

        cashbankaccountata.addAll(cashbankaccountatanow);

        cashbankdata.clear();
        cashbankdata.addAll(cashbank);
      });



  }

  bool checkDataExists( List<Map<String,dynamic>>accounts,String setupid)
  {
    bool a=false;

    for(int i=0;i<accounts.length;i++) {
      Map<String, dynamic> mapval = accounts[i];
      String setupid1 = mapval[DatabaseTables.ACCOUNTS_setupid];

      if(setupid.compareTo(setupid1)==0)
      {
        a=true;
      }



    }

    return a;
  }


  Future<double> getFinalOpeningBalance(double openingbalance,String setupid)
  async {

    double finalbalance=0;

    finalbalance=openingbalance;


    var m=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);
    List<Map<String, dynamic>> v=m;

    for(Map a in v) {
      String setupidaccount = a[DatabaseTables.ACCOUNTS_setupid];

      if (setupidaccount.compareTo(setupid) == 0)
      {
        String accountsdate = a[DatabaseTables.ACCOUNTS_date];
        String accountsamount = a[DatabaseTables.ACCOUNTS_amount];

        // String setupidaccount=a[DatabaseTables.ACCOUNTS_setupid];

        String accountstype = a[DatabaseTables.ACCOUNTS_type].toString();

        double amount = double.parse(accountsamount);

        DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(
            accountsdate);

        DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(
            datetxt2);

        if (accountsdateparsed.compareTo(accountsdateselected) == 0) {
          if (setupidaccount.compareTo(setupid) == 0) {
            if (accountstype.compareTo(DataConstants.debit.toString()) == 0) {
              finalbalance = finalbalance + amount;
            }
            else {
              finalbalance = finalbalance - amount;
            }
          }
        }

        else if (accountsdateparsed.isBefore(accountsdateselected)) {
          if (setupidaccount.compareTo(setupid) == 0) {
            if (accountstype.compareTo(DataConstants.debit.toString()) == 0) {
              finalbalance = finalbalance + amount;
            }
            else {
              finalbalance = finalbalance - amount;
            }
          }
        }
      }

    }

    return finalbalance;

  }
}