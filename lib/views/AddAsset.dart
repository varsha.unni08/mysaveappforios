

import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share_extend/share_extend.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/domain/RemindDate.dart';
// import 'package:save_flutter/domain/RemindDateOfAsset.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';

import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_auth/firebase_auth.dart';
import 'package:googleapis/drive/v3.dart' as drive;
 import 'package:file_picker/file_picker.dart';
//
 import 'package:share/share.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:saveappforios/database/DBTables.dart';

import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/RemindDate.dart';
import 'AddAccountSetup.dart';
import 'FileOptions.dart';



class AddAssetListPage extends StatefulWidget {
  final String title;
  final String assetid;



  const AddAssetListPage({Key? key, required this.title, required this.assetid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddAssetListPage(assetid);

}

class _AddAssetListPage extends State<AddAssetListPage> {

  String diaryid = "0";
  String assetid="0";


  _AddAssetListPage(this.assetid);

  DatabaseHelper dbhelper=new DatabaseHelper();


  String date = "",
      month = "",
      year = "";

  String title="";


  List<String>subject = ["Select asset account"];

  String subjectdata = "Select asset account";


  String reminddatetxt = ""
      "Remind date";

  String datetxt2 = "Select end date";

  String languagedropdown = 'Select your language';

  TextEditingController commentcontroller = new TextEditingController();
  TextEditingController feedbackcontroller = new TextEditingController();
  TextEditingController amountcontroller = new TextEditingController();
  TextEditingController remarkcontroller = new TextEditingController();

  String datetxt="Select date of purchase";


  // DatabaseHelper dbhelper = new DatabaseHelper();

  String stdate="",endate="";


  String accountType="Asset account";


  List<RemindDate>reminddates=[];


  int clickcount=0;

  String accountid="0";

  List<Map<String,dynamic>> uploadeddoc=[];
  List<Map<String,dynamic>> uploadeddocfromdialog=[];

  TextEditingController titleController=new TextEditingController();

  String selecteddata="";

  String filepath="";


  @override
  void initState() {
    // TODO: implement initState


    setupAccountData();

    if(assetid.compareTo("0")!=0) {
      getAssetById(assetid);
    }

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }





  @override
  Widget build(BuildContext context) {
    return Scaffold( resizeToAvoidBottomInset: true,
      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Add asset",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),


        body: Stack(
        children: <Widget>[

          
    SingleChildScrollView(
    child: Column(

    children: <Widget>[

      Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8):EdgeInsets.all(11):EdgeInsets.all(14)

          ,child:Row(children: [
            Expanded(child: Padding(padding: EdgeInsets.all(0),
              child: Container(
                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:55:75,

                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),

                child:  DropdownButtonHideUnderline(

                  child: ButtonTheme(
                    alignedDropdown: true,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder()),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(

                          isExpanded: true,
                          value: subjectdata,
                          items: subject
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              subjectdata = newValue!;

                              // getBudgetData();

                              setDataByAccountName(subjectdata);




                            });
                          },
                          style: Theme.of(context).textTheme.bodyText1,

                        ),
                      ),
                    ),
                  ),
                ),


              ),


            ),flex: 2,)







            ,

            Expanded(child:  Padding(
                padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8): EdgeInsets.all(11):EdgeInsets.all(14),

                child: FloatingActionButton(
                  onPressed: () async{


                    //   showBubjectDialog();

                    Map results = await Navigator.of(context)
                        .push(new MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) {
                        return new AddAccountSettinglistpage(title: "account setup",accountType: accountType, accountsetupid: '0',);
                      },
                    ));

                    if (results != null &&
                        results.containsKey('accountsetupdata')) {
                      setState(() {
                        var accountsetupdata =
                        results['accountsetupdata'];

                        int acs =
                        accountsetupdata as int;
                        //
                        if(acs>0)
                          {
                            setupAccountData();
                         }


                      });
                     }

                  },
                  child: Icon(Icons.add, color: Colors.white, size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 29:35:40,),
                  backgroundColor: Colors.blue,

                  elevation: 5,
                  splashColor: Colors.grey,
                )),flex: 1,)




          ],)),


      Padding(
        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 0) :EdgeInsets.only(left:21.0,right: 21.0,top:15,bottom: 0),
        // padding: EdgeInsets.all(15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.black
        ), child: TextField(
          controller: amountcontroller,

          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            hintText: 'Amount',
          ),

          onChanged: (text) {
            //TemRegData.email=text;

          },
        )),
      ),


      Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(8) : EdgeInsets.all(11) :EdgeInsets.all(14),
        child:   SizedBox(

            width: double.infinity,
            child: Container(child:     Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[
                      Expanded(child:  Padding(
                          padding: EdgeInsets.all(10),
                          child: Icon(Icons.calendar_month,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26,)),flex: 1,)


                     ,
                      Expanded(child: Container(
                          child: TextButton(
                            onPressed: () async {
                              var now = DateTime.now();

                              date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                              month=now.month.toString();
                              year=now.year.toString();

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {



                                                date= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();


                                                datetxt=date;



                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4) : EdgeInsets.all(6),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });







                            },
                            child: Text(
                              datetxt,
                              style: TextStyle(
                                  color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:17),
                            ),
                          )),flex: 3,)




                    ],
                  ),
                )))),),


      Align(

        alignment: Alignment.topCenter,
        child:   Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.fromLTRB(15, 10, 15, 10) : EdgeInsets.fromLTRB(18, 12, 18, 12):EdgeInsets.fromLTRB(21, 15, 21, 15),
          child:  Container(
            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  50:60:70,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xF0E21133),
                borderRadius: BorderRadius.circular(10)),
            child: TextButton(
              onPressed: () {


                setState(() {

                  reminddates.add(new RemindDate());

                });



              },
              child: Text(
                'Set remind dates',
                style:
                TextStyle(color: Colors.white, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18),
              ),
            ),
          ),
        ),),

        Align(

        alignment: Alignment.topCenter,
    child:   Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 10) : EdgeInsets.fromLTRB(18, 13, 18, 13) : EdgeInsets.fromLTRB(21, 16, 21, 16),


    child: Container(child:
    MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.builder(
          physics: BouncingScrollPhysics(),

          shrinkWrap: true,

          itemCount: reminddates.length,
          itemBuilder: (context, index) {
            return  Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) : EdgeInsets.all(6):EdgeInsets.all(8),

                child: Card(
                elevation: 4,
                child:

                Container(height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 90:120:150,

                  child:  Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(10)   ,

                child: Stack(

                  children: [

                    Align(
                      child: Container(

                        child:

                        GestureDetector(

                            onTap: (){

                              setState(() {

                                reminddates.removeAt(index);

                                //  clickcount--;
                              });



                            },

                            child:   Padding(

                                padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(3):EdgeInsets.all(5),
                                child:


                                Icon(Icons.close,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:18:21 ,)
                            )
                        )




                        ,


                      ),
alignment: FractionalOffset.topRight,

                    ),

                    Align(

                      alignment: FractionalOffset.center,

                      child: Row(
                        children: [


                              Expanded(child:Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black38)
                                  ),

                                  child:GestureDetector(

                                      onTap: (){

                                        var now = DateTime.now();

                                        date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                                        month=now.month.toString();
                                        year=now.year.toString();

                                        showModalBottomSheet(
                                            context: context,
                                            builder: (context) {
                                              return Container(
                                                  child: Column(children: [
                                                    Expanded(
                                                      child: CupertinoDatePicker(
                                                        mode: CupertinoDatePickerMode
                                                            .date,
                                                        initialDateTime: DateTime(
                                                            now.year,
                                                            now.month,
                                                            now.day),
                                                        onDateTimeChanged:
                                                            (DateTime newDateTime) {



                                                          date= newDateTime.day
                                                              .toString() +
                                                              "-" +
                                                              newDateTime.month
                                                                  .toString() +
                                                              "-" +
                                                              newDateTime.year
                                                                  .toString();


                                                          reminddatetxt=date;



                                                          //print(date);
                                                          // Do something
                                                        },
                                                      ),
                                                      flex: 2,
                                                    ),
                                                    Padding(
                                                      padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4): EdgeInsets.all(6),
                                                      child: Container(
                                                        height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                        width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                                        decoration: BoxDecoration(
                                                            color: Color(0xF0233048),
                                                            borderRadius:
                                                            BorderRadius.circular(
                                                                10)),
                                                        child: TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(context);

                                                            setState(() {
                                                              reminddatetxt = date;

                                                              reminddates[index].date=reminddatetxt;

                                                            });
                                                          },
                                                          child: Text(
                                                            'Ok',
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ]));
                                            });


                                      },

                                      child: Row(

                                        children: [
                                          (reminddates[index].date.isNotEmpty)?  Padding(
                                              padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
                                              child:Text(reminddates[index].date)):Padding(padding:
                                          ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?   EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),


                                              child:Text("Remind date")),

                                          Padding(
                                              padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(3, 0, 3, 0) : EdgeInsets.fromLTRB(5, 0, 5, 0) : EdgeInsets.fromLTRB(6, 0, 6, 0),
                                              child: Icon(Icons.calendar_month,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,)),
                                        ],
                                      )
                                  )








                              ),flex: 1, )

                              ,

                              Expanded(child: Container(
                                // decoration: BoxDecoration(
                                //     border: Border.all(color: Colors.black38)
                                // ),
                                  child:  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(3) : EdgeInsets.all(5):EdgeInsets.all(6) ,
                                      child: TextField(

                                        onChanged: (text) {

                                          reminddates[index].description=text;


                                        },

                                        decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.black26, width: 0.8),
                                          ),

                                          hintText: 'Description',

                                        ),
                                      ))





                              ),flex: 1,)


                          ,






                        ],






                      ),


                    )


                  ],



                )




                ,))










            ));
          },
        ))
    ),)

        ),

      Align(

        alignment: Alignment.topCenter,
        child:   Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(12, 8, 12, 8) : EdgeInsets.fromLTRB(15, 10, 15, 10):EdgeInsets.fromLTRB(21, 15, 21, 15),
          child:  Container(
            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xF0E21133),
                borderRadius: BorderRadius.circular(10)),
            child: TextButton(
              onPressed: () {

                // Dialog forgotpasswordDialog = Dialog(
                //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                //   child: Container(
                //     height: 300.0,
                //     width: 700.0,
                //
                //     child: Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       children: <Widget>[
                //         Padding(
                //           padding:  EdgeInsets.all(15.0),
                //           child: new Theme(data: new ThemeData(
                //               hintColor: Colors.black38
                //           ), child: TextField(
                //
                //             keyboardType: TextInputType.text,
                //             controller: titleController,
                //
                //             decoration: InputDecoration(
                //               focusedBorder: OutlineInputBorder(
                //                 borderSide: BorderSide(color: Colors.black38, width: 0.5),
                //               ),
                //               enabledBorder: OutlineInputBorder(
                //                 borderSide: BorderSide(color: Colors.black38, width: 0.5),
                //               ),
                //               hintText: 'Enter the title',
                //
                //
                //             ),
                //           )),
                //         ),
                //
                //         Padding(padding: EdgeInsets.all(8),
                //
                //           child:TextButton(onPressed: () {
                //
                //
                //
                //             showDialog(
                //                 context: context,
                //                 builder: (_) {
                //                   return MyFileOptionsDialog();
                //                 }).then((value) => {
                //
                //               selecteddata =value["option"].toString(),
                //
                //               if(selecteddata.compareTo("1")==0)
                //                 {
                //
                //                   filePickThroughGallery().then((value) => {
                //
                //
                //
                //
                //                     filepath=value.toString()
                //
                //
                //
                //                   })
                //                 }
                //               else if(selecteddata.compareTo("2")==0)
                //                 {
                //                   filePickThroughdocument().then((value) => {
                //
                //                     filepath=value.toString()
                //
                //                   })
                //                 }
                //
                //
                //
                //
                //
                //
                //
                //
                //             });
                //
                //
                //           }, child: Text("Select a file",style: TextStyle(fontSize: 14),),) ,),
                //
                //
                //
                //         Padding(padding: EdgeInsets.all(8),
                //
                //           child:Text(filepath) ,),
                //
                //         Padding(
                //           padding: EdgeInsets.all(15.0),
                //
                //           child: Container(
                //
                //             width: 150,
                //             height: 55,
                //             decoration: BoxDecoration(
                //
                //                 color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                //             child:Align(
                //               alignment: Alignment.center,
                //               child: TextButton(
                //
                //                 onPressed:() {
                //
                //                   Navigator.pop(context);
                //
                //                   if(titleController.text.isNotEmpty) {
                //
                //                     if(filepath.isNotEmpty)
                //                     {
                //
                //
                //
                //                       uploadFile(filepath,titleController.text);
                //                       titleController.text="";
                //                       filepath="";
                //                     }
                //                     else{
                //
                //                       ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                //                         content: Text("Select a file"),
                //                       ));
                //                     }
                //
                //
                //
                //
                //
                //                   }
                //                   else{
                //
                //                     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                //                       content: Text("Enter the title"),
                //                     ));
                //                   }
                //
                //
                //                 },
                //
                //                 child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                //             ),
                //
                //
                //
                //             //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                //           ),
                //
                //
                //           // ,
                //         ),
                //         // Padding(padding: EdgeInsets.only(top: 50.0)),
                //         // TextButton(onPressed: () {
                //         //   Navigator.of(context).pop();
                //         // },
                //         //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                //       ],
                //     ),
                //   ),
                // );
                //
                //
                //
                // showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);


                showDialog(
                    context: context,
                    builder: (_) {
                      return FilePickOptionsPage(title: "filepick",code: 1,);
                    }).then((value) => {


                  filepath=value["FilePath"].toString(),

                  title=value["Title"].toString(),

                  uploadFile(filepath, title)





                });




              },
              child: Text(
                'Upload documents',
                style:
                TextStyle(color: Colors.white, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:18),
              ),
            ),
          ),
        ),),


      (uploadeddoc.length>0)?  Align(

        alignment: Alignment.topCenter,
        child:   Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(12, 8, 12, 8) : EdgeInsets.fromLTRB(15, 10, 15, 10):EdgeInsets.fromLTRB(18, 12, 18, 12),
          child:  Container(
            height: 50,
            width: double.infinity,

            child: TextButton(
              onPressed: () {



                showAllAssetDocument();




              },
              child: Text(
                'View documents',
                style:
                TextStyle(color: Colors.blueAccent, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:16:18),
              ),
            ),
          ),
        ),):Container(),





      Padding(
        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 15) : EdgeInsets.only(left:18.0,right: 18.0,top:12,bottom: 18) :EdgeInsets.only(left:21.0,right: 21.0,top:15,bottom: 21),
        // padding: EdgeInsets.all(15),
        child:Container(
        height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 80:100:120,


        child:  Theme(data: new ThemeData(
            hintColor: Colors.black26
        ), child: TextField(
          maxLines: 4,
          controller: remarkcontroller,

          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black26, width: 0.5),
            ),
            hintText: 'Enter remarks',
          ),

          onChanged: (text) {
            //TemRegData.email=text;

          },
        )),
      )),


      Align(

        alignment: Alignment.topCenter,
        child:   Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12):EdgeInsets.all(14),
          child:  Container(
            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xFF006b6b),
                borderRadius: BorderRadius.circular(10)),
            child: TextButton(
              onPressed: () async {

                // String datetxt1 = "Select start date";
                // String datetxt2 = "Select end date";
               // String subjectdata = "Select asset account";

                if(datetxt.compareTo("Select purchase date")!=0)
                  {

                    if(subjectdata.compareTo("Select asset account")!=0)
                      {


                        List<Map<String,dynamic>>rminddates=[];

                            for(int i=0;i<reminddates.length;i++)
                              {
                                Map<String,dynamic> mjobject=new Map();
                                RemindDate rmdate=reminddates[i];
                                if(rmdate.date.compareTo("")!=0)
                                  {
                                    mjobject['reminddate']=rmdate.date;

                                  }
                                else{
                                  mjobject['reminddate']="";
                                }
                                if(rmdate.description.compareTo("")!=0)
                                {
                                  mjobject['description']=rmdate.description;

                                }
                                else{
                                  mjobject['description']="";
                                }
                                //var js=json.encode(mjobject);
                                rminddates.add(mjobject);
                              }





                        Map<String,dynamic> assetdata=new Map();
                        assetdata['name']=accountid;
                        assetdata['amount']=amountcontroller.text.toString();
                        assetdata['purchase_date']=datetxt;
                        assetdata['remind_date']=rminddates;
                        assetdata['remarks']=remarkcontroller.text.toString();
                        assetdata['documents']=uploadeddoc;

                        var js=json.encode(assetdata);
                        Map<String, dynamic> data_To_Table=new Map();
                        data_To_Table['data']=js.toString();

                        if(assetid.compareTo("0")!=0) {

                          dbhelper.update(data_To_Table, DatabaseTables.TABLE_ASSET, assetid);
                        }
                        else {
                          dbhelper.insert(
                              data_To_Table, DatabaseTables.TABLE_ASSET);
                        }

Navigator.pop(context,{"accountsetupdata":1});





                      }
                    else{

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Select asset account"),
                      ));
                    }






                  }
                else{

                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text("Select purchase date"),
                  ));
                }








                // showData();

              },
              child: Text(
                'Submit',
                style:
                TextStyle(color: Colors.white, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
              ),
            ),
          ),
        ),),



    ]

    ))])






    );

  }





showAllAssetDocument()
{





  showDialog(
      context: context,
      builder: (_) {
        return MyDocumentDialog(uploadeddoc);
      }).then((value) => {



    uploadeddocfromdialog=value["content"],



    refresh(uploadeddocfromdialog)









  });









}

void refresh(List<Map<String,dynamic>>doc)
{

  if(doc.length>0)
    {
      setState(() {

      //  uploadeddoc.clear();
        uploadeddoc=doc;
      });
    }
  else{
    setState(() {

      uploadeddoc.clear();

    });

  }


}














  void uploadFile(String path,String title)async
  {

    print(path);

    final dbHelper = new DatabaseHelper();


    File file=new File(path);

    final GoogleSignIn googleSignIn = GoogleSignIn( scopes: [
      'email',
      'https://www.googleapis.com/auth/drive.file'

    ],clientId: "458214687015-el4rrn8pke6c6g6e04oglgu5a4eb6jsu.apps.googleusercontent.com" );

    final GoogleSignInAccount? googleSignInAccount =
    await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount!.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );



    var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    var dri = drive.DriveApi(client);
    drive.File fileToUpload = drive.File();

    fileToUpload.parents = ["root"];

    var response = await dri.files.create(
      fileToUpload,
      uploadMedia: drive.Media(file.openRead(), file.lengthSync()),
    );


    print(response.id);


    Map<String,dynamic> mjobject=new Map();
    mjobject['name']=title;
    mjobject['fileid']=response.id;
    mjobject['filename']=file.path.split('/').last;

   // var js=json.encode(mjobject);



    setState(() {


      uploadeddoc.add(mjobject);
    });






  // getFileData();



  }


  void setupAccountData() async {
    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Amount ="";
    accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      String Accounttype = jsondata["Accounttype"];
       Amount = jsondata["Amount"];
      String Type = jsondata["Type"];

      if(Accounttype.compareTo("Asset account")==0)
      {
        accountsetupdata.add(Accountname);
      }

    }


    setState(() {

      subject.clear();

      subjectdata=accountsetupdata[0];
      amountcontroller.text="0";

      subject.addAll(accountsetupdata);

    });
  }





  setDataByAccountName(String name) async
  {

    List<Map<String, dynamic>> a =
        await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Amount ="0";
    String accid="0";
    accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      if(Accountname.compareTo(name)==0) {
        String Accounttype = jsondata["Accounttype"];
        Amount = jsondata["Amount"];
        accid=id.toString();
      }
    }
      setState(() {



        accountid=accid;

        amountcontroller.text=Amount;



      });

  }




  getAssetById(String id) async
  {

    List<Map<String, dynamic>> a =
        await dbhelper.getDataByid(DatabaseTables.TABLE_ASSET,id);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));

    var ab=a[0];


     // print(ab);

      int dataid = ab["keyid"];
      String data=ab["data"];

    var jsondata = jsonDecode(data);


    String idacc=jsondata['name'];

    setState(() {

      accountid=idacc;
    });


    var v1 = await dbhelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, idacc);

    List<Map<String, dynamic>> ab1 = v1;

    Map<String, dynamic> mapdata1 = ab1[0];
    String d1 = mapdata1['data'];
    var jsd = jsonDecode(d1);

    String Accountname = jsd["Accountname"];



    for(String ah in subject)
      {

        if(ah.compareTo(Accountname)==0)
          {


            setState(() {

subjectdata=        ah;

            });

            break;

          }


      }










    String amount=jsondata['amount'];
    String purchase_date=jsondata['purchase_date'];



    // String aa=jsondata['remind_date'].toString();
    //
    // String documents=jsondata['documents'].toString();

    //var abj=jsonEncode(aa);

    String d = json.encode(jsondata['documents']);

    print(d);


    setState(() {

      datetxt=purchase_date;
      amountcontroller.text=amount;









    });






    var j = jsonDecode(d);


    List<dynamic>? tags = j != null ? List.from(j) : null;



    List<Map<String,dynamic>>doc=[];


    for (var b in tags! )
      {

       // String name=b['name'];
       // String fileid=b['fileid'];
       // String filename=b['filename'];

        doc.add(b);

      }

    setState(() {

      uploadeddoc.addAll(doc);
    });



    String rmdatedata = json.encode(jsondata['remind_date']);

    var j1 = jsonDecode(rmdatedata);


    List<dynamic>? tags1 = j1 != null ? List.from(j1) : null;


    List<RemindDate>rmdatedoc=[];

    for (var b in tags1! )
    {

      RemindDate remindDate=new RemindDate();
      remindDate.description=b['description'];
      remindDate.date=b['reminddate'];

      // String name=b['name'];
      // String fileid=b['fileid'];
      // String filename=b['filename'];

      rmdatedoc.add(remindDate);

    }

setState(() {

  reminddates.addAll(rmdatedoc);
});




    // final Map<String, dynamic> rmd = json.decode(jsondata['remind_date']);
    // print(rmd);
    //
    //
    // var docj=jsonEncode(documents);
    //
    // print(docj);








  }


  Future<String?> filePickThroughdocument()async
  {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    File? file=null ;

    if (result != null) {
      file = File(result.files.single.path!);

      // print(file.path);
      //
      //




    } else {
      // User canceled the picker
    }


    return file!.path;
  }




  Future<String?> filePickThroughGallery() async
  {
    final ImagePicker _picker = ImagePicker();
    // Pick an image
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    CroppedFile? croppedFile =null;
    // print(image!.path) ;

    if(image!=null) {
      croppedFile = await ImageCropper()
          .cropImage(
          sourcePath: image.path,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
            CropAspectRatioPreset.ratio3x2,
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.ratio4x3,
            CropAspectRatioPreset.ratio16x9
          ],
          // androidUiSettings: AndroidUiSettings(
          //     toolbarTitle: 'Cropper',
          //     toolbarColor: Colors.deepOrange,
          //     toolbarWidgetColor: Colors.white,
          //     initAspectRatio: CropAspectRatioPreset
          //         .original,
          //     lockAspectRatio: false),
          // iosUiSettings: IOSUiSettings(
          //   minimumAspectRatio: 1.0,
          // )
      );
    }

    return croppedFile!.path;

    //return null;

    //  uploadImage(croppedFile);

    // final GoogleSignIn googleSignIn = GoogleSignIn();
    //
    // final GoogleSignInAccount googleSignInAccount =
    //     await googleSignIn.signIn();
    //
    // // final GoogleSignInAuthentication googleSignInAuthentication =
    // // await googleSignInAccount.authentication;
    // //
    // // final AuthCredential credential = GoogleAuthProvider.credential(
    // //   accessToken: googleSignInAuthentication.accessToken,
    // //   idToken: googleSignInAuthentication.idToken,
    // // );
    //
    //
    //
    // var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    // var dri = drive.DriveApi(client);
    // drive.File fileToUpload = drive.File();
    //
    // fileToUpload.parents = ["root"];
    //
    // var response = await dri.files.create(
    //   fileToUpload,
    //   uploadMedia: drive.Media(croppedFile!.openRead(), croppedFile!.lengthSync()),
    // );
    //
    //
    // print(response.id);




  }




}

class GoogleHttpClient extends http.BaseClient {
  Map<String, String> _headers;
  GoogleHttpClient(this._headers) : super();



  final http.Client _client = new http.Client();



  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return _client.send(request..headers.addAll(_headers));
  }


}


class MyDocumentDialog extends StatefulWidget{

  List<Map<String,dynamic>> uploadedDoc=[];


  MyDocumentDialog(this.uploadedDoc);

  @override
  _MyDocumentDialogState createState() => new _MyDocumentDialogState(uploadedDoc);



}
class _MyDocumentDialogState extends State<MyDocumentDialog> {

  List<Map<String,dynamic>> uploadeddoc=[];


  _MyDocumentDialogState(this.uploadeddoc);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Widget okButton = TextButton(
      child: Text("OK",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
      onPressed: () {

        Navigator.pop(context,{"content":uploadeddoc});


      },
    );



    return AlertDialog(
      actions: [
        okButton,
      ],
    content: Container(
      height: MediaQuery.of(context).size.height, // Change as per your requirement
      width:  MediaQuery.of(context).size.width, // Change as per your requirement
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: uploadeddoc.length,
        itemBuilder: (BuildContext context, int index) {

          Map<String, dynamic> data_To_Table=uploadeddoc[index];



          String title = data_To_Table["name"];


          String fileid=data_To_Table['fileid'];
          String filename=data_To_Table['filename'];




          return Container(
              width:  MediaQuery.of(context).size.width,
              height: 130,

              child:Card(

                elevation: 7,


                child: Column(

                  children: [


                    Padding(

                      padding: EdgeInsets.all(10),
                      child:      Text(title,style: TextStyle(fontSize: 15,color: Colors.black),),


                    ),



                    Divider(
                      thickness: 1,
                      color: Colors.black26,
                    ),
                    Padding(
                        padding: EdgeInsets.all(2),
                        child: Row(
                          children: [
                            Expanded(
                              child: TextButton(
                                onPressed: () async {





                                  downloadFileData( fileid, filename);




                                },
                                child: Text(
                                  "Download",
                                  style: TextStyle(
                                    fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18,
                                      color: Colors.lightGreen),
                                ),
                              ),
                              flex: 2,
                            ),
                            Container(
                              width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?1:1.1:1.3,
                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:50:60,
                              color: Colors.grey,
                            ),
                            Expanded(
                              child: TextButton(
                                onPressed: () {

                                  deleteFileData( fileid, filename,index);

                                //  Navigator.pop(context);

                                },
                                child: Text(
                                  "Delete",
                                  style: TextStyle(
                                    fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19,
                                      color: Colors.redAccent),
                                ),
                              ),
                              flex: 2,
                            ),
                          ],
                        ))









                  ],





                ),

              )

          );
        },
      ),
    ) ,

    );
  }

  void downloadFileData(String fileid,String filename) async
  {
    final GoogleSignIn googleSignIn = GoogleSignIn( scopes: [
      'email',
      'https://www.googleapis.com/auth/drive.file'

    ],clientId: "458214687015-el4rrn8pke6c6g6e04oglgu5a4eb6jsu.apps.googleusercontent.com");

    final GoogleSignInAccount? googleSignInAccount =
    await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount!.authentication;

    // final AuthCredential credential = GoogleAuthProvider.credential(
    //   accessToken: googleSignInAuthentication.accessToken,
    //   idToken: googleSignInAuthentication.idToken,
    // );
    var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    var dri = drive.DriveApi(client);
    drive.Media file = await dri.files.get( fileid, downloadOptions: drive.DownloadOptions.fullMedia) as drive.Media;

    // var response = await dri.files.delete(fileId)

    final directory = await getApplicationDocumentsDirectory();
    print(directory.path);
    final saveFile = File('${directory.path}/${new DateTime.now().millisecondsSinceEpoch}$filename');

    List<int> dataStore = [];




    file.stream.listen((data) {
      print("DataReceived: ${data.length}");
      dataStore.insertAll(dataStore.length, data);
    }, onDone: () {
      print("Task Done");
      saveFile.writeAsBytes(dataStore);
      print("File saved at ${saveFile.path}");

      // Share.shareFiles([saveFile.path],
      //     text: "File from google drive");

      ShareExtend.share(saveFile.path, "file");



    }, onError: (error) {
      print("Some Error");
    });





  }


  void deleteFileData(String fileid,String filename,int p) async{
    final dbHelper = new DatabaseHelper();
    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount? googleSignInAccount =
    await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount!.authentication;

    // final AuthCredential credential = GoogleAuthProvider.credential(
    //   accessToken: googleSignInAuthentication.accessToken,
    //   idToken: googleSignInAuthentication.idToken,
    // );
    var client = GoogleHttpClient(await googleSignInAccount!.authHeaders);
    var dri = drive.DriveApi(client);
    await dri.files.delete(fileid);


    setState(() {

      uploadeddoc.removeAt(p);
    });








  }

}





class MyFileOptionsDialog extends StatefulWidget {





  @override
  _MyDialogState createState() => new _MyDialogState();


}


class _MyDialogState extends State<MyFileOptionsDialog> {



  int galleryOrFile=0;

  int gallery=1;
  int file=2;





  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AlertDialog(
        content: Container(
          height: 200.0,
          width: 400.0,

          child: Row(

            children: [

              Expanded(child: Column(

                children: [

                  GestureDetector(

                    onTap: (){
                      galleryOrFile=file;
                      Navigator.pop(context,{'option':galleryOrFile});


                    },

                    child:  Image.asset('images/folder.png',width: 50,height: 50,fit: BoxFit.fill,),



                  ),


                  GestureDetector(

                      onTap: (){
                        galleryOrFile=file;
                        Navigator.pop(context,{'option':galleryOrFile});

                      },

                      child:   Padding(padding: EdgeInsets.all(5),child: Text("File",style: TextStyle(fontSize: 13,color: Colors.black),),)



                  )








                ],


              ),flex: 1,),

              Expanded(child: Column(

                children: [


                  GestureDetector(


                    onTap: (){

                      galleryOrFile=gallery;

                      Navigator.pop(context,{'option':galleryOrFile});
                    },

                    child:  Image.asset('images/gallery.png',width: 50,height: 50,fit: BoxFit.fill,),
                  ),

                  GestureDetector(

                    onTap: (){

                      galleryOrFile=gallery;


                      Navigator.pop(context,{'option':galleryOrFile});



                    },

                    child:  Padding(padding: EdgeInsets.all(5),child: Text("Gallery",style: TextStyle(fontSize: 13,color: Colors.black),),) ,




                  )






                ],


              ),flex: 1,)





            ],


          ),

        )

    );
  }





}
