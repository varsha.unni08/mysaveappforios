import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/database/DBTables.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;

import '../database/DatabaseHelper.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import 'AddAccountSetup.dart';


class BudgetEditpage extends StatefulWidget {
  final String title;

  final String id;


  const BudgetEditpage(
      {Key? key, required this.title, required this.id})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _BudgetEditpage(id);
}

class _BudgetEditpage extends State<BudgetEditpage> {

  String date = "";
  String date1 = "";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Month", "Amount", "Action"];

  DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<Cashbankaccount> cashbankdata = [];

  String dropdownyear = "";

  List<String>yeardatadropdown = [];

  TextEditingController amountcontroller = new TextEditingController();

  List<String> arrmonth = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  final dbHelper = new DatabaseHelper();

  List<String>expenseaccount = [];

  List<String>tableaccount = [];

  String expense = "";

   String id="0";


  _BudgetEditpage(this.id);

  @override
  void initState() {
    // TODO: implement initState

     setupYearDataForDropDown();
     setupAccountData();

     SystemChrome.setPreferredOrientations([
       DeviceOrientation.portraitDown,
       DeviceOrientation.portraitUp,
     ]);
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),

        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Edit Budget"),
        centerTitle: false,
      ),

      body: Stack(
        children: [

        Padding(padding: EdgeInsets.fromLTRB(5, 15, 5, 0)

        ,child:Row(children: [
              SizedBox(

                  width: 270,
                  child:Padding(padding: EdgeInsets.all(10),
                    child: Container(
                      height: 60,

                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black38)),

                      child:  DropdownButtonHideUnderline(

                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: InputDecorator(
                            decoration: const InputDecoration(border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(

                                isExpanded: true,
                                value: expense,
                                items: expenseaccount
                                    .map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    expense = newValue!;

                                    // getBudgetData();




                                  });
                                },
                                style: Theme.of(context).textTheme.bodyText1,

                              ),
                            ),
                          ),
                        ),
                      ),


                    ),


                  )





              ),
              Padding(
                  padding: EdgeInsets.all(12),

                  child: FloatingActionButton(
                    onPressed: () async{


                      Map results = await Navigator.of(context)
                          .push(new MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) {
                          return new AddAccountSettinglistpage(title: "account setup",accountType: "Expense account", accountsetupid: '0',);
                        },
                      ));

                      if (results != null &&
                          results.containsKey('accountsetupdata')) {
                        setState(() {
                          var accountsetupdata =
                          results['accountsetupdata'];

                          int acs =
                          accountsetupdata as int;
                          //
                          // if(acs>0)
                          //   {
                          setupAccountData();
                          //  }


                        });
                      }

                    },
                    child: Icon(Icons.add, color: Colors.white, size: 29,),
                    backgroundColor: Colors.blue,

                    elevation: 5,
                    splashColor: Colors.grey,
                  ))

            ],)) ,

          Padding(padding: EdgeInsets.fromLTRB(5, 15, 5, 0)

              ,child:
                SizedBox(


                    child:Padding(
                      padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
                      child: Row(
                        textDirection: TextDirection.ltr,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Container(

                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  controller: amountcontroller,
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    hintText: 'Amount',
                                  ),
                                ),
                              ),
                            ),
                            flex: 3,
                          ),

                        ],
                      ),
                    ),





                ),


             ) ,
          Padding(
              padding: const EdgeInsets.fromLTRB(5, 170, 5, 10),
              child:Container(
                width: double.infinity,
                height: 60.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                    // red as border color
                  ),
                ),
                child: DropdownButtonHideUnderline(

                  child: ButtonTheme(
                    alignedDropdown: true,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder()),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(

                          isExpanded: true,
                          value: dropdownyear,
                          items: yeardatadropdown
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              dropdownyear = newValue!;

                              getBudgetData();



                            });
                          },
                          style: Theme.of(context).textTheme.bodyText1,

                        ),
                      ),
                    ),
                  ),
                ),
              )),

          Align(

            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 250, 5, 0),
              child: Container(
                height: 50,
                width: 150,
                decoration: BoxDecoration(
                    color: Color(0xF0233048),
                    borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                  onPressed: () async{

                    Map<String, dynamic> b=await getBudgetData();

                    String accountid=b['accountname'].toString();

                    String month=b['month'].toString();
                    int setupid=await getAccountIndex(expense);

                    var mjobject=new Map();
                    mjobject['year']=dropdownyear;
                    mjobject['month']=month;
                    mjobject['amount']=amountcontroller.text;
                    mjobject['accountname']=setupid;

                    var js=json.encode(mjobject);

                    Map<String, dynamic> data_To_Table=new Map();
                    data_To_Table['data']=js.toString();
                    int insertedid = await dbHelper.update(data_To_Table,DatabaseTables.TABLE_BUDGET,id.toString());

                    Navigator.of(context).pop({'updated':1 });






                  },
                  child: Text(
                    'Update',
                    style:
                    TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
              ),
            ),),

          Align(

            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 320, 5, 0),
              child: Container(
                height: 50,
                width: 150,
                decoration: BoxDecoration(
                    color: Color(0xF0233048),
                    borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                  onPressed: () async{


                 await dbHelper.deleteDataByid(id,DatabaseTables.TABLE_BUDGET);

                    Navigator.of(context).pop({'updated':1 });






                  },
                  child: Text(
                    'Delete',
                    style:
                    TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
              ),
            ),),

        ],


      ),
    );

  }

  void setupAccountData() async {

    String account="";
    List<Map<String, dynamic>> a =
    await dbHelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);



    List<String> accountsetupdata = [];


    for (int i=0;i< a.length;i++) {

Map ab=a[i];
      String id = ab["keyid"].toString();
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      String Accounttype = jsondata["Accounttype"];
      String Amount = jsondata["Amount"];
      String Type = jsondata["Type"];

      if(Accounttype.compareTo("Expense account")==0)
      {

        accountsetupdata.add(Accountname);
      }

    }


    Map<String, dynamic> b=await getBudgetData();

    String accountid=b['accountname'].toString();

    String amount=b['amount'].toString();

    var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, accountid);


    List<Map<String, dynamic>> accountsettings=value;

    Map ab=accountsettings[0];
    int id = ab["keyid"];
    String data = ab["data"];

    var jsondata = jsonDecode(data);

    String Accountname = jsondata["Accountname"];

    for(int j=0;j<accountsetupdata.length;j++)
      {

        if(accountsetupdata[j].compareTo(Accountname)==0)
          {
            account=accountsetupdata[j];
            break;
          }

      }







    setState(() {

      expenseaccount.clear();

      expense=account;
      amountcontroller.text=amount;

      expenseaccount.addAll(accountsetupdata);
    });
  }

  void setupYearDataForDropDown()async
  {
    var now=DateTime.now();
    int y=now.year;

    List<String>yeardata=[];


    for(int i=0;i<6;i++)
    {
      int y1=y+i;
      yeardata.add(y1.toString());

    }


    Map<String, dynamic> b=await getBudgetData();

    String year=b['year'].toString();



    for(String a in yeardata)
      {
        if(year.compareTo(a)==0)
          {
            y=int.parse(a);
            break;
          }
      }





    setState(() {

      yeardatadropdown.clear();
      yeardatadropdown.addAll(yeardata);

      dropdownyear=y.toString();

    });



  }

  Future<Map<String, dynamic>> getBudgetData()async{

    var v = await dbHelper.getDataByid(
        DatabaseTables.TABLE_BUDGET, id.toString());

    List<Map<String, dynamic>> ab = v;

    Map<String, dynamic> mapdata = ab[0];
    String d = mapdata['data'];
    Map<String, dynamic> map = jsonDecode(d);

    return map;

   // return null;



  }


  Future<int> getAccountIndex(String accountname)
  async {

    int id =0;
    List<Map<String, dynamic>> a =
    await dbHelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    for (Map ab in a) {
      print(ab);


      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];


      if(Accountname.compareTo(accountname)==0)
      {
        id = ab["keyid"];
        break;

      }

    }

    return id;
  }

}