import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/design/ResponsiveInfo.dart';
import 'package:saveappforios/utils/Tutorials.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
//

// import 'package:horizontal_data_table/horizontal_data_table.dart';


import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../domain/Paymentdata.dart';
import '../projectconstants/DataConstants.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';





class PaymentPage extends StatefulWidget {
  final String title;

  const PaymentPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PaymentPage();
}

class _PaymentPage extends State<PaymentPage> {

  String date="",month="",year="";

  String datetxt="Select date";

  List<String>accountsdata=[];

  List<DataRow>accountsdatarow=[];

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";

  List<String>tableheaddata = [
    "Date",
    "Account name",
    "Amount",
    "Cash/Bank",
    "Action"
  ];


  List<DataColumn>tableheadings=[];

  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;
  String currentmonth="";

  int currentmonthindex=0;

  String selectedmonth="",selectedyear="";

  List<PaymentData>allpaymentdata=[];


  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();



    selectedmonth=now.month.toString();
    selectedyear=now.year.toString();

    showAccountDetails();
    Tutorial.showTutorial(Tutorial.paymenttutorial, context, Tutorial.paymentvouchertutorial);

   // loadTableHeading();

    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          toolbarHeight: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?60:80:100,
            backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Payment",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:17:22),),
          centerTitle: false,
        ),




        body: Stack(


    children: [


    Column(

          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [

       SizedBox(
         height:
         ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?160:190:250,

         child:
         Stack(

           children: [

             Align(

               alignment: FractionalOffset.topCenter,
               child: Padding(

                 padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5) :EdgeInsets.fromLTRB(15, 10, 15, 10) :EdgeInsets.fromLTRB(20, 15, 20, 15),
                 child:   Container(child:      Container(
                   decoration: BoxDecoration(
                       border: Border.all(color: Colors.black38)),
                   child: Row(
                     textDirection: TextDirection.rtl,
                     children: <Widget>[

                       Expanded(child:

                       InkWell(
                         child:Padding(
                             padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(25),
                             child: Icon(
                               Icons.calendar_month,
                               color: Colors.black54,
                               size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:35:45,

                             )) ,
                         onTap: (){
                           var now = DateTime.now();

                           date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                           month=now.month.toString();
                           year=now.year.toString();

                           int monthnumber=int.parse(month);
                           currentmonthindex=monthnumber-1;

                           currentmonth=months[monthnumber-1];
                           currentyear=year;




                           showDialog(
                               context: context,
                               builder: (_) {
                                 return MyDialog();
                               }).then((value) => {


                             selectedmonth=value["selecteddata"].toString().split(",")[0],
                             selectedyear=value["selecteddata"].toString().split(",")[1],






                             showAccountDetails()




                           });
                         },

                       )


                         ,flex: 1, )
                       ,
                       Expanded(child:  Container(
                           child: TextButton(
                             onPressed: () async {
                               var now = DateTime.now();

                               date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                               month=now.month.toString();
                               year=now.year.toString();

                               int monthnumber=int.parse(month);
                               currentmonthindex=monthnumber-1;

                               currentmonth=months[monthnumber-1];
                               currentyear=year;




                               showDialog(
                                   context: context,
                                   builder: (_) {
                                     return MyDialog();
                                   }).then((value) => {


                                 selectedmonth=value["selecteddata"].toString().split(",")[0],
                                 selectedyear=value["selecteddata"].toString().split(",")[1],






                                 showAccountDetails()




                               });


                             },
                             child: Text(
                               datetxt,
                               style: TextStyle(
                                   color: Colors.black38, fontSize: 12),
                             ),
                           )),flex: 2,  )
                     ],
                   ),
                 )) ,
               ),
             ),

             Align(

               alignment: FractionalOffset.bottomCenter,
               child:   (accountsdata.length>0)?

               SizedBox(

                 width: double.infinity,


                 child:

                 Padding(

                     padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 0) :EdgeInsets.fromLTRB(15, 10, 15, 0) :EdgeInsets.fromLTRB(20, 15, 20, 0),

                     child:    MediaQuery.removePadding(
                       removeBottom: true,
                       context: context,
                       removeTop: true, child:GridView.builder(
                       primary: false,


                       physics: ScrollPhysics(),

                       shrinkWrap: true,
                       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                           crossAxisCount: 5,
                           crossAxisSpacing: 0.0,
                           mainAxisSpacing: 0.0,
                           childAspectRatio: 1

                       ),
                       itemCount: tableheaddata.length,
                       itemBuilder: (context, index) {
                         return  Container(

                             decoration: BoxDecoration(
                               border: Border.all(
                                 color: Colors.black54,
                                 width: 0.3,
                               ),
                             ),

                             child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),maxLines: 2,)));


                       },
                     )
                       ,) ),
               )





                   : Container(

                   width: double.infinity,
                   height: double.infinity,

                   child:   Stack(



                     children: [

                       Align(

                         alignment: FractionalOffset.center,

                         child: Padding(
                           padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.only(left:10.0,right: 10.0,top:10,bottom: 0):EdgeInsets.only(left:15.0,right: 15.0,top:15,bottom: 0):EdgeInsets.only(left:18.0,right: 18.0,top:18,bottom: 0),
                           // padding: EdgeInsets.all(15),
                           child: new Theme(data: new ThemeData(
                               hintColor: Colors.white
                           ), child: Text(

                             "No data found", style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?12:15:18 ),



                           )),
                         ),
                       )



                     ],


                   )
               ),
             )


           ],
         ),


       )









           ,






          ],

      )

      ,





        Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 160, 10, 0) :EdgeInsets.fromLTRB(15, 190, 15, 0) :EdgeInsets.fromLTRB(20, 250, 20, 0),

          child:
      SingleChildScrollView(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [

             MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child:GridView.builder(
                  physics: ScrollPhysics(),
                  primary: false,
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 5,
                      crossAxisSpacing: 0.0,
                      mainAxisSpacing: 0.0,
                      childAspectRatio:1

                  ),
                  itemCount: accountsdata.length,
                  itemBuilder: (context, index) {
                    return Container(

                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black54,
                          width: 0.3,
                        ),
                      ),

                      child:

                      ((index+1)%5==0)?
                      Center(child:TextButton(onPressed: ()async {

                        print(accountsdata[index].split(",")[0]);
                        Map results = await Navigator.of(context)
                            .push(new MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) {
                            return new AddPaymentPage(
                              title: "Payment",accountsid: accountsdata[index].split(",")[0],



                            );
                          },
                        ));

                        if (results != null &&
                            results.containsKey('accountadded')) {


                          int code=results['accountadded'];
                          if(code==1)
                          {

                            showAccountDetails();


                          }
                        }

                      },
                          child:Text(accountsdata[index].split(",")[1],style: TextStyle(fontSize: 13,color: Colors.deepOrange),)))
                          :Center(child:Text(accountsdata[index],style: TextStyle(fontSize: 13,color: Colors.black),)),

                    );
                  },
                ))

           ,




          ],
        ),
      ))







          ,




      Align(alignment: Alignment.bottomRight,

          child:Padding(
              padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(12) :EdgeInsets.all(19) :EdgeInsets.all(25),

              child: FloatingActionButton(
                onPressed: () async{



    Map results = await Navigator.of(context)
        .push(new MaterialPageRoute<dynamic>(
    builder: (BuildContext context) {
    return new AddPaymentPage(
    title: "Payment", accountsid: "0",
    );
    },
    ));

    if (results != null &&
    results.containsKey('accountadded')) {


      int code=results['accountadded'];
      if(code==1)
        {

          showAccountDetails();


    }
    }

                },
                child: Icon(Icons.add, color: Colors.white, size: 29,),
                backgroundColor: Colors.blue,

                elevation: 5,
                splashColor: Colors.grey,
              ))),
    ],







        ));
  }












  void showAccountDetails() async
  {
    List<String>tabledata=[];

    List<PaymentData>paymentdata=[];

    var v ;

      v = await new DatabaseHelper().getAccountDataByMonthYear(
          selectedmonth,selectedyear);

      setState(() {

        datetxt=selectedmonth+"/"+selectedyear;
      });



      List<Map<String, dynamic>> m=v;

      for(int i=0;i<m.length;i++)
        {

          PaymentData data_payment=new PaymentData();
          Map<String, dynamic> mapval=m[i];

          int aid=mapval[DatabaseTables.ACCOUNTS_id];

          int vouchertype=mapval[ DatabaseTables.ACCOUNTS_VoucherType];




            if (mapval[DatabaseTables.ACCOUNTS_entryid] == '0') {
              if(vouchertype==DataConstants.paymentvoucher) {
              tabledata.add(mapval[DatabaseTables.ACCOUNTS_date]);

              data_payment.date = mapval[DatabaseTables.ACCOUNTS_date];

              String accid = mapval[DatabaseTables.ACCOUNTS_setupid];

              var value = await new DatabaseHelper().getDataByid(
                  DatabaseTables.TABLE_ACCOUNTSETTINGS, accid);


              List<Map<String, dynamic>> accountsettings = value;
              if (accountsettings.length > 0) {
                Map<String, dynamic> m_acc = accountsettings[0];
                String data = m_acc['data'];

                var js = jsonDecode(data);

                tabledata.add(js['Accountname']);
                data_payment.debitaccount = js['Accountname'];
              }


              tabledata.add(mapval[DatabaseTables.ACCOUNTS_amount]);
              data_payment.amount = mapval[DatabaseTables.ACCOUNTS_amount];

              int accountid = mapval[DatabaseTables.ACCOUNTS_id];

              var varcredit = await new DatabaseHelper().getAccounDataByEntryid(
                  accountid.toString());


              List<Map<String, dynamic>> AccountPaymentcredit = varcredit;
              if (accountsettings.length > 0) {
                Map<String, dynamic> m = AccountPaymentcredit[0];

                String acccreditsetting = m[DatabaseTables.ACCOUNTS_setupid];

                var accountsettingcredit = await new DatabaseHelper()
                    .getDataByid(
                    DatabaseTables.TABLE_ACCOUNTSETTINGS, acccreditsetting);

                List<Map<String,
                    dynamic>> accountsettings = accountsettingcredit;
                if (accountsettings.length > 0) {
                  Map<String, dynamic> m_acc = accountsettings[0];
                  String data = m_acc['data'];

                  var js = jsonDecode(data);

                  tabledata.add(js['Accountname']);
                  data_payment.creditaccount = js['Accountname'];
                }
              }
              tabledata.add(aid.toString() + "," + "Edit/Delete");
              data_payment.action = "Edit/Delete";
              paymentdata.add(data_payment);
            }
          }

        }

      setState(() {

        if(accountsdata.length>0)
          {
            accountsdata.clear();

            allpaymentdata.clear();
          }

allpaymentdata.addAll(paymentdata);



        List<DataRow>dt=[];

        accountsdata.addAll(tabledata);
      });














  }










}


class MyDialog extends StatefulWidget {



  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

   // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    month=now.month.toString();
    year=now.year.toString();



    setState(() {
      int monthnumber=int.parse(month);
      currentmonthindex=monthnumber-1;

      currentmonth=months[monthnumber-1];
      currentyear=year;

      currentyearnumber=int.parse(currentyear);
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: 300.0,
        width: 600.0,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: EdgeInsets.all(15.0),
              child: Container(
                  width: 150,

                  child:Row(

                    children: [

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {

                            if(currentmonthindex>0) {
                              currentmonthindex =
                                  currentmonthindex -1;
                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });
                            }



                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child: Text(currentmonth,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {

                            if(currentmonthindex<11) {
                              currentmonthindex =
                                  currentmonthindex + 1;

                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });

                            }



                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {


                            currentyearnumber =
                                  currentyearnumber - 1;

                              setState(() {
                              currentyear=currentyearnumber.toString();
                              });




                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child:  Text(currentyear,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {
                            currentyearnumber =
                                currentyearnumber + 1;

                            setState(() {
                              currentyear=currentyearnumber.toString();
                            });
                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),


                    ],


                  )


              ),
            ),







            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      int ab=currentmonthindex+1;


                      String currentyearmonth=ab.toString()+","+currentyearnumber.toString();



                    //  Navigator.pop(context,'selecteddata':currentyearmonth);

                      Navigator.of(context).pop({'selecteddata':currentyearmonth});


                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),


            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
      // actions: <Widget>[
      //   FlatButton(
      //       child: Text('Switch'),
      //       onPressed: () => setState(() {
      //         _c == Colors.redAccent
      //             ? _c = Colors.blueAccent
      //             : _c = Colors.redAccent;
      //       }))
      // ],
    );
  }





}
