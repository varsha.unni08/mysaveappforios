import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'dart:io';

import '../design/ResponsiveInfo.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'login',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: FilePickOptionsPage(title: 'login',code: 0,),
      debugShowCheckedModeBanner: false,
    );
  }
}

class FilePickOptionsPage extends StatefulWidget {
  final String title;
  final int code;

  const FilePickOptionsPage({Key? key, required this.title,required this.code}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FilePage(code);
}

class _FilePage extends State<FilePickOptionsPage> {

  int code=0;

  _FilePage(this.code);

  int galleryOrFile=0;

  int gallery=1;
  int file=2;
  String filepath="";

  TextEditingController titleController=new TextEditingController();


  TextEditingController namecontroller=new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: true,

      body: Container(

        width: double.infinity,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(15.0) : EdgeInsets.all(18.0)  :EdgeInsets.all(25.0),
              child: (code==0)? Text("You are going to upload your important documents to your Google Drive.We will keep this file credentials safe and secure.If you are not intrested  press 'Cancel' button",maxLines: 10,style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  12:14:16,color: Colors.black),)
           :Text("You are going to upload your important Asset's documents to your Google Drive.We will keep this file credentials safe and secure.If you are not intrested  press 'Cancel' button",maxLines: 10,style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  12:14:16,color: Colors.black),),



    ),


            Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(15.0) : EdgeInsets.all(18.0)  :EdgeInsets.all(25.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(

                keyboardType: TextInputType.text,
                controller: titleController,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Enter the title',


                ),
              )),
            ),

            Padding(

              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10.0) : EdgeInsets.all(15.0)  :EdgeInsets.all(20.0),


              child: Row(

                children: [

                  Expanded(child: Column(

                    children: [

                      GestureDetector(

                        onTap: (){
                          galleryOrFile=file;
                          //  Navigator.pop(context,{'option':galleryOrFile});
                          filePickThroughdocument().then((value) => {

                            filepath=value.toString()

                          });

                        },

                        child:  Image.asset('images/folder.png',width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,fit: BoxFit.fill,),



                      ),


                      GestureDetector(

                          onTap: (){
                            galleryOrFile=file;
                            // Navigator.pop(context,{'option':galleryOrFile});

                            filePickThroughdocument().then((value) => {

                              filepath=value.toString()




                            });

                          },

                          child:   Padding(padding: EdgeInsets.all(5),child: Text("File",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:16:19,color: Colors.black),),)



                      )








                    ],


                  ),flex: 1,),

                  Expanded(child: Column(

                    children: [


                      GestureDetector(


                        onTap: (){

                          galleryOrFile=gallery;

                          filePickThroughGallery().then((value) => {




                            setState(() {
                              filepath=value.toString();
                            })



                          });

                          // Navigator.pop(context,{'option':galleryOrFile});
                        },

                        child:  Image.asset('images/gallery.png',width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,fit: BoxFit.fill,),
                      ),

                      GestureDetector(

                        onTap: (){

                          galleryOrFile=gallery;

                          filePickThroughGallery().then((value) => {


                          setState(() {
                          filepath=value.toString();
                          })





                          });


                          // Navigator.pop(context,{'option':galleryOrFile});



                        },

                        child:  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15) ,


                          child: Text("Gallery",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:16:19,color: Colors.black),),) ,




                      )






                    ],


                  ),flex: 1,)





                ],


              ),),






            Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(8) : EdgeInsets.all(12) :EdgeInsets.all(14),

              child:Text(filepath,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),) ,),

            Padding(
              padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10.0) : EdgeInsets.all(20.0):EdgeInsets.all(25.0),

              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,

                children: [
                  Expanded(child: Container(



                    child:Align(
                      alignment: Alignment.center,
                      child: TextButton(

                        onPressed:() {

                            Navigator.pop(context);




                        },

                        child: Text('Cancel', style: TextStyle(color: Colors.blue,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19) ,) ,),
                    ),



                    //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                  ),flex: 1, ),

                  Expanded(child: Container(



                    child:Align(
                      alignment: Alignment.center,
                      child: TextButton(

                        onPressed:() {

                          //  Navigator.pop(context);

                          if(titleController.text.isNotEmpty) {

                            if(filepath.isNotEmpty)
                            {


                              Navigator.pop(context,{"FilePath":filepath,"Title":titleController.text});



                              //uploadFile(filepath,titleController.text);
                              titleController.text="";
                              filepath="";
                            }
                            else{

                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Select a file"),
                              ));
                            }





                          }
                          else{

                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Enter the title"),
                            ));
                          }


                        },

                        child: Text('Submit', style: TextStyle(color: Colors.blue,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19) ,) ,),
                    ),



                    //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                  ),flex: 1, )


                ],

              )






              // ,
            ),
            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),

    );

  }
  Future<String> filePickThroughdocument()async
  {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    File? file=null ;

    if (result != null) {
      file = File(result.files.single.path!);

      // print(file.path);
      //
      //




    } else {
      // User canceled the picker
    }




    return file!.path;


  }




  Future<String> filePickThroughGallery() async
  {
    final ImagePicker _picker = ImagePicker();
    // Pick an image
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    CroppedFile? croppedFile =null;
    // print(image!.path) ;

    if(image!=null) {
      croppedFile = await ImageCropper()
          .cropImage(
          sourcePath: image.path,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
            CropAspectRatioPreset.ratio3x2,
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.ratio4x3,
            CropAspectRatioPreset.ratio16x9
          ],
          // androidUiSettings: AndroidUiSettings(
          //     toolbarTitle: 'Cropper',
          //     toolbarColor: Colors.deepOrange,
          //     toolbarWidgetColor: Colors.white,
          //     initAspectRatio: CropAspectRatioPreset
          //         .original,
          //     lockAspectRatio: false),
          // iosUiSettings: IOSUiSettings(
          //   minimumAspectRatio: 1.0,
          // )
      );
    }

    return croppedFile!.path;







    //  uploadImage(croppedFile);
    //
    // final GoogleSignIn googleSignIn = GoogleSignIn();
    //
    // final GoogleSignInAccount googleSignInAccount =
    //     await googleSignIn.signIn();
    //
    // // final GoogleSignInAuthentication googleSignInAuthentication =
    // // await googleSignInAccount.authentication;
    // //
    // // final AuthCredential credential = GoogleAuthProvider.credential(
    // //   accessToken: googleSignInAuthentication.accessToken,
    // //   idToken: googleSignInAuthentication.idToken,
    // // );
    //
    //
    //
    // var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    // var dri = drive.DriveApi(client);
    // drive.File fileToUpload = drive.File();
    //
    // fileToUpload.parents = ["root"];
    //
    // var response = await dri.files.create(
    //   fileToUpload,
    //   uploadMedia: drive.Media(croppedFile!.openRead(), croppedFile!.lengthSync()),
    // );
    //
    //
    // print(response.id);




  }

  }