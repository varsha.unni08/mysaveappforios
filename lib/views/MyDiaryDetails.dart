import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/domain/Diarysubjects.dart';
// import 'package:save_flutter/domain/MyDiary.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/mainviews/AddMyDiary.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:saveappforios/database/DBTables.dart';

import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/MyDiary.dart';
import 'AddMyDiary.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:open_file/open_file.dart';






class MyDiaryDetailspage extends StatefulWidget {
  final String title;
  final String subjectid;

  final String startdate;
  final String enddate;
  final String subject;




  const MyDiaryDetailspage(
      {Key? key, required this.title, required this.subjectid, required this.startdate, required this.enddate, required this.subject})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyDiaryDetailspage(subjectid,startdate,enddate,subject);
}

class _MyDiaryDetailspage extends State<MyDiaryDetailspage> {

  String subjectid="0";

   String startdate;
   String enddate;
  String subject;

   DatabaseHelper dbhelper=new DatabaseHelper();

   List<MydiaryContent>mydiarycontent_all=[];


  _MyDiaryDetailspage(this.subjectid, this.startdate, this.enddate, this. subject);

  @override
  void initState() {
    // TODO: implement initState




getDiaryDetails(subjectid);


    super.initState();

SystemChrome.setPreferredOrientations([
  DeviceOrientation.portraitDown,
  DeviceOrientation.portraitUp,
]);
  }
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("My Diary details "+subject , style: TextStyle(fontSize:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
        centerTitle: false,
      ),

      body: Stack(

        children: [

        Align(

        alignment: Alignment.topLeft,
        child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(0, 5, 0, 0) : EdgeInsets.fromLTRB(0, 8, 0, 0):EdgeInsets.fromLTRB(0, 11, 0, 0),

        child: Container(width: double.infinity,
        height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
        child: (TextButton(onPressed: () {

          downloaddiaryPdf();
        },
        child: Text("Download pdf",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),),)),) ,))

          ,

          Align(

              alignment: Alignment.topCenter,
              child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(0, 70, 0, 0) : EdgeInsets.fromLTRB(0, 90, 0, 0):EdgeInsets.fromLTRB(0, 120, 0, 0),

                child: Column(
                    children: [
                      Expanded(
                        child: Container(child:
                        MediaQuery.removePadding(
                            context: context,
                            removeTop: true,
                            child: ListView.builder(
                              physics: BouncingScrollPhysics(),

                              shrinkWrap: true,

                              itemCount: mydiarycontent_all.length,
                              itemBuilder: (context, index) {
                                return  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12) :EdgeInsets.all(14)  ,

                                    child: Card(
                                    elevation: 7,
                                    child:

                                    Column(children: [
                                      Container(
                                        width:double.infinity,

                                        height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?70:90:110,

                                        child: Row(
                                          children: [

                                          Expanded(
                                          child:Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10) : EdgeInsets.all(12):EdgeInsets.all(14),

                                              child: Column(children: [

                                                Expanded(child: Text(
                                                  overflow: TextOverflow.ellipsis,
                                                  mydiarycontent_all[index].comments,

                                                  style:
                                                  TextStyle(color: Colors.black, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                ),flex: 1,)



                                                ,

                                                Expanded(child: Text(mydiarycontent_all[index].date,maxLines: 1,
                                                  style:
                                                  TextStyle(color: Colors.black, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                ),flex: 1,)

                                                ],)  ),flex: 2,)  ,

                                  Expanded(child:Padding(padding: EdgeInsets.fromLTRB(70, 0, 0, 0),

                                      child:GestureDetector(onTap: ()async{

                                        Map results = await Navigator.of(context)
                                            .push(new MaterialPageRoute<dynamic>(
                                          builder: (BuildContext context) {
                                            return new AddMyDiarypage(
                                              title: "My diary details", diaryid: mydiarycontent_all[index].id,
                                            );
                                          },
                                        ));

                                        if (results != null &&
                                            results.containsKey('accountadded')) {


                                          int code=results['accountadded'];
                                          if(code==1)
                                          {

                                            //showAccountDetails();
                                            //  showDiarySubjectData();

                                          }
                                        }






                                      },

                                        child:Image.asset("images/edit.png",width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:30:40,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:30:40,) ,)


                                  ),flex: 1, )
                                  ,







                                          ],


                                        ),


                                      ),

                                    ],)










                                ));
                              },
                            ))
                        ),flex: 3,
                      ),

                    ]
                ),
              )





          ),
        ],

      ),



    );
  }



  downloaddiaryPdf() async
  {

    var d=DateTime.now().toString();

    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    var filePath = tempPath + '/'+d+'.pdf';

    File f= await  new File(filePath).create();
    PdfDocument document = PdfDocument();

    PdfPage page=  document.pages.add();

   page.graphics.drawString(
       subject, PdfStandardFont(PdfFontFamily.helvetica, 20),
        brush: PdfSolidBrush(PdfColor(0, 0, 0)),
        bounds: Rect.fromLTWH(0, 0, 500, 50));

   double m=40;

    for (MydiaryContent md in mydiarycontent_all)
      {

        String a=md.date+"\n"+md.comments;

        page.graphics.drawString(
           a, PdfStandardFont(PdfFontFamily.helvetica, 20),
            brush: PdfSolidBrush(PdfColor(0, 0, 0)),
            bounds: Rect.fromLTWH(0, m, 500, 50));

        m=m+100;

      }

var a=await document.save();
    f.writeAsBytes(a);
// Dispose the document.
    document.dispose();

    OpenFile.open(f.path);
  }


  getDiaryDetails(String subjectid) async
  {

    bool isdateexist=false;

    if(startdate.compareTo("Select start date")!=0&&enddate.compareTo("Select end date")!=0)

      {
        isdateexist=true;

      }
    else{

      isdateexist=false;
    }





    List<Map<String,dynamic>> a= await dbhelper.queryAllRows(DatabaseTables.DIARY_table);

    List<MydiaryContent>mydiarycontent=[];

    for (Map ab in a) {

      int id = ab["keyid"];
      String data = ab["data"];
      var jsondata = jsonDecode(data);

      String date = jsondata["date"];
      String contents = jsondata["content"];
      String subject = jsondata["subject"];

      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(date);

      if(subject.compareTo(subjectid)==0)
        {

          if(isdateexist) {
            DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(startdate);

            DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(enddate);
            if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
                accountsdateparsed.isBefore(accountsdatend)) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }
            else if (accountsdateparsed.isAfter(accountsdatestart) &&
                accountsdateparsed.compareTo(accountsdatend) == 0) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }
            else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
                accountsdateparsed.compareTo(accountsdatend) == 0) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }

            else if (accountsdateparsed.isAfter(accountsdatestart) &&
                accountsdateparsed.isBefore(accountsdatend)) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }
          }
          else{

            MydiaryContent content = new MydiaryContent(
                id.toString(), subjectid, "0", date, contents);
            mydiarycontent.add(content);
          }

        }



    }

    setState(() {

      mydiarycontent_all.clear();
      mydiarycontent_all.addAll(mydiarycontent);

    });


  }

}