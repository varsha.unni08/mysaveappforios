import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';


import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import '../projectconstants/DataConstants.dart';
import '../utils/Tutorials.dart';
import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:open_file/open_file.dart';

import 'CashbankAccountDetails.dart';

class IncomeExpenditurePage extends StatefulWidget {
  final String title;

  const IncomeExpenditurePage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _IncomeExpenditurePage();

}

class _IncomeExpenditurePage extends State<IncomeExpenditurePage> {


  String date = "",
      date1 = "";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Account name", "Debit", "Credit", "Action"];

  DatabaseHelper dbhelper = new DatabaseHelper();

  List<String>incomedata=[];
  List<String>expensedata=[];

  bool isviewVisible=false;
  bool isexpenseviewVisible=false;

  double incomeamount=0,expenseamount=0;

  List<Cashbankaccount>cashbank_income=[];
  List<Cashbankaccount>cashbank_expense=[];




  List<AccountSetupData> incomeaccsetupdata = [];
  List<AccountSetupData> expenseaccsetupdata = [];

  bool isincomevisible=false;
double totalincomeamount=0,totalexpenseamount=0;

String headingmessage="";



  @override
  void initState() {
    // TODO: implement initState
    Tutorial.showTutorial(Tutorial.incomexp_tutorial, context, Tutorial.incomexptutorial);
    showCurrentMonthAndYear();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(


      resizeToAvoidBottomInset: true,

      appBar: AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:70:80,

        flexibleSpace: Container(

            color: Color(0xFF096c6c),
            width: double.infinity,
            padding: EdgeInsets.all(10),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Container(
                        margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.fromLTRB(2, 10, 0, 0) : EdgeInsets.fromLTRB(4, 20, 0, 0):EdgeInsets.fromLTRB(6, 30, 0, 0),
                        alignment: Alignment.center,
                        child: Center(
                            child: new InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.arrow_back,color: Colors.white,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,)))),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(
                        margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 10, 0, 0) : EdgeInsets.fromLTRB(13, 20, 0, 0):EdgeInsets.fromLTRB(16, 30, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Row(children: [

                          Expanded(child: Text(
                            "Income Expenditure statement ",
                            style: TextStyle(
                                fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13 : 16:19, color: Colors.white),
                          ),flex: 2,),

                          Expanded(child:Center(
                              child: new InkWell(
                                  onTap: () async {


                                    var d=DateTime.now().toString();

                                    Directory tempDir = await getTemporaryDirectory();
                                    String tempPath = tempDir.path;
                                    var filePath = tempPath + '/'+d+'.pdf';

                                    File f= await  new File(filePath).create();

                                    final PdfDocument document = PdfDocument();

                                    final PdfPage page = document.pages.add();


// Create a PDF grid class to add tables.
                                    final PdfGrid grid = PdfGrid();
// Specify the grid column count.
                                    grid.columns.add(count: 3);

                                    final PdfGridRow headerRow = grid.headers.add(1)[0];
                                    headerRow.cells[0].value = 'Account name';
                                    headerRow.cells[1].value = 'Debit';
                                    headerRow.cells[2].value = 'Credit';

// Set header font.
                                    headerRow.style.font =
                                        PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.bold);
                                    PdfGridRow row  ;

                                    for (int i=0;i<cashbank_income.length;i++)
                                    {
                                      row = grid.rows.add();
                                      row.cells[0].value = cashbank_income[i].accountname;

                                      if(cashbank_income[i].amount>=0) {

                                        double a=cashbank_income[i].amount*1;;

                                        row.cells[1].value =a.toString();

                                        row.cells[2].value = "";

                                    }
                                      else{

                                        row.cells[1].value = "";

                                        double a=cashbank_income[i].amount*-1;;

                                        row.cells[2].value =a.toString();


                                      }



                                    }

                                    grid.style.cellPadding = PdfPaddings(left: 5, top: 5);
// Draw table in the PDF page.
                                    grid.draw(
                                        page: page,
                                        bounds: Rect.fromLTWH(
                                            0, 0, page.getClientSize().width, page.getClientSize().height));








                                    final PdfPage page1 = document.pages.add();


// Create a PDF grid class to add tables.
                                    final PdfGrid grid1 = PdfGrid();
// Specify the grid column count.
                                    grid1.columns.add(count: 3);

                                    final PdfGridRow headerRow1 = grid1.headers.add(1)[0];
                                    headerRow1.cells[0].value = 'Account name';
                                    headerRow1.cells[1].value = 'Debit';
                                    headerRow1.cells[2].value = 'Credit';

// Set header font.
                                    headerRow.style.font =
                                        PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.bold);
                                    PdfGridRow row1  ;

                                    for (int i=0;i<cashbank_expense.length;i++)
                                    {
                                      row1 = grid1.rows.add();
                                      row1.cells[0].value = cashbank_expense[i].accountname;

                                      if(cashbank_expense[i].amount>=0) {

                                        double a=cashbank_expense[i].amount*1;;

                                        row1.cells[1].value =a.toString();

                                        row1.cells[2].value = "";

                                      }
                                      else{

                                        row1.cells[1].value = "";

                                        double a=cashbank_expense[i].amount*-1;;

                                        row1.cells[2].value =a.toString();


                                      }



                                    }

                                    grid1.style.cellPadding = PdfPaddings(left: 5, top: 5);
// Draw table in the PDF page.
                                    grid1.draw(
                                        page: page1,
                                        bounds: Rect.fromLTWH(
                                            0, 0, page.getClientSize().width, page.getClientSize().height));

// Save the document.
                                    var a=await document.save();
                                    f.writeAsBytes(a);
// Dispose the document.
                                    document.dispose();

                                    OpenFile.open(f.path);









                                  },
                                  child: Icon(Icons.download,color: Colors.white,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 25:30:35,)))  ,flex: 1,)



                        ],)),
                    flex: 5,
                  ),
                ])) ,
        backgroundColor: Color(0xFF096c6c),
        centerTitle: false,

      ),

      body: SingleChildScrollView(
      child: Column(


        children: [

        Align(

        alignment: Alignment.topCenter,
        child: Row(
          children: [

            Expanded(child: Padding(
                padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                          child: GestureDetector(
                            onTap: (){
                              var newDateTime = DateTime.now();

                              date= newDateTime.day
                                  .toString() +
                                  "-" +
                                  newDateTime.month
                                      .toString() +
                                  "-" +
                                  newDateTime.year
                                      .toString();



                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  newDateTime.year,
                                                  newDateTime.month,
                                                  newDateTime.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {


                                                date= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();






                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 :60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  150:160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt1 = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });


                            }

                            ,
                            child:Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,) ,



                          )



                          ),
                      Expanded(
                          child: TextButton(
                            onPressed: () {
                              var newDateTime = DateTime.now();

                              date= newDateTime.day
                                  .toString() +
                                  "-" +
                                  newDateTime.month
                                      .toString() +
                                  "-" +
                                  newDateTime.year
                                      .toString();



                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  newDateTime.year,
                                                  newDateTime.month,
                                                  newDateTime.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {


                                                date= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();






                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 :60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  150:160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt1 = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });



                              // showCustomDatePicker(build(context));


                            },
                            child: Text(
                              datetxt1,
                              style: TextStyle(
                                  color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  12:14:16),
                            ),
                          ))
                    ],
                  ),
                )),flex: 1,),
            Expanded(child: Padding(
                padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                          child: GestureDetector(

                            child:Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,) ,

                            onTap: (){

                              var newDateTime = DateTime.now();

                              date1= newDateTime.day
                                  .toString() +
                                  "-" +
                                  newDateTime.month
                                      .toString() +
                                  "-" +
                                  newDateTime.year
                                      .toString();



                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  newDateTime.year,
                                                  newDateTime.month,
                                                  newDateTime.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {


                                                date1= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();






                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  50 :60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  150 :160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt2 = date1;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });

                            },
                          )






                      ),
                      Expanded(
                          child: TextButton(
                            onPressed: () {
                              var newDateTime = DateTime.now();

                              date1= newDateTime.day
                                  .toString() +
                                  "-" +
                                  newDateTime.month
                                      .toString() +
                                  "-" +
                                  newDateTime.year
                                      .toString();



                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  newDateTime.year,
                                                  newDateTime.month,
                                                  newDateTime.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {


                                                date1= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();






                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  50 :60:70,
                                              width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  150 :160:170,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt2 = date1;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });


                              // showCustomDatePicker(build(context));


                            },
                            child: Text(
                              datetxt2,
                              style: TextStyle(
                                  color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  12:14:17),
                            ),
                          ))
                    ],
                  ),
                )),flex: 1,)


          ],

        ),
      ),

      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.fromLTRB(0, 10, 0, 0) : EdgeInsets.fromLTRB(0, 13, 0, 0):EdgeInsets.fromLTRB(0, 16, 0, 0),
        child:      Align(         alignment: Alignment.topCenter,child: Padding(
          padding: const EdgeInsets.all(2),
          child: Container(
            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:60:70,
            width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150 :160:170,
            decoration: BoxDecoration(
                color: Color(0xF0233048),
                borderRadius: BorderRadius.circular(10)),
            child: TextButton(
              onPressed: () {

                DateTime startdate = new DateFormat("dd-MM-yyyy").parse(datetxt1);

                DateTime enddate = new DateFormat("dd-MM-yyyy").parse(datetxt2);

                if(enddate.isAfter(startdate)||startdate.compareTo(enddate)==0)
                {

                  showData(startdate,enddate);


                }


              },
              child: Text(
                'Submit',
                style:
                TextStyle(color: Colors.white, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15 :17:19),
              ),
            ),
          ),
        ),),),

          Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(12) : EdgeInsets.all(14):EdgeInsets.all(16),
          child: Text(headingmessage,style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18,color: Colors.black),) ,),

          isviewVisible
              ? Stack(children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Card(
                    elevation: 5,
                    child: new InkWell(
                        onTap: () {
                          setState(() {
                            if (!isincomevisible) {
                              isincomevisible = true;
                            } else {
                              isincomevisible = false;
                            }
                          });
                        },
                        child: Container(
                            child: Column(children: [
                              Container(
                                  width: double.infinity,
                                  child: Padding(
                                      padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11):EdgeInsets.all(14),
                                      child: Row(
                                          textDirection: ui.TextDirection.ltr,
                                          children: [
                                            Expanded(child: Container(
                                              child: Align(
                                                alignment:
                                                Alignment.centerLeft,
                                                child: Center(
                                                    child: Container(

                                                      child:Text("Total income : "+totalincomeamount.toString(),
                                                          style: TextStyle(
                                                              fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,
                                                              color: Colors
                                                                  .black))
                                                      ,

                                                    )),
                                              ),

                                            ),flex: 1,

                                            )

                                            ,
                                            Expanded(child: Container(
                                              child: GestureDetector(
                                                onTap: () {



                                                  setState(() {
                                                    if (!isincomevisible) {
                                                      isincomevisible = true;
                                                    } else {
                                                      isincomevisible = false;
                                                    }
                                                  });

                                                },
                                                child: Icon(
                                                  Icons.arrow_drop_down,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,
                                                ),
                                              ),

                                            ),flex: 1,)



                                          ]))),
                              isincomevisible
                                  ? Column(children: [

                               Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                                  child:

                                  Align(alignment: Alignment.topCenter,child: MediaQuery.removePadding(
                                      context: context,
                                      removeTop: true,
                                      removeBottom: true,
                                      child:GridView.builder(
                                        physics: BouncingScrollPhysics(),

                                        shrinkWrap: true,
                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 4,
                                          crossAxisSpacing: 0.0,
                                          mainAxisSpacing: 0.0,
                                          childAspectRatio: 0.7
                                        ),
                                        itemCount: tableheaddata.length,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding:EdgeInsets.all(0),

                                            child: Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: Colors.black54,
                                                    width: 0.3,
                                                  ),
                                                ),

                                                child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13 :15:17,color: Colors.black),))),

                                          );
                                        },
                                      )),),








                                ),




                                  Align(alignment: Alignment.topCenter,child: MediaQuery.removePadding(
                                      context: context,
                                      removeTop: true,
                                      child:GridView.builder(
                                        physics: BouncingScrollPhysics(),

                                        shrinkWrap: true,
                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 4,
                                          crossAxisSpacing: 0.0,
                                          mainAxisSpacing: 0.0,
                                          childAspectRatio: 1
                                        ),
                                        itemCount: incomedata.length,
                                        itemBuilder: (context, index) {
                                          return


                                          Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: Colors.black54,
                                                  width: 0.3,
                                                ),
                                              ),

                                                  child:


                                                  (incomedata[index].contains("View"))?

                                                  Center(child:TextButton(
                                                      onPressed: () {

                                                        int a=index+1;
                                                        int b=a~/4;

                                                        Cashbankaccount cb=cashbank_income[b-1];


                                                        // //
                                                        print(cb.accountname);
                                                        //
                                                        //
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) =>
                                                            CashbankstatementDetailsPage(title: "Ledger details details", accid: incomedata[index].split(",")[1], cb: cb, Startdate: datetxt1, Enddate: datetxt2,)));

                                                      },
                                                      child: Text(incomedata[index].split(",")[0],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.green)
                                                        ,))):Center(child:Text(incomedata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.black),)));


                                        },
                                      ))



                                  ,






                                )


                              ])
                                  : Container()
                            ]))),
                  ),
                  Card(
                    elevation: 5,
                    child: new InkWell(
                        onTap: () {
                          setState(() {
                            if (!isexpenseviewVisible) {
                              isexpenseviewVisible = true;
                            } else {
                              isexpenseviewVisible = false;
                            }
                          });
                        },
                        child: Container(
                            child: Column(children: [
                              Container(
                                  width: double.infinity,
                                  child: Padding(
                                      padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) : EdgeInsets.all(11):EdgeInsets.all(14),
                                      child: Row(
                                          textDirection: ui.TextDirection.ltr,
                                          children: [
                                            Expanded(child: Container(
                                              child: Align(
                                                alignment:
                                                Alignment.centerLeft,
                                                child: Center(
                                                    child: Container(

                                                      child: Text("Total Expense : "+totalexpenseamount.toString(),
                                                          style: TextStyle(
                                                              fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13 :15:18,
                                                              color: Colors
                                                                  .black))


                                                      ,

                                                    )),
                                              ),

                                            ),flex: 1,)

                                            ,

                                            Expanded(child: Container(
                                              child: GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    if (!isexpenseviewVisible) {
                                                      isexpenseviewVisible = true;
                                                    } else {
                                                      isexpenseviewVisible = false;
                                                    }
                                                  });


                                                },
                                                child: Icon(Icons.arrow_drop_down,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,),
                                              ),

                                            ),flex: 1,)


                                          ]))),
                              isexpenseviewVisible
                                  ? Column(children: [

                                Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                                  child:

                                  Align(alignment: Alignment.topCenter,child: MediaQuery.removePadding(
                                      context: context,
                                      removeTop: true,
                                      removeBottom: true,
                                      child:GridView.builder(
                                        physics: BouncingScrollPhysics(),

                                        shrinkWrap: true,
                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 4,
                                          crossAxisSpacing: 0.0,
                                          mainAxisSpacing: 0.0,
                                          childAspectRatio: 1
                                        ),
                                        itemCount: tableheaddata.length,
                                        itemBuilder: (context, index) {
                                          return  Container(

                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: Colors.black54,
                                                  width: 0.3,
                                                ),
                                              ),

                                                child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.black),)));
                                        },
                                      )),),








                                ),




                                  Align(alignment: Alignment.topCenter,child: MediaQuery.removePadding(
                                      context: context,
                                      removeTop: true,
                                      child:GridView.builder(
                                        physics: BouncingScrollPhysics(),

                                        shrinkWrap: true,
                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 4,
                                          crossAxisSpacing: 0.0,
                                          mainAxisSpacing: 0.0,
                                          childAspectRatio: 0.7
                                        ),
                                        itemCount: expensedata.length,
                                        itemBuilder: (context, index) {
                                          return Container(

                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  color: Colors.black54,
                                                  width: 0.3,
                                                ),
                                              ),

                                                  child:


                                                  (expensedata[index].contains("View"))?

                                                  Center(child:TextButton(
                                                      onPressed: () async {

                                                        int a=index+1;
                                                        int b=a~/4;

                                                        Cashbankaccount cb=cashbank_expense[b-1];


                                                        // //
                                                    print(cb.accountname);
                                                        //
                                                        //
                                                        Navigator.push(
                                                            context, MaterialPageRoute(builder: (_) =>
                                                            CashbankstatementDetailsPage(title: "Ledger details details", accid: expensedata[index].split(",")[1], cb: cb, Startdate: datetxt1, Enddate: datetxt2,)));

                                                      },
                                                      child: Text(expensedata[index].split(",")[0],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.green)
                                                        ,))):Center(child:Text(expensedata[index],style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.black),)));
                                        },
                                      ))



                                  ,






                                )


                              ])
                                  : Container()
                            ]))),
                  ),


                ],
              ),
            )
          ])
              : Container()



      ]


      ),
      )


    );
  }


  void showCurrentMonthAndYear()
  async{



    var now=DateTime.now();

    setState(() {

      datetxt1="1"+"-"+now.month.toString()+"-"+now.year.toString();
      datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    });

    DateTime startdate = new DateFormat("dd-MM-yyyy").parse(datetxt1);

    DateTime enddate = new DateFormat("dd-MM-yyyy").parse(datetxt2);

    showData(startdate,enddate);




  }


  void showData(DateTime startdate,DateTime enddate)
 async {

    List<Map<String,dynamic>>allaccountsettings=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);

    List<AccountSetupData> incomeaccount = [];
    List<AccountSetupData> expenseaccount = [];

    for (Map ab in allaccountsettings) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);
      String Accounttype = jsondata["Accounttype"];

      String Accountname = jsondata["Accountname"];

      String Amount = jsondata["Amount"];
      String Type = jsondata["Type"];
      
      if(Accounttype.compareTo("Income account")==0)
        {
          AccountSetupData dataaccsetup = new AccountSetupData();
          dataaccsetup.id = id.toString();
          dataaccsetup.Accountname = Accountname;
          dataaccsetup.Accounttype = Accounttype;
          dataaccsetup.Amount = Amount;
          dataaccsetup.Type = Type;

          incomeaccount.add(dataaccsetup);

        }

     else if(Accounttype.compareTo("Expense account")==0)
      {
        AccountSetupData dataaccsetup = new AccountSetupData();
        dataaccsetup.id = id.toString();
        dataaccsetup.Accountname = Accountname;
        dataaccsetup.Accounttype = Accounttype;
        dataaccsetup.Amount = Amount;
        dataaccsetup.Type = Type;

        expenseaccount.add(dataaccsetup);

      }
      
      
    }

    List<Map<String, dynamic>>accountsdata= await getSelectedAccountData(startdate, enddate);

    List<Map<String, dynamic>>incomeFilteredaccount=[];

    List<Map<String, dynamic>>expenseFilteredaccount=[];

    for(int i=0;i<accountsdata.length;i++) {
      Map<String, dynamic> mapval = accountsdata[i];
      String accountsetupid = mapval[DatabaseTables.ACCOUNTS_setupid];
      for(AccountSetupData acc in incomeaccount)
        {
          if(accountsetupid.compareTo(acc.id)==0)
            {

              if(!checkDataExists(incomeFilteredaccount,acc.id)) {
                incomeFilteredaccount.add(mapval);
              }


            }
        }
      for(AccountSetupData eacc in expenseaccount)
      {
        if(accountsetupid.compareTo(eacc.id)==0)
        {

          if(!checkDataExists(expenseFilteredaccount,eacc.id)) {
            expenseFilteredaccount.add(mapval);
          }


        }
      }
    }

    List<String>incomestring=[];
    List<String>expensestring=[];

    List<Cashbankaccount>cbincome=[];
    List<Cashbankaccount>cbexpense=[];

    double totalicome=0,totalexpense=0;

    String message="";

    for(int i=0;i<incomeFilteredaccount.length;i++) {

      Cashbankaccount cashbankaccount=new Cashbankaccount();
      Map<String, dynamic> mapval = incomeFilteredaccount[i];
      String accountsetupid = mapval[DatabaseTables.ACCOUNTS_setupid];

      var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, accountsetupid);


      List<Map<String, dynamic>> accountsettings=value;

      Map ab=accountsettings[0];

      // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

      int id = ab["keyid"];
      cashbankaccount.accountid=id.toString();
      String data = ab["data"];

      var jsondata = jsonDecode(data);
      String Amount = jsondata["Amount"];

      String Accountname = jsondata["Accountname"];

      double openingbalance=double.parse(Amount);


      double closingbalance=await getFinalOpeningBalance(openingbalance, accountsetupid);
      cashbankaccount.amount=closingbalance;
      cashbankaccount.accountname=Accountname;

      totalicome=totalicome+closingbalance;

      incomestring.add(Accountname);


      if(closingbalance>=0)
        {

          incomestring.add(closingbalance.toString());
          incomestring.add("");
        }
      else{

        double a=closingbalance*-1;
        incomestring.add("");
        incomestring.add(a.toString());



      }

      cbincome.add(cashbankaccount);

      incomestring.add("View,"+id.toString());


      //incomefiltered completed


    }




    for(int i=0;i<expenseFilteredaccount.length;i++) {

      Cashbankaccount cashbankaccount=new Cashbankaccount();
      Map<String, dynamic> mapval = expenseFilteredaccount[i];
      String accountsetupid = mapval[DatabaseTables.ACCOUNTS_setupid];

      var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, accountsetupid);


      List<Map<String, dynamic>> accountsettings=value;

      Map ab=accountsettings[0];

      // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

      int id = ab["keyid"];
      cashbankaccount.accountid=id.toString();
      String data = ab["data"];

      var jsondata = jsonDecode(data);
      String Amount = jsondata["Amount"];

      String Accountname = jsondata["Accountname"];
      cashbankaccount.accountname=Accountname;

      double openingbalance=double.parse(Amount);


      double closingbalance=await getFinalOpeningBalance(openingbalance, accountsetupid);
      cashbankaccount.amount=closingbalance;

      expensestring.add(Accountname);

      totalexpense=totalexpense+closingbalance;
      if(closingbalance>=0)
      {

        expensestring.add(closingbalance.toString());
        expensestring.add("");
      }
      else{

        double a=closingbalance*-1;
        expensestring.add("");
        expensestring.add(a.toString());



      }

      cbexpense.add(cashbankaccount);

      expensestring.add("View"+","+id.toString());


      //incomefiltered completed


    }

    if(totalexpense>totalicome)
      {

        if(totalexpense<0)
        {
          totalexpense=totalexpense*-1;
        }
        if(totalicome<0)
        {
          totalicome=totalicome*-1;
        }
        double a=totalexpense-totalicome;

        message="Expense over income "+a.toString();


      }
    else if(totalicome>totalexpense)
      {
        if(totalexpense<0)
        {
          totalexpense=totalexpense*-1;
        }
        if(totalicome<0)
        {
          totalicome=totalicome*-1;
        }
        double a=totalicome-totalexpense;

        message="Income over expense "+a.toString();
      }
    else{

      message="Income and expense are equal";
    }



    setState(() {

      incomedata.clear();
      expensedata.clear();

      incomedata.addAll(incomestring);
      expensedata.addAll(expensestring);

      isviewVisible=true;

      totalincomeamount=totalicome;
      totalexpenseamount=totalexpense;
      headingmessage=message;


      cashbank_income.addAll(cbincome);
      cashbank_expense.addAll(cbexpense);


    });












    
    

  }

  Future<double> getFinalOpeningBalance(double openingbalance,String setupid)
  async {

    double finalbalance=0;

    finalbalance=openingbalance;

    DateTime startdate = new DateFormat("dd-MM-yyyy").parse(datetxt1);

    DateTime enddate = new DateFormat("dd-MM-yyyy").parse(datetxt2);


    var m=await getSelectedAccountData(startdate, enddate);
    List<Map<String, dynamic>> v=m;

    for(Map a in v) {
      String setupidaccount = a[DatabaseTables.ACCOUNTS_setupid];

      if (setupidaccount.compareTo(setupid) == 0)
      {
        String accountsdate = a[DatabaseTables.ACCOUNTS_date];
        String accountsamount = a[DatabaseTables.ACCOUNTS_amount];
        String accountstype = a[DatabaseTables.ACCOUNTS_type].toString();
        double amount = double.parse(accountsamount);
        DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(
            accountsdate);
        DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(
            datetxt2);

        if (accountsdateparsed.compareTo(accountsdateselected) == 0) {
          if (setupidaccount.compareTo(setupid) == 0) {
            if (accountstype.compareTo(DataConstants.debit.toString()) == 0) {
              finalbalance = finalbalance + amount;
            }
            else {
              finalbalance = finalbalance - amount;
            }
          }
        }

        else if (accountsdateparsed.isBefore(accountsdateselected)) {
          if (setupidaccount.compareTo(setupid) == 0) {
            if (accountstype.compareTo(DataConstants.debit.toString()) == 0) {
              finalbalance = finalbalance + amount;
            }
            else {
              finalbalance = finalbalance - amount;
            }
          }
        }
      }

    }

    return finalbalance;

  }




  Future<List<Map<String, dynamic>>> getSelectedAccountData(DateTime startdate,DateTime endDate) async
  {

    List<Map<String,dynamic>>accountselecteddata=[];


    List<Map<String,dynamic>>accounts=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);

    for(int i=0;i<accounts.length;i++) {
      Map<String, dynamic> mapval = accounts[i];


      String accountdate = mapval[DatabaseTables.ACCOUNTS_date];
      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(
          accountdate);

      if (accountsdateparsed.compareTo(startdate) == 0 &&
          accountsdateparsed.isBefore(endDate)) {
        accountselecteddata.add(mapval);
      }

      else if (accountsdateparsed.isAfter(startdate) &&
          accountsdateparsed.compareTo(endDate) == 0) {
        accountselecteddata.add(mapval);
      }

      else if (accountsdateparsed.compareTo(startdate) == 0 &&
          accountsdateparsed.compareTo(endDate) == 0) {
        accountselecteddata.add(mapval);
      }

      else if (accountsdateparsed.isAfter(startdate) &&
          accountsdateparsed.isBefore(endDate)) {
        accountselecteddata.add(mapval);
      }
    }

    return accountselecteddata;

  }

  bool checkDataExists( List<Map<String,dynamic>>accounts,String setupid)
  {
    bool a=false;

    for(int i=0;i<accounts.length;i++) {
      Map<String, dynamic> mapval = accounts[i];
      String setupid1 = mapval[DatabaseTables.ACCOUNTS_setupid];

      if(setupid.compareTo(setupid1)==0)
      {
        a=true;
      }



    }

    return a;
  }
}