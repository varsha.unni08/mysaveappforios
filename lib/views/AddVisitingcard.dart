import 'dart:convert';

import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/TemregData.dart';
// import 'package:save_flutter/domain/country.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:io';
import 'dart:ui' as ui;
import 'package:share_extend/share_extend.dart';
//
// import 'package:lite_rolling_switch/lite_rolling_switch.dart';

// import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;

 import 'package:shared_preferences/shared_preferences.dart';
 import 'package:page_indicator/page_indicator.dart';
import 'package:path_provider/path_provider.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../design/ResponsiveInfo.dart';


void main() {
  // runApp(MyApp());

  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: AddVisitcardpage(title: 'Add visiting card', cardid: '0',),
      debugShowCheckedModeBanner: false,
    );
  }
}

class AddVisitcardpage extends StatefulWidget {
  AddVisitcardpage({Key? key, required this.title, required this.cardid}) : super(key: key);

  final String title;

  final String cardid;

  @override
  _AddVisitcardpageState createState() => _AddVisitcardpageState(cardid);
}

class _AddVisitcardpageState extends State<AddVisitcardpage> {
  int selectedcardindex = 0;

  int code = 0;

  String cardid='';


  _AddVisitcardpageState(this.cardid);

  List<String> cardimagebackgrounddata = [
    "cone.jpg",
    "ctwo.jpg",
    "cthree.jpg",
    "cfour.jpg",
    "cfive.jpg",
    "csix.jpg",
    "cseven.jpg",
    "ceight.jpg"
  ];

   DatabaseHelper dbhelper=new DatabaseHelper();

  List<Widget> nsdwidget = [];

  TextEditingController namecontroller=new TextEditingController();
  TextEditingController phonecontroller=new TextEditingController();
  TextEditingController whatsappcontroller=new TextEditingController();

  TextEditingController emailcontroller=new TextEditingController();
  TextEditingController companynamecontroller=new TextEditingController();

  TextEditingController designationcontroller=new TextEditingController();
  TextEditingController websitecontroller=new TextEditingController();

  TextEditingController facebookcontroller=new TextEditingController();
  TextEditingController instagramcontroller=new TextEditingController();
  TextEditingController youtubecontroller=new TextEditingController();
  TextEditingController companyaddresscontroller=new TextEditingController();

  bool isimageloaded=false;

  late CroppedFile f;
  @override
  void initState() {
    // TODO: implement initState

    if(cardid.isNotEmpty)
      {
        setdataForEditing(cardid);
      }



    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    // sleep(const Duration(seconds:6));
    // startTime();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < cardimagebackgrounddata.length; i++) {
      nsdwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.89349112,
        width: double.infinity,
        child: Card(
            child: Image.asset(
          "images/" + cardimagebackgrounddata[i],
          width: double.infinity,
          height: (MediaQuery.of(context).size.width) / 1.89349112,
        )),
      ));
    }
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () =>  Navigator.pop(context, {'accountsetupdata':1}),
        ),
        title: Text(
          "Your business card details",
          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:16:19),
        ),
        centerTitle: false,
      ),
      body:WillPopScope(
    onWillPop: () async {
      Navigator.pop(context, {'accountsetupdata':1});
    return false;
    },child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(children: [
            Expanded(
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15) :EdgeInsets.all(20),
                    child: Text(
                      "Business card background",
                      style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  20:24:28, color: Colors.black),
                    ),
                  ),
                  Container(
                      height: (MediaQuery.of(context).size.width) / 1.89349112,
                      width: double.infinity,
                      child: PageIndicatorContainer(
                          length: cardimagebackgrounddata.length,
                          align: IndicatorAlign.bottom,
                          indicatorSpace: 8.0,
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(20),
                          indicatorColor: Colors.black12,
                          indicatorSelectorColor: Colors.blueGrey,
                          shape: IndicatorShape.circle(size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?10:15:20),
                          child: PageView.builder(
                            scrollDirection: Axis.horizontal,
                            // controller: controller,
                            itemBuilder: (BuildContext context, int index) {
                              print(index);
                              selectedcardindex = index;
                              return getNetWorkWidget(index);
                            },
                            itemCount: cardimagebackgrounddata.length,
                            // children: nsdwidget,
                          ))),
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: namecontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Name',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Padding(
                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(
                                data: new ThemeData(hintColor: Colors.black38),
                                child: TextField(
                                  controller: phonecontroller,
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    hintText: 'Phone number',
                                  ),
                                  onChanged: (text) {},
                                )),
                          ),
                          flex: 2),
                      Expanded(
                          child: Padding(
                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(
                                data: new ThemeData(hintColor: Colors.black38),
                                child: TextField(
                                  controller: whatsappcontroller,
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    hintText: 'Whatsapp number',
                                  ),
                                  onChanged: (text) {},
                                )),
                          ),
                          flex: 2)
                    ],
                  ),
                  Padding(
                    padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: emailcontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Email',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(

                          controller: companynamecontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Company name',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Padding(
                    padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: designationcontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Designation/profession',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: websitecontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Website',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: facebookcontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Facebook link',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: instagramcontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Instagram link',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: youtubecontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Youtube link',
                          ),
                          onChanged: (text) {},
                        )),
                  ),
                  Padding(
                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),
                    // padding: EdgeInsets.all(15),
                    child: new Theme(
                        data: new ThemeData(hintColor: Colors.black38),
                        child: TextField(
                          controller: companyaddresscontroller,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 0.5),
                            ),
                            hintText: 'Company address',
                          ),
                          onChanged: (text) {},
                        )),
                  ),

                  Padding(
                      padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(8):EdgeInsets.all(12),

                      child:Center(child:Text("Company logo",style: TextStyle(fontSize: 14,color: Colors.black)),)

                  ),

        Padding(
          padding: const EdgeInsets.all(10),

          child: isimageloaded ?Image.file(File(f.path),width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,):Image.asset("images/gallery.png",width: 50,height: 50,)

        ),

                  Padding(
                      padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15) : EdgeInsets.all(20),

                      child:Center(child:

                      TextButton(onPressed: ()async {
                        Widget yesButton = TextButton(
                          child: Text("Yes",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                          onPressed: () async {
                            Navigator.pop(context);

                            final ImagePicker _picker = ImagePicker();
                            // Pick an image
                            final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

                            // print(image!.path) ;

                            if(image!=null) {
                              CroppedFile? croppedFile = await ImageCropper()
                                  .cropImage(
                                sourcePath: image.path,
                                aspectRatioPresets: [
                                  CropAspectRatioPreset.square,
                                  CropAspectRatioPreset.ratio3x2,
                                  CropAspectRatioPreset.original,
                                  CropAspectRatioPreset.ratio4x3,
                                  CropAspectRatioPreset.ratio16x9
                                ],
                                // androidUiSettings: AndroidUiSettings(
                                //     toolbarTitle: 'Cropper',
                                //     toolbarColor: Colors.deepOrange,
                                //     toolbarWidgetColor: Colors.white,
                                //     initAspectRatio: CropAspectRatioPreset
                                //         .original,
                                //     lockAspectRatio: false),
                                // iosUiSettings: IOSUiSettings(
                                //   minimumAspectRatio: 1.0,
                                // )
                              );


                              setState(() {
                                f=croppedFile!;
                                isimageloaded=true;

                              });


                            }
                          },
                        );


                        Widget noButton = TextButton(
                          child: Text("No",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        );

                        // set up the AlertDialog
                        AlertDialog alert = AlertDialog(
                          title: Text("Save",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                          content: Text("You can select your company logo image .We will keep this image safe and secure .Do you want to continue ?",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                          actions: [yesButton, noButton],
                        );

                        // show the dialog
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return alert;
                          },
                        );





    },
                      child:Text("Pick image",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:15:18,color: Colors.black))),)

                  )
                ],
              )),
              flex: 5,
            ),
            Expanded(
              child: Center(
                child: TextButton(
                    onPressed: () {
                      showSelectedCardDialog(selectedcardindex);
                    },
                    child: Text("Submit",
                        style: TextStyle(color: Colors.blue, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18))),
              ),
              flex: 1,
            )
          ])),
      )
    );
  }

  Widget getNetWorkWidget(int p) {
    return nsdwidget[p];
  }

  showSelectedCardDialog(int index) {

    GlobalKey globalKey=new GlobalKey();
    GlobalKey globalKey1=new GlobalKey();
    List<String> cardimagebackgrounddata = [
      "cone.jpg",
      "ctwo.jpg",
      "cthree.jpg",
      "cfour.jpg",
      "cfive.jpg",
      "csix.jpg",
      "cseven.jpg",
      "ceight.jpg"
    ];

    List<String>data=['Text','Image'];

    int selectedoption=0;

    Dialog sharedialog = Dialog(

        //this right here
        child: Container(
          height: (MediaQuery.of(context).size.height),
          width: (MediaQuery.of(context).size.width),
          child: Column(
            children: [
              Expanded(
                child:RepaintBoundary(
                  key: globalKey,child: Container(
                  color: Colors.white,
                  child: Card(
                      child: Stack(
                    children: [
                      Container(
                        width: (MediaQuery.of(context).size.width),
                        height: (MediaQuery.of(context).size.width) ,
                        child: Image.asset(
                          "images/" + cardimagebackgrounddata[index],
                          width: (MediaQuery.of(context).size.width),
                          height: (MediaQuery.of(context).size.width) ,
                          fit: BoxFit.fill,
                        ),
                      ),

              Container(
                width: (MediaQuery.of(context).size.width),
                height: (MediaQuery.of(context).size.width) ,
                   child: Column(children:[


                      Expanded(child:    Row(
                        children: [
                          Expanded(
                            child: Column(children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(10) : EdgeInsets.all(15),
                                  child: Text(
                                    namecontroller.text,
                                    style: TextStyle(
                                        fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18, color: Colors.black),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(10) : EdgeInsets.all(15),
                                  child: Text(
                                    designationcontroller.text,
                                    style: TextStyle(
                                        fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18, color: Colors.black),
                                  ),
                                ),
                              ),
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Row(

                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[

                                        Padding(padding: EdgeInsets.all(5),
                                          child:  Image.asset("images/call.png",
                                              width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                              fit: BoxFit.cover) ,)
                                        ,
                                        Text(
                                          phonecontroller.text,
                                          style: TextStyle(
                                              fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18,
                                              color: Colors.black),
                                        ),

                                      ])),
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Row(

                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[

                                        Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(11),
                                          child:  Image.asset("images/whatsapp.png",
                                              width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                              fit: BoxFit.cover) ,)
                                        ,
                                        Text(
                                          whatsappcontroller.text,
                                          style: TextStyle(
                                              fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18,
                                              color: Colors.black),
                                        ),

                                      ])),
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Row(

                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[

                                        Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(11),
                                          child:  Image.asset("images/email.png",
                                              width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                              fit: BoxFit.cover) ,)
                                        ,
                                        Text(
                                          emailcontroller.text,
                                          style: TextStyle(
                                              fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18,
                                              color: Colors.black),
                                        ),

                                      ])),


                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(11),
                                    child: Text(
                                      websitecontroller.text,
                                      style: TextStyle(
                                          fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18, color: Colors.black),
                                    ),

                                  )),

                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(11),
                                    child: Text(
                                      companynamecontroller.text,
                                      style: TextStyle(
                                          fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18, color: Colors.black),
                                    ),
                                  )
                              ),

                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(11),
                                    child: Text(
                                      companyaddresscontroller.text,
                                      maxLines: 4,

                                      style: TextStyle(
                                          fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:15:18, color: Colors.black),
                                    ),
                                  )
                              ),


                            ]),
                            flex: 3,
                          ),
                          Expanded(
                            child:


                            Column(

                              children: [

                                Expanded(child: Align(
                                    alignment: Alignment.center,  child:(isimageloaded)?Image.file(File(f.path),     width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30 : 40:50,
                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30 : 40:50,

                                    fit: BoxFit.cover):Image.asset("images/gallery.png",
                                    width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30 : 40:50,
                                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30 : 40:50,
                                    fit: BoxFit.cover)),flex: 2,)

                                ,

                Expanded(child:

                                Align(
                                    alignment: Alignment.center,  child:

                                Padding(

                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(11),


                                    child:Container(


                                        child: RepaintBoundary(
                                            key: globalKey1,

                                            child:(websitecontroller.text.isNotEmpty)?
                                            QrImage(
                                                data: websitecontroller.text,):Container(
                                              width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30 : 40:50,
                                              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30 : 40:50,
                                            ))




                                    )





                                )),flex: 2,)


                              ],





                            ),
                            flex: 2,
                          )
                        ],
                      ),
                      flex:4)
                 ,

                      Expanded(child:   Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(8) :EdgeInsets.all(11),

                      child:  Row(
                          children: [

                          Expanded(child: Row(
                              children: [Image.asset("images/facebook.png",
                                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                              ),Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(5, 0, 0, 0) : EdgeInsets.fromLTRB(8, 0, 0, 0):EdgeInsets.fromLTRB(11, 0, 0, 0),child: Text(facebookcontroller.text),)]),flex: 2,),
                            Expanded(child: Row(
                                children: [Image.asset("images/youtube.png",
                                  width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                ),Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(5, 0, 0, 0) : EdgeInsets.fromLTRB(8, 0, 0, 0):EdgeInsets.fromLTRB(11, 0, 0, 0),child: Text(youtubecontroller.text),)]),flex: 2,),

                            Expanded(child: Row(
                                children: [Image.asset("images/instagram.png",
                                  width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20 : 30:40,
                                ),Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(5, 0, 0, 0) : EdgeInsets.fromLTRB(8, 0, 0, 0):EdgeInsets.fromLTRB(11, 0, 0, 0),child: Text(instagramcontroller.text),)]),flex: 2,),




            ])),flex:1)





                    ])),




                    ],
                  )),
                ),),
                flex: 5,
              ),
              Expanded(
                child: Container(
                  child:Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(10) :EdgeInsets.all(15),

                      child:TextButton(onPressed: () async {

                        final RenderRepaintBoundary boundary = globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
                        final ui.Image image = await boundary.toImage();
                        final ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
                        // final Uint8List pngBytes = byteData!.buffer.asUint8List();
                        File? f=await  writeToFile(byteData!);





                        var mjobject=new Map();
                        mjobject['name']=namecontroller.text;
                        mjobject['phone']=phonecontroller.text;
                        mjobject['email']=emailcontroller.text;
                        mjobject['address']=companyaddresscontroller.text;

                        mjobject['whatsapp']=whatsappcontroller.text;
                        mjobject['designation']=designationcontroller.text;
                        mjobject['website']=websitecontroller.text;
                        mjobject['company']=companynamecontroller.text;

                        mjobject['cardbg']=index;

                        mjobject['fb']=facebookcontroller.text;
                        mjobject['youtube']=youtubecontroller.text;
                        mjobject['instagram']=instagramcontroller.text;



                        var js=json.encode(mjobject);

                        Map<String, dynamic> data_To_Table=new Map();
                        data_To_Table['data']=js.toString();





                        Navigator.pop(context);



                        AlertDialog dialog =   AlertDialog(
                                  title: Text("Share as"),
                                  content: SingleChildScrollView(
                                    child: Container(
                                      width: double.infinity,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: data
                                            .map((e) => RadioListTile(
                                          title: Text(e),
                                          value: e,


                                          onChanged: (value) {

                                            print(value);

                                            if(value.toString().compareTo("Image")==0)
                                              {

                                              if(cardid.isNotEmpty)
                                              {
                                                dbhelper.update(data_To_Table,
                                                    DatabaseTables
                                                        .TABLE_VISITCARD,cardid);
                                              }
                                              else {
                                                dbhelper.insert(data_To_Table,
                                                    DatabaseTables
                                                        .TABLE_VISITCARD);
                                              }

                                                ShareExtend.share(f!.path, "file");
                                              }
                                            else{
                                              if(cardid.isNotEmpty)
                                              {
                                                dbhelper.update(data_To_Table,
                                                    DatabaseTables
                                                        .TABLE_VISITCARD,cardid);
                                              }
                                              else {
                                                dbhelper.insert(data_To_Table,
                                                    DatabaseTables
                                                        .TABLE_VISITCARD);
                                              }

                                              ShareExtend.share("Name : "+namecontroller.text+"\n Phone :"+phonecontroller.text+"\n Email : "+
                                                  emailcontroller.text+"\n Company address : "+companyaddresscontroller.text+"\n Whatsapp : "+
                                                  whatsappcontroller.text+"\n Designation : "+designationcontroller.text+"\n Website : "+
                                                  websitecontroller.text+"\n Company name : "+companynamecontroller.text+
                                                  "\n Facebook : "+facebookcontroller.text+"\n Instagram : "+instagramcontroller.text+"\n"+
                                                  "\n youtube : "+youtubecontroller.text, "text");
                                             }
                                            //
                                               Navigator.of(context).pop();

                                          }, groupValue: '',
                                        ))
                                            .toList(),
                                      ),
                                    ),
                                  ));


                        showDialog(context: context, builder: (BuildContext context) => dialog);




                      //

                      },
                      child: Text("Share",style: TextStyle(fontSize: 14),),))
                ),
                flex: 1,
              )
            ],
          ),
        ));

    showDialog(
        context: context, builder: (BuildContext context) => sharedialog);
  }

  setdataForEditing(String cardid)
  async {
    dynamic data=await dbhelper.getDataByid(DatabaseTables.TABLE_VISITCARD, cardid);


    List<Map<String, dynamic>> accountsettings=data;

    Map ab=accountsettings[0];



  int id = ab["keyid"];
  String data1 = ab["data"];

  var jsondata = jsonDecode(data1);


  setState(() {

  namecontroller.text=jsondata['name'];
  phonecontroller.text=jsondata['phone'];
  emailcontroller.text=jsondata['email'];
  companyaddresscontroller.text=jsondata['address'];
  whatsappcontroller.text=jsondata['whatsapp'];
  designationcontroller.text=jsondata['designation'];
  websitecontroller.text=jsondata['website'];
  companynamecontroller.text=jsondata['company'];
  selectedcardindex=int.parse(jsondata['cardbg'].toString());
  facebookcontroller.text=jsondata['fb'];
  youtubecontroller.text=jsondata['youtube'];
  instagramcontroller.text=jsondata['instagram'];

  });

















  }

  Future<File?> writeToFile(ByteData data) async {

    var d=DateTime.now().toString();
    final buffer = data.buffer;
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    var filePath = tempPath + '/'+d+'.png'; // file_01.tmp is dump file, can be anything
    return new File(filePath).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));




  }

  Future<File?> getImageFileFromAssets(String path) async {
    final byteData = await rootBundle.load('assets/$path');

    final file = File('${(await getTemporaryDirectory()).path}/$path');
    await file.writeAsBytes(byteData.buffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return file;


  }
}
