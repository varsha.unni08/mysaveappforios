import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:saveappforios/database/DBTables.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/domain/LiabilityData.dart';
// import 'package:save_flutter/mainviews/AddInsurance.dart';
// import 'package:save_flutter/mainviews/AddInvestment.dart';
// import 'package:save_flutter/mainviews/AddLiability.dart';
// import 'package:save_flutter/mainviews/CashbankAccountDetails.dart';
//
//
// import 'package:save_flutter/domain/Paymentdata.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:save_flutter/mainviews/AddRecipt.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';

import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import '../domain/LiabilityData.dart';
import '../utils/Tutorials.dart';
import 'AddBillVoucher.dart';
import 'AddInvestment.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;


class InvestmentListPage extends StatefulWidget {
  final String title;

  const InvestmentListPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _InvestmentListPage();

}

class _InvestmentListPage extends State<InvestmentListPage> {


  String date = "",
      date1 = "";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Account name", "Debit", "Credit", "Action"];

   DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<LiabilityData>lbdata=[];

  List<Cashbankaccount> cashbankdata = [];


  @override
  void initState() {
    // TODO: implement initState

    Tutorial.showTutorial(Tutorial. investment_tutorial, context, Tutorial.investmenttutorial);

    getInsuranceData();

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Investment",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),

      body: Stack(

          children:[

            Align(

                alignment: Alignment.topCenter,
                child:   Padding(padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(15, 10, 15, 10) : EdgeInsets.fromLTRB(18, 12, 18, 12): EdgeInsets.fromLTRB(22, 15, 22, 15),


                  child: Container(child:
                  MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(),

                        shrinkWrap: true,

                        itemCount: lbdata.length,
                        itemBuilder: (context, index) {
                          return  Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4) :EdgeInsets.all(6):EdgeInsets.all(8),child: Card(
                            elevation: 6,
                            child: Container(


                              width: double.infinity,


                              child: Padding(



                                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(12):EdgeInsets.all(14),
                                  child: Column(

                                    children: [

                                      Padding(



                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(5):EdgeInsets.all(7):EdgeInsets.all(9),
                                          child:
                                          Row(
                                            children: [

                                              Expanded(
                                                child: Text("Account name  ",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:19),),
                                                flex: 2,
                                              ),
                                              Expanded(
                                                child: Text(":",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17)),
                                                flex: 1,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  lbdata[index].name,
                                                  style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),
                                                ),
                                                flex: 2,
                                              )


                                            ],

                                          )),

                                      Padding(



                                         padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(5):EdgeInsets.all(7):EdgeInsets.all(9),
                                          child:
                                          Row(
                                            children: [

                                              Expanded(
                                                child: Text("Amount  ",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17)),
                                                flex: 2,
                                              ),
                                              Expanded(
                                                child: Text(":",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17)),
                                                flex: 1,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  lbdata[index].amount,
                                                  style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),
                                                ),
                                                flex: 2,
                                              )


                                            ],

                                          )),


                                      Divider(
                                        thickness: 1,
                                        color: Colors.black26,
                                      ),

                                      Padding(
                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4):EdgeInsets.all(6),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: TextButton(
                                                  onPressed: () async {



                                                    Map results = await Navigator.of(context)
                                                        .push(new MaterialPageRoute<dynamic>(
                                                      builder: (BuildContext context) {
                                                        return new AddInvestmentPage(
                                                          title: "AddLiability", liabilityid: lbdata[index].id,
                                                        );
                                                      },
                                                    ));

                                                    if (results != null &&
                                                        results.containsKey('accountsetupdata')) {
                                                      setState(() {
                                                        var acc_selected =
                                                        results['accountsetupdata'];

                                                        int acs =
                                                        acc_selected as int;

                                                        if(acs!=0) {



                                                          getInsuranceData();



                                                        }




                                                      });
                                                    }






                                                  },
                                                  child: Text(
                                                    "Edit",
                                                    style: TextStyle(
                                                        color: Colors.lightGreen,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),
                                                  ),
                                                ),
                                                flex: 2,
                                              ),
                                              Container(
                                                width: 1,
                                                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?40:50:60,
                                                color: Colors.grey,
                                              ),
                                              Expanded(
                                                child: TextButton(
                                                  onPressed: () {

                                                    Widget yesButton = TextButton(
                                                        child: Text("Yes",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
                                                        onPressed: () async {

                                                          Navigator.pop(context);

                                                          deleteLiabilityData( lbdata[index].id,index);


                                                        });



                                                    Widget noButton = TextButton(
                                                      child: Text("No",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                    );

                                                    // set up the AlertDialog
                                                    AlertDialog alert = AlertDialog(
                                                      title: Text("Save",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                                                      content: Text("Do you want to delete now ?",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18)),
                                                      actions: [yesButton, noButton],
                                                    );

                                                    // show the dialog
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        return alert;
                                                      },
                                                    );





                                                  },
                                                  child: Text(
                                                    "Delete",
                                                    style: TextStyle(
                                                      fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18,

                                                        color: Colors.redAccent),
                                                  ),
                                                ),
                                                flex: 2,
                                              ),
                                            ],
                                          ))
                                    ],



                                  )),
                            ),
                          ));
                        },
                      ))
                  ),)

            ),



            Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                    padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(12) : EdgeInsets.all(16): EdgeInsets.all(20),
                    child: FloatingActionButton(
                      onPressed: () async {

                        Map results = await Navigator.of(context)
                            .push(new MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) {
                            return new AddInvestmentPage(
                              title: "AddInsurance", liabilityid: '0',
                            );
                          },
                        ));

                        if (results != null &&
                            results.containsKey('accountsetupdata')) {
                          setState(() {
                            var acc_selected =
                            results['accountsetupdata'];

                            int acs =
                            acc_selected as int;

                            if(acs!=0) {



                              getInsuranceData();



                            }




                          });
                        }



                      },
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?29:35:45,
                      ),
                      backgroundColor: Colors.blue,
                      elevation: 5,
                      splashColor: Colors.grey,
                    ))),

          ]



      ),










    );


  }

  deleteLiabilityData(String id,int index)
  {
    dbhelper.deleteDataByid(id, DatabaseTables.INVESTMENT_table);

    setState(() {

      lbdata.removeAt(index);
    });
  }


  getInsuranceData() async
  {

    // assetdata['name']=accountid;
    // assetdata['amount']=amountcontroller.text.toString();
    // assetdata['purchase_date']=datetxt;
    // assetdata['remind_date']=rminddates;
    // assetdata['remarks']=remarkcontroller.text.toString();

    List<LiabilityData>asd=[];




    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.INVESTMENT_table);
    List<LiabilityData> acsdata = [];


    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["name"];
      String amount=jsondata["amount"];
      LiabilityData asdata=new LiabilityData();
      asdata.id=id.toString();
      asdata.name=await getDataById(Accountname);
      asdata.amount=amount;

      acsdata.add(asdata);



    }

    setState(() {
      lbdata.clear();

      lbdata.addAll(acsdata);
    });

  }


  Future<String> getDataById(String id) async
  {
    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Accountname = "";

    // accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int subid = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);


      if (subid.toString().compareTo(id) == 0) {
        Accountname = jsondata["Accountname"];
      }
    }

    //String Accountname="";

    return Accountname;
  }

}