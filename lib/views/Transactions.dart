import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';





import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/CashBankAccountDart.dart';
import '../projectconstants/DataConstants.dart';
import '../utils/Tutorials.dart';
import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;


class TransactionPage extends StatefulWidget {
  final String title;

  const TransactionPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _TransactionPage();

}


class _TransactionPage extends State<TransactionPage> {


  String date = "";
  String date1="";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Date", "Amount", "Debit", "Credit"];

  DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> ledgerdata = [];

  List<Cashbankaccount> cashbankdata = [];


  @override
  void initState() {
    // TODO: implement initState
   // showCurrentMonthAndYear();
    Tutorial.showTutorial(Tutorial.transaction_tutorial, context, Tutorial.transactiontutorial);
    showDates();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(

      resizeToAvoidBottomInset: true,

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Transactions",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18),),
        centerTitle: false,
      ),


      body: Stack(


        children: [



              SizedBox(
                height:
                ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?160:190:250,

                child:Stack(

                  children: [


                    Align(


                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,

                        children: [
                          Row(
                            children: [

                              Expanded(child: Padding(
                                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black38)),
                                    child: Row(
                                      textDirection: ui.TextDirection.rtl,
                                      children: <Widget>[
                                        Padding(
                                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),
                                            child: GestureDetector(
                                              onTap: (){

                                                var now = DateTime.now();

                                                date= now.day
                                                    .toString() +
                                                    "-" +
                                                    now.month
                                                        .toString() +
                                                    "-" +
                                                    now.year
                                                        .toString();

                                                // showCustomDatePicker(build(context));

                                                showModalBottomSheet(
                                                    context: context,
                                                    builder: (context) {
                                                      return Container(
                                                          child: Column(children: [
                                                            Expanded(
                                                              child: CupertinoDatePicker(
                                                                mode: CupertinoDatePickerMode
                                                                    .date,
                                                                initialDateTime: DateTime(
                                                                    now.year,
                                                                    now.month,
                                                                    now.day),
                                                                onDateTimeChanged:
                                                                    (DateTime newDateTime) {



                                                                  date= newDateTime.day
                                                                      .toString() +
                                                                      "-" +
                                                                      newDateTime.month
                                                                          .toString() +
                                                                      "-" +
                                                                      newDateTime.year
                                                                          .toString();


                                                                  datetxt1=date;



                                                                  //print(date);
                                                                  // Do something
                                                                },
                                                              ),
                                                              flex: 2,
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.all(2),
                                                              child: Container(
                                                                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                                width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                                                decoration: BoxDecoration(
                                                                    color: Color(0xF0233048),
                                                                    borderRadius:
                                                                    BorderRadius.circular(
                                                                        10)),
                                                                child: TextButton(
                                                                  onPressed: () {
                                                                    Navigator.pop(context);

                                                                    setState(() {
                                                                      datetxt1 = date;
                                                                    });
                                                                  },
                                                                  child: Text(
                                                                    'Ok',
                                                                    style: TextStyle(
                                                                        color: Colors.white,
                                                                        fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ]));
                                                    });

                                              },

                                              child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,),
                                            )



                                        ),
                                        Expanded(
                                            child: TextButton(
                                              onPressed: () {
                                                var now = DateTime.now();

                                                date= now.day
                                                    .toString() +
                                                    "-" +
                                                    now.month
                                                        .toString() +
                                                    "-" +
                                                    now.year
                                                        .toString();

                                                // showCustomDatePicker(build(context));

                                                showModalBottomSheet(
                                                    context: context,
                                                    builder: (context) {
                                                      return Container(
                                                          child: Column(children: [
                                                            Expanded(
                                                              child: CupertinoDatePicker(
                                                                mode: CupertinoDatePickerMode
                                                                    .date,
                                                                initialDateTime: DateTime(
                                                                    now.year,
                                                                    now.month,
                                                                    now.day),
                                                                onDateTimeChanged:
                                                                    (DateTime newDateTime) {



                                                                  date= newDateTime.day
                                                                      .toString() +
                                                                      "-" +
                                                                      newDateTime.month
                                                                          .toString() +
                                                                      "-" +
                                                                      newDateTime.year
                                                                          .toString();


                                                                  datetxt1=date;



                                                                  //print(date);
                                                                  // Do something
                                                                },
                                                              ),
                                                              flex: 2,
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.all(2),
                                                              child: Container(
                                                                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                                width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:160:170,
                                                                decoration: BoxDecoration(
                                                                    color: Color(0xF0233048),
                                                                    borderRadius:
                                                                    BorderRadius.circular(
                                                                        10)),
                                                                child: TextButton(
                                                                  onPressed: () {
                                                                    Navigator.pop(context);

                                                                    setState(() {
                                                                      datetxt1 = date;
                                                                    });
                                                                  },
                                                                  child: Text(
                                                                    'Ok',
                                                                    style: TextStyle(
                                                                        color: Colors.white,
                                                                        fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ]));
                                                    });



                                                // showCustomDatePicker(build(context));


                                              },
                                              child: Text(
                                                datetxt1,
                                                style: TextStyle(
                                                    color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                                              ),
                                            ))
                                      ],
                                    ),
                                  )),flex: 1,),
                              Expanded(child: Padding(
                                  padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black38)),
                                    child: Row(
                                      textDirection: ui.TextDirection.rtl,
                                      children: <Widget>[
                                        Padding(
                                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(13):EdgeInsets.all(16),

                                            child:GestureDetector(
                                              onTap: (){
                                                var now = DateTime.now();

                                                date1= now.day
                                                    .toString() +
                                                    "-" +
                                                    now.month
                                                        .toString() +
                                                    "-" +
                                                    now.year
                                                        .toString();

                                                // showCustomDatePicker(build(context));

                                                showModalBottomSheet(
                                                    context: context,
                                                    builder: (context) {
                                                      return Container(
                                                          child: Column(children: [
                                                            Expanded(
                                                              child: CupertinoDatePicker(
                                                                mode: CupertinoDatePickerMode
                                                                    .date,
                                                                initialDateTime: DateTime(
                                                                    now.year,
                                                                    now.month,
                                                                    now.day),
                                                                onDateTimeChanged:
                                                                    (DateTime newDateTime) {


                                                                  date1= newDateTime.day
                                                                      .toString() +
                                                                      "-" +
                                                                      newDateTime.month
                                                                          .toString() +
                                                                      "-" +
                                                                      newDateTime.year
                                                                          .toString();


                                                                  datetxt2=date1;



                                                                  //print(date);
                                                                  // Do something
                                                                },
                                                              ),
                                                              flex: 2,
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.all(2),
                                                              child: Container(
                                                                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150:160:170,
                                                                decoration: BoxDecoration(
                                                                    color: Color(0xF0233048),
                                                                    borderRadius:
                                                                    BorderRadius.circular(
                                                                        10)),
                                                                child: TextButton(
                                                                  onPressed: () {
                                                                    Navigator.pop(context);

                                                                    setState(() {
                                                                      datetxt2 = date1;
                                                                    });
                                                                  },
                                                                  child: Text(
                                                                    'Ok',
                                                                    style: TextStyle(
                                                                        color: Colors.white,
                                                                        fontSize: 15),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ]));
                                                    });

                                              },

                                              child:Icon(Icons.calendar_month,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,) ,

                                            )



                                        ),
                                        Expanded(
                                            child: TextButton(
                                              onPressed: () {
                                                var now = DateTime.now();

                                                date1= now.day
                                                    .toString() +
                                                    "-" +
                                                    now.month
                                                        .toString() +
                                                    "-" +
                                                    now.year
                                                        .toString();

                                                // showCustomDatePicker(build(context));

                                                showModalBottomSheet(
                                                    context: context,
                                                    builder: (context) {
                                                      return Container(
                                                          child: Column(children: [
                                                            Expanded(
                                                              child: CupertinoDatePicker(
                                                                mode: CupertinoDatePickerMode
                                                                    .date,
                                                                initialDateTime: DateTime(
                                                                    now.year,
                                                                    now.month,
                                                                    now.day),
                                                                onDateTimeChanged:
                                                                    (DateTime newDateTime) {


                                                                  date1= newDateTime.day
                                                                      .toString() +
                                                                      "-" +
                                                                      newDateTime.month
                                                                          .toString() +
                                                                      "-" +
                                                                      newDateTime.year
                                                                          .toString();


                                                                  datetxt2=date1;



                                                                  //print(date);
                                                                  // Do something
                                                                },
                                                              ),
                                                              flex: 2,
                                                            ),
                                                            Padding(
                                                              padding: const EdgeInsets.all(2),
                                                              child: Container(
                                                                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150:160:170,
                                                                decoration: BoxDecoration(
                                                                    color: Color(0xF0233048),
                                                                    borderRadius:
                                                                    BorderRadius.circular(
                                                                        10)),
                                                                child: TextButton(
                                                                  onPressed: () {
                                                                    Navigator.pop(context);

                                                                    setState(() {
                                                                      datetxt2 = date1;
                                                                    });
                                                                  },
                                                                  child: Text(
                                                                    'Ok',
                                                                    style: TextStyle(
                                                                        color: Colors.white,
                                                                        fontSize: 15),
                                                                  ),
                                                                ),
                                                              ),
                                                            )
                                                          ]));
                                                    });


                                                // showCustomDatePicker(build(context));


                                              },
                                              child: Text(
                                                datetxt2,
                                                style: TextStyle(
                                                    color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                                              ),
                                            ))
                                      ],
                                    ),
                                  )),flex: 1,)


                            ],

                          ),

    Stack(

    children: [

                         Align(
                           alignment: FractionalOffset.center,
                           child: Padding(
                             padding: const EdgeInsets.all(2),
                             child: Container(
                               height: 50,
                               width: 150,
                               decoration: BoxDecoration(
                                   color: Color(0xF0233048),
                                   borderRadius: BorderRadius.circular(10)),
                               child: TextButton(
                                 onPressed: () {

                                   showData();

                                 },
                                 child: Text(
                                   'Submit',
                                   style:
                                   TextStyle(color: Colors.white, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                 ),
                               ),
                             ),
                           ),

                         )


                          ])


                        ],



                      ),
                      alignment: FractionalOffset.topCenter,
                    ),


                    Align(
                      alignment: FractionalOffset.bottomCenter,

                      child:(ledgerdata.length>0)?Padding(padding:

                      ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(10, 0, 10, 0) : EdgeInsets.fromLTRB(15, 0, 15, 0):EdgeInsets.fromLTRB(20, 0, 20, 0),

                         child: MediaQuery.removePadding(
                              context: context,
                              removeTop: true,
                              removeBottom: true,
                              child:GridView.builder(
                                physics: BouncingScrollPhysics(),

                                shrinkWrap: true,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 4,
                                    crossAxisSpacing: 0.0,
                                    mainAxisSpacing: 0.0,
                                    childAspectRatio:2
                                ),
                                itemCount: tableheaddata.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding:EdgeInsets.all(0),

                                    child: Container(
                                        height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 100:130:160,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black54,
                                            width: 0.3,
                                          ),
                                        ),


                                        child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),))),

                                  );
                                },
                              ))):Container(),

                    )







                  ],



                ),



                )






,










          Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 160, 10, 0) :  EdgeInsets.fromLTRB(15, 190, 15, 0): EdgeInsets.fromLTRB(20, 250, 20, 0),
            child: Column(
                children: [
                  Expanded(
                    child: Container(child:
            MediaQuery.removePadding(
                context: context,
                removeTop: true,
                        child: GridView.builder(
                          physics: BouncingScrollPhysics(),

                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 4,
                            crossAxisSpacing: 0.0,
                            mainAxisSpacing: 0.0,
                            childAspectRatio: 0.7
                          ),
                          itemCount: ledgerdata.length,
                          itemBuilder: (context, index) {
                            return Container(

                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black54,
                                    width: 0.3,
                                  ),
                                ),




                                child:


                                    Center(child:Text(ledgerdata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18,color: Colors.black),)));
                          },
                        ))
                    ),flex: 3,
                  ),

                ]
            ),)

        ]

      )




    );
  }

  void showDates()
  {
    List<String> cashbankaccountatanow = [];

    var now=DateTime.now();

    // datetxt1="1"+"-"+now.month.toString()+"-"+now.year.toString();
    // datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    setState(() {

      datetxt1="1"+"-"+now.month.toString()+"-"+now.year.toString();
      datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();



    });

    showData();
  }

  void showData() async
  {

    List<String>data=[];

    DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(datetxt1);

    DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(datetxt2);

    if(accountsdateselected.isAfter(accountsdateparsed)||accountsdateselected.compareTo(accountsdateparsed)==0)
    {


      var dbdata=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);

      List<Map<String,dynamic>>accounts=dbdata;



      for(int i=0;i<accounts.length;i++) {
        Map<String, dynamic> mapval = accounts[i];
        String dateaccount = mapval[DatabaseTables.ACCOUNTS_date];

        String setupid = mapval[DatabaseTables.ACCOUNTS_setupid];

        String amount = mapval[DatabaseTables.ACCOUNTS_amount];

        String accounttype = mapval[DatabaseTables.ACCOUNTS_type].toString();

        DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(dateaccount);

        DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(datetxt1);

        DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(datetxt2);

        var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupid);


        List<Map<String, dynamic>> accsettings1=value;

        Map ab1=accsettings1[0];

        // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]



         int id1 = ab1["keyid"];
         String data1 = ab1["data"];

        var jsondata1 = jsonDecode(data1);



        String Accountname=jsondata1["Accountname"];






        if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
            accountsdateparsed.isBefore(accountsdatend)) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
            {
              data.add(amount);
              data.add("");
            }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
            {
              data.add("");
              data.add(amount);

            }






        }
        else if (accountsdateparsed.isAfter(accountsdatestart) &&
            accountsdateparsed.compareTo(accountsdatend) == 0) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
          {
            data.add(amount);
            data.add("");
          }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
          {
            data.add("");
            data.add(amount);

          }



        }
        else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
            accountsdateparsed.compareTo(accountsdatend) == 0) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
          {
            data.add(amount);
            data.add("");
          }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
          {
            data.add("");
            data.add(amount);

          }


        }
        else if (accountsdateparsed.isAfter(accountsdatestart) &&
            accountsdateparsed.isBefore(accountsdatend)) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
          {
            data.add(amount);
            data.add("");
          }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
          {
            data.add("");
            data.add(amount);

          }


        }



        }





      setState(() {

        ledgerdata.clear();

        ledgerdata.addAll(data);
      });

      }
    else{


    }




  }


}