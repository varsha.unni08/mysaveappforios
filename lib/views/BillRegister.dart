import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:open_file/open_file.dart';


import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../projectconstants/DataConstants.dart';








class BillRegisterPage extends StatefulWidget {

  final String title;




  const BillRegisterPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState()  => _BillPageDetailsPage(title);

}


class _BillPageDetailsPage extends State<BillRegisterPage> {

  String title;




  String accountname="";

  DatabaseHelper dbhelper=new DatabaseHelper();

  List<String>accdata=[];

  List<String>tableheaddata = [

    "Date",
    "Billno",
    "Customer",

    "Amount"
  ];


  _BillPageDetailsPage(this.title);

  @override
  void initState() {
    // TODO: implement initState

checkBillData();
    super.initState();
SystemChrome.setPreferredOrientations([
  DeviceOrientation.portraitDown,
  DeviceOrientation.portraitUp,
]);
  }


  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
    leading: IconButton(
    icon: Icon(Icons.arrow_back, color: Colors.white),
    onPressed: () => Navigator.of(context).pop(),
    ),
    title: Text("Bill register",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17),),
    centerTitle: false,
    ),
      body: Stack(
          children: [


            SizedBox(
              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?70:90:110,
              child: Stack(

                children: [

                  Align(

                    alignment: FractionalOffset.bottomCenter,
                    child: (accdata.length>0)?    Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(10,0,10,0):EdgeInsets.fromLTRB(13,0,13,0):EdgeInsets.fromLTRB(16,0,16,0),




                        child: MediaQuery.removePadding(
                          context: context,
                          removeBottom: true,
                          removeTop: true, child:GridView.builder(
                          physics: BouncingScrollPhysics(),

                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              crossAxisSpacing: 0.0,
                              mainAxisSpacing: 0.0,
                              childAspectRatio: 2.0

                          ),
                          itemCount: tableheaddata.length,
                          itemBuilder: (context, index) {
                            return  Container(

                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black54,
                                    width: 0.3,
                                  ),
                                ),

                                child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17,color: Colors.black),maxLines: 2,)));


                          },
                        )
                          ,)



                    ):Container(),


                  )

                ],

              ) ,


            ),




            (accdata.length>0)?    Padding(padding:


            ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?
            EdgeInsets.fromLTRB(10, 70, 10, 0) : EdgeInsets.fromLTRB(13, 90, 13, 0):EdgeInsets.fromLTRB(16, 110, 16, 0)

                ,child:MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    removeBottom: true,
                    child:GridView.builder(
                      physics: BouncingScrollPhysics(),

                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          crossAxisSpacing: 0.0,
                          mainAxisSpacing: 0.0,
                          childAspectRatio:2.0

                      ),
                      itemCount: accdata.length,
                      itemBuilder: (context, index) {
                        return Container(

                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black54,
                              width: 0.3,
                            ),
                          ),

                          child:

                          Center(child:Text(accdata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13 :15:17,color: Colors.black),)),

                        );
                      },
                    ))):Container(),

          ]

      ),



    );
  }


  void checkBillData() async
  {
    List<Map<String, dynamic>> m=await new DatabaseHelper().getAccounDataByEntryid("0");
    List<Map<String, dynamic>> v=m;

    List<String>accountdata=[];

    for(Map a in v) {
      String vouchertype = a[DatabaseTables.ACCOUNTS_VoucherType].toString();

      int vtype=int.parse(vouchertype);

      if(vtype==DataConstants.billvoucher)
        {
          String date = a[DatabaseTables.ACCOUNTS_date];
          String billno = a[DatabaseTables.ACCOUNTS_billVoucherNumber];
          String setupidno = a[DatabaseTables.ACCOUNTS_setupid];

          var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupidno);


          List<Map<String, dynamic>> accountsettings=value;

          Map ab=accountsettings[0];

          // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

          int id = ab["keyid"];
          String data = ab["data"];

          var jsondata = jsonDecode(data);

          String Accountname = jsondata["Accountname"];



          String amount = a[DatabaseTables.ACCOUNTS_amount];

          accountdata.add(date);
          accountdata.add(billno);
          accountdata.add(Accountname);
          accountdata.add(amount);


        }


    }

    setState(() {

      accdata.clear();
          accdata.addAll(accountdata);
    });




  }



}