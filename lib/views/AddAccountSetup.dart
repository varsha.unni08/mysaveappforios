import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/design/ResponsiveInfo.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../domain/AccountSetupdata.dart';
import '../domain/Accountsettings.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Accountsettings',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: AddAccountSettinglistpage(
          title: 'Accountsettings', accountType: 'all', accountsetupid: '0',),
      debugShowCheckedModeBanner: false,
    );
  }
}

class AddAccountSettinglistpage extends StatefulWidget {
  final String title;

  final String accountType;

 final String accountsetupid;

  const AddAccountSettinglistpage(
      {Key? key, required this.title, required this.accountType, required this.accountsetupid })
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _AddAccountSettinglistpage(accountType,accountsetupid);
}

class _AddAccountSettinglistpage extends State<AddAccountSettinglistpage> {
  String accountType;

  String accountsetupid;

  _AddAccountSettinglistpage(this.accountType,this.accountsetupid);

  final String dropdown_account = "Select an account";

  List<String> arr = ["Select an account"];
  List<String> arrradion = ["Bank", "Cash"];

  List<String> accountsetupcategory = [
    "Select an account",
    "Asset account",
    "Bank",
    "Cash",
    "Credit card",
    "Customers",
    "Expense account",
    "Income account",
    "Insurance",
    "Investment",
    "Liability account"
  ];

  late String dropdownValue = "Select an account";

  List<String> type = [ "Debit", "Credit"];

  late String dropdowntype = "Debit";

  int bid = 2, cid = 0;
   final dbHelper = new DatabaseHelper();

  List<Accountsettings> acc = [];
  List<Accountsettings> accs = [];
  List<Accountsettings> dummy = [];

  TextEditingController accountnamecontroller = new TextEditingController();

  TextEditingController openingbalancecontroller = new TextEditingController();

  AccountSetupData accsetupdata=new AccountSetupData() ;

  @override
  void initState() {
    // TODO: implement initState

    if(accountsetupid.compareTo("0")!=0)
      {
        getDataAccountSetup(accountsetupid);
      }

    if(accountType.isNotEmpty)
      {

        setState(() {
          dropdownValue=accountType;
        });

      }
    else{

    }


    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Add account setup",style: TextStyle(fontSize: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)?14:16:19),),
          centerTitle: false,
        ),
        body:  SingleChildScrollView(


           child: Column(
              children: [
                Padding(
                  padding: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? EdgeInsets.only(
                      left: 15.0, right: 15.0, top: 10, bottom: 0) : EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15, bottom: 0) :EdgeInsets.only(
                      left: 25.0, right: 25.0, top: 20, bottom: 0),
                  // padding: EdgeInsets.all(15),
                  child: new Theme(
                      data: new ThemeData(hintColor: Colors.black26),
                      child: TextField(
                        controller: accountnamecontroller,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: Colors.black, width: 0.5),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: Colors.black, width: 0.5),
                          ),
                          hintText: 'Account name',
                        ),
                        onChanged: (text) {
                          //TemRegData.email=text;
                        },
                      )),
                ),
                Padding(
                    padding: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15) :EdgeInsets.all(20),
                    child: Container(
                      width: double.infinity,
                      height: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? 40:55:65,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          // red as border color
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: InputDecorator(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                value: dropdownValue,
                                items: accountsetupcategory
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    dropdownValue = newValue.toString();
                                  });

                                  setupAccountType(newValue.toString());
                                },
                                //  value: dropdownValue,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )),
                Padding(
                  padding: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? EdgeInsets.only(
                      left: 15.0, right: 15.0, top: 10, bottom: 0) : EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 15, bottom: 0):
                  EdgeInsets.only(
                      left: 25.0, right: 25.0, top: 20, bottom: 0),
                  // padding: EdgeInsets.all(15),
                  child: new Theme(
                      data: new ThemeData(hintColor: Colors.black26),
                      child: TextField(
                        controller: openingbalancecontroller,
                        keyboardType: TextInputType.number,
                        style: TextStyle(fontSize:ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)?14:16:19 ),
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: Colors.black, width: 0.5),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: Colors.black, width: 0.5),
                          ),
                          hintText: 'Opening balance',
                        ),
                        onChanged: (text) {
                          //TemRegData.email=text;
                        },
                      )),
                ),
                Padding(
                    padding: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(20),
                    child: Container(
                      width: double.infinity,
                      height:ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? 40:55:65,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          // red as border color
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: InputDecorator(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                value: dropdowntype,
                                items: type.map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                onChanged: (String? newValue) {

                                  setState(() {
                                    dropdowntype=newValue.toString();
                                  });



                                },
                                //  value: dropdownValue,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )),
                Padding(
                  padding: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(20),
                  child: Container(
                    height:ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? 45:55:70,
                    width: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)?  150:180:200,
                    decoration: BoxDecoration(
                        color: Color(0xF0233048),
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                      onPressed: () async {
                        if (accountnamecontroller.value.text
                            .toString()
                            .isNotEmpty) {
                          if (dropdownValue.compareTo("Select an account") != 0) {
                            String value = "0";

                            if (openingbalancecontroller.value.text.isNotEmpty) {
                              value = openingbalancecontroller.value.text;
                            }

                            var mjobject = new Map();
                            mjobject['Accountname'] =
                                accountnamecontroller.value.text;
                            mjobject['Accounttype'] = dropdownValue;
                            mjobject['Amount'] = value;
                            mjobject['Type'] = dropdowntype;

                            var js = json.encode(mjobject);
                            Map<String, dynamic> data_To_Table = new Map();
                            data_To_Table['data'] = js.toString();

                            if(accountsetupid.compareTo("0")!=0)
                            {
                              int id = await dbHelper.update(data_To_Table,
                                  DatabaseTables.TABLE_ACCOUNTSETTINGS,accountsetupid);
                              Navigator.of(context).pop({'accountsetupdata':int.parse(accountsetupid) });
                            }
                            else {
                              int id = await dbHelper.insert(data_To_Table,
                                  DatabaseTables.TABLE_ACCOUNTSETTINGS);
                              Navigator.of(context).pop({'accountsetupdata': id});
                            }


                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Select account category"),
                            ));
                          }
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Enter your account name"),
                          ));
                        }
                      },
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.white, fontSize: ResponsiveInfo.isSmallMobile(context)?ResponsiveInfo.isMobile(context)? 14:16:19),
                      ),
                    ),
                  ),
                ),
              ],
            )

       


        )




        );
  }

  void addAccountSetup() {}

  void setupAccountType(String accounttype) {
    if (accounttype.compareTo("Asset account") == 0) {
      setState(() {
        dropdowntype = "Debit";
      });
    } else if (accounttype.compareTo("Bank") == 0) {
      setState(() {
        dropdowntype = "Debit";
      });
    } else if (accounttype.compareTo("Cash") == 0) {
      setState(() {
        dropdowntype = "Debit";
      });
    } else if (accounttype.compareTo("Credit card") == 0) {
      setState(() {
        dropdowntype = "Credit";
      });
    } else if (accounttype.compareTo("Customers") == 0) {
      setState(() {
        dropdowntype = "Debit";
      });
    } else if (accounttype.compareTo("Expense account") == 0) {
      setState(() {
        dropdowntype = "Debit";
      });
    } else if (accounttype.compareTo("Income account") == 0) {
      setState(() {
        dropdowntype = "Credit";
      });
    } else if (accounttype.compareTo("Insurance") == 0) {
      setState(() {
        dropdowntype = "Debit";
      });
    } else if (accounttype.compareTo("Investment") == 0) {
      setState(() {
        dropdowntype = "Debit";
      });
    } else if (accounttype.compareTo("Liability account") == 0) {
      setState(() {
        dropdowntype = "Credit";
      });
    }
  }



  Future<void> getDataAccountSetup(String dataid) async
   {
    var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, dataid);


    List<Map<String, dynamic>> accountsettings=value;

    Map ab=accountsettings[0];

   // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

    int id = ab["keyid"];
    String data = ab["data"];

    var jsondata = jsonDecode(data);

    String Accountname = jsondata["Accountname"];
    String Accounttype = jsondata["Accounttype"];
    String Amount = jsondata["Amount"];
    String Type = jsondata["Type"];

    AccountSetupData dataaccsetup = new AccountSetupData();
    dataaccsetup.id = id.toString();
    dataaccsetup.Accountname = Accountname;
    dataaccsetup.Accounttype = Accounttype;
    dataaccsetup.Amount = Amount;
    dataaccsetup.Type = Type;

    setState(() {
      accountnamecontroller.text=Accountname;
      openingbalancecontroller.text=Amount;

      dropdowntype=Type;
      dropdownValue=Accounttype;


    });





 //  accsetupdata.add(dataaccsetup);



  }
}
