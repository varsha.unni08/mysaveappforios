import 'dart:convert';

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
 import 'package:path_provider/path_provider.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountledger.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
//
//
// import 'package:save_flutter/domain/Paymentdata.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:save_flutter/mainviews/AddRecipt.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:open_file/open_file.dart';


import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Accountledger.dart';
import '../domain/CashBankAccountDart.dart';
import '../projectconstants/DataConstants.dart';




class CashbankstatementDetailsPage extends StatefulWidget {
  final String title;


   final String accid;

   final Cashbankaccount cb;

   final String Startdate;

  final String Enddate;



  const CashbankstatementDetailsPage({Key? key, required this.title,  required this.accid, required this.cb, required this.Startdate, required this.Enddate})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _CashbankstatementDetailsPage(accid,cb,Startdate,Enddate);

}

class _CashbankstatementDetailsPage extends State<CashbankstatementDetailsPage> {


  DatabaseHelper dbhelper=new DatabaseHelper();

   String accname="";
   String aacid="0";

  String startdate="";
  String enddate="";

  Cashbankaccount cb;

  List<String>accledgerfull=[];


  List<Accountledger>accledgerfullforPdf=[];
  List<String>tableheaddata=["Date","Name","Amount","Debit/Credit"];


   _CashbankstatementDetailsPage( this.aacid,this.cb, this. startdate, this. enddate);

  @override
  void initState() {
    // TODO: implement initState

    showAccountDetails(aacid);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {


    double closingbalance=cb.amount*-1;
    // TODO: implement build
    return Scaffold(

      resizeToAvoidBottomInset: true,

      appBar: AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?60:70:80,

        flexibleSpace: Container(

            color: Color(0xFF096c6c),
            width: double.infinity,
            padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Container(
                        margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(2, 50, 0, 0) : EdgeInsets.fromLTRB(4, 60, 0, 0):EdgeInsets.fromLTRB(6, 70, 0, 0),
                        alignment: Alignment.center,
                        child: Center(
                            child: new InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.arrow_back,color: Colors.white,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,)))),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(
                        margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 50, 0, 0) : EdgeInsets.fromLTRB(15, 60, 0, 0):EdgeInsets.fromLTRB(20, 70, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Row(children: [

                          Expanded(child: Text(
                            "Ledger : "+accname,
                            style: TextStyle(
                                fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13 : 16:19, color: Colors.white),
                          ),flex: 2,),

                          Expanded(child:Center(
                              child: new InkWell(
                                  onTap: () async {


                                    var d=DateTime.now().toString();

                                    Directory tempDir = await getTemporaryDirectory();
                                    String tempPath = tempDir.path;
                                    var filePath = tempPath + '/'+d+'.pdf';

                                  File f= await  new File(filePath).create();

                                    final PdfDocument document = PdfDocument();

                                    final PdfPage page = document.pages.add();
// Create a PDF grid class to add tables.
                                    final PdfGrid grid = PdfGrid();
// Specify the grid column count.
                                    grid.columns.add(count: 4);
// Add a grid header row.
                                    final PdfGridRow headerRow = grid.headers.add(1)[0];
                                    headerRow.cells[0].value = 'Date';
                                    headerRow.cells[1].value = 'Name';
                                    headerRow.cells[2].value = 'Amount';
                                    headerRow.cells[3].value = 'Debit/Credit';
// Set header font.
                                    headerRow.style.font =
                                        PdfStandardFont(PdfFontFamily.helvetica, 10, style: PdfFontStyle.bold);
                                    PdfGridRow row  ;

                                    for (int i=0;i<accledgerfullforPdf.length;i++)
                                      {
                                           row = grid.rows.add();
                                           row.cells[0].value = accledgerfullforPdf[i].date;
                                           row.cells[1].value = accledgerfullforPdf[i].accountname;
                                           row.cells[2].value = accledgerfullforPdf[i].amount;
                                           row.cells[3].value = accledgerfullforPdf[i].debitcredit;

                                      }

                                    grid.style.cellPadding = PdfPaddings(left: 5, top: 5);
// Draw table in the PDF page.
                                    grid.draw(
                                        page: page,
                                        bounds: Rect.fromLTWH(
                                            0, 0, page.getClientSize().width, page.getClientSize().height));

                                    final PdfPageTemplateElement footerTemplate =
                                    PdfPageTemplateElement( Rect.fromLTWH(0, 0, page.getClientSize().width, page.getClientSize().height));

                                    if(cb.amount>=0) {
//Draw text in the footer.
                                      footerTemplate.graphics.drawString(
                                          'Closing balance : '+cb.amount.toString(),
                                          PdfStandardFont(
                                              PdfFontFamily.helvetica, 12),
                                          bounds: Rect.fromLTWH(0, 0, page.getClientSize().width, page.getClientSize().height));
                                    }
                                    else{

                           double a=cb.amount*-1;
//Draw text in the footer.
                                        footerTemplate.graphics.drawString(
                                            'Closing balance : '+a.toString(),
                                            PdfStandardFont(
                                                PdfFontFamily.helvetica, 12),
                                            bounds: Rect.fromLTWH(0, 0, page.getClientSize().width, page.getClientSize().height));

                                    }

                                    var a=await document.save();
// Save the document.
                                    f.writeAsBytes(a);
// Dispose the document.
                                    document.dispose();

                                    OpenFile.open(f.path);





                                  },
                                  child: Icon(Icons.download,color: Colors.white,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,)))  ,flex: 1,)



                        ],)),
                    flex: 5,
                  ),
                ])) ,
        backgroundColor: Color(0xFF096c6c),
        centerTitle: false,

      ),

      body: Stack(

        children: [


          Align(

            alignment: FractionalOffset.topCenter,

            child: SizedBox(
              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?120 :150:180,

              child: Stack(

                children: [

                  Align(
                    alignment: FractionalOffset.topCenter,
                    child: Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(15) : EdgeInsets.all(20):EdgeInsets.all(25),

                        child:Text("Period from "+startdate+ " to "+ enddate,style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18,color: Colors.black),)),

                  ),

                  Align(

                    alignment: FractionalOffset.bottomCenter,
                    child: MediaQuery.removePadding(
                        context: context,
                        removeTop: true,
                        removeBottom: true,
                        child:GridView.builder(
                          physics: BouncingScrollPhysics(),

                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              crossAxisSpacing: 0.0,
                              mainAxisSpacing: 0.0,
                              childAspectRatio: 2
                          ),
                          itemCount: tableheaddata.length,
                          itemBuilder: (context, index) {
                            return Container(
                                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 100:120:140,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black54,
                                    width: 0.3,
                                  ),
                                ),

                                child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18,color: Colors.black),)));


                          },
                        )),

                  )







                ],


              ),

            )



            ,

          ),











          Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 120, 0, 5) : EdgeInsets.fromLTRB(0, 150, 0, 10):EdgeInsets.fromLTRB(0, 180, 0, 15),
          child: Stack(
              children: [
                Align(
                    child: MediaQuery.removePadding(
                      context: context,
                        removeTop: true,
                        child: GridView.builder(
                          physics: BouncingScrollPhysics(),

                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 4,
                            crossAxisSpacing: 0.0,
                            mainAxisSpacing: 0.0,
                            childAspectRatio: 1
                          ),
                          itemCount: accledgerfull.length,
                          itemBuilder: (context, index) {
                            return


                               Container(

                                   decoration: BoxDecoration(
                                     border: Border.all(
                                       color: Colors.black54,
                                       width: 0.3,
                                     ),
                                   ),

                                    child:


                                  Center(child:Text(accledgerfull[index],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18,color: Colors.black),)));
                          },
                        )
                    ),alignment: FractionalOffset.topCenter,
                ),
                Align(child: Center(child:

                (cb.amount>0)?Text("Closing balance : "+cb.amount.toString()+" (Debit)",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18 )):Text("Closing balance : "+closingbalance.toString()+" (Credit)",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18 ),)),alignment: FractionalOffset.bottomCenter,)
              ]
          ),)


        ],


      ),

    );
  }

  void showAccountDetails(String accountid) async
  {

    List<Accountledger>accledger=[];
    List<String>dataledger=[];

    List<Map<String,dynamic>>accountsettings=await dbhelper.getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS,accountid);



    Map ab=accountsettings[0];

    // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

    int id = ab["keyid"];
    String data = ab["data"];

    var jsondata = jsonDecode(data);

    String Accountname = jsondata["Accountname"];

    setState(() {

      accname=Accountname;
    });


    var dbdata=await dbhelper.getAccounDataBySetupid(aacid);

    List<Map<String,dynamic>>accounts=dbdata;

    var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, aacid);


    List<Map<String, dynamic>> accsettings1=value;

    Map ab1=accsettings1[0];

    // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]



    // int id1 = ab1["keyid"];
    // String data1 = ab1["data"];

    var jsondata1 = jsonDecode(data);


    String Amount = jsondata1["Amount"];
    String Type = jsondata1["Type"];


    Accountledger accountledger=new Accountledger();
    accountledger.accountname="Opening balance";
    accountledger.amount=Amount;
    accountledger.date="1-"+startdate.split("-")[1]+"-"+startdate.split("-")[2];
    accountledger.debitcredit=Type;
    accledger.add(accountledger);




    dataledger.add("1-"+startdate.split("-")[1]+"-"+startdate.split("-")[2]);
    dataledger.add("Opening balance");
    dataledger.add(Amount);
    dataledger.add(Type);

    for(int i=0;i<accounts.length;i++) {
      Map<String, dynamic> mapval = accounts[i];


      String accountdate=  mapval[DatabaseTables.ACCOUNTS_date];

      String accounttype="";

      accounttype=mapval[DatabaseTables.ACCOUNTS_type].toString();


      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(accountdate);

      DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(startdate);

      DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(enddate);






      String setupid = mapval[DatabaseTables.ACCOUNTS_setupid];

      String accountid = mapval[DatabaseTables.ACCOUNTS_id].toString();

      String accountentryid = mapval[DatabaseTables.ACCOUNTS_entryid].toString();

      List<Map<String, dynamic>> accbyEntry = await dbhelper.getAccounDataByEntryid(accountid);
      List<Map<String, dynamic>> accbyId = await dbhelper.getAccountDataByID(accountentryid);

      String accountnameledger="";
      String amountledger="";


      if(accbyEntry.length>0)
        {
          Map<String, dynamic> mapvalentry = accbyEntry[0];

          String setupid = mapvalentry[DatabaseTables.ACCOUNTS_setupid];
          amountledger=mapvalentry[DatabaseTables.ACCOUNTS_amount];

           accountnameledger=await getAccountsetupname(setupid);


        }

    else  if(accbyId.length>0)
      {
        Map<String, dynamic> mapvalentry = accbyId[0];

        String setupid = mapvalentry[DatabaseTables.ACCOUNTS_setupid];
        amountledger=mapvalentry[DatabaseTables.ACCOUNTS_amount];

         accountnameledger=await getAccountsetupname(setupid);


      }



      // var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupid);
      //
      //
      // List<Map<String, dynamic>> accountsettings=value;
      //
      // Map ab=accountsettings[0];
      //
      // // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]
      //
      // int id = ab["keyid"];
      // String data = ab["data"];
      //
      // var jsondata = jsonDecode(data);
      //
      // String Accountname = jsondata["Accountname"];
      // String Accounttype = jsondata["Accounttype"];
      // String Amount = jsondata["Amount"];
      // String Type = jsondata["Type"];




        if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
            accountsdateparsed.isBefore(accountsdatend)) {


          Accountledger accountledger=new Accountledger();
          accountledger.accountname=accountnameledger;
          accountledger.amount=amountledger;
          accountledger.date=accountdate;


          dataledger.add(accountdate);
          dataledger.add(accountnameledger);
          dataledger.add(amountledger);


          if (accounttype.compareTo(
              DataConstants.debit.toString()) == 0) {
            dataledger.add("Debit");
            accountledger..debitcredit="Debit";
          }
          else {
            dataledger.add("Credit");

            accountledger..debitcredit="Credit";
          }

         // accledger.add(accountledger);


          // accountledger.accountname="Opening balance";
          // accountledger.amount=Amount;
          // accountledger.date="1-"+startdate.split("-")[1]+"-"+startdate.split("-")[2];
          // accountledger.debitcredit=Type;
          accledger.add(accountledger);
        }
        else if (accountsdateparsed.isAfter(accountsdatestart) &&
            accountsdateparsed.compareTo(accountsdatend) == 0) {

          Accountledger accountledger=new Accountledger();
          accountledger.accountname=accountnameledger;
          accountledger.amount=amountledger;
          accountledger.date=accountdate;


          dataledger.add(accountdate);
          dataledger.add(accountnameledger);
          dataledger.add(amountledger);
          if (accounttype.compareTo(
              DataConstants.debit.toString()) == 0) {
            dataledger.add("Debit");

            accountledger..debitcredit="Debit";
          }
          else {
            dataledger.add("Credit");

            accountledger..debitcredit="Credit";
          }

          accledger.add(accountledger);
        }
        else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
            accountsdateparsed.compareTo(accountsdatend) == 0) {

          Accountledger accountledger=new Accountledger();
          accountledger.accountname=accountnameledger;
          accountledger.amount=amountledger;
          accountledger.date=accountdate;


          dataledger.add(accountdate);
          dataledger.add(accountnameledger);
          dataledger.add(amountledger);
          if (accounttype.compareTo(
              DataConstants.debit.toString()) == 0) {
            dataledger.add("Debit");

            accountledger..debitcredit="Debit";
          }
          else {
            dataledger.add("Credit");

            accountledger..debitcredit="Credit";
          }

          accledger.add(accountledger);
        }
        else if (accountsdateparsed.isAfter(accountsdatestart) &&
            accountsdateparsed.isBefore(accountsdatend)) {

          Accountledger accountledger=new Accountledger();
          accountledger.accountname=accountnameledger;
          accountledger.amount=amountledger;
          accountledger.date=accountdate;


          dataledger.add(accountdate);
          dataledger.add(accountnameledger);
          dataledger.add(amountledger);
          if (accounttype.compareTo(
              DataConstants.debit.toString()) == 0) {
            dataledger.add("Debit");

            accountledger..debitcredit="Debit";
          }
          else {
            dataledger.add("Credit");

            accountledger..debitcredit="Credit";
          }

          accledger.add(accountledger);
        }




          // Accountledger accountledger=new Accountledger();
          // accountledger.amount=mapval[DatabaseTables.ACCOUNTS_amount];
          // accountledger.accountname=mapval[DatabaseTables.ACCOUNTS_amount];




    }


    setState(() {

      accledgerfullforPdf.clear();

      accledgerfullforPdf.addAll(accledger);

      accledgerfull.clear();
     accledgerfull.addAll(dataledger);
    });
    }



    Future<String> getAccountsetupname(String setupid)
    async {
      var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupid);


      List<Map<String, dynamic>> accsettings1=value;

      Map ab1=accsettings1[0];

      // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]



      int id1 = ab1["keyid"];
      String data1 = ab1["data"];

      var jsondata1 = jsonDecode(data1);


      // String Amount = jsondata1["Amount"];
      // String Type = jsondata1["Type"];

      String Accountname=jsondata1["Accountname"];



      return Accountname;

    }

}