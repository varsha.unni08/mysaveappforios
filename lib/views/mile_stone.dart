import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../design/ResponsiveInfo.dart';
import '../domain/MileStoneEntity.dart';
import '../utils/StaticValues.dart';


class MileStone extends StatefulWidget {
  const MileStone() : super();

  @override
  _MileStoneState createState() => _MileStoneState();
}

class _MileStoneState extends State<MileStone> {


  TextEditingController amountcontroller = new TextEditingController();

  List<MileStoneEntity>mls=[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(StaticValues.milestoneadded.length>0)
      {

        setState(() {
          mls.clear();
          mls.addAll(StaticValues.milestoneadded);
        });

      }
    else {
      setState(() {
        mls.add(new MileStoneEntity());
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Mile stone",
          style: TextStyle(
              fontSize: ResponsiveInfo.isMobile(context)
                  ? ResponsiveInfo.isSmallMobile(context)
                      ? 14
                      : 16
                  : 19),
        ),
        centerTitle: false,
        actions: [

          Padding(padding: EdgeInsets.all(8),

            child:  GestureDetector(
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 25,
              ),
              onTap: () {

                setState(() {

                  mls.add(new MileStoneEntity());
                });

              },
            ),


          ),

          Padding(padding: EdgeInsets.all(8),

            child:  GestureDetector(
              child: Icon(
                Icons.check,
                color: Colors.white,
                size: 25,
              ),
              onTap: () {

                Navigator.of(context).pop({'accountsetupdata': 1});

              },
            ),


          )

        ],
      ),
      body:


      Stack(
        children: [
          Align(
            alignment: FractionalOffset.topCenter,
            child: ListView.builder(
                itemCount: mls.length,
                itemBuilder: (BuildContext context, int index) {
                  return (mls[index].selected==0)?  Card(
                      elevation: 4,
                      child: Padding(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.all(5),
                                      child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Colors.black38,
                                              // red as border color
                                            ),
                                          ),
                                          child: Row(

                                            children: <Widget>[
                                              Expanded(
                                                  child: TextButton(
                                                    onPressed: () async {
                                                      // showAccountsDialog();

                                                      var now = DateTime.now();

                                                      String date = now.day.toString() +
                                                          "-" +
                                                          now.month.toString() +
                                                          "-" +
                                                          now.year.toString();

                                                      String  month = now.month.toString();
                                                      String   year = now.year.toString();

                                                      // showCustomDatePicker(build(context));

                                                      showModalBottomSheet(
                                                          context: context,
                                                          builder: (context) {
                                                            return Container(
                                                                child: Column(children: [
                                                                  Expanded(
                                                                    child: CupertinoDatePicker(
                                                                      mode: CupertinoDatePickerMode
                                                                          .date,
                                                                      initialDateTime: DateTime(
                                                                          now.year,
                                                                          now.month,
                                                                          now.day),
                                                                      onDateTimeChanged:
                                                                          (DateTime newDateTime) {
                                                                        date = newDateTime.day
                                                                            .toString() +
                                                                            "-" +
                                                                            newDateTime.month
                                                                                .toString() +
                                                                            "-" +
                                                                            newDateTime.year
                                                                                .toString();

                                                                        month = newDateTime.month
                                                                            .toString();
                                                                        year = newDateTime.year
                                                                            .toString();

                                                                        //print(date);
                                                                        // Do something
                                                                      },
                                                                    ),
                                                                    flex: 2,
                                                                  ),
                                                                  Padding(
                                                                    padding: const EdgeInsets.all(2),
                                                                    child: Container(
                                                                      height: 50,
                                                                      width: 150,
                                                                      decoration: BoxDecoration(
                                                                          color: Color(0xF0233048),
                                                                          borderRadius:
                                                                          BorderRadius.circular(
                                                                              10)),
                                                                      child: TextButton(
                                                                        onPressed: () {
                                                                          Navigator.pop(context);

                                                                          setState(() {
                                                                            mls[index].start_date = date;
                                                                          });
                                                                        },
                                                                        child: Text(
                                                                          'Ok',
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 15),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  )
                                                                ]));
                                                          });
                                                    },
                                                    child: Center(
                                                        child: Text( (mls[index].start_date.isNotEmpty)?mls[index].start_date: "Start Date",
                                                            textAlign:
                                                            TextAlign.center,
                                                            style: TextStyle(
                                                                color:
                                                                Colors.black38,
                                                                fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                    context)
                                                                    ? ResponsiveInfo
                                                                    .isSmallMobile(
                                                                    context)
                                                                    ? 12
                                                                    : 13
                                                                    : 16))),
                                                  ),flex: 2,),

                                              Expanded(child: Padding(
                                                padding: EdgeInsets.all(10),
                                                child: Icon(
                                                  Icons.arrow_drop_down,
                                                  size: ResponsiveInfo.isMobile(
                                                      context)
                                                      ? ResponsiveInfo
                                                      .isSmallMobile(
                                                      context)
                                                      ? 25
                                                      : 30
                                                      : 35,
                                                  color: Colors.black12,
                                                ),
                                              ),flex: 1,)


                                            ],
                                          ))),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.all(5),
                                      child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Colors.black38,
                                              // red as border color
                                            ),
                                          ),
                                          child: Row(

                                            children: <Widget>[

                                              Expanded(
                                                  child: TextButton(
                                                    onPressed: () async {
                                                      // showAccountsDialog();

                                                      var now = DateTime.now();

                                                      String date = now.day.toString() +
                                                          "-" +
                                                          now.month.toString() +
                                                          "-" +
                                                          now.year.toString();

                                                      String  month = now.month.toString();
                                                      String   year = now.year.toString();

                                                      // showCustomDatePicker(build(context));

                                                      showModalBottomSheet(
                                                          context: context,
                                                          builder: (context) {
                                                            return Container(
                                                                child: Column(children: [
                                                                  Expanded(
                                                                    child: CupertinoDatePicker(
                                                                      mode: CupertinoDatePickerMode
                                                                          .date,
                                                                      initialDateTime: DateTime(
                                                                          now.year,
                                                                          now.month,
                                                                          now.day),
                                                                      onDateTimeChanged:
                                                                          (DateTime newDateTime) {
                                                                        date = newDateTime.day
                                                                            .toString() +
                                                                            "-" +
                                                                            newDateTime.month
                                                                                .toString() +
                                                                            "-" +
                                                                            newDateTime.year
                                                                                .toString();

                                                                        month = newDateTime.month
                                                                            .toString();
                                                                        year = newDateTime.year
                                                                            .toString();

                                                                        //print(date);
                                                                        // Do something
                                                                      },
                                                                    ),
                                                                    flex: 2,
                                                                  ),
                                                                  Padding(
                                                                    padding: const EdgeInsets.all(2),
                                                                    child: Container(
                                                                      height: 50,
                                                                      width: 150,
                                                                      decoration: BoxDecoration(
                                                                          color: Color(0xF0233048),
                                                                          borderRadius:
                                                                          BorderRadius.circular(
                                                                              10)),
                                                                      child: TextButton(
                                                                        onPressed: () {
                                                                          Navigator.pop(context);

                                                                          setState(() {
                                                                            mls[index].end_date = date;
                                                                          });
                                                                        },
                                                                        child: Text(
                                                                          'Ok',
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 15),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  )
                                                                ]));
                                                          });

                                                      // if (results != null &&
                                                      //     results.containsKey('selecteddata')) {
                                                      //   setState(() {
                                                      //     var acc_selected =
                                                      //     results['selecteddata'];
                                                      //
                                                      //     Accountsettings acs =
                                                      //     acc_selected as Accountsettings;
                                                      //
                                                      //     setState(() {
                                                      //       _accountsettings = acs;
                                                      //
                                                      //       accountdata = acs.data;
                                                      //     });
                                                      //   });
                                                      // }
                                                    },
                                                    child: Center(
                                                        child: Text((mls[index].end_date.isNotEmpty)?mls[index].end_date: "End Date",
                                                            textAlign:
                                                            TextAlign.center,
                                                            style: TextStyle(
                                                                color:
                                                                Colors.black38,
                                                                fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                    context)
                                                                    ? ResponsiveInfo
                                                                    .isSmallMobile(
                                                                    context)
                                                                    ? 12
                                                                    : 13
                                                                    : 16))),
                                                  ),flex: 2,),

                                              Expanded(child: Padding(
                                                padding: EdgeInsets.all(10),
                                                child: Icon(
                                                  Icons.arrow_drop_down,
                                                  size: ResponsiveInfo.isMobile(
                                                      context)
                                                      ? ResponsiveInfo
                                                      .isSmallMobile(
                                                      context)
                                                      ? 25
                                                      : 30
                                                      : 35,
                                                  color: Colors.black12,
                                                ),
                                              ),flex: 1,)


                                            ],
                                          ))),
                                  flex: 1,
                                )
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                              child: TextField(
                                onChanged: (text){

                                  mls[index].amount=text;
                                },
                                keyboardType: TextInputType.number,
                                controller: amountcontroller,
                                  decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black38, width: 0.5),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black38, width: 0.5),
                                  ),
                                  hintText: 'Amount',

                                ),
                              ),
                            ),
                            Padding(
                                padding: ResponsiveInfo.isMobile(context)
                                    ? ResponsiveInfo.isSmallMobile(context)
                                        ? EdgeInsets.all(3)
                                        : EdgeInsets.all(6)
                                    : EdgeInsets.all(9),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: TextButton(
                                        onPressed: () async {

                                          if(mls[index].start_date.isNotEmpty)
                                            {

                                              if(mls[index].end_date.isNotEmpty)
                                              {

                                                if(  mls[index].amount.isNotEmpty)
                                                {

                                                  if(checkMileStoneDates(index)) {
                                                    DateFormat dateFormat = DateFormat(
                                                        "dd-MM-yyyy");
                                                    DateTime sdt1 = dateFormat
                                                        .parse(
                                                        mls[index].start_date);
                                                    DateTime sdt2 = dateFormat
                                                        .parse(
                                                        mls[index].end_date);

                                                    if (sdt1.compareTo(sdt2) <
                                                        0) {
                                                      setState(() {

                                                        amountcontroller.text="";

                                                        mls[index].selected = 1;

                                                        StaticValues
                                                            .milestoneadded
                                                            .clear();

                                                        StaticValues
                                                            .milestoneadded
                                                            .addAll(mls);




                                                      });
                                                    }
                                                    else {
                                                      if (sdt1.compareTo(
                                                          sdt2) == 0) {
                                                        ScaffoldMessenger.of(
                                                            context)
                                                            .showSnackBar(
                                                          SnackBar(
                                                              content: Text(
                                                                  "Start date and End date cannot be equal")),
                                                        );
                                                      }
                                                      else {
                                                        ScaffoldMessenger.of(
                                                            context)
                                                            .showSnackBar(
                                                          SnackBar(
                                                              content: Text(
                                                                  "Start Date must be before the End Date")),
                                                        );
                                                      }
                                                    }
                                                  }
                                                  else{

                                                    ScaffoldMessenger.of(
                                                        context)
                                                        .showSnackBar(
                                                      SnackBar(
                                                          content: Text(
                                                              "Start Date must be after  End Date of Previous Milestone")),
                                                    );


                                                  }


                                                }
                                                else{

                                                  ScaffoldMessenger.of(context).showSnackBar(
                                                    SnackBar(content: Text("Enter amount")),
                                                  );

                                                }


                                              }
                                              else{

                                                ScaffoldMessenger.of(context).showSnackBar(
                                                  SnackBar(content: Text("Select end date")),
                                                );

                                              }


                                            }
                                          else{

                                            ScaffoldMessenger.of(context).showSnackBar(
                                              SnackBar(content: Text("Select start date")),
                                            );

                                          }







                                        },
                                        child: Text(
                                          "Add",
                                          style: TextStyle(
                                              fontSize: ResponsiveInfo.isMobile(
                                                      context)
                                                  ? ResponsiveInfo
                                                          .isSmallMobile(
                                                              context)
                                                      ? 11
                                                      : 12
                                                  : 14,
                                              color: Colors.lightGreen),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                    Expanded(
                                      child: TextButton(
                                        onPressed: () {

                                          setState(() {

                                            mls.removeAt(index);

                                            StaticValues.milestoneadded.clear();
                                            StaticValues.milestoneadded.addAll(mls);

                                          });



                                        },
                                        child: Text(
                                          "Delete",
                                          style: TextStyle(
                                              fontSize: ResponsiveInfo.isMobile(
                                                      context)
                                                  ? ResponsiveInfo
                                                          .isSmallMobile(
                                                              context)
                                                      ? 11
                                                      : 12
                                                  : 14,
                                              color: Colors.redAccent),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      )) :Card(
                    elevation: 4,

                    child: Row(


                      children: [


                        Expanded(child:    Column(

                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,

                          children: [

                            Padding(padding: EdgeInsets.all(5),


                              child: Text("Start Date : "+mls[index].start_date,style: TextStyle(fontSize: 13,color: Colors.black),),



                            ),


                            Padding(padding: EdgeInsets.all(5),


                              child: Text("End Date : "+mls[index].end_date,style: TextStyle(fontSize: 13,color: Colors.black),),



                            ),

                            Padding(padding: EdgeInsets.all(5),


                              child: Text("Amount : "+mls[index].amount,style: TextStyle(fontSize: 13,color: Colors.black),),



                            )




                          ],

                        ),flex: 2, ),


                        Expanded(child: Container(

                          child:  GestureDetector(
                            onTap: () {


                              setState(() {

                                mls[index].selected=0;
                                amountcontroller.text=mls[index].amount;


                              });

                            },
                  child:

                          Icon(Icons.arrow_forward_ios,size: 25,color: Colors.black12,),)

                        ),flex: 1,)



                      ],


                    )




                  );
                }),
          )
        ],
      ),
    );
  }




  bool checkMileStoneDates(int p)
  {
    bool a=true;

    if(mls.length>1) {

      if(p>0)
        {
          String mle_enddate=mls[p-1].end_date;

          String mle_startdate=mls[p].start_date;


          DateFormat dateFormat = DateFormat("dd-MM-yyyy");
          DateTime sdt1 = dateFormat.parse(mle_startdate);
          DateTime sdt2 = dateFormat.parse(mle_enddate);
          if(sdt1.compareTo(sdt2) > 0){
    a=true;
          }
          else{

            a=false;
          }

        }



    }

    return a;
  }



}
