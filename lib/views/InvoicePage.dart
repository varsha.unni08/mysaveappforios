import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/connection/DataConnection.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/mainviews/DropdownlistPage.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'package:share_extend/share_extend.dart';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:ui' as ui;

import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';





class InvoicePage extends StatefulWidget {
  final String title;
  final String date;
  final String billno;
  final String buyer;
  final String transactions;
  final String amount;
  final String sgst;
  final String cgst;
  final String igst;
  final String amounttopay;
  final String countryid;
  final String stateid;



  const InvoicePage(
      {Key? key, required this.title, required this.date,required this.billno,required  this.buyer,required this.transactions, required this.amount,required this.sgst,required this.cgst, required this.igst,required this.amounttopay,required this.countryid,required this.stateid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _InvoicePage(date,billno,buyer,transactions,amount,sgst,cgst,igst,amounttopay,countryid,stateid);
}

class _InvoicePage extends State<InvoicePage> {



  final dbHelper = new DatabaseHelper();



   String bdate="";
   String billno="";
   String buyer="";
   String transactions="";
   String amount="";
   String sgst="";
   String cgst="";
   String igst="";
   String amounttopay="";
   String countryid="";
   String stateid="";


  _InvoicePage(

      this.bdate,
      this.billno,
      this.buyer,
      this.transactions,
      this.amount,
      this.sgst,
      this.cgst,
      this.igst,
      this.amounttopay,
      this.countryid,
      this.stateid);

  @override
  void initState() {
    // TODO: implement initState
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    GlobalKey globalKey=new GlobalKey();

    return Scaffold(


        appBar: AppBar(
          automaticallyImplyLeading: false,

          flexibleSpace: Container(

              color: Color(0xFF096c6c),
              width: double.infinity,
              padding: EdgeInsets.all(10),
              child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Container(
                          margin: const EdgeInsets.fromLTRB(2, 10, 0, 0),
                          alignment: Alignment.center,
                          child: Center(
                              child: new InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Icon(Icons.arrow_back,color: Colors.white,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,)))),
                      flex: 1,
                    ),
                    Expanded(
                      child: Container(
                          margin: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Invoice",
                            style: TextStyle(
                                fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:15:17, color: Colors.white),
                          )),
                      flex: 3,
                    ),
                    Expanded(
                      child: GestureDetector(

                        onTap: ()async{

                          final RenderRepaintBoundary boundary = globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
                          final ui.Image image = await boundary.toImage();
                          final ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
                          // final Uint8List pngBytes = byteData!.buffer.asUint8List();
                          File f=await  writeToFile(byteData!);

                          ShareExtend.share(f.path, "file");
                        },

                        child:    Container(
                            margin: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                            alignment: Alignment.centerLeft,
                            child:Icon(Icons.download,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:25:30,color: Colors.white,) ),

                      )

                   ,
                      flex: 1,
                    )
                  ])) ,
          backgroundColor: Color(0xFF096c6c),
          centerTitle: false,

        ),

      body:
      SingleChildScrollView(

     child: Container(
    child:RepaintBoundary(
         key: globalKey,

        child: Column(

          children: [

            Padding(padding: EdgeInsets.all(2),

            child:  Row(children: [

              Expanded(child: Container(


                 height:height/2.6,

                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                ),

                child: Column(

                  children: [

                    Padding(padding: EdgeInsets.all(2),

                      child:Text("Century Gate Software Solutions Pvt.Ltd",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 16:18:20,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,)

                    ,
                    Padding(padding: EdgeInsets.all(2),

                        child: Text("Integra ERP, Cosmopolitan Road\nAwsini Junction, Thrissur, Kerala – 680020\nPhone: 04872322006, 9846109500\nmail@mysaving.in, mail@integraerp.in\n",style: TextStyle(fontSize: 13,fontStyle: FontStyle.normal),)),

                    Text("GSTIN:32AADCC3668C1Z2,\nState: 32 Kerala"
                      ,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:15:17,fontStyle: FontStyle.normal),)


                  ],


                ),



              ),flex: 1,)
              
              ,
        Expanded(child:  Container(

                //
                // width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 110:145:190,
          height:height/2.6,

                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black)
                ),

                child: Column(

                  children: [


                    Center(
                      
                      child: Image.asset("images/ic_launcher.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  50:70:90,fit: BoxFit.fill,),
                      
                      
                    ),

                    Padding(padding: EdgeInsets.all(5),

                      child:Text("www.mysaving.in",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  16:18:20,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,)




                  ],

                ),



              ),flex: 1),



            ],),),

            Padding(padding: EdgeInsets.all(2),
            child: Center(child: Text("INVOICE",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  13:15:17,fontWeight: FontWeight.bold),),),),

            Padding(padding: EdgeInsets.all(5),

            child:
            Column(

              children: [

              Container(
              width: width,



              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black)
              ),

              child: Row(children: [

                  Container(

                      width: width/2.1,


                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black)
                      ),

                      child: Padding(padding: EdgeInsets.all(5),

                        child:Text("Bill no : "+billno,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,)

                  ),

                  Container(






                      child: Padding(padding: EdgeInsets.all(5),

                        child:Text("Date : "+bdate,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,)

                  )




                ],)),

                Container(
                    width: width,



                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black)
                    ),

                    child: Padding(padding: EdgeInsets.all(5),

                      child:Text("Buyer : "+buyer,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,)

                ),
                Container(
                    width: width,



                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black)
                    ),

                    child: Padding(padding: EdgeInsets.all(5),

                      child:Text("Transaction Id : "+transactions,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,)

                ),
                Container(
                    width: width,



                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black)
                    ),

                    child:
                        Row(

                          children: [
                            Container(padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black)
                              ),
                              width: width/1.8,

                              child:Text("Particulars",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                            Container(padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black)
                              ),

                              child:Text("Qty",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                            Container(padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black)
                              ),

                              child:Text("Rate",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                            Container(padding: EdgeInsets.all(5),

                              child:Text("Amount",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),



                          ],

                        )





                ),
                Container(
                    width: width,



                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black)
                    ),

                    child:
                    Row(

                      children: [
                        Container(padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),
                          width: width/1.8,

                          child:Text("Save my personal app",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                        Container(padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),

                          child:Text("    1",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                        Container(padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),

                          child:Text(amount,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                        Container(padding: EdgeInsets.all(5),

                          child:Text(amount,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),



                      ],

                    )





                ),
                Container(
                    width: width,



                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black)
                    ),

                    child:
                    Row(

                      children: [
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),
                          width: width/1.8,
                          height: 110,

                          child:Text("Amount in words : Two thousand twenty two rupees",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                        Container(
                          height: 110,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),

                          child:Text(" GST",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,),
                        Container(
                          height: 110,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black)
                          ),

                          child:Column(

                            children: [
                          Container(




                            child:Text(" SGST",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,

                          ),
                              Divider(
                                  color: Colors.black,
                                thickness: 1,


                              ),
                              Container(


                                child:Text(" CGST",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,

                              ),
                              Divider(
                                  color: Colors.black
                              ),
                              Container(


                                child:Text(" IGST",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,

                              ),
                              Divider(
                                  color: Colors.black
                              ),

                            ],


                          ) ,),
                        Container(

                          child:Column(

                            children: [
                              (countryid.compareTo("1")==0&&stateid.compareTo("12")==0) ?     Container(


                                child:Text("  "+cgst,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,

                              ):Container( child:Text("      ",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),)),


                              Divider(
                                color: Colors.black,
                                thickness: 1,


                              ),
                              (countryid.compareTo("1")==0&&stateid.compareTo("12")==0) ?   Container(


                                child:Text(" "+sgst,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,

                              ):Container( child:Text("      ",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),)),

                              Divider(
                                color: Colors.black,
                                thickness: 1,


                              ),
                              (countryid.compareTo("1")==0&&stateid.compareTo("12")==0) ?Container(child:Text("      ",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),)): Container(


                                child:Text("      "+igst,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) ,

                              ),



                              Divider(
                                color: Colors.black,
                                thickness: 1,


                              ),

                            ],


                          ) ,),



                      ],

                    ),






    ),

               Container(

                 decoration: BoxDecoration(
                     border: Border.all(color: Colors.black)
                 ),
                 child:   Row(

                     children: [
                       Container(

                         width: width/1.8,
                         height: 70,

                         child:
                         Center(child:
                         Text("Net Total Amount ",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),) )

                         ,),

                    Container(width: 1,
                    height: 70,
                    color: Colors.black,),

                       Container(


                         height: 70,

                         child:Center(child:

                         Text("      "+amounttopay,style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),))

                         ,)







                     ]) ,
               ),


                Container(


                  height: 70,

                  child:Center(child:

                  Text("Century Gate software solutions private limited. ",style: TextStyle(fontSize: 14,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),))

                  ,)

              ],
            )


            ),

            // Center(
            //   child: TextButton( onPressed: () async{
            //
            //     // final RenderRepaintBoundary boundary = globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
            //     // final ui.Image image = await boundary.toImage();
            //     // final ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
            //     // // final Uint8List pngBytes = byteData!.buffer.asUint8List();
            //     // File f=await  writeToFile(byteData!);
            //     //
            //     // ShareExtend.share(f.path, "file");
            //
            //
            //   }
            //
            //       , child: Text("Download",style: TextStyle(fontSize: 13),)) ,
            // ),







          ],


        ),


      ),

      )

      )




    );

  }


  Future<File> writeToFile(ByteData data) async {

    var d=DateTime.now().toString();
    final buffer = data.buffer;
    Directory tempDir = await getApplicationDocumentsDirectory();
    String tempPath = tempDir.path;
    var filePath = tempPath + '/'+d+'.png'; // file_01.tmp is dump file, can be anything
    return new File(filePath).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));


  }

  }