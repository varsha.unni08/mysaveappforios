import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
//
// import 'package:save_flutter/domain/Paymentdata.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:save_flutter/mainviews/AddBankVoucher.dart';
// import 'package:save_flutter/mainviews/Addjournalvoucher.dart';
//
// import 'package:save_flutter/projectconstants/DataConstants.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Paymentdata.dart';
import '../projectconstants/DataConstants.dart';
import '../utils/Tutorials.dart';
import 'AddBankVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';





class BankVoucherPage extends StatefulWidget {
  final String title;

  const BankVoucherPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BankVoucherPage();
}

class _BankVoucherPage extends State<BankVoucherPage> {

  String date = "",
      month = "",
      year = "";

  String datetxt = "Select date";

  List<String>accountsdata = [];

  List<DataRow>accountsdatarow = [];

  List<String>months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  String currentyear = "";

  List<String>tableheaddata = [
    "Date",
    "Debit",
    "Amount",
    "Credit",
    "Action"
  ];


  List<DataColumn>tableheadings = [];

  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;
  String currentmonth = "";

  int currentmonthindex = 0;

  String selectedmonth = "",
      selectedyear = "";


  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();
    Tutorial.showTutorial(Tutorial.bankvoucherttorial, context, Tutorial.bankvouchertutorial);

    selectedmonth = now.month.toString();
    selectedyear = now.year.toString();

    showAccountDetails();

    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }






  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Bank voucher",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),

      body: Stack(

        children: [


          Column(

            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              SizedBox(
                width: double.infinity,
                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?200:250:300,

                child: Stack(

                  children: [


                    Align(
                      alignment: FractionalOffset.topCenter,
                      child:  Container(child:     Padding(
                          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10) :EdgeInsets.all(15):EdgeInsets.all(20),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black38)),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[

                                Expanded(child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10) :EdgeInsets.all(15):EdgeInsets.all(20),
                                    child: InkWell(

                                      onTap: (){

                                        var now = DateTime.now();

                                        date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                                        month=now.month.toString();
                                        year=now.year.toString();

                                        int monthnumber=int.parse(month);
                                        currentmonthindex=monthnumber-1;

                                        currentmonth=months[monthnumber-1];
                                        currentyear=year;




                                        showDialog(
                                            context: context,
                                            builder: (_) {
                                              return MyDialog();
                                            }).then((value) => {


                                          selectedmonth=value["selecteddata"].toString().split(",")[0],
                                          selectedyear=value["selecteddata"].toString().split(",")[1],



                                          showAccountDetails()

                                        });




                                      },

                                      child: Icon(Icons.calendar_month, size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26 ,),
                                    )


                                ),flex: 1,)
                                ,
                                Expanded(
                                  child:SizedBox(child: TextButton(
                                    onPressed: () async {
                                      var now = DateTime.now();

                                      date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

                                      month=now.month.toString();
                                      year=now.year.toString();

                                      int monthnumber=int.parse(month);
                                      currentmonthindex=monthnumber-1;

                                      currentmonth=months[monthnumber-1];
                                      currentyear=year;




                                      showDialog(
                                          context: context,
                                          builder: (_) {
                                            return MyDialog();
                                          }).then((value) => {


                                        selectedmonth=value["selecteddata"].toString().split(",")[0],
                                        selectedyear=value["selecteddata"].toString().split(",")[1],



                                        showAccountDetails()

                                      });


                                    },
                                    child: Text(
                                      datetxt,
                                      style: TextStyle(
                                          color: Colors.black38, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  12:15:17),
                                    ),
                                  )),
                                  flex: 3,
                                )
                              ],
                            ),
                          ))),


                    ),

                    Align(
                      alignment: FractionalOffset.bottomCenter,
                      child:(accountsdata.length>0)?    Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 0, 10, 0) : EdgeInsets.fromLTRB(15, 0, 15, 0) : EdgeInsets.fromLTRB(20, 0, 20, 0),

                          child:  MediaQuery.removePadding(
                              context: context,
                              removeTop: true,
                              removeBottom: true,


                              child: GridView.builder(
                                physics: BouncingScrollPhysics(),

                                shrinkWrap: true,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 5,
                                    crossAxisSpacing: 0.0,
                                    mainAxisSpacing: 0.0,
                                    childAspectRatio: 1
                                ),
                                itemCount: tableheaddata.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding:EdgeInsets.all(0),

                                    child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black54,
                                            width: 0.3,
                                          ),
                                        ),

                                        height: 100,

                                        child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),))),

                                  );
                                },
                              )




                          )):Container() ,


                    )


                  ],



                ),



              )




            ],



          ),










          Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(10, 200, 10, 0) :EdgeInsets.fromLTRB(15, 250, 15, 0):EdgeInsets.fromLTRB(20, 300, 20, 0)

              ,child:MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  removeBottom: true,
                  child:GridView.builder(
                    physics: BouncingScrollPhysics(),

                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 5,
                      crossAxisSpacing: 0.0,
                      mainAxisSpacing: 0.0,
                      childAspectRatio: 0.7

                    ),
                    itemCount: accountsdata.length,
                    itemBuilder: (context, index) {
                      return Container(

                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black54,
                            width: 0.3,
                          ),
                        ),

                        child:

                        ((index+1)%5==0)?
                        Center(child:TextButton(onPressed: ()async {


                          Map results = await Navigator.of(context)
                              .push(new MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) {
                              return new AddBankVoucherPage(
                                title: "Add bank voucher",accountsid: accountsdata[index].split(",")[0],

                              );
                            },
                          ));

                          if (results != null &&
                              results.containsKey('accountadded')) {


                            int code=results['accountadded'];
                            if(code==1)
                            {

                              showAccountDetails();


                            }
                          }

                        },
                            child:Text(accountsdata[index].split(",")[1],style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 13:16:19,color: Colors.deepOrange),)))
                            :Center(child:Text(accountsdata[index],style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),)),

                      );
                    },
                  ))),

          Align(alignment: Alignment.bottomRight,

              child:Padding(
                  padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(12):EdgeInsets.all(16):EdgeInsets.all(20),

                  child: FloatingActionButton(
                    onPressed: () async{



                      Map results = await Navigator.of(context)
                          .push(new MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) {
                          return new AddBankVoucherPage(
                            title: "Add bank voucher",accountsid: '0',

                          );
                        },
                      ));

                      if (results != null &&
                          results.containsKey('accountadded')) {


                        int code=results['accountadded'];
                        if(code==1)
                        {

                          showAccountDetails();


                        }
                      }

                    },
                    child: Icon(Icons.add, color: Colors.white, size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 29:35:40,),
                    backgroundColor: Colors.blue,

                    elevation: 5,
                    splashColor: Colors.grey,
                  ))),
        ],
      ),




    );
  }

  void showAccountDetails() async
  {
    List<String>tabledata=[];

    List<PaymentData>paymentdata=[];
    Map<String, dynamic> mapval=new Map();

    Map<String, dynamic> mapval1=new Map();

    var js=null,js1=null;

    var v ;

    v = await new DatabaseHelper().getAccountDataByMonthYear(
        selectedmonth,selectedyear);

    setState(() {

      datetxt=selectedmonth+"/"+selectedyear;
    });



    List<Map<String, dynamic>> m=v;

    for(int i=0;i<m.length;i++)
    {

      PaymentData data_payment=new PaymentData();
 mapval=m[i];

      int aid=mapval[DatabaseTables.ACCOUNTS_id];

      int vouchertype=mapval[ DatabaseTables.ACCOUNTS_VoucherType];




      if (mapval[DatabaseTables.ACCOUNTS_entryid] == '0') {
        if(vouchertype==DataConstants.bankvoucher) {
          tabledata.add(mapval[DatabaseTables.ACCOUNTS_date]);

          data_payment.date = mapval[DatabaseTables.ACCOUNTS_date];

          String accid = mapval[DatabaseTables.ACCOUNTS_setupid];

          var value = await new DatabaseHelper().getDataByid(
              DatabaseTables.TABLE_ACCOUNTSETTINGS, accid);


          List<Map<String, dynamic>> accountsettings = value;
          if (accountsettings.length > 0) {
            Map<String, dynamic> m_acc = accountsettings[0];
            String data = m_acc['data'];

             js = jsonDecode(data);

            // tabledata.add(js['Accountname']);
            //
            // data_payment.debitaccount = js['Accountname'];
          }



          data_payment.amount = mapval[DatabaseTables.ACCOUNTS_amount];

          int accountid = mapval[DatabaseTables.ACCOUNTS_id];

          var varcredit = await new DatabaseHelper().getAccounDataByEntryid(
              accountid.toString());


          List<Map<String, dynamic>> AccountPaymentcredit = varcredit;
          if (accountsettings.length > 0) {
            mapval1 = AccountPaymentcredit[0];

            String acccreditsetting = mapval1[DatabaseTables.ACCOUNTS_setupid];

            var accountsettingcredit = await new DatabaseHelper()
                .getDataByid(
                DatabaseTables.TABLE_ACCOUNTSETTINGS, acccreditsetting);

            List<Map<String,
                dynamic>> accountsettings = accountsettingcredit;
            if (accountsettings.length > 0) {
              Map<String, dynamic> m_acc = accountsettings[0];
              String data = m_acc['data'];

               js1 = jsonDecode(data);

              // tabledata.add(js1['Accountname']);
              // data_payment.creditaccount = js1['Accountname'];
            }
          }

          if(mapval[DatabaseTables.ACCOUNTS_type].toString().compareTo(DataConstants.debit.toString())==0)
            {
              tabledata.add(js['Accountname']);
            }
          else if(mapval1[DatabaseTables.ACCOUNTS_type].toString().compareTo(DataConstants.debit.toString())==0){
            tabledata.add(js1['Accountname']);

          }


          tabledata.add(mapval[DatabaseTables.ACCOUNTS_amount]);

          if(mapval1[DatabaseTables.ACCOUNTS_type].toString().compareTo(DataConstants.credit.toString())==0)
          {
            tabledata.add(js1['Accountname']);
          }
          else if(mapval[DatabaseTables.ACCOUNTS_type].toString().compareTo(DataConstants.credit.toString())==0){
            tabledata.add(js['Accountname']);

          }


          tabledata.add(aid.toString() + "," + "Edit/Delete");
          data_payment.action = "Edit/Delete";
          paymentdata.add(data_payment);
        }
      }

    }

    setState(() {

      if(accountsdata.length>0)
      {
        accountsdata.clear();
      }


      List<DataRow>dt=[];

      accountsdata.addAll(tabledata);
    });














  }


}


class MyDialog extends StatefulWidget {



  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

    // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    month=now.month.toString();
    year=now.year.toString();



    setState(() {
      int monthnumber=int.parse(month);
      currentmonthindex=monthnumber-1;

      currentmonth=months[monthnumber-1];
      currentyear=year;

      currentyearnumber=int.parse(currentyear);
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: 300.0,
        width: 600.0,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: EdgeInsets.all(15.0),
              child: Container(
                  width: 150,

                  child:Row(

                    children: [

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {

                            if(currentmonthindex>0) {
                              currentmonthindex =
                                  currentmonthindex -1;
                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });
                            }



                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child: Text(currentmonth,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {

                            if(currentmonthindex<11) {
                              currentmonthindex =
                                  currentmonthindex + 1;

                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });

                            }



                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {


                            currentyearnumber =
                                currentyearnumber - 1;

                            setState(() {
                              currentyear=currentyearnumber.toString();
                            });




                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child:  Text(currentyear,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {
                            currentyearnumber =
                                currentyearnumber + 1;

                            setState(() {
                              currentyear=currentyearnumber.toString();
                            });
                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),


                    ],


                  )


              ),
            ),







            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      int ab=currentmonthindex+1;


                      String currentyearmonth=ab.toString()+","+currentyearnumber.toString();



                      //  Navigator.pop(context,'selecteddata':currentyearmonth);

                      Navigator.of(context).pop({'selecteddata':currentyearmonth});


                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),


            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
      // actions: <Widget>[
      //   FlatButton(
      //       child: Text('Switch'),
      //       onPressed: () => setState(() {
      //         _c == Colors.redAccent
      //             ? _c = Colors.blueAccent
      //             : _c = Colors.redAccent;
      //       }))
      // ],
    );
  }





}
