import 'dart:convert';

import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:save_flutter/connection/DataConnection.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/Profiledata.dart';
// import 'package:save_flutter/mainviews/DropdownlistPage.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
// import 'package:shared_preferences/shared_preferences.dart';
 import 'package:share_extend/share_extend.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Accountsettings.dart';
import '../domain/Profiledata.dart';
import '../projectconstants/DataConstants.dart';
import 'DropdownlistPage.dart';

void main() {
  // runApp(MyApp());

  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BillVoucher',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: AddBillVoucherPage(title: 'BillVoucher', accountsid: "0"),
      debugShowCheckedModeBanner: false,
    );
  }
}

class AddBillVoucherPage extends StatefulWidget {
  final String title;
  final String accountsid;

  const AddBillVoucherPage(
      {Key? key, required this.title, required this.accountsid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddBillVoucherPage(accountsid);
}

class _AddBillVoucherPage extends State<AddBillVoucherPage> {
  final String dropdown_account = "Select an account";

  List<String> arr = ["Select an account"];
  List<String> arrradion = ["Bank", "Cash"];

  String accountdata = "Select customers";

  String bankcashaccountdata = "Select an income account";

  String date = "", month = "", year = "";

  late Accountsettings _accountsettings;

  late Accountsettings bankcash_accountsettings;


  final dbHelper = new DatabaseHelper();

  List<Accountsettings> acc = [];
  List<Accountsettings> accs = [];

  String datetxt = "Select date";
  int billvouchernumber=0;
  String billnum="";

  bool isAccountadded=false;
  int addedaccount=0;

  TextEditingController remarkscontroller = new TextEditingController();
  TextEditingController amountcontroller = new TextEditingController();

  String accountsid;

  _AddBillVoucherPage(this.accountsid);

  bool isDownloadable=false;

  @override
   initState()  {
    // TODO: implement initState



    if (accountsid.compareTo("0") != 0) {
      setDataForEditing(accountsid);
    }
    else{
      showBillnumberData();
    }

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Scaffold(
        appBar:  AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop({'accountadded': 1})
          ),
          title: Text("Add Billvoucher",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  14:16:18),),
          centerTitle: false,
        ),
        body: SingleChildScrollView(

            child: Stack(alignment: Alignment.topLeft, children: <Widget>[
              Padding(
                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 20, 0, 0): EdgeInsets.fromLTRB(0, 30, 0, 0):EdgeInsets.fromLTRB(0, 40, 0, 0),
                  child: SingleChildScrollView(
                    child: Column(children: [

                      //  Navigator.of(context).pop({'accountadded': 1});
                      (isAccountadded)?  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
                      child: SizedBox(child: (

                        TextButton( onPressed: () {     showBill(); },
                        child: Text("Click here to download ",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:18),))


                      ),),):Container(),

                      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(15, 20, 15, 10) : EdgeInsets.fromLTRB(20, 25, 20, 15):EdgeInsets.fromLTRB(25, 30, 25, 20),
                        child: Row(children: [

                          Expanded(child: Text("Bill number",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19)),flex: 3,),
                          Expanded(child: Text(":"),flex: 3,),
                          Expanded(child: Text(billnum,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),),flex: 3,)




                        ],),),







                      Padding(
                          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black38)),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),
                                    child: GestureDetector(
                                      onTap: (){

                                        var now = DateTime.now();

                                        date = now.day.toString() +
                                            "-" +
                                            now.month.toString() +
                                            "-" +
                                            now.year.toString();

                                        month = now.month.toString();
                                        year = now.year.toString();

                                        // showCustomDatePicker(build(context));

                                        showModalBottomSheet(
                                            context: context,
                                            builder: (context) {
                                              return Container(
                                                  child: Column(children: [
                                                    Expanded(
                                                      child: CupertinoDatePicker(
                                                        mode: CupertinoDatePickerMode
                                                            .date,
                                                        initialDateTime: DateTime(
                                                            now.year,
                                                            now.month,
                                                            now.day),
                                                        onDateTimeChanged:
                                                            (DateTime newDateTime) {
                                                          date = newDateTime.day
                                                              .toString() +
                                                              "-" +
                                                              newDateTime.month
                                                                  .toString() +
                                                              "-" +
                                                              newDateTime.year
                                                                  .toString();

                                                          month = newDateTime.month
                                                              .toString();
                                                          year = newDateTime.year
                                                              .toString();

                                                          //print(date);
                                                          // Do something
                                                        },
                                                      ),
                                                      flex: 2,
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.all(2),
                                                      child: Container(
                                                        height: 50,
                                                        width: 150,
                                                        decoration: BoxDecoration(
                                                            color: Color(0xF0233048),
                                                            borderRadius:
                                                            BorderRadius.circular(
                                                                10)),
                                                        child: TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(context);

                                                            setState(() {
                                                              datetxt = date;
                                                            });
                                                          },
                                                          child: Text(
                                                            'Ok',
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontSize: 15),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ]));
                                            });
                                      },

                                      child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26 ,)
                                      ,
                                    )





                                ),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () {
                                        var now = DateTime.now();

                                        date = now.day.toString() +
                                            "-" +
                                            now.month.toString() +
                                            "-" +
                                            now.year.toString();

                                        month = now.month.toString();
                                        year = now.year.toString();

                                        // showCustomDatePicker(build(context));

                                        showModalBottomSheet(
                                            context: context,
                                            builder: (context) {
                                              return Container(
                                                  child: Column(children: [
                                                    Expanded(
                                                      child: CupertinoDatePicker(
                                                        mode: CupertinoDatePickerMode
                                                            .date,
                                                        initialDateTime: DateTime(
                                                            now.year,
                                                            now.month,
                                                            now.day),
                                                        onDateTimeChanged:
                                                            (DateTime newDateTime) {
                                                          date = newDateTime.day
                                                              .toString() +
                                                              "-" +
                                                              newDateTime.month
                                                                  .toString() +
                                                              "-" +
                                                              newDateTime.year
                                                                  .toString();

                                                          month = newDateTime.month
                                                              .toString();
                                                          year = newDateTime.year
                                                              .toString();

                                                          //print(date);
                                                          // Do something
                                                        },
                                                      ),
                                                      flex: 2,
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.all(2),
                                                      child: Container(
                                                        height: 50,
                                                        width: 150,
                                                        decoration: BoxDecoration(
                                                            color: Color(0xF0233048),
                                                            borderRadius:
                                                            BorderRadius.circular(
                                                                10)),
                                                        child: TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(context);

                                                            setState(() {
                                                              datetxt = date;
                                                            });
                                                          },
                                                          child: Text(
                                                            'Ok',
                                                            style: TextStyle(
                                                                color: Colors.white,
                                                                fontSize: 15),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ]));
                                            });
                                      },
                                      child: Text(
                                        datetxt,
                                        style: TextStyle(
                                            color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                                      ),
                                    ))
                              ],
                            ),
                          )),
                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(15):  EdgeInsets.all(20): EdgeInsets.all(25),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                // red as border color
                              ),
                            ),

                            //padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15):EdgeInsets.all(20),
                                  child: Icon(Icons.arrow_drop_down,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,),
                                ),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () async {
                                        // showAccountsDialog();

                                        Map results = await Navigator.of(context)
                                            .push(new MaterialPageRoute<dynamic>(
                                          builder: (BuildContext context) {
                                            return new Dropdownlistpage(
                                              title: "dropdownlist",
                                              accountType:
                                              "Customers",
                                            );
                                          },
                                        ));

                                        if (results != null &&
                                            results.containsKey('selecteddata')) {
                                          setState(() {
                                            var acc_selected =
                                            results['selecteddata'];

                                            Accountsettings acs =
                                            acc_selected as Accountsettings;

                                            setState(() {
                                              _accountsettings = acs;

                                              accountdata = acs.data;
                                            });
                                          });
                                        }
                                      },
                                      child: Center(
                                          child: Text(accountdata,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16))),
                                    ))
                              ],
                            ),
                          )),
                      Padding(
                        padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5) : EdgeInsets.all(10):EdgeInsets.all(15),
                        child: Row(
                          textDirection: TextDirection.ltr,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 0, 10, 0) : EdgeInsets.fromLTRB(15, 0, 15, 0):EdgeInsets.fromLTRB(20, 0, 20, 0),

                                  child: TextField(
                                    keyboardType: TextInputType.number,
                                    controller: amountcontroller,
                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black38, width: 0.5),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black38, width: 0.5),
                                      ),
                                      hintText: 'Amount',
                                    ),
                                  ),

                              ),
                              flex: 3,
                            ),

                          ],
                        ),
                      ),

                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(15) : EdgeInsets.all(20):EdgeInsets.all(25),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                // red as border color
                              ),
                            ),

                            //padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15):EdgeInsets.all(20),
                                  child: Icon(Icons.arrow_drop_down,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,),
                                ),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () async {
                                        // showAccountsDialog();
                                        String dataacctype = "";



                                        Map results = await Navigator.of(context)
                                            .push(new MaterialPageRoute<dynamic>(
                                          builder: (BuildContext context) {
                                            return new Dropdownlistpage(
                                                title: "dropdownlist",
                                                accountType: "Income account");
                                          },
                                        ));

                                        if (results != null &&
                                            results.containsKey('selecteddata')) {
                                          setState(() {
                                            var acc_selected =
                                            results['selecteddata'];

                                            Accountsettings acs =
                                            acc_selected as Accountsettings;

                                            setState(() {
                                              bankcash_accountsettings = acs;

                                              bankcashaccountdata = acs.data;
                                            });
                                          });
                                        }
                                      },
                                      child: Center(
                                          child: Text(bankcashaccountdata,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:16))),
                                    ))
                              ],
                            ),
                          )),
                      Padding(
                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15):EdgeInsets.all(20),
                        child: Container(
                          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?100 : 130:170,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black38)),
                          child: TextField(
                            controller: remarkscontroller,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(
                                  10):EdgeInsets.all(
                                  15):EdgeInsets.all(
                                  20),
                              hintText: 'Remarks',
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4):EdgeInsets.all(6),
                        child: Container(
                          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                          width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150:160:170,
                          decoration: BoxDecoration(
                              color: Color(0xF0233048),
                              borderRadius: BorderRadius.circular(10)),
                          child: TextButton(
                            onPressed: () {

                              if (accountsid.compareTo("0") != 0) {
                                updateAccounts(accountsid);
                              }
                              else {
                                submitAccounts();
                              }
                            },
                            child: Text(
                              'Save',
                              style:
                              TextStyle(color: Colors.white, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                            ),
                          ),
                        ),
                      ),
                    ]),
                  ))
            ]))), onWillPop :()async {
      Navigator.of(context).pop({'accountadded': 1});
      return false;

    })
    ;
  }



  void updateAccounts(String accountsid) async
  {
    if (date.isNotEmpty) {
      if (_accountsettings != null) {
        if (amountcontroller.text.isNotEmpty) {
          if (bankcash_accountsettings != null) {
            Map<String, dynamic> m = Map();
            m[DatabaseTables.ACCOUNTS_entryid] = "0";
            m[DatabaseTables.ACCOUNTS_date] = date;
            m[DatabaseTables.ACCOUNTS_setupid] = _accountsettings.id;
            m[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
            m[DatabaseTables.ACCOUNTS_VoucherType] =
                DataConstants.billvoucher.toString();
            m[DatabaseTables.ACCOUNTS_type] = DataConstants.debit.toString();

            m[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
            m[DatabaseTables.ACCOUNTS_year] = year;
            m[DatabaseTables.ACCOUNTS_month] = month;
            m[DatabaseTables.ACCOUNTS_billVoucherNumber]=billvouchernumber.toString();

            new DatabaseHelper()
                .updateAccountData(m, DatabaseTables.TABLE_ACCOUNTS,accountsid).then((value) async {






              List<Map<String, dynamic>> a = await dbHelper.getAccounDataByEntryid(accountsid);

              Map<String, dynamic> accountdata1 = a[0];

              int accountsentrydataid=accountdata1[DatabaseTables.ACCOUNTS_id];
              //  var a = value;

              // if (cid == 3) {}

              Map<String, dynamic> m1 = Map();
              m1[DatabaseTables.ACCOUNTS_entryid] = accountsid;
              m1[DatabaseTables.ACCOUNTS_date] = date;
              m1[DatabaseTables.ACCOUNTS_setupid] = bankcash_accountsettings.id;
              m1[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
              m[DatabaseTables.ACCOUNTS_VoucherType] =
                  DataConstants.billvoucher.toString();
              m[DatabaseTables.ACCOUNTS_type] = DataConstants.credit.toString();

              m1[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
              m1[DatabaseTables.ACCOUNTS_year] = year;
              m1[DatabaseTables.ACCOUNTS_month] = month;
              m1[DatabaseTables.ACCOUNTS_billVoucherNumber]=billvouchernumber.toString();

              new DatabaseHelper()
                  .updateAccountData(m1, DatabaseTables.TABLE_ACCOUNTS,accountsentrydataid.toString()).then((value) {
               // Navigator.of(context).pop({'accountadded': 1});

                setState(() {
                  isAccountadded=true;
                });

              });



            }



            );




          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter amount"),
          ));
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Select date"),
        ));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Select date"),
      ));
    }
  }




  void submitAccounts() {
    if (date.isNotEmpty) {
      if (_accountsettings != null) {
        if (amountcontroller.text.isNotEmpty) {
          if (bankcash_accountsettings != null) {
            Map<String, dynamic> m = Map();
            m[DatabaseTables.ACCOUNTS_entryid] = "0";
            m[DatabaseTables.ACCOUNTS_date] = date;
            m[DatabaseTables.ACCOUNTS_setupid] = _accountsettings.id;
            m[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
            m[DatabaseTables.ACCOUNTS_VoucherType] =
                DataConstants.billvoucher.toString();
            m[DatabaseTables.ACCOUNTS_type] = DataConstants.debit.toString();

            m[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
            m[DatabaseTables.ACCOUNTS_year] = year;
            m[DatabaseTables.ACCOUNTS_month] = month;
            m[DatabaseTables.ACCOUNTS_billVoucherNumber]=billvouchernumber;

            new DatabaseHelper()
                .insert(m, DatabaseTables.TABLE_ACCOUNTS)
                .then((value) {
              var a = value;

              accountsid=a.toString();

              // if (cid == 3) {}

              Map<String, dynamic> m1 = Map();
              m1[DatabaseTables.ACCOUNTS_entryid] = a.toString();
              m1[DatabaseTables.ACCOUNTS_date] = date;
              m1[DatabaseTables.ACCOUNTS_setupid] = bankcash_accountsettings.id;
              m1[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
              m1[DatabaseTables.ACCOUNTS_VoucherType] =
                  DataConstants.billvoucher.toString();
              m1[DatabaseTables.ACCOUNTS_type] = DataConstants.credit.toString();

              m1[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
              m1[DatabaseTables.ACCOUNTS_year] = year;
              m1[DatabaseTables.ACCOUNTS_month] = month;
              m1[DatabaseTables.ACCOUNTS_billVoucherNumber]=billvouchernumber;

              new DatabaseHelper()
                  .insert(m1, DatabaseTables.TABLE_ACCOUNTS)
                  .then((value) async {
                print("Second record $value");
                final datacount = await SharedPreferences.getInstance();
                datacount.setInt(DataConstants.billnumber,billvouchernumber );

                setState(() {
                  isAccountadded=true;
                });

               // Navigator.of(context).pop({'accountadded': 1});
              });
            });






          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter amount"),
          ));
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Select date"),
        ));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Select date"),
      ));
    }
  }

  void showBillnumberData() async
  {
    final datacount = await SharedPreferences.getInstance();

    if(datacount.getInt(DataConstants.billnumber )!=null) {
      billvouchernumber =datacount.getInt(DataConstants.billnumber )!;
    }


    billvouchernumber=billvouchernumber+1;

    setState(() {
      billnum=  DataConstants.billvoucherNumber+billvouchernumber.toString();
    });
  }

  void setDataForEditing(String accountid) async {
    List<Map<String, dynamic>> a = await dbHelper.getAccountDataByID(accountid);

    Map<String, dynamic> accountdata1 = a[0];

    String accountsetupid = accountdata1[DatabaseTables.ACCOUNTS_setupid];

    String billno = accountdata1[DatabaseTables.ACCOUNTS_billVoucherNumber];
    billvouchernumber=int.parse(billno);




    var v = await dbHelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, accountsetupid.toString());

    List<Map<String, dynamic>> ab = v;

    Map<String, dynamic> mapdata = ab[0];
    String d = mapdata['data'];
    Map<String, dynamic> map = jsonDecode(d);


    Accountsettings accdata =
    Accountsettings(mapdata['keyid'].toString(), map['Accountname']);







    List<Map<String, dynamic>> aentry = await dbHelper.getAccounDataByEntryid(accountid);
    Map<String, dynamic> accountdataentry = aentry[0];

    String entrysetupid = accountdataentry[DatabaseTables.ACCOUNTS_setupid];

    var v1 = await dbHelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, entrysetupid.toString());

    List<Map<String, dynamic>> ab1 = v1;

    Map<String, dynamic> mapdata1 = ab1[0];
    String d1 = mapdata1['data'];
    Map<String, dynamic> map1 = jsonDecode(d1);

    String accounttype=map1['Accounttype'];

    // show accounttype in dropdown

    Accountsettings acs =
    new Accountsettings(mapdata1['keyid'].toString(),map1['Accountname']);



    setState(() {
      date = accountdata1[DatabaseTables.ACCOUNTS_date];
      datetxt = date;
      year = date.split("-")[2];
      month = date.split("-")[1];
      remarkscontroller.text = accountdata1[DatabaseTables.ACCOUNTS_remarks];

      amountcontroller.text = accountdata1[DatabaseTables.ACCOUNTS_amount];

      // accountdata = accdata.;
      _accountsettings = accdata;
      accountdata = accdata.data;


        billnum=  DataConstants.billvoucherNumber+billvouchernumber.toString();



      bankcash_accountsettings = acs;

      bankcashaccountdata = acs.data;



    });
  }


  void showBill() async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getUserDetails +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;

    print(response);
    var json = jsonDecode(response);

    Profiledata profile=Profiledata.fromJson(json);

    print(profile.data.full_name);



    List<Map<String, dynamic>> a = await dbHelper.getAccountDataByID(accountsid);

    Map<String, dynamic> accountdata1 = a[0];


    String entrysetupid = accountdata1[DatabaseTables.ACCOUNTS_setupid];
    String billno=accountdata1[DatabaseTables.ACCOUNTS_billVoucherNumber];

    var v1 = await dbHelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, entrysetupid.toString());

    List<Map<String, dynamic>> ab1 = v1;

    Map<String, dynamic> mapdata1 = ab1[0];
    String d1 = mapdata1['data'];
    Map<String, dynamic> map1 = jsonDecode(d1);

    String customer = map1['Accountname'];

// js['Accountname'];

GlobalKey globalKey=new GlobalKey();
      Dialog forgotpasswordDialog = Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
        child: Container(
          height: double.infinity,
          width: double.infinity,
          child:Stack(
            children: [
              Align(

                child:RepaintBoundary(
          key: globalKey,child:Container(

                  color: Colors.white,

                child: Column(


                  children: <Widget>[
                    Padding(
                      padding:  EdgeInsets.all(6.0),
                      child: new Theme(data: new ThemeData(
                          hintColor: Colors.black38
                      ), child: Text(

                        profile.data.full_name,
                        style: TextStyle(fontStyle: FontStyle.normal,fontSize: 16),



                      )),
                    ),
                    Padding(
                      padding:  EdgeInsets.all(6.0),
                      child: new Theme(data: new ThemeData(
                          hintColor: Colors.black38
                      ), child: Text(

                        profile.data.mobile,
                        style: TextStyle(fontStyle: FontStyle.normal,fontSize: 16),



                      )),
                    ),
                    Padding(
                      padding:  EdgeInsets.all(6.0),
                      child: new Theme(data: new ThemeData(
                          hintColor: Colors.black38
                      ), child: Text(

                        profile.data.email_id,
                        style: TextStyle(fontStyle: FontStyle.normal,fontSize: 16),



                      )),
                    ),



                    Padding(
                      padding:  EdgeInsets.all(6.0),
                      child:   Divider(color: Colors.black,
                        thickness: 1,),
                    ),
                    Padding(
                      padding:  EdgeInsets.all(6.0),
                      child: new Theme(data: new ThemeData(
                          hintColor: Colors.black38
                      ), child: Text(

                        "BILL",
                        style: TextStyle(fontStyle: FontStyle.italic,fontSize: 16,color: Colors.black),



                      )),
                    ),

                    Padding(
                        padding:  EdgeInsets.all(6.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [

                            Expanded(child: Text("Date : "+accountdata1[DatabaseTables.ACCOUNTS_date]),flex: 2,),
                            Expanded(child: Text("Bill no. : "+billno),flex: 2,)




                          ],


                        )
                    ),
                    Padding(
                      padding:  EdgeInsets.all(5.0),
                      child:   Divider(color: Colors.black,
                        thickness: 1,),
                    ),

                    Padding(
                      padding: EdgeInsets.all(6),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Customer"),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              customer,
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(6),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Amount"),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              accountdata1[DatabaseTables.ACCOUNTS_amount],
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(6),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("Remarks"),
                            flex: 2,
                          ),
                          Expanded(
                            child: Text(":"),
                            flex: 1,
                          ),
                          Expanded(
                            child: Text(
                              accountdata1[DatabaseTables.ACCOUNTS_remarks],
                              style: TextStyle(fontSize: 13),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                    ),




                  ],
                )
                )) ,
                alignment: Alignment.topCenter,
              ),

  Align(   alignment: Alignment.bottomCenter,child:  Padding(
                padding: EdgeInsets.all(10),

                child:Center (child: Container(

                  width: 150,
                  height: 55,
                  decoration: BoxDecoration(

                      color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                  child:Align(
                    alignment: Alignment.center,
                    child: TextButton(

                      onPressed:() async {

                        final RenderRepaintBoundary boundary = globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
                        final ui.Image image = await boundary.toImage();
                        final ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
                       // final Uint8List pngBytes = byteData!.buffer.asUint8List();
                      File? f=await  writeToFile(byteData!);

                        ShareExtend.share(f!.path, "file");

                        // Share.shareFile(f,
                        //     subject: 'Share ScreenShot',
                        //     text: 'Hello, check your share files!',
                        //
                        // );

                       // new File(filePath).writeAsBytes(data);
                        //print(pngBytes);


                      },

                      child: Text('Download', style: TextStyle(color: Colors.white) ,) ,),
                  ),


))
                  //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                ),


                // ,
              ),


            ],
          )


        ),
      );



      showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);


    }

  // Future<ui.Image> toImage({ double pixelRatio = 1.0 }) {
  //   assert(!debugNeedsPaint);
  //   final OffsetLayer offsetLayer = layer! as OffsetLayer;
  //   return offsetLayer.toImage(Offset.zero & size, pixelRatio: pixelRatio);
  // }

  Future<File?> writeToFile(ByteData data) async {

    var d=DateTime.now().toString();
    final buffer = data.buffer;
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    var filePath = tempPath + '/'+d+'.png'; // file_01.tmp is dump file, can be anything
    return new File(filePath).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));



  }

}
