import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Accountsettings.dart';
import '../projectconstants/DataConstants.dart';
import 'DropdownlistPage.dart';
// import 'package:save_flutter/connection/DataConnection.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/mainviews/DropdownlistPage.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Payment',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: AddJournalPage(title: 'payment', accountsid: "0"),
      debugShowCheckedModeBanner: false,
    );
  }
}

class AddJournalPage extends StatefulWidget {
  final String title;
  final String accountsid;

  const AddJournalPage(
      {Key? key, required this.title, required this.accountsid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddJournalPage(accountsid);
}

class _AddJournalPage extends State<AddJournalPage> {
  final String dropdown_account = "Select an account";

  List<String> arr = ["Select an account"];
  List<String> arrradion = ["Bank", "Cash"];

  String accountdata = "Select debit account";

  String bankcashaccountdata = "Select credit account";

  String date = "",
      month = "",
      year = "";

  late Accountsettings _accountsettings;

  late Accountsettings bankcash_accountsettings;

  int bid = 2,
      cid = 0;
  final dbHelper = new DatabaseHelper();

  List<Accountsettings> acc = [];
  List<Accountsettings> accs = [];

  String datetxt = "Select date";

  TextEditingController remarkscontroller = new TextEditingController();
  TextEditingController amountcontroller = new TextEditingController();

  String accountsid;

  _AddJournalPage(this.accountsid);

  @override
  void initState() {
    // TODO: implement initState

    if (accountsid.compareTo("0") != 0) {
      setDataForEditing(accountsid);
    }

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }


  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Add journal voucher",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
          centerTitle: false,
        ),
        body: Container(
            height: double.infinity,
            width: double.infinity,
            child: Stack(alignment: Alignment.topLeft, children: <Widget>[
              Padding(
                  padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 15, 0, 0) :EdgeInsets.fromLTRB(0, 20, 0, 0):EdgeInsets.fromLTRB(0, 25, 0, 0),
                  child: SingleChildScrollView(
                    child: Column(children: [
                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8) :EdgeInsets.all(12):EdgeInsets.all(16),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black38)),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(20),
                                    child: Icon(Icons.calendar_month,color: Colors.black26,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,)),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () {
                                        var now = DateTime.now();

                                        date = now.day.toString() +
                                            "-" +
                                            now.month.toString() +
                                            "-" +
                                            now.year.toString();

                                        month = now.month.toString();
                                        year = now.year.toString();

                                        // showCustomDatePicker(build(context));

                                        showModalBottomSheet(
                                            context: context,
                                            builder: (context) {
                                              return Container(
                                                  child: Column(children: [
                                                    Expanded(
                                                      child: CupertinoDatePicker(
                                                        mode: CupertinoDatePickerMode
                                                            .date,
                                                        initialDateTime: DateTime(
                                                            now.year,
                                                            now.month,
                                                            now.day),
                                                        onDateTimeChanged:
                                                            (
                                                            DateTime newDateTime) {
                                                          date = newDateTime.day
                                                              .toString() +
                                                              "-" +
                                                              newDateTime.month
                                                                  .toString() +
                                                              "-" +
                                                              newDateTime.year
                                                                  .toString();

                                                          month =
                                                              newDateTime.month
                                                                  .toString();
                                                          year =
                                                              newDateTime.year
                                                                  .toString();

                                                          //print(date);
                                                          // Do something
                                                        },
                                                      ),
                                                      flex: 2,
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                          .all(2),
                                                      child: Container(
                                                        height: 50,
                                                        width: 150,
                                                        decoration: BoxDecoration(
                                                            color: Color(
                                                                0xF0233048),
                                                            borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                                10)),
                                                        child: TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(
                                                                context);

                                                            setState(() {
                                                              datetxt = date;
                                                            });
                                                          },
                                                          child: Text(
                                                            'Ok',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 15),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ]));
                                            });
                                      },
                                      child: Text(
                                        datetxt,
                                        style: TextStyle(
                                            color: Colors.black38,
                                            fontSize: 12),
                                      ),
                                    ))
                              ],
                            ),
                          )),
                      Padding(
                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(20),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                // red as border color
                              ),
                            ),

                            //padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                  padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8):EdgeInsets.all(11):EdgeInsets.all(16),
                                  child: Icon(Icons.arrow_drop_down,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26,),
                                ),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () async {
                                        // showAccountsDialog();

                                        Map results = await Navigator.of(
                                            context)
                                            .push(
                                            new MaterialPageRoute<dynamic>(
                                              builder: (BuildContext context) {
                                                return new Dropdownlistpage(
                                                  title: "dropdownlist",
                                                  accountType:
                                                  DataConstants.allAccountType,
                                                );
                                              },
                                            ));

                                        if (results != null &&
                                            results.containsKey(
                                                'selecteddata')) {
                                          setState(() {
                                            var acc_selected =
                                            results['selecteddata'];

                                            Accountsettings acs =
                                            acc_selected as Accountsettings;

                                            setState(() {
                                              _accountsettings = acs;

                                              accountdata = acs.data;
                                            });
                                          });
                                        }
                                      },
                                      child: Center(
                                          child: Text(accountdata,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:16:20))),
                                    ))
                              ],
                            ),
                          )),
                      Padding(
                        padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(5) :EdgeInsets.all(10):EdgeInsets.all(15),
                        child: Row(
                          textDirection: TextDirection.ltr,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 0, 10, 0):EdgeInsets.fromLTRB(20, 0, 20, 0):EdgeInsets.fromLTRB(20, 0, 20, 0),
                                child: Container(
                                  width: double.infinity,
                                  height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  50.0:70:90,
                                  child: TextField(
                                    keyboardType: TextInputType.number,
                                    controller: amountcontroller,
                                    decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black38, width: 0.5),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black38, width: 0.5),
                                      ),
                                      hintText: 'Amount',
                                    ),
                                  ),
                                ),
                              ),
                              flex: 3,
                            ),

                          ],
                        ),
                      ),

                      Padding(
                          padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(5) :EdgeInsets.all(10):EdgeInsets.all(15),

                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.black38,
                                // red as border color
                              ),
                            ),

                            //padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Padding(
                                 padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(5) :EdgeInsets.all(10):EdgeInsets.all(15),

                                  child: Icon(Icons.arrow_drop_down,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35 ,color: Colors.black26,),
                                ),
                                Expanded(
                                    child: TextButton(
                                      onPressed: () async {
                                        // showAccountsDialog();
                                        String dataacctype = "";

                                        if (cid == 3) {
                                          dataacctype =
                                              DataConstants.cashAccountType;
                                        } else if (bid == 2) {
                                          dataacctype =
                                              DataConstants.bankAccountType;
                                        }

                                        Map results = await Navigator.of(
                                            context)
                                            .push(
                                            new MaterialPageRoute<dynamic>(
                                              builder: (BuildContext context) {
                                                return new Dropdownlistpage(
                                                    title: "dropdownlist",
                                                    accountType: dataacctype);
                                              },
                                            ));

                                        if (results != null &&
                                            results.containsKey(
                                                'selecteddata')) {
                                          setState(() {
                                            var acc_selected =
                                            results['selecteddata'];

                                            Accountsettings acs =
                                            acc_selected as Accountsettings;

                                            setState(() {
                                              bankcash_accountsettings = acs;

                                              bankcashaccountdata = acs.data;
                                            });
                                          });
                                        }
                                      },
                                      child: Center(
                                          child: Text(bankcashaccountdata,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:15:18))),
                                    ))
                              ],
                            ),
                          )),
                      Padding(
                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(20),
                        child: Container(
                          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  100:150:200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black38)),
                          child: TextField(
                            controller: remarkscontroller,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15):EdgeInsets.all(20),
                              hintText: 'Remarks',
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(4):EdgeInsets.all(8):EdgeInsets.all(12),
                        child: Container(
                          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,
                          width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150:170:200,
                          decoration: BoxDecoration(
                              color: Color(0xF0233048),
                              borderRadius: BorderRadius.circular(10)),
                          child: TextButton(
                            onPressed: () {
                              if (accountsid.compareTo("0") != 0) {
                                updateAccounts(accountsid);
                              }
                              else {
                                submitAccounts();
                              }
                            },
                            child: Text(
                              'Submit',
                              style:
                              TextStyle(color: Colors.white, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:17:20),
                            ),
                          ),
                        ),
                      ),
                    ]),
                  ))
            ])));
  }


  void updateAccounts(String accountsid) async
  {
    if (date.isNotEmpty) {
      if (_accountsettings != null) {
        if (amountcontroller.text.isNotEmpty) {
          if (bankcash_accountsettings != null) {
            Map<String, dynamic> m = Map();
            m[DatabaseTables.ACCOUNTS_entryid] = "0";
            m[DatabaseTables.ACCOUNTS_date] = date;
            m[DatabaseTables.ACCOUNTS_setupid] = _accountsettings.id;
            m[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
            m[DatabaseTables.ACCOUNTS_VoucherType] =
                DataConstants.journalvoucher.toString();
            m[DatabaseTables.ACCOUNTS_type] = DataConstants.debit.toString();

            m[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
            m[DatabaseTables.ACCOUNTS_year] = year;
            m[DatabaseTables.ACCOUNTS_month] = month;

            // new DatabaseHelper()
            //     .updateAccountData(m, DatabaseTables.TABLE_ACCOUNTS, accountsid)
            //     .then((value) async {
            //   List<Map<String, dynamic>> a = await dbHelper
            //       .getAccounDataByEntryid(accountsid);
            //
            //   Map<String, dynamic> accountdata1 = a[0];
            //
            //   int accountsentrydataid = accountdata1[DatabaseTables
            //       .ACCOUNTS_id];
              //  var a = value;

              // if (cid == 3) {}

              Map<String, dynamic> m1 = Map();
              m1[DatabaseTables.ACCOUNTS_entryid] = accountsid;
              m1[DatabaseTables.ACCOUNTS_date] = date;
              m1[DatabaseTables.ACCOUNTS_setupid] = bankcash_accountsettings.id;
              m1[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
              m[DatabaseTables.ACCOUNTS_VoucherType] =
                  DataConstants.journalvoucher.toString();
              m[DatabaseTables.ACCOUNTS_type] = DataConstants.credit.toString();

              m1[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
              m1[DatabaseTables.ACCOUNTS_year] = year;
              m1[DatabaseTables.ACCOUNTS_month] = month;

              // new DatabaseHelper()
              //     .updateAccountData(m1, DatabaseTables.TABLE_ACCOUNTS,
              //     accountsentrydataid.toString()).then((value) {
              //   Navigator.of(context).pop({'accountadded': 1});
              // });
           // }


         //   );
          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter amount"),
          ));
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Select date"),
        ));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Select date"),
      ));
    }
  }


  void submitAccounts() {
    if (date.isNotEmpty) {
      if (_accountsettings != null) {
        if (amountcontroller.text.isNotEmpty) {
          if (bankcash_accountsettings != null) {
            Map<String, dynamic> m = Map();
            m[DatabaseTables.ACCOUNTS_entryid] = "0";
            m[DatabaseTables.ACCOUNTS_date] = date;
            m[DatabaseTables.ACCOUNTS_setupid] = _accountsettings.id;
            m[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
            m[DatabaseTables.ACCOUNTS_VoucherType] =
                DataConstants.journalvoucher.toString();
            m[DatabaseTables.ACCOUNTS_type] = DataConstants.debit.toString();

            m[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
            m[DatabaseTables.ACCOUNTS_year] = year;
            m[DatabaseTables.ACCOUNTS_month] = month;

            new DatabaseHelper()
                .insert(m, DatabaseTables.TABLE_ACCOUNTS)
                .then((value) {
              var a = value;

              // if (cid == 3) {}

              Map<String, dynamic> m1 = Map();
              m1[DatabaseTables.ACCOUNTS_entryid] = a.toString();
              m1[DatabaseTables.ACCOUNTS_date] = date;
              m1[DatabaseTables.ACCOUNTS_setupid] = bankcash_accountsettings.id;
              m1[DatabaseTables.ACCOUNTS_amount] = amountcontroller.text;
              m1[DatabaseTables.ACCOUNTS_VoucherType] =
                  DataConstants.journalvoucher.toString();
              m1[DatabaseTables.ACCOUNTS_type] = DataConstants.credit.toString();

              m1[DatabaseTables.ACCOUNTS_remarks] = remarkscontroller.text;
              m1[DatabaseTables.ACCOUNTS_year] = year;
              m1[DatabaseTables.ACCOUNTS_month] = month;

              new DatabaseHelper()
                  .insert(m1, DatabaseTables.TABLE_ACCOUNTS)
                  .then((value) {
                print("Second record $value");

                Navigator.of(context).pop({'accountadded': 1});
              });
            });
          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter amount"),
          ));
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Select date"),
        ));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Select date"),
      ));
    }
  }

  void setDataForEditing(String accountid) async {
    List<Map<String, dynamic>> a = await dbHelper.getAccountDataByID(accountid);

    Map<String, dynamic> accountdata1 = a[0];

    String accountsetupid = accountdata1[DatabaseTables.ACCOUNTS_setupid];

    var v = await dbHelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, accountsetupid.toString());

    List<Map<String, dynamic>> ab = v;

    Map<String, dynamic> mapdata = ab[0];
    String d = mapdata['data'];
    Map<String, dynamic> map = jsonDecode(d);


    Accountsettings accdata =
    Accountsettings(mapdata['keyid'].toString(), map['Accountname']);


    List<Map<String, dynamic>> aentry = await dbHelper.getAccounDataByEntryid(
        accountid);
    Map<String, dynamic> accountdataentry = aentry[0];

    String entrysetupid = accountdataentry[DatabaseTables.ACCOUNTS_setupid];

    var v1 = await dbHelper.getDataByid(
        DatabaseTables.TABLE_ACCOUNTSETTINGS, entrysetupid.toString());

    List<Map<String, dynamic>> ab1 = v1;

    Map<String, dynamic> mapdata1 = ab1[0];
    String d1 = mapdata1['data'];
    Map<String, dynamic> map1 = jsonDecode(d1);

    String accounttype = map1['Accounttype'];

    // show accounttype in dropdown

    Accountsettings acs =
    new Accountsettings(mapdata1['keyid'].toString(), map1['Accountname']);


    setState(() {
      date = accountdata1[DatabaseTables.ACCOUNTS_date];
      datetxt = date;
      year = date.split("-")[2];
      month = date.split("-")[1];
      remarkscontroller.text = accountdata1[DatabaseTables.ACCOUNTS_remarks];

      amountcontroller.text = accountdata1[DatabaseTables.ACCOUNTS_amount];

      // accountdata = accdata.;
      _accountsettings = accdata;
      accountdata = accdata.data;




      bankcash_accountsettings = acs;

      bankcashaccountdata = acs.data;
    });
  }
}