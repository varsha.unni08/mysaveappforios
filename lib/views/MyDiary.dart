import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/AccountSetupdata.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/domain/CashBankAccountDart.dart';
// import 'package:save_flutter/domain/Diarysubjects.dart';
// import 'package:save_flutter/domain/MyDiary.dart';
// import 'package:save_flutter/mainviews/AddAccountSetup.dart';
// import 'package:save_flutter/mainviews/AddMyDiary.dart';
// import 'package:save_flutter/mainviews/MyDiaryDetails.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Diarysubjects.dart';
import '../domain/MyDiary.dart';
import '../utils/Tutorials.dart';
import 'AddMyDiary.dart';
import 'MyDiaryDetails.dart';






class MyDiarypage extends StatefulWidget {
  final String title;




  const MyDiarypage(
      {Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyDiarypage();
}

class _MyDiarypage extends State<MyDiarypage> {


  String date = "";
  String date1="";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";

  List<String>subject=["Select subject"];

  String subjectdata="Select subject";

   DatabaseHelper dbhelper=new DatabaseHelper();
  List<DiarySubjects>mydiary=[];

  bool isdataexists=false;



  @override
  void initState() {
    // TODO: implement initState

    Tutorial.showTutorial(Tutorial. mydiarytuorial, context, Tutorial.mydiarytutorial);
    showDiarySubjectData();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    super.initState();
  }


  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: true,

        appBar: AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Mty diary" , style: TextStyle(fontSize:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:16:18),),
          centerTitle: false,
        ),


        body: Container(
          width: double.infinity,
          height: double.infinity,

          child:      Stack(


                  children: [

                    Align(

                      alignment: Alignment.topCenter,
                      child: Row(
                        children: [

                          Expanded(child: Padding(
                              padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15) : EdgeInsets.all(20),
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black38)),
                                child: Row(
                                  textDirection: ui.TextDirection.rtl,
                                  children: <Widget>[
                                    Padding(
                                        padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(15) : EdgeInsets.all(20),
                                        child: GestureDetector(

                                          child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26 ,) ,

                                          onTap: (){
                                            var now = DateTime.now();

                                            date= now.day
                                                .toString() +
                                                "-" +
                                                now.month
                                                    .toString() +
                                                "-" +
                                                now.year
                                                    .toString();

                                            // showCustomDatePicker(build(context));

                                            showModalBottomSheet(
                                                context: context,
                                                builder: (context) {
                                                  return Container(
                                                      child: Column(children: [
                                                        Expanded(
                                                          child: CupertinoDatePicker(
                                                            mode: CupertinoDatePickerMode
                                                                .date,
                                                            initialDateTime: DateTime(
                                                                now.year,
                                                                now.month,
                                                                now.day),
                                                            onDateTimeChanged:
                                                                (DateTime newDateTime) {



                                                              date= newDateTime.day
                                                                  .toString() +
                                                                  "-" +
                                                                  newDateTime.month
                                                                      .toString() +
                                                                  "-" +
                                                                  newDateTime.year
                                                                      .toString();


                                                              datetxt1=date;



                                                              //print(date);
                                                              // Do something
                                                            },
                                                          ),
                                                          flex: 2,
                                                        ),
                                                        Padding(
                                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(2) :  EdgeInsets.all(4) :  EdgeInsets.all(6),
                                                          child: Container(
                                                            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 :60:80,
                                                            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150 :170:190,
                                                            decoration: BoxDecoration(
                                                                color: Color(0xF0233048),
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    10)),
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(context);

                                                                setState(() {
                                                                  datetxt1 = date;
                                                                });
                                                              },
                                                              child: Text(
                                                                'Ok',
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ]));
                                                });


                                          },
                                        )



                                        ),
                                    Expanded(
                                        child: TextButton(
                                          onPressed: () {
                                            var now = DateTime.now();

                                            date= now.day
                                                .toString() +
                                                "-" +
                                                now.month
                                                    .toString() +
                                                "-" +
                                                now.year
                                                    .toString();

                                            // showCustomDatePicker(build(context));

                                            showModalBottomSheet(
                                                context: context,
                                                builder: (context) {
                                                  return Container(
                                                      child: Column(children: [
                                                        Expanded(
                                                          child: CupertinoDatePicker(
                                                            mode: CupertinoDatePickerMode
                                                                .date,
                                                            initialDateTime: DateTime(
                                                                now.year,
                                                                now.month,
                                                                now.day),
                                                            onDateTimeChanged:
                                                                (DateTime newDateTime) {



                                                              date= newDateTime.day
                                                                  .toString() +
                                                                  "-" +
                                                                  newDateTime.month
                                                                      .toString() +
                                                                  "-" +
                                                                  newDateTime.year
                                                                      .toString();


                                                              datetxt1=date;



                                                              //print(date);
                                                              // Do something
                                                            },
                                                          ),
                                                          flex: 2,
                                                        ),
                                                        Padding(
                                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.all(2) :  EdgeInsets.all(4) :  EdgeInsets.all(6),
                                                          child: Container(
                                                            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 :60:80,
                                                            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150 :170:190,
                                                            decoration: BoxDecoration(
                                                                color: Color(0xF0233048),
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    10)),
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(context);

                                                                setState(() {
                                                                  datetxt1 = date;
                                                                });
                                                              },
                                                              child: Text(
                                                                'Ok',
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 15:17:19),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ]));
                                                });



                                            // showCustomDatePicker(build(context));


                                          },
                                          child: Text(
                                            datetxt1,
                                            style: TextStyle(
                                                color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:17),
                                          ),
                                        ))
                                  ],
                                ),
                              )),flex: 1,),
                          Expanded(child: Padding(
                              padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(12): EdgeInsets.all(14),
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black38)),
                                child: Row(
                                  textDirection: ui.TextDirection.rtl,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.all(10),
                                        child: GestureDetector(
                                          onTap: (){

                                            var now = DateTime.now();

                                            date1= now.day
                                                .toString() +
                                                "-" +
                                                now.month
                                                    .toString() +
                                                "-" +
                                                now.year
                                                    .toString();

                                            // showCustomDatePicker(build(context));

                                            showModalBottomSheet(
                                                context: context,
                                                builder: (context) {
                                                  return Container(
                                                      child: Column(children: [
                                                        Expanded(
                                                          child: CupertinoDatePicker(
                                                            mode: CupertinoDatePickerMode
                                                                .date,
                                                            initialDateTime: DateTime(
                                                                now.year,
                                                                now.month,
                                                                now.day),
                                                            onDateTimeChanged:
                                                                (DateTime newDateTime) {


                                                              date1= newDateTime.day
                                                                  .toString() +
                                                                  "-" +
                                                                  newDateTime.month
                                                                      .toString() +
                                                                  "-" +
                                                                  newDateTime.year
                                                                      .toString();


                                                              datetxt2=date1;



                                                              //print(date);
                                                              // Do something
                                                            },
                                                          ),
                                                          flex: 2,
                                                        ),
                                                        Padding(
                                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4): EdgeInsets.all(6),
                                                          child: Container(
                                                            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150:170:190,
                                                            decoration: BoxDecoration(
                                                                color: Color(0xF0233048),
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    10)),
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(context);

                                                                setState(() {
                                                                  datetxt2 = date1;
                                                                });
                                                              },
                                                              child: Text(
                                                                'Ok',
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ]));
                                                });

                                          },

                                          child: Icon(Icons.calendar_month,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?25:30:35,color: Colors.black26 ,)) ,

                                        )




                                        ,
                                    Expanded(
                                        child: TextButton(
                                          onPressed: () {
                                            var now = DateTime.now();

                                            date1= now.day
                                                .toString() +
                                                "-" +
                                                now.month
                                                    .toString() +
                                                "-" +
                                                now.year
                                                    .toString();

                                            // showCustomDatePicker(build(context));

                                            showModalBottomSheet(
                                                context: context,
                                                builder: (context) {
                                                  return Container(
                                                      child: Column(children: [
                                                        Expanded(
                                                          child: CupertinoDatePicker(
                                                            mode: CupertinoDatePickerMode
                                                                .date,
                                                            initialDateTime: DateTime(
                                                                now.year,
                                                                now.month,
                                                                now.day),
                                                            onDateTimeChanged:
                                                                (DateTime newDateTime) {


                                                              date1= newDateTime.day
                                                                  .toString() +
                                                                  "-" +
                                                                  newDateTime.month
                                                                      .toString() +
                                                                  "-" +
                                                                  newDateTime.year
                                                                      .toString();


                                                              datetxt2=date1;



                                                              //print(date);
                                                              // Do something
                                                            },
                                                          ),
                                                          flex: 2,
                                                        ),
                                                        Padding(
                                                          padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(2) : EdgeInsets.all(4): EdgeInsets.all(6),
                                                          child: Container(
                                                            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,
                                                            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150:170:190,
                                                            decoration: BoxDecoration(
                                                                color: Color(0xF0233048),
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    10)),
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.pop(context);

                                                                setState(() {
                                                                  datetxt2 = date1;
                                                                });
                                                              },
                                                              child: Text(
                                                                'Ok',
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ]));
                                                });


                                            // showCustomDatePicker(build(context));


                                          },
                                          child: Text(
                                            datetxt2,
                                            style: TextStyle(
                                                color: Colors.black38, fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 12:14:16),
                                          ),
                                        ))
                                  ],
                                ),
                              )),flex: 1,)


                        ],

                      ),
                    ),

                    Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 70, 10, 0): EdgeInsets.fromLTRB(15, 90, 15, 0): EdgeInsets.fromLTRB(20, 110, 20, 0)

                      ,child:
                      Align(

                          alignment: Alignment.topCenter,
                          child:  SizedBox(

                              width: double.infinity,
                              child:Padding(padding: EdgeInsets.all(0),
                                child: Container(
                                  height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:60:70,

                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black38)),

                                  child:  DropdownButtonHideUnderline(

                                    child: ButtonTheme(
                                      alignedDropdown: true,
                                      child: InputDecorator(
                                        decoration: const InputDecoration(border: OutlineInputBorder()),
                                        child: DropdownButtonHideUnderline(
                                          child: DropdownButton(

                                            isExpanded: true,
                                            value: subjectdata,
                                            items: subject
                                                .map<DropdownMenuItem<String>>((String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(value),
                                              );
                                            }).toList(),
                                            onChanged: (String? newValue) async{

                                              List<DiarySubjects>subjects=   await getDiarybysubjectList(newValue.toString());
                                              print(subjects.length);
                                              //     subjectdata = newValue!;

                                              setState(() {
                                                subjectdata = newValue.toString();
                                                mydiary.clear();

                                                mydiary.addAll(subjects);

                                                if(subjects.length>0)
                                                {
                                                  isdataexists=true;
                                                }
                                                else{
                                                  isdataexists=false;
                                                }




                                              });
                                            },
                                            style: Theme.of(context).textTheme.bodyText1,

                                          ),
                                        ),
                                      ),
                                    ),
                                  ),


                                ),


                              )





                          )),


                    ),

                    Align(

                      alignment: Alignment.topCenter,
                      child: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  Padding(padding: EdgeInsets.fromLTRB(0, 150, 0, 0) ): Padding(padding: EdgeInsets.fromLTRB(0, 180, 0, 0)) : Padding(padding: EdgeInsets.fromLTRB(0, 220, 0, 0),
                        child:  Container(
                          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50 : 60 :70,
                          width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 150 :160:170,
                          decoration: BoxDecoration(
                              color: Color(0xF0233048),
                              borderRadius: BorderRadius.circular(10)),
                          child: TextButton(
                            onPressed: () {

                              // String datetxt1 = "Select start date";
                              // String datetxt2 = "Select end date";


                              if(datetxt2.compareTo("Select end date")!=0&&datetxt1.compareTo("Select start date")!=0)
                                {

                                  DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(datetxt1);

                                  DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(datetxt2);
                                if(accountsdateselected.isAfter(accountsdateparsed)||accountsdateselected.compareTo(accountsdateparsed)==0) {
                                  if (subjectdata.compareTo("Select subject") !=
                                      0) {



                                    getDiarybysubjectListAccordingtoDate(
                                        subjectdata);



                                  }

                                  else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(
                                          content: Text("Select subject"),
                                        ));
                                  }
                                }
                                else{
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text("Select date properly"),
                                      ));

                                }







                                }
                              else{

                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                  content: Text("Select date properly"),
                                ));
                              }

                              // showData();

                            },
                            child: Text(
                              'Submit',
                              style:
                              TextStyle(color: Colors.white, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                            ),
                          ),
                        ),
                      ),),




                    Align(

                        alignment: Alignment.topCenter,
                        child: Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 200, 0, 0): EdgeInsets.fromLTRB(0, 240, 0, 0): EdgeInsets.fromLTRB(0, 290, 0, 0),

                            child: Column(
                                children: [
                                  Expanded(
                                    child: Container(child:
                                    MediaQuery.removePadding(
                                        context: context,
                                        removeTop: true,
                                        child: ListView.builder(
                                          physics: BouncingScrollPhysics(),

                                          shrinkWrap: true,

                                          itemCount: mydiary.length,
                                          itemBuilder: (context, index) {
                                            return  Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14): EdgeInsets.all(18) ,

                                                child: Card(
                                              elevation: 7,
                                              child:

                                                  Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    mainAxisAlignment: MainAxisAlignment.center,

                                                    children: [


                                                    Container(
                                                      width:double.infinity,

                                                      height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:80:100,

                                                      child: Row(
                                                        children: [

                                                          Expanded(child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10) : EdgeInsets.all(14):EdgeInsets.all(18),

                                                              child: Container(child:Text(
                                                                mydiary[index].subject,
                                                                style:
                                                                TextStyle(color: Colors.black, fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:17:19),
                                                              ),)  ),
                                                          flex: 3,),

                                              Expanded(

                                                  child:GestureDetector(onTap: ()async{

                                                    // print("tapped");

                                                    Map results = await Navigator.of(context)
                                                        .push(new MaterialPageRoute<dynamic>(
                                                      builder: (BuildContext context) {
                                                        return new MyDiaryDetailspage(
                                                          title: "My diary details", subjectid: mydiary[index].id, enddate: datetxt2, startdate: datetxt1, subject: mydiary[index].subject,
                                                        );
                                                      },
                                                    ));

                                                    if (results != null &&
                                                        results.containsKey('accountadded')) {


                                                      int code=results['accountadded'];
                                                      if(code==1)
                                                      {

                                                        //showAccountDetails();
                                                        showDiarySubjectData();

                                                      }
                                                    }


                                                  },

                                                    child:Image.asset("images/rightarrowblack.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 20:30:40,height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 20 :30:40,) ,)




                                              ,flex: 1,)






                                                        ],


                                                      ),


                                                    ),

                                                  ],)










                                            ));
                                          },
                                        ))
                                    ),flex: 3,
                                  ),

                                ]
                            ),
                        )





                        ),





                    Align(alignment: Alignment.bottomRight,

                        child:Padding(
                            padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(12) : EdgeInsets.all(14) : EdgeInsets.all(16),

                            child: FloatingActionButton(
                              onPressed: () async{



                                Map results = await Navigator.of(context)
                                    .push(new MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) {
                                    return new AddMyDiarypage(
                                        title: "Payment", diaryid: '0',
                                    );
                                  },
                                ));

                                if (results != null &&
                                    results.containsKey('accountadded')) {


                                  int code=results['accountadded'];
                                  if(code==1)
                                  {

                                  //  showAccountDetails();
                                    showDiarySubjectData();

                                  }
                                }

                              },
                              child: Icon(Icons.add, color: Colors.white, size: 29,),
                              backgroundColor: Colors.blue,

                              elevation: 5,
                              splashColor: Colors.grey,
                            ))),

                  ]

              )




        )










    );

  }


  Future<List<DiarySubjects>> getDiarybysubjectListAccordingtoDate(String subject) async
  {



    DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(datetxt1);

    DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(datetxt2);

    List<Map<String,dynamic>> a= await dbhelper.queryAllRows(DatabaseTables.DIARY_table);

    String subjectid=await getInFromSubjectname(subject);
     List<DiarySubjects>msublist=[];
    String mcomments="";

    Set<String>diarysubid={};
    int id =0;

    List<MydiaryContent>diarycontent=[];

    for (Map ab in a) {

      int id = ab["keyid"];
      String data = ab["data"];
      var jsondata = jsonDecode(data);



      String date = jsondata["date"];


      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(date);

      String content = jsondata["content"];
      String subject = jsondata["subject"];

      if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
          accountsdateparsed.isBefore(accountsdatend)) {

        diarysubid.add(subject);
      }
      else if (accountsdateparsed.isAfter(accountsdatestart) &&
          accountsdateparsed.compareTo(accountsdatend) == 0) {
        diarysubid.add(subject);

      }
      else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
          accountsdateparsed.compareTo(accountsdatend) == 0) {

        diarysubid.add(subject);
      }

      else if (accountsdateparsed.isAfter(accountsdatestart) &&
          accountsdateparsed.isBefore(accountsdatend)) {

        diarysubid.add(subject);
      }









    }


    for(var a in diarysubid)
    {
      String subjectname=await getDiarySubjectData(a);

      if(subjectid.compareTo(a)==0)
      {

        DiarySubjects msub=new DiarySubjects(a, subjectname);

        msublist.add(msub);


        // mcomments.add(subjectname);
      }

    }

    MydiaryContent content=new MydiaryContent(id.toString(), subjectid, subjectdata, date, mcomments);

    diarycontent.add(content);

    return msublist;
  }

  Future<List<DiarySubjects>> getDiarybysubjectList(String subject) async
  {


    List<MydiaryContent>mdc=[];

    List<Map<String,dynamic>> a= await dbhelper.queryAllRows(DatabaseTables.DIARY_table);

    String subjectid=await getInFromSubjectname(subject);
    List<DiarySubjects>msublist=[];
    String mcomments="";

    Set<String>diarysubid={};

    int id =0;


    for (Map ab in a) {

      int id = ab["keyid"];
      String data = ab["data"];
      var jsondata = jsonDecode(data);

      String date = jsondata["date"];
      String content = jsondata["content"];
      String subject = jsondata["subject"];



      diarysubid.add(subject);







    }


    for(var a in diarysubid)
      {
        String subjectname=await getDiarySubjectData(a);

        if(subjectid.compareTo(a)==0)
        {

          DiarySubjects msub=new DiarySubjects(a, subjectname);

          msublist.add(msub);


          // mcomments.add(subjectname);
        }

      }

    MydiaryContent content=new MydiaryContent(id.toString(), subjectid, subjectdata, date, mcomments);

    mdc.add(content);

    return msublist;
  }






  Future<String> getDiarySubjectData(String subid) async
  {
   List<Map<String,dynamic>>m=await  dbhelper.queryAllRows( DatabaseTables.DIARYSUBJECT_table);

    String data="";

    for(Map a in m)
    {

    String b=  a['keyid'].toString();

    if(b.compareTo(subid)==0)
      {
        data=a['data'].toString();
        break;
      }

      //subjectdata1.add();



    }




    return data;

  }



  void showDiarySubjectData() async
  {
    List<Map<String,dynamic>>m=await  dbhelper.queryAllRows( DatabaseTables.DIARYSUBJECT_table);
    List<String>subjectdata1=[];
    subjectdata1.add("Select subject");

    for(Map a in m)
    {

      subjectdata1.add(a['data'].toString());

    }


    setState(() {

      subject.clear();
      subject.addAll(subjectdata1);

      subjectdata=subjectdata1[0];


    });




  }

  getInFromSubjectname(String subject) async
  {
    List<Map<String,dynamic>>m=await  dbhelper.queryAllRows( DatabaseTables.DIARYSUBJECT_table);
    String datasub="";
   // subjectdata1.add("Select subject");

    for(Map a in m)
    {

      String id=a['keyid'].toString();
      String sub=a['data'].toString();

      if(sub.compareTo(subject)==0)
      {
        datasub=id;
        break;
      }


      // subjectdata1.add();

    }

    return datasub;

  }

}