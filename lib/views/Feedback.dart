import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../domain/Feedbackdata.dart';
import '../domain/Feedbackfulldata.dart';
import '../projectconstants/DataConstants.dart';



class FeedbackPage extends StatefulWidget {
  final String title;

  const FeedbackPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FeedbackPage();

}

class _FeedbackPage extends State<FeedbackPage> {

  TextEditingController feedbackcontroller=new TextEditingController();
  List<FeedbackList> data=[];


  @override
  void initState() {
    // TODO: implement initState

    getFeedback();
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Feedback",style: TextStyle(fontSize: 14),),
          centerTitle: false,
        ),




        body: Stack(children: [

          (data.length>0)? Align(alignment: Alignment.topLeft,

          child:  Padding(
            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
      child: ListView.builder(
          padding: const EdgeInsets.all(1),
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              // color: Colors.amber[colorCodes[index]],
              child: Center(
                  child: Card(
                    elevation: 5,
                    child: Container(
                      color: Colors.white,
                      width: double.infinity,
                      child: Column(
                        children: [

                          Row(children: [

                            Padding(padding: EdgeInsets.all(10),

                            child:Image.asset('images/message.png',width: 50,height: 50,) ,)
                            ,

                            Padding(padding: EdgeInsets.all(10),child:  Text(data[index].feedback_msg,style: TextStyle(fontSize: 15,color: Colors.black),maxLines: 6,))


                          ],),


                          (data[index].reply_msg!=null )?  Row(children: [

                            Padding(padding: EdgeInsets.fromLTRB(60, 10, 10, 10),

                              child:Text("Reply : ",style: TextStyle(fontSize: 12,color: Colors.black),) ,)
                            ,

                            Padding(padding: EdgeInsets.all(5),child:  Text(data[index].reply_msg,style: TextStyle(fontSize: 15,color: Colors.black),maxLines: 6,))


                          ],):Container()


                        ],




                      ),

                    ),
                  )),
            );
          }),
    ),

          ):Container(),



          Align(alignment: Alignment.bottomRight,

              child:Padding(
                  padding: EdgeInsets.all(12),

                  child: FloatingActionButton(
                    onPressed: () async{


                      showFeedbackDialog();



                    },
                    child: Icon(Icons.add, color: Colors.white, size: 29,),
                    backgroundColor: Colors.blue,

                    elevation: 5,
                    splashColor: Colors.grey,
                  ))),
        ],));

  }


  void showFeedbackDialog()
  {

    Dialog feedbackdialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        color: Colors.white,
        height: 300.0,
        width: 800.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:  EdgeInsets.all(15.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: feedbackcontroller,
                keyboardType: TextInputType.number,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Feedback message',


                ),
              )),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {


                      if(feedbackcontroller.text.toString().isNotEmpty) {



                        submitFeedback(feedbackcontroller.text.toString());



                        Navigator.pop(context);

                        //   checkMobileNumber(otpcodeController.text.toString());
                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Enter your feedback"),
                        ));
                      }


                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),










            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),







      ),
    );



    showDialog(context: context, builder: (BuildContext context) => feedbackdialog);
  }




  void submitFeedback(String message)async
  {
    final datacount = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl+DataConstants.addFeedback),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!

      },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

          'uuid': date,
          'timestamp': date,
          'message':message
        }

    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    feedbackcontroller.text="";

    var jsondata = jsonDecode(response);
    print(jsondata['status']);

    if (jsondata['status'] == 1) {

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Feedback submitted successfully"),
      ));


      getFeedback();

    }


  }


  void getFeedback() async
  {
    final datacount = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.get(
        Uri.parse(DataConstants.baseurl+DataConstants.getFeedback+"?timestamp="+date),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!

        },



    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);


    var json = jsonDecode(response);

    FeedbackFulldata ndashboard = FeedbackFulldata.fromJson(json);

    if(ndashboard.status==1)
      {

        setState(() {
          data.clear();

          data.addAll(ndashboard.data);
        });

      }
    else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));
    }




  }

  }