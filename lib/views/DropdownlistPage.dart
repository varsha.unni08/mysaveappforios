import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../database/DBTables.dart';
import '../database/DatabaseHelper.dart';
import '../design/ResponsiveInfo.dart';
import '../domain/Accountsettings.dart';
import '../projectconstants/DataConstants.dart';
import 'AddAccountSetup.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
// import 'package:save_flutter/domain/Accountsettings.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
//
// import 'AddAccountSetup.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Payment',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: Dropdownlistpage(title: 'payment',accountType:'all'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Dropdownlistpage extends StatefulWidget {
  final String title;

  final String accountType;

  const Dropdownlistpage({Key? key, required this.title, required this.accountType}) : super(key: key);



  @override
  State<StatefulWidget> createState() => _Dropdownlistpage(accountType);
}


class _Dropdownlistpage extends State<Dropdownlistpage> {

  String accountType;


  _Dropdownlistpage(this.accountType);

  final String dropdown_account = "Select an account";

  List<String> arr = ["Select an account"];
  List<String> arrradion = ["Bank", "Cash"];

  int bid = 2,
      cid = 0;
  final dbHelper = new DatabaseHelper();

  List<Accountsettings> acc = [];
  List<Accountsettings> accs = [];
  List<Accountsettings>dummy=[];


  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }



  @override
  void initState() {
    // TODO: implement initState
    getAccountSettingsFromDB();

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
  return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar:   AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Select an account",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 14:16:19),),
        centerTitle: false,
      ),
      body: Container(
      height: double.infinity,
      width: double.infinity,

        child: Container(
          height: double.infinity,
          width: double.infinity,
          child: Stack(
            children: <Widget>[






    Container(child:  Padding(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: new Theme(
                      data: new ThemeData(hintColor: Colors.black38),
                      child: TextField(
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: Colors.black38, width: 0.5),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: Colors.black38, width: 0.5),
                          ),
                          hintText: 'Search...',
                        ),
                        onChanged: (text) {
                          accs.clear();
                          List<Accountsettings> acssdummy = [];
                          acssdummy.addAll(dummy);

                          if (text.isNotEmpty) {

                            print("text is not empty");
                            //accs.clear();

                            for (Accountsettings a in acc) {
                              if (a.data.toLowerCase().contains(text.toLowerCase())||a.data.toUpperCase().contains(text.toUpperCase())) {
                                accs.add(a);
                              }
                            }

                            print(accs.length);

                            setState(() {
                              acc.clear();
                              acc.addAll(accs);
                            });
                          } else {
                            print("text is empty");
                            setState(() {
                              acc.clear();
                              acc.addAll(acssdummy);
                            });
                          }
                        },
                      )),
                ),),


    Container(child:   Padding(
                padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
                child: new Container(
                  child:MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    child:ListView.builder(
                    itemCount: acc.length,
                    itemBuilder: (context, index) {

                      return GestureDetector(
                        child: Padding(
                            padding:EdgeInsets.all(15),
                           child: Text(acc[index].data,style: TextStyle(fontSize: 15),)),
                          onTap: (){

                            Navigator.of(context).pop({'selecteddata':acc[index]});

                          },


                      );
                    },
                  ),
                ),)
              )),

              Align(alignment: Alignment.bottomRight,

                  child:Padding(
                      padding: EdgeInsets.all(12),

                      child: FloatingActionButton(
                        onPressed: ()async {

                          // Navigator.push(
                          //     context, MaterialPageRoute(builder: (_) => AddAccountSettinglistpage(title: "account setup",accountType: accountType, accountsetupid: '0',)));


                          // Navigator.push(
                          //     context, MaterialPageRoute(builder: (_) => AddPaymentPage(title: "Payment",)));


                          Map results = await Navigator.of(context)
                              .push(new MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) {
                              return new AddAccountSettinglistpage(title: "account setup",accountType: accountType, accountsetupid: '0',);
                            },
                          ));

                          if (results != null &&
                              results.containsKey('accountsetupdata')) {
                            setState(() {
                              var accountsetupdata =
                              results['accountsetupdata'];

                              int acs =
                              accountsetupdata as int;
                              //
                              // if(acs>0)
                              //   {
                                  getAccountSettingsFromDB();
                              //  }


                            });
                          }

                        },
                        child: Icon(Icons.add, color: Colors.white, size: 29,),
                        backgroundColor: Colors.blue,

                        elevation: 5,
                        splashColor: Colors.grey,
                      ))),
            ],
          ),
        ),

      )

  );
  }


  void getAccountSettingsFromDB() async
  {
    List<Map<String, dynamic>> data =
        await dbHelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);

    dummy.clear();

    for (int i = 0; i < data.length; i++) {
      Map<String, dynamic> mapdata = data[i];
      String d = mapdata['data'];
      Map<String, dynamic> map = jsonDecode(d);

      String accounttype = map['Accounttype'];




      if (accountType.compareTo(DataConstants.bankAccountType) == 0) {
        if (accounttype.compareTo('Bank') == 0) {
          var accdata =
          Accountsettings(mapdata['keyid'].toString(), map['Accountname']);
          dummy.add(accdata);
        }
        // accs = acc;
      }

      if (accountType.compareTo(DataConstants.cashAccountType) == 0) {
        if (accounttype.compareTo('Cash') == 0) {
          var accdata =
          Accountsettings(mapdata['keyid'].toString(), map['Accountname']);
          dummy.add(accdata);
        }
        // accs = acc;
      }

      if (accountType.compareTo(DataConstants.allAccountType) == 0) {

          var accdata =
          Accountsettings(mapdata['keyid'].toString(), map['Accountname']);
          dummy.add(accdata);

        // accs = acc;
      }

      if (accountType.compareTo(DataConstants.customerAccountType) == 0) {

        if (accounttype.compareTo('Customers') == 0) {
          var accdata =
          Accountsettings(mapdata['keyid'].toString(), map['Accountname']);
          dummy.add(accdata);
        }

        // accs = acc;
      }

      if (accountType.compareTo(DataConstants.incomeAccountType) == 0) {

        if (accounttype.compareTo('Income account') == 0) {
          var accdata =
          Accountsettings(mapdata['keyid'].toString(), map['Accountname']);
          dummy.add(accdata);
        }

        // accs = acc;
      }
      if (accountType.compareTo(DataConstants.investmentType) == 0) {

        if (accounttype.compareTo('Investment') == 0) {
          var accdata =
          Accountsettings(mapdata['keyid'].toString(), map['Accountname']);
          dummy.add(accdata);
        }

        // accs = acc;
      }

    }

    setState(() {
      acc.clear();


      dummy.sort((a, b) => a.data.compareTo(b.data));

      acc.addAll(dummy);
    });
  }


}