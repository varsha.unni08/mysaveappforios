import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saveappforios/database/DBTables.dart';
import 'package:saveappforios/database/DatabaseHelper.dart';
// import 'package:save_flutter/database/DBTables.dart';
// import 'package:save_flutter/database/DatabaseHelper.dart';
//
// import 'package:save_flutter/domain/Paymentdata.dart';
// import 'package:horizontal_data_table/horizontal_data_table.dart';
// import 'package:save_flutter/mainviews/AddRecipt.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';

import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';





class AddWalletPage extends StatefulWidget {
  final String title;
  final String balance;

  const AddWalletPage({Key? key, required this.title, required this.balance}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddWalletPage();


}

class _AddWalletPage extends State<AddWalletPage> {

String datetext="";
final dbHelper = new DatabaseHelper();

TextEditingController amountcontroller=new TextEditingController();
String date = "", month = "", year = "";

String netbalance="0";

  @override
  void initState() {
    // TODO: implement initState


    var now = DateTime.now();


    setState(() {
      datetext=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

      month=now.month.toString();
      year=now.year.toString();
    });



    // loadTableHeading();

    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }
@override
void dispose() {
  // TODO: implement dispose
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  super.dispose();
}

  @override
  Widget build(BuildContext context) {
    return


      WillPopScope(
        onWillPop: () async {


          Navigator.of(context).pop({'accountadded': 1});

      return true;
    },
    child: Scaffold(

      resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop({'accountadded': 1}),
          ),
          title: Text("Add money to wallet",style: TextStyle(fontSize: 14),),
          centerTitle: false,
        ),

      body: SingleChildScrollView(child:Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(15,15, 15, 10),

            //padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              textDirection: TextDirection.ltr,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.center,
              //Center Row contents horizontally,
              crossAxisAlignment:
              CrossAxisAlignment.center,
              children: <Widget>[
                Container(child:
                Text("Wallet balance :  " ,style: TextStyle(color: Colors.black,fontSize: 14),

                )),
                Container(child: Text(" 0 ",style: TextStyle(color: Colors.black,fontSize: 14))
                  )
              ],
            ),
          ),
       // Navigator.of(context).pop({'accountadded': 1});
             Padding(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: TextDirection.rtl,
                  children: <Widget>[

                    Expanded(child:


                    Padding(
                        padding: EdgeInsets.all(10),
                        child: InkWell(

                          child:Image.asset(
                            "images/calendar.png",
                            width: 25,
                            height: 25,
                          ) ,

                          onTap: (){


                            var now = DateTime.now();
                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {
                                              date = newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();

                                              month = newDateTime.month
                                                  .toString();
                                              year = newDateTime.year
                                                  .toString();

                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: Container(
                                            height: 50,
                                            width: 150,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  datetext = date;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });

                          },
                        )



                        ),
                      flex: 1,

                    )
                  ,

                    Expanded(child:Container(
                      child: SizedBox(child: TextButton(
                        onPressed: () async {
                          var now = DateTime.now();
                          showModalBottomSheet(
                              context: context,
                              builder: (context) {
                                return Container(
                                    child: Column(children: [
                                      Expanded(
                                        child: CupertinoDatePicker(
                                          mode: CupertinoDatePickerMode
                                              .date,
                                          initialDateTime: DateTime(
                                              now.year,
                                              now.month,
                                              now.day),
                                          onDateTimeChanged:
                                              (DateTime newDateTime) {
                                            date = newDateTime.day
                                                .toString() +
                                                "-" +
                                                newDateTime.month
                                                    .toString() +
                                                "-" +
                                                newDateTime.year
                                                    .toString();

                                            month = newDateTime.month
                                                .toString();
                                            year = newDateTime.year
                                                .toString();

                                            //print(date);
                                            // Do something
                                          },
                                        ),
                                        flex: 2,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(2),
                                        child: Container(
                                          height: 50,
                                          width: 150,
                                          decoration: BoxDecoration(
                                              color: Color(0xF0233048),
                                              borderRadius:
                                              BorderRadius.circular(
                                                  10)),
                                          child: TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);

                                              setState(() {
                                                datetext = date;
                                              });
                                            },
                                            child: Text(
                                              'Ok',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15),
                                            ),
                                          ),
                                        ),
                                      )
                                    ]));
                              });



                        },
                        child: Text(
                          datetext,
                          style: TextStyle(
                              color: Colors.black38, fontSize: 12),
                        ),
                      ),width: 250,),),flex: 3, )

                  ],
                ),
              )),

          Padding(
            padding: EdgeInsets.all(5),
            child:
                Container(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Container(
                      width: double.infinity,
                      height: 50.0,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: amountcontroller,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.black38, width: 0.5),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.black38, width: 0.5),
                          ),
                          hintText: 'Amount',
                        ),
                      ),
                    ),
                  ),

                ),



          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15,10, 15, 10),

            //padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              textDirection: TextDirection.ltr,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.center,
              //Center Row contents horizontally,
              crossAxisAlignment:
              CrossAxisAlignment.center,
              children: <Widget>[
                Container(child:
                Text("Net balance :  " ,style: TextStyle(color: Colors.black,fontSize: 14),

                )),
                Container(child: Text(netbalance,style: TextStyle(color: Colors.black,fontSize: 14))
                  ,)
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(15),
            child :   Container(
              height: 50,
              width: 150,
              decoration: BoxDecoration(
                  color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
              child: TextButton(
                onPressed: () async {

                  if(amountcontroller.text.toString().isNotEmpty)
                    {

                      var mjobject=new Map();
                      mjobject['Walletbalance']="";
                      mjobject['date']=datetext;
                      mjobject['month']=month;
                      mjobject['year']=year;
                      mjobject['amount']=amountcontroller.text.toString();


                      var js=json.encode(mjobject);

                      Map<String, dynamic> data_To_Table=new Map();
                      data_To_Table['data']=js.toString();
                      int id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_WALLET);
                      amountcontroller.text="";
                       showNetBalance();


                    }
                  else{

                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Enter the amount"),
                    ));
                  }



                },
                child: Text(
                  'Save',
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              ),
            ),),





        ],


      ),

      )

    )
      );
  }


  Future<void> showNetBalance()
  async {
    List<Map<String, dynamic>> a =
        await dbHelper.queryAllRows(DatabaseTables.TABLE_WALLET);
    double amountdata=0;

    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String amount = jsondata["amount"];
      amountdata=amountdata+double.parse(amount);




    }

    setState(() {


      netbalance=amountdata.toString();
    });


  }
}