import 'dart:io';

import 'package:connectivity/connectivity.dart';


class DataConnectionStatus{

  static   Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }


  static Future<bool> checkNetConnection() async
  {
    bool isnetavailable=false;
    try {
      final result = await InternetAddress.lookup('https://www.google.com/');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');

        isnetavailable=true;
      }
      else{
        isnetavailable=false;
      }
    } on SocketException catch (_) {
      print('not connected');

      isnetavailable=false;
    }

    return      isnetavailable;;
  }
}
