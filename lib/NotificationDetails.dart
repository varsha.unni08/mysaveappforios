import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
// import 'package:save_flutter/domain/MessageData.dart';
// import 'package:save_flutter/domain/NotificationMessage.dart';
// import 'package:save_flutter/projectconstants/DataConstants.dart';
 import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:saveappforios/projectconstants/DataConstants.dart';

import 'design/ResponsiveInfo.dart';
import 'domain/MessageData.dart';

class MyApp extends StatelessWidget {

  late MessageData msg;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: NotificationDetailsPage(title: 'registration', messagedata: msg,),
      debugShowCheckedModeBanner: false,
    );
  }
}

class NotificationDetailsPage extends StatefulWidget {
  final String title;
  final MessageData messagedata;

  const NotificationDetailsPage({Key? key, required this.title, required this.messagedata}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotificationDetailsPage(messagedata);

}

class _NotificationDetailsPage extends State<NotificationDetailsPage> {

  late MessageData messageData;


  _NotificationDetailsPage(this.messageData);



  @override
  void initState() {
    // TODO: implement initState

    updateReadStatus();
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: true,


        appBar:  AppBar(
          backgroundColor: Color(0xFF096c6c),
          toolbarHeight: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?60:70:90,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Notification details",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),),
          centerTitle: false,
        ),

        body: Column(

          children: [

           Padding(padding: EdgeInsets.all(8),

           child:Text(
             messageData.message.title,
             style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),
           ) ,) ,

            Padding(padding: EdgeInsets.all(8),

              child:Text(
                messageData.message.message,
                style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?13:16:19),
              ) ,)





          ],


        )



    );

  }

  updateReadStatus() async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.updateNotificationStatus),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
      body: {
        'messageid':messageData.id,
        'timestamp':date.toString()
      }

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    var json = jsonDecode(response);;






  }

}
